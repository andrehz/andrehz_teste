*** Settings ***				
Documentation      FASTSHOP - Verificar disponibilidade de um SKU
...                Dado que acesso o endpoint do Catálogo Regionalizado
...                Quando verifico a disponibilidade do produto
...                Então retorno status 200

Resource           ../../../resources/services.robot

*** Variables ***
${Status_Code}                                                  200
${Cep_do_Cliente}                                               01425001
${Status_Resultado_Regionalizado}                               True     
${Status_Disponivel_Regiao}                                     True   


*** Test Cases ***	
VERIFICAR DISPONIBILIDADE DOS PRODUTOS FASTSHOP NO CATALOGO REGIONALIZADO
    [Template]	       Verificar disponibilidade do Sku via api
    [Tags]	           VERIFICAR_DISPONIBILIDADE  VERIFICAR_DISPONIBILIDADE_PIPE              
    
    01AACFT001         ${Cep_do_Cliente}     ${Status_Code}	
	01AHDMC301         ${Cep_do_Cliente}     ${Status_Code}	
	01ASURF001         ${Cep_do_Cliente}     ${Status_Code}	
	01AWRCC001         ${Cep_do_Cliente}     ${Status_Code}	
	01LP36156          ${Cep_do_Cliente}     ${Status_Code}	
	1GHOMEMINICNZB     ${Cep_do_Cliente}     ${Status_Code}	
	1GHOMEMINIPTOB     ${Cep_do_Cliente}     ${Status_Code}	
	1PPRIVCFIP7PTO     ${Cep_do_Cliente}     ${Status_Code}	
	1PPRIVCFIP8PTO     ${Cep_do_Cliente}     ${Status_Code}	
	1PPRIVCFIPXCNZ     ${Cep_do_Cliente}     ${Status_Code}	
	1PPRIVCFIPXPTO     ${Cep_do_Cliente}     ${Status_Code}	
	1PPRIVCGA5CLR      ${Cep_do_Cliente}     ${Status_Code}	
	1PPRIVCJ3          ${Cep_do_Cliente}     ${Status_Code}	
	1PPRIVCJ4CLR       ${Cep_do_Cliente}     ${Status_Code}	
	1PPRIVCJ5          ${Cep_do_Cliente}     ${Status_Code}	
	1PPRIVCJ6CLR       ${Cep_do_Cliente}     ${Status_Code}	
	1PPRIVCJ7          ${Cep_do_Cliente}     ${Status_Code}	
	1PPRIVCJ7P         ${Cep_do_Cliente}     ${Status_Code}	
	1PPRIVCZ3CLR       ${Cep_do_Cliente}     ${Status_Code}	
	1PPRIVCZ4CLR       ${Cep_do_Cliente}     ${Status_Code}	
	1PPRIVG4CLR        ${Cep_do_Cliente}     ${Status_Code}	
	