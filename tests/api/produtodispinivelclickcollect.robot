*** Settings ***				
Documentation      FASTSHOP - CLICK COLLECT    
...                Realizar uma consulta se o produto está disponivel na cidade Click Colect com sucesso via API.

Resource    ../../resources/services.robot

*** Test Cases ***	
VERIFICAR SE O PRODUTO ESTÁ DISPONIVEL EM UM ESTADO E CIDADE
    [Template]                              Verificar se o produto está disponibel em um estado cidade
    [Tags]                                  CLICKCOLLECTESTADOCIDADEOK            CLICKAPI
    #Sku                                    #Estado                               #Cidade
    WAQP252110CVDB                          SP                                    S%C3%A3o%20%20Paulo  
    WAQP252110CVDB                          SP                                    S%C3%A3o%20%20Paulo  
    WAQP252110CVDB                          SP                                    S%C3%A3o%20%20Paulo  
    WAQP252110CVDB                          SP                                    S%C3%A3o%20%20Paulo  
    WAQP252110CVDB                          SP                                    S%C3%A3o%20%20Paulo  
    WAQP252110CVDB                          SP                                    S%C3%A3o%20%20Paulo  
