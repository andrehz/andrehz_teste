*** Settings ***
Documentation       FastShop - De-para de Lojas x Tabelas de vendas
...                 Dado que acesso o site Price
...                 E realizo login com e-mail e senha
...                 E faço a validação do código
...                 E acesso a página De-para de lojas x tabelas de vendas da Forma de pagamento
...                 Quando eu clico em Adicionar
...                 E preencho os campos com informações válidas
...                 E clico em Salvar
...                 Então o item é adicionado

Resource            ../../../resources/services.robot

*** Variables ***
${AuthorizationApi}                 Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ0ZWRpbGVub25zIiwiZW1haWwiOiJ0ZXN0ZXFhQGZhc3RzaG9wLmNvbS5iciIsInBlcm1pc3Npb25zIjpbIkRvbWFpbiBVc2VycyIsIlZQTl9HRVJFTlRFUyIsIlRJLUlOVEVSTkVULVJFU1RSSVRBIiwiQlJNQV9RVU9UQV8yNTBNQiIsIkdSUF9KSVJBX0FDRVNTTyIsIkdSUF9QUk9NT0NBT19HRVJFTkNJQUxfUUFfQUNFU1NPIiwiR1JQX0NPTkZMVUVOQ0VfQUNFU1NPIiwiR1JQX1BSRUNPX0dFUkVOQ0lBTF9RQV9BQ0VTU08iXSwiaXNzIjoicHJpY2UtbWFuYWdlbWVudCIsImlhdCI6MTYzNTc3MTE0OCwiZXhwIjo3OTA0NjU0MzQwfQ.pBiUP1lt2Q87gAkhXzdYkg_wT_jRp607G936MqNCLhM
#EXISTEM DUAS VARIÁVEIS PARA STATUS CODE, POIS PARA ADICIONAR RETORNA 201 E PARA EXCLUIR RETORNA 200
${Status_Code_Adicionar}            201
${Status_Code_Excluir}              200
${Nome_Tabela_Numeros}              11
${Nome_Tabela_Letras}               ZZ
${Nome_Tabela_Numero_Letra}         1Z
${Nome_Tabela_Letra_Numero}         Z1
${Nome}                             Teste
${Tabela_Numeros}                   11
${Tabela_Letras}                    ZZ
${Tabela_Numero_Letra}              1Z
${Tabela_Letra_Numero}              Z1
${Descricao}                        Descricao Teste

*** Test Cases ***
REALIZAR ADIÇÃO DE DE-PARA DE LOJA E TABELA DE VENDA COM NOME E TABELA DE VENDA IGUAIS COM DOIS NÚMEROS
    [Template]                      Adicionar de-para de loja e tabela de venda com nome e tabela iguais com dois numeros
    [Tags]                          ADD_DE_PARA_LOJA_TABELA_NOME_TABELA_IGUAIS_NUMEROS     ADICIONAR_DE_PARA_LOJA_TABELA_VENDA_PIPE
    ${AuthorizationApi}     ${Status_Code_Adicionar}        ${Status_Code_Excluir}      ${Nome_Tabela_Numeros}      ${Descricao}

REALIZAR ADIÇÃO DE DE-PARA DE LOJA E TABELA DE VENDA COM NOME E TABELA DE VENDA IGUAIS COM DUAS LETRAS
    [Template]                      Adicionar de-para de loja e tabela de venda com nome e tabela iguais com duas letras
    [Tags]                          ADD_DE_PARA_LOJA_TABELA_NOME_TABELA_IGUAIS_LETRAS     ADICIONAR_DE_PARA_LOJA_TABELA_VENDA_PIPE
    ${AuthorizationApi}     ${Status_Code_Adicionar}        ${Status_Code_Excluir}      ${Nome_Tabela_Letras}      ${Descricao}

REALIZAR ADIÇÃO DE DE-PARA DE LOJA E TABELA DE VENDA COM NOME E TABELA DE VENDA IGUAIS COM NÚMERO E LETRA
    [Template]                      Adicionar de-para de loja e tabela de venda com nome e tabela iguais com numero e letra
    [Tags]                          ADD_DE_PARA_LOJA_TABELA_NOME_TABELA_IGUAIS_NUMERO_LETRA     ADICIONAR_DE_PARA_LOJA_TABELA_VENDA_PIPE
    ${AuthorizationApi}     ${Status_Code_Adicionar}        ${Status_Code_Excluir}      ${Nome_Tabela_Numero_Letra}      ${Descricao}

REALIZAR ADIÇÃO DE DE-PARA DE LOJA E TABELA DE VENDA COM NOME E TABELA DE VENDA IGUAIS COM LETRA E NÚMERO
    [Template]                      Adicionar de-para de loja e tabela de venda com nome e tabela iguais com letra e numero
    [Tags]                          ADD_DE_PARA_LOJA_TABELA_NOME_TABELA_IGUAIS_LETRA_NUMERO     ADICIONAR_DE_PARA_LOJA_TABELA_VENDA_PIPE
    ${AuthorizationApi}     ${Status_Code_Adicionar}        ${Status_Code_Excluir}      ${Nome_Tabela_Letra_Numero}      ${Descricao}

REALIZAR ADIÇÃO DE DE-PARA DE LOJA E TABELA DE VENDA COM TABELA DE VENDA COM DOIS NÚMEROS
    [Template]                      Adicionar de-para de loja e tabela de venda com tabela de venda com dois numeros
    [Tags]                          ADD_DE_PARA_LOJA_TABELA_TABELA_NUMEROS     ADICIONAR_DE_PARA_LOJA_TABELA_VENDA_PIPE
    ${AuthorizationApi}     ${Status_Code_Adicionar}        ${Status_Code_Excluir}      ${Nome}     ${Descricao}        ${Nome_Tabela_Numeros}

REALIZAR ADIÇÃO DE DE-PARA DE LOJA E TABELA DE VENDA COM TABELA DE VENDA COM DUAS LETRAS
    [Template]                      Adicionar de-para de loja e tabela de venda com tabela de venda com duas letras
    [Tags]                          ADD_DE_PARA_LOJA_TABELA_TABELA_LETRAS     ADICIONAR_DE_PARA_LOJA_TABELA_VENDA_PIPE
    ${AuthorizationApi}     ${Status_Code_Adicionar}        ${Status_Code_Excluir}      ${Nome}     ${Descricao}        ${Nome_Tabela_Letras}

REALIZAR ADIÇÃO DE DE-PARA DE LOJA E TABELA DE VENDA COM TABELA DE VENDA COM NÚMERO E LETRA
    [Template]                      Adicionar de-para de loja e tabela de venda com tabela de venda com numero e letra
    [Tags]                          ADD_DE_PARA_LOJA_TABELA_TABELA_NUMERO_LETRA     ADICIONAR_DE_PARA_LOJA_TABELA_VENDA_PIPE
    ${AuthorizationApi}     ${Status_Code_Adicionar}        ${Status_Code_Excluir}      ${Nome}     ${Descricao}        ${Nome_Tabela_Numero_Letra}

REALIZAR ADIÇÃO DE DE-PARA DE LOJA E TABELA DE VENDA COM TABELA DE VENDA COM LETRA E NÚMERO
    [Template]                      Adicionar de-para de loja e tabela de venda com tabela de venda com letra e numero
    [Tags]                          ADD_DE_PARA_LOJA_TABELA_TABELA_LETRA_NUMERO     ADICIONAR_DE_PARA_LOJA_TABELA_VENDA_PIPE
    ${AuthorizationApi}     ${Status_Code_Adicionar}        ${Status_Code_Excluir}      ${Nome}     ${Descricao}        ${Nome_Tabela_Letra_Numero}

