*** Settings ***
Documentation       FastShop - Identificadores de Lojas Virtuais
...                 Dado que acesso o site Price
...                 E realizo login com e-mail e senha
...                 E faço a validação do código
...                 E acesso a página Identificadores de lojas virtuais da Forma de pagamento
...                 Quando eu clico em Adicionar
...                 E preencho os campos com informações válidas
...                 E clico em Salvar
...                 Então o item é adicionado

Resource            ../../../resources/services.robot

*** Variables ***
${AuthorizationApi}              Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ0ZWRpbGVub25zIiwiZW1haWwiOiJ0ZXN0ZXFhQGZhc3RzaG9wLmNvbS5iciIsInBlcm1pc3Npb25zIjpbIkRvbWFpbiBVc2VycyIsIlZQTl9HRVJFTlRFUyIsIlRJLUlOVEVSTkVULVJFU1RSSVRBIiwiQlJNQV9RVU9UQV8yNTBNQiIsIkdSUF9KSVJBX0FDRVNTTyIsIkdSUF9QUk9NT0NBT19HRVJFTkNJQUxfUUFfQUNFU1NPIiwiR1JQX0NPTkZMVUVOQ0VfQUNFU1NPIiwiR1JQX1BSRUNPX0dFUkVOQ0lBTF9RQV9BQ0VTU08iXSwiaXNzIjoicHJpY2UtbWFuYWdlbWVudCIsImlhdCI6MTYzNTc3MTE0OCwiZXhwIjo3OTA0NjU0MzQwfQ.pBiUP1lt2Q87gAkhXzdYkg_wT_jRp607G936MqNCLhM
#EXISTEM DUAS VARIÁVEIS PARA STATUS CODE, POIS PARA ADICIONAR RETORNA 201 E PARA EXCLUIR RETORNA 200
${Status_Code_Adicionar}         201
${Status_Code_Excluir}           200
${Nome}                          Identificador de Loja Virtual Teste
${Descricao}                     Descricao Teste

*** Test Cases ***
REALIZAR ADIÇÃO DE UM IDENTIFICADOR DE LOJA VIRTUAL
    [Template]                   Adicionar identificador de loja virtual
    [Tags]                       ADICIONAR_IDENTIFICA_LOJA_VIRTUAL
    ${AuthorizationApi}     ${Status_Code_Adicionar}        ${Status_Code_Excluir}      ${Nome}     ${Descricao}
