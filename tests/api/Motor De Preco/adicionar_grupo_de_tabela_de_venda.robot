*** Settings ***
Documentation       FASTSHOP - Grupos de Tabelas de Vendas
...                 Dado que acesso o site Price
...                 E realizo login com e-mail e senha
...                 E faço a validação do código
...                 E acesso a página Grupo de tabelas de vendas da Forma de pagamento
...                 Quando eu clico em Adicionar
...                 E preencho os campos com informações válidas
...                 E clico em Salvar
...                 Então o item é adicionado

Resource            ../../../resources/services.robot

*** Variables ***
${AuthorizationApi}                 Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ0ZWRpbGVub25zIiwiZW1haWwiOiJ0ZXN0ZXFhQGZhc3RzaG9wLmNvbS5iciIsInBlcm1pc3Npb25zIjpbIkRvbWFpbiBVc2VycyIsIlZQTl9HRVJFTlRFUyIsIlRJLUlOVEVSTkVULVJFU1RSSVRBIiwiQlJNQV9RVU9UQV8yNTBNQiIsIkdSUF9KSVJBX0FDRVNTTyIsIkdSUF9QUk9NT0NBT19HRVJFTkNJQUxfUUFfQUNFU1NPIiwiR1JQX0NPTkZMVUVOQ0VfQUNFU1NPIiwiR1JQX1BSRUNPX0dFUkVOQ0lBTF9RQV9BQ0VTU08iXSwiaXNzIjoicHJpY2UtbWFuYWdlbWVudCIsImlhdCI6MTYzNTc3MTE0OCwiZXhwIjo3OTA0NjU0MzQwfQ.pBiUP1lt2Q87gAkhXzdYkg_wT_jRp607G936MqNCLhM
${Status_Code}                      200
${Nome}                             Grupo Tabela de Vendas Teste
${Descricao}                        Descricao Teste
${Tabelas_De_Vendas_Com_01}         Z1
${Tabelas_De_Vendas_Com_02}         Z1,Z2
${Tabelas_De_Vendas_Com_03}         Z1,Z2,Z3
${Preco_Minimo_Venda_Ativado}       true
${Preco_Minimo_Venda_Desativado}    false

*** Test Cases ***
REALIZAR UMA ADIÇÃO DE GRUPO DE TABELA DE VENDA COM UMA TABELA E COM PREÇO MÍNIMO DE VENDA
    [Template]                      Adicionar grupo de tabela de venda com uma tabela e com preco minimo de venda
    [Tags]                          ADICIONAR_GRUPO_TABELA_VENDA_UMA_TABELA_COM_PMV        ADICIONAR_GRUPO_TABELA_VENDA_PIPE
    ${AuthorizationApi}     ${Status_Code}      ${Nome}     ${Descricao}        ${Preco_Minimo_Venda_Ativado}       ${Tabelas_De_Vendas_Com_01}         

REALIZAR UMA ADIÇÃO DE GRUPO DE TABELA DE VENDA COM UMA TABELA E SEM PREÇO MÍNIMO DE VENDA
    [Template]                      Adicionar grupo de tabela de venda com uma tabela e sem preco minimo de venda
    [Tags]                          ADICIONAR_GRUPO_TABELA_VENDA_UMA_TABELA_SEM_PMV        ADICIONAR_GRUPO_TABELA_VENDA_PIPE
    ${AuthorizationApi}     ${Status_Code}      ${Nome}     ${Descricao}        ${Preco_Minimo_Venda_Desativado}        ${Tabelas_De_Vendas_Com_01}         

REALIZAR UMA ADIÇÃO DE GRUPO DE TABELA DE VENDA COM DUAS TABELAS E COM PREÇO MÍNIMO DE VENDA
    [Template]                      Adicionar grupo de tabela de venda com duas tabelas e com preco minimo de venda
    [Tags]                          ADICIONAR_GRUPO_TABELA_VENDA_DUAS_TABELAS_COM_PMV        ADICIONAR_GRUPO_TABELA_VENDA_PIPE
    ${AuthorizationApi}     ${Status_Code}      ${Nome}     ${Descricao}        ${Preco_Minimo_Venda_Ativado}       ${Tabelas_De_Vendas_Com_02}         

REALIZAR UMA ADIÇÃO DE GRUPO DE TABELA DE VENDA COM DUAS TABELAS E SEM PREÇO MÍNIMO DE VENDA
    [Template]                      Adicionar grupo de tabela de venda com duas tabelas e sem preco minimo de venda
    [Tags]                          ADICIONAR_GRUPO_TABELA_VENDA_DUAS_TABELAS_SEM_PMV        ADICIONAR_GRUPO_TABELA_VENDA_PIPE
    ${AuthorizationApi}     ${Status_Code}      ${Nome}     ${Descricao}        ${Preco_Minimo_Venda_Desativado}        ${Tabelas_De_Vendas_Com_02}         

REALIZAR UMA ADIÇÃO DE GRUPO DE TABELA DE VENDA COM TRÊS TABELAS E COM PREÇO MÍNIMO DE VENDA
    [Template]                      Adicionar grupo de tabela de venda com tres tabelas e com preco minimo de venda
    [Tags]                          ADICIONAR_GRUPO_TABELA_VENDA_TRES_TABELAS_COM_PMV        ADICIONAR_GRUPO_TABELA_VENDA_PIPE
    ${AuthorizationApi}     ${Status_Code}      ${Nome}     ${Descricao}        ${Preco_Minimo_Venda_Ativado}       ${Tabelas_De_Vendas_Com_03}         

REALIZAR UMA ADIÇÃO DE GRUPO DE TABELA DE VENDA COM TRÊS TABELAS E SEM PREÇO MÍNIMO DE VENDA
    [Template]                      Adicionar grupo de tabela de venda com tres tabelas e sem preco minimo de venda
    [Tags]                          ADICIONAR_GRUPO_TABELA_VENDA_TRES_TABELAS_SEM_PMV        ADICIONAR_GRUPO_TABELA_VENDA_PIPE
    ${AuthorizationApi}     ${Status_Code}      ${Nome}     ${Descricao}        ${Preco_Minimo_Venda_Desativado}        ${Tabelas_De_Vendas_Com_03}         
