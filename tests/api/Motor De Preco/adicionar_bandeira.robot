*** Settings ***				
Documentation      FASTSHOP - Bandeiras
...                Dado que acesso o site Price
...                E realizo login com e-mail e senha
...                E faço a validação do código
...                E acesso a página Bandeiras da Forma de pagamento
...                Quando eu clico em Adicionar
...                E preencho os campos com informações válidas
...                E clico em Salvar
...                Então o item é adicionado

Resource           ../../../resources/services.robot

*** Variables ***
${AuthorizationApi}                             Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ0ZWRpbGVub25zIiwiZW1haWwiOiJ0ZXN0ZXFhQGZhc3RzaG9wLmNvbS5iciIsInBlcm1pc3Npb25zIjpbIkRvbWFpbiBVc2VycyIsIlZQTl9HRVJFTlRFUyIsIlRJLUlOVEVSTkVULVJFU1RSSVRBIiwiQlJNQV9RVU9UQV8yNTBNQiIsIkdSUF9KSVJBX0FDRVNTTyIsIkdSUF9QUk9NT0NBT19HRVJFTkNJQUxfUUFfQUNFU1NPIiwiR1JQX0NPTkZMVUVOQ0VfQUNFU1NPIiwiR1JQX1BSRUNPX0dFUkVOQ0lBTF9RQV9BQ0VTU08iXSwiaXNzIjoicHJpY2UtbWFuYWdlbWVudCIsImlhdCI6MTYzNTc3MTE0OCwiZXhwIjo3OTA0NjU0MzQwfQ.pBiUP1lt2Q87gAkhXzdYkg_wT_jRp607G936MqNCLhM
${Status_Code}                                  200
${De_Para_Legado}                               5010
${Nome_Mastercard}                              Mastercard Teste
${URL_Imagem_Martercard}                        https://www.mastercard.com.br/content/dam/mccom/global/logos/logo-mastercard-mobile.svg
${Nome_Visa}                                    Visa Teste
${URL_Imagem_Visa}                              https://upload.wikimedia.org/wikipedia/commons/thumb/5/5e/Visa_Inc._logo.svg/2560px-Visa_Inc._logo.svg.png
${Nome_Amex}                                    Amex Teste
${URL_Imagem_Amex}                              https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/American_Express_logo_%282018%29.svg/1200px-American_Express_logo_%282018%29.svg.png
${Nome_Elo}                                     Elo Teste
${URL_Imagem_Elo}                               https://www.abcdacomunicacao.com.br/wp-content/uploads/elo_logo-baixa.png
${Nome_Diners}                                  Diners Teste
${URL_Imagem_Diners}                            https://upload.wikimedia.org/wikipedia/commons/a/a6/Diners_Club_Logo3.svg

*** Test Cases ***	
REALIZAR UMA ADIÇÃO DE BANDEIRA MASTERCARD COM URL
    [Template]                                                               Adicionar bandeira mastercard com url via api
    [Tags]                                                                   ADICIONAR_MASTERCARD_COM_URL    ADICIONAR_CARTAO_B_PIPE              
    ${Authorizationapi}    ${Status_Code}    ${Nome_Mastercard}    ${De_Para_Legado}     ${URL_Imagem_Martercard}    

REALIZAR UMA ADIÇÃO DE BANDEIRA MASTERCARD SEM URL
    [Template]                                                               Adicionar bandeira mastercard sem url via api
    [Tags]                                                                   ADICIONAR_MASTERCARD_SEM_URL    ADICIONAR_CARTAO_B_PIPE              
    ${Authorizationapi}    ${Status_Code}    ${Nome_Mastercard}    ${De_Para_Legado}    

REALIZAR UMA ADIÇÃO DE BANDEIRA VISA COM URL
    [Template]                                                               Adicionar bandeira visa com url via api
    [Tags]                                                                   ADICIONAR_VISA_COM_URL    ADICIONAR_CARTAO_B_PIPE              
    ${Authorizationapi}    ${Status_Code}    ${Nome_Visa}    ${De_Para_Legado}     ${URL_Imagem_Visa}    

REALIZAR UMA ADIÇÃO DE BANDEIRA VISA SEM URL
    [Template]                                                               Adicionar bandeira visa sem url via api
    [Tags]                                                                   ADICIONAR_VISA_SEM_URL    ADICIONAR_CARTAO_B_PIPE               
    ${Authorizationapi}    ${Status_Code}    ${Nome_Visa}    ${De_Para_Legado}

REALIZAR UMA ADIÇÃO DE BANDEIRA AMEX COM URL
    [Template]                                                               Adicionar bandeira amex com url via api
    [Tags]                                                                   ADICIONAR_AMEX_COM_URL    ADICIONAR_CARTAO_B_PIPE              
    ${Authorizationapi}    ${Status_Code}    ${Nome_Amex}    ${De_Para_Legado}     ${URL_Imagem_Amex}    

REALIZAR UMA ADIÇÃO DE BANDEIRA AMEX SEM URL
    [Template]                                                               Adicionar bandeira amex sem url via api
    [Tags]                                                                   ADICIONAR_AMEX_SEM_URL    ADICIONAR_CARTAO_B_PIPE              
    ${Authorizationapi}    ${Status_Code}    ${Nome_Amex}    ${De_Para_Legado}

REALIZAR UMA ADIÇÃO DE BANDEIRA ELO COM URL
    [Template]                                                               Adicionar bandeira elo com url via api
    [Tags]                                                                   ADICIONAR_ELO_COM_URL    ADICIONAR_CARTAO_B_PIPE               
    ${Authorizationapi}    ${Status_Code}    ${Nome_Elo}    ${De_Para_Legado}     ${URL_Imagem_Elo}    

REALIZAR UMA ADIÇÃO DE BANDEIRA ELO SEM URL
    [Template]                                                               Adicionar bandeira elo sem url via api
    [Tags]                                                                   ADICIONAR_ELO_SEM_URL    ADICIONAR_CARTAO_B_PIPE               
    ${Authorizationapi}    ${Status_Code}    ${Nome_Elo}    ${De_Para_Legado}

REALIZAR UMA ADIÇÃO DE BANDEIRA DINERS COM URL
    [Template]                                                               Adicionar bandeira diners com url via api
    [Tags]                                                                   ADICIONAR_DINERS_COM_URL    ADICIONAR_CARTAO_B_PIPE               
    ${Authorizationapi}    ${Status_Code}    ${Nome_Diners}    ${De_Para_Legado}     ${URL_Imagem_Diners}    

REALIZAR UMA ADIÇÃO DE BANDEIRA DINERS SEM URL
    [Template]                                                               Adicionar bandeira diners sem url via api
    [Tags]                                                                   ADICIONAR_DINERS_SEM_URL    ADICIONAR_CARTAO_B_PIPE               
    ${Authorizationapi}    ${Status_Code}    ${Nome_Diners}    ${De_Para_Legado}
