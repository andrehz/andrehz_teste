*** Settings ***				
Documentation      FASTSHOP - Tipos de Pagamentos
...                Dado que acesso o site Price
...                E realizo login com e-mail e senha
...                E faço a validação do código
...                E acesso a página Tipo de pagamentos da Forma de pagamento
...                Quando eu clico em Adicionar
...                E preencho os campos com informações válidas
...                E clico em Salvar
...                Então o item é adicionado

Resource           ../../../resources/services.robot

*** Variables ***
${AuthorizationApi}                                             Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ0ZWRpbGVub25zIiwiZW1haWwiOiJ0ZXN0ZXFhQGZhc3RzaG9wLmNvbS5iciIsInBlcm1pc3Npb25zIjpbIkRvbWFpbiBVc2VycyIsIlZQTl9HRVJFTlRFUyIsIlRJLUlOVEVSTkVULVJFU1RSSVRBIiwiQlJNQV9RVU9UQV8yNTBNQiIsIkdSUF9KSVJBX0FDRVNTTyIsIkdSUF9QUk9NT0NBT19HRVJFTkNJQUxfUUFfQUNFU1NPIiwiR1JQX0NPTkZMVUVOQ0VfQUNFU1NPIiwiR1JQX1BSRUNPX0dFUkVOQ0lBTF9RQV9BQ0VTU08iXSwiaXNzIjoicHJpY2UtbWFuYWdlbWVudCIsImlhdCI6MTYzNTc3MTE0OCwiZXhwIjo3OTA0NjU0MzQwfQ.pBiUP1lt2Q87gAkhXzdYkg_wT_jRp607G936MqNCLhM
${Status_Code}                                                  200
${Nome}                                                         Teste Automatizado Teste
${Descricao}                                                    Teste Automatizado
${Ordem_Da_Exibicao_Na_Promocao}                                1
${Permite_Parcelamento_Desativado}                              false
${Permite_Parcelamento_Ativado}                                 true
${Bandeira_Desativado}                                          false
${Bandeira_Ativado}                                             true
${Tipo_De_Pagamento_Faturado_Desativado}                        false
${Tipo_De_Pagamento_Faturado_Ativado}                           true
${Permite_BIN_De_Cartoes_Desativado}                            false
${Permite_BIN_De_Cartoes_Ativado}                               true
${Permite_Pagamento_De_produtos_Marktplace_Desativado}          false
${Permite_Pagamento_De_Produtos_Marktplace_Ativado}             true
${Permite_Pagamento_De_Produtos_M1_Desativado}                  false
${Permite_Pagamento_De_Produtos_M1_Ativado}                     true
${Permite_Pagamento_De_Produtos_Click&Collect_Desativado}       false
${Permite_Pagamento_De_Produtos_Click&Collect_Ativado}          true
${Restringir_Pagamento_Por_Tipo_De_Entrega_Desativado}          false
${Restringir_Pagamento_Por_Tipo_De_Entrega_Ativado}             true
${Permite_Combinar_Com_O_Mesmo_Tipo_De_Pagamento_Desativado}    false
${Permite_Combinar_Com_O_Mesmo_Tipo_De_Pagamento_Ativado}       true
${Permite_Dar_Destaque_Na_Promocao_Desativado}                  false
${Permite_Dar_Destaque_Na_Promocao_Ativado}                     true
${De_Para_O_Legado}                                             5010
${Parcelamento_Maximo}                                          18
#Usar sempre em "Restringir pagamento por Tipo de Entrega ATIVO"
${Deliveries}                                                   Ultra Fast


*** Test Cases ***	
REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO
    [Template]                                                               Adicionar um tipo de pagamento via api
    [Tags]                                                                   ADICIONAR_TIPO_DE_PAGAMENTO    ADICIONAR_TIPO_DE_PAGAMENTO_PIPE              
    ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_exibicao_na_promocao}    ${Permite_Parcelamento_Desativado}     ${Bandeira_Desativado}		${Tipo_De_Pagamento_Faturado_Desativado}		${Permite_BIN_De_Cartoes_Desativado}		${Permite_Pagamento_De_produtos_Marktplace_Desativado}		${Permite_Pagamento_De_Produtos_M1_Desativado}		${Permite_Pagamento_De_Produtos_Click&Collect_Desativado}		${Restringir_Pagamento_Por_Tipo_De_Entrega_Desativado}		${Permite_Combinar_Com_O_Mesmo_Tipo_De_Pagamento_Desativado}		${Permite_Dar_Destaque_Na_Promocao_Desativado}      ${De_para_o_legado}     ${Parcelamento_Maximo}

REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM PERMITE PARCELAMENTO ATIVO
    [Template]                                                               Adicionar um tipo de pagamento com permissao de parcelamento via api
    [Tags]                                                                   ADICIONAR_TIPO_DE_PAGAMENTO_PARCELAMENTO    ADICIONAR_TIPO_DE_PAGAMENTO_PIPE          
    ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_exibicao_na_promocao}    ${Permite_Parcelamento_Ativado}     ${Bandeira_Desativado}		${Tipo_De_Pagamento_Faturado_Desativado}		${Permite_BIN_De_Cartoes_Desativado}		${Permite_Pagamento_De_produtos_Marktplace_Desativado}		${Permite_Pagamento_De_Produtos_M1_Desativado}		${Permite_Pagamento_De_Produtos_Click&Collect_Desativado}		${Restringir_Pagamento_Por_Tipo_De_Entrega_Desativado}		${Permite_Combinar_Com_O_Mesmo_Tipo_De_Pagamento_Desativado}		${Permite_Dar_Destaque_Na_Promocao_Desativado}      ${De_para_o_legado}     ${Parcelamento_Maximo}

REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM BANDEIRA ATIVO
    [Template]                                                               Adicionar um tipo de pagamento com bandeira via api
    [Tags]                                                                   ADICIONAR_TIPO_DE_PAGAMENTO_BADEIRA   ADICIONAR_TIPO_DE_PAGAMENTO_PIPE        
    ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_exibicao_na_promocao}    ${Permite_Parcelamento_Desativado}     ${Bandeira_Ativado}		${Tipo_De_Pagamento_Faturado_Desativado}		${Permite_BIN_De_Cartoes_Desativado}		${Permite_Pagamento_De_produtos_Marktplace_Desativado}		${Permite_Pagamento_De_Produtos_M1_Desativado}		${Permite_Pagamento_De_Produtos_Click&Collect_Desativado}		${Restringir_Pagamento_Por_Tipo_De_Entrega_Desativado}		${Permite_Combinar_Com_O_Mesmo_Tipo_De_Pagamento_Desativado}		${Permite_Dar_Destaque_Na_Promocao_Desativado}      ${De_para_o_legado}     ${Parcelamento_Maximo}

REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM TIPO DE PAGAMENTO FATURADO ATIVO
    [Template]                                                               Adicionar um tipo de pagamento com tipo de pagamento faturado via api
    [Tags]                                                                   ADICIONAR_TIPO_DE_PAGAMENTO_FATURADO    ADICIONAR_TIPO_DE_PAGAMENTO_PIPE          
    ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_exibicao_na_promocao}    ${Permite_Parcelamento_Desativado}     ${Bandeira_Desativado}		${Tipo_De_Pagamento_Faturado_Ativado}		${Permite_BIN_De_Cartoes_Desativado}		${Permite_Pagamento_De_produtos_Marktplace_Desativado}		${Permite_Pagamento_De_Produtos_M1_Desativado}		${Permite_Pagamento_De_Produtos_Click&Collect_Desativado}		${Restringir_Pagamento_Por_Tipo_De_Entrega_Desativado}		${Permite_Combinar_Com_O_Mesmo_Tipo_De_Pagamento_Desativado}		${Permite_Dar_Destaque_Na_Promocao_Desativado}      ${De_para_o_legado}     ${Parcelamento_Maximo}

REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM PERMITE BIN DE CARTÕES ATIVO
    [Template]                                                               Adicionar um tipo de pagamento com permissao de bin de cartoes via api
    [Tags]                                                                   ADICIONAR_TIPO_DE_PAGAMENTO_BIN    ADICIONAR_TIPO_DE_PAGAMENTO_PIPE          
    ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_exibicao_na_promocao}    ${Permite_Parcelamento_Desativado}     ${Bandeira_Desativado}		${Tipo_De_Pagamento_Faturado_Desativado}		${Permite_BIN_De_Cartoes_Ativado}		${Permite_Pagamento_De_produtos_Marktplace_Desativado}		${Permite_Pagamento_De_Produtos_M1_Desativado}		${Permite_Pagamento_De_Produtos_Click&Collect_Desativado}		${Restringir_Pagamento_Por_Tipo_De_Entrega_Desativado}		${Permite_Combinar_Com_O_Mesmo_Tipo_De_Pagamento_Desativado}		${Permite_Dar_Destaque_Na_Promocao_Desativado}      ${De_para_o_legado}     ${Parcelamento_Maximo}

REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM PERMITE PAGAMENTO DE PRODUTOS MARKETPLACE ATIVO
    [Template]                                                               Adicionar um tipo de pagamento com permissao de pagamento de produtos marktplcace via api
    [Tags]                                                                   ADICIONAR_TIPO_DE_PAGAMENTO_MKT    ADICIONAR_TIPO_DE_PAGAMENTO_PIPE        
    ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_exibicao_na_promocao}    ${Permite_Parcelamento_Desativado}     ${Bandeira_Desativado}		${Tipo_De_Pagamento_Faturado_Desativado}		${Permite_BIN_De_Cartoes_Desativado}		${Permite_Pagamento_De_produtos_Marktplace_Ativado}		${Permite_Pagamento_De_Produtos_M1_Desativado}		${Permite_Pagamento_De_Produtos_Click&Collect_Desativado}		${Restringir_Pagamento_Por_Tipo_De_Entrega_Desativado}		${Permite_Combinar_Com_O_Mesmo_Tipo_De_Pagamento_Desativado}		${Permite_Dar_Destaque_Na_Promocao_Desativado}      ${De_para_o_legado}     ${Parcelamento_Maximo}

REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM PERMITE PAGAMENTO DE PRODUTOS M1 ATIVO
    [Template]                                                               Adicionar um tipo de pagamento com permissao de pagamento de produtos m1 via api
    [Tags]                                                                   ADICIONAR_TIPO_DE_PAGAMENTO_M1    ADICIONAR_TIPO_DE_PAGAMENTO_PIPE          
    ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_exibicao_na_promocao}    ${Permite_Parcelamento_Desativado}     ${Bandeira_Desativado}		${Tipo_De_Pagamento_Faturado_Desativado}		${Permite_BIN_De_Cartoes_Desativado}		${Permite_Pagamento_De_produtos_Marktplace_Desativado}		${Permite_Pagamento_De_Produtos_M1_Ativado}		${Permite_Pagamento_De_Produtos_Click&Collect_Desativado}		${Restringir_Pagamento_Por_Tipo_De_Entrega_Desativado}		${Permite_Combinar_Com_O_Mesmo_Tipo_De_Pagamento_Desativado}		${Permite_Dar_Destaque_Na_Promocao_Desativado}      ${De_para_o_legado}     ${Parcelamento_Maximo}

REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM PERMITE PAGAMENTO DE PRODUTOS CLICK&COLLECT ATIVO
    [Template]                                                               Adicionar um tipo de pagamento com permissao de pagamento de produtos clickcollect via api
    [Tags]                                                                   ADICIONAR_TIPO_DE_PAGAMENTO_CLICK_COLLECT    ADICIONAR_TIPO_DE_PAGAMENTO_PIPE          
    ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_exibicao_na_promocao}    ${Permite_Parcelamento_Desativado}     ${Bandeira_Desativado}		${Tipo_De_Pagamento_Faturado_Desativado}		${Permite_BIN_De_Cartoes_Desativado}		${Permite_Pagamento_De_produtos_Marktplace_Desativado}		${Permite_Pagamento_De_Produtos_M1_Desativado}		${Permite_Pagamento_De_Produtos_Click&Collect_Ativado}		${Restringir_Pagamento_Por_Tipo_De_Entrega_Desativado}		${Permite_Combinar_Com_O_Mesmo_Tipo_De_Pagamento_Desativado}		${Permite_Dar_Destaque_Na_Promocao_Desativado}      ${De_para_o_legado}     ${Parcelamento_Maximo}

REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM Restringir pagamento por Tipo de Entrega ATIVO
    [Template]                                                               Adicionar um tipo de pagamento com restricao de pagamento por tipo de entrega via api
    [Tags]                                                                   ADICIONAR_TIPO_DE_PAGAMENTO_ENTREGA      ADICIONAR_TIPO_DE_PAGAMENTO_PIPE          
    ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_exibicao_na_promocao}    ${Permite_Parcelamento_Desativado}     ${Bandeira_Desativado}		${Tipo_De_Pagamento_Faturado_Desativado}		${Permite_BIN_De_Cartoes_Desativado}		${Permite_Pagamento_De_produtos_Marktplace_Desativado}		${Permite_Pagamento_De_Produtos_M1_Desativado}		${Permite_Pagamento_De_Produtos_Click&Collect_Desativado}		${Restringir_Pagamento_Por_Tipo_De_Entrega_Ativado}		${Permite_Combinar_Com_O_Mesmo_Tipo_De_Pagamento_Desativado}		${Permite_Dar_Destaque_Na_Promocao_Desativado}      ${De_para_o_legado}     ${Parcelamento_Maximo}      ${Deliveries}

REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM PERMITE COMBINAR COM O MESMO TIPO DE PAGAMENTO ATIVO
    [Template]                                                               Adicionar um tipo de pagamento com permissao de combinar mesmo tipo de pagamento via api
    [Tags]                                                                   ADICIONAR_TIPO_DE_PAGAMENTO_COMBINAR_MESMO_PAGAMENTO    ADICIONAR_TIPO_DE_PAGAMENTO_PIPE         
    ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_exibicao_na_promocao}    ${Permite_Parcelamento_Desativado}     ${Bandeira_Desativado}		${Tipo_De_Pagamento_Faturado_Desativado}		${Permite_BIN_De_Cartoes_Desativado}		${Permite_Pagamento_De_produtos_Marktplace_Desativado}		${Permite_Pagamento_De_Produtos_M1_Desativado}		${Permite_Pagamento_De_Produtos_Click&Collect_Desativado}		${Restringir_Pagamento_Por_Tipo_De_Entrega_Desativado}		${Permite_Combinar_Com_O_Mesmo_Tipo_De_Pagamento_Ativado}		${Permite_Dar_Destaque_Na_Promocao_Desativado}      ${De_para_o_legado}     ${Parcelamento_Maximo}

REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM PERMITE DAR DESTAQUE NA PROMOÇÃO ATIVO
    [Template]                                                               Adicionar um tipo de pagamento com permissao de dar destaque na promocao via api
    [Tags]                                                                   ADICIONAR_TIPO_DE_PAGAMENTO_DESTAQUE_PROMO    ADICIONAR_TIPO_DE_PAGAMENTO_PIPE          
    ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_exibicao_na_promocao}    ${Permite_Parcelamento_Desativado}     ${Bandeira_Desativado}		${Tipo_De_Pagamento_Faturado_Desativado}		${Permite_BIN_De_Cartoes_Desativado}		${Permite_Pagamento_De_produtos_Marktplace_Desativado}		${Permite_Pagamento_De_Produtos_M1_Desativado}		${Permite_Pagamento_De_Produtos_Click&Collect_Desativado}		${Restringir_Pagamento_Por_Tipo_De_Entrega_Desativado}		${Permite_Combinar_Com_O_Mesmo_Tipo_De_Pagamento_Desativado}		${Permite_Dar_Destaque_Na_Promocao_Ativado}      ${De_para_o_legado}     ${Parcelamento_Maximo}

REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM PERMITE BIN DE CARTÕES ATIVO e PERMITE PAGAMENTO DE PRODUTOS MARKETPLACE ATIVO
    [Template]                                                               Adicionar um tipo de pagamento com permissao de bin de cartoes e Produtos de Marktplcace via api
    [Tags]                                                                   ADICIONAR_TIPO_DE_PAGAMENTO_BIN_MKT    ADICIONAR_TIPO_DE_PAGAMENTO_PIPE          
    ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_exibicao_na_promocao}    ${Permite_Parcelamento_Desativado}     ${Bandeira_Desativado}		${Tipo_De_Pagamento_Faturado_Desativado}		${Permite_BIN_De_Cartoes_Ativado}		${Permite_Pagamento_De_produtos_Marktplace_Ativado}		${Permite_Pagamento_De_Produtos_M1_Desativado}		${Permite_Pagamento_De_Produtos_Click&Collect_Desativado}		${Restringir_Pagamento_Por_Tipo_De_Entrega_Desativado}		${Permite_Combinar_Com_O_Mesmo_Tipo_De_Pagamento_Desativado}		${Permite_Dar_Destaque_Na_Promocao_Desativado}      ${De_para_o_legado}     ${Parcelamento_Maximo}

REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM PERMITE PAGAMENTO DE PRODUTOS CLICK&COLLECT PERMITE PAGAMENTO DE PRODUTOS MARKETPLACE ATIVO e Restringir pagamento por Tipo de Entrega ATIVO
    [Template]                                                               Adicionar um tipo de pagamento com CLICK & COLLECT E RESTRINGE TIPO DE ENTREGA via api
    [Tags]                                                                   ADICIONAR_TIPO_DE_PAGAMENTO_CLICK_ENTREGA    ADICIONAR_TIPO_DE_PAGAMENTO_PIPE          
    ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_exibicao_na_promocao}    ${Permite_Parcelamento_Desativado}     ${Bandeira_Desativado}		${Tipo_De_Pagamento_Faturado_Desativado}		${Permite_BIN_De_Cartoes_Desativado}		${Permite_Pagamento_De_produtos_Marktplace_Desativado}		${Permite_Pagamento_De_Produtos_M1_Desativado}		${Permite_Pagamento_De_Produtos_Click&Collect_Ativado}		${Restringir_Pagamento_Por_Tipo_De_Entrega_Ativado}		${Permite_Combinar_Com_O_Mesmo_Tipo_De_Pagamento_Desativado}		${Permite_Dar_Destaque_Na_Promocao_Desativado}      ${De_para_o_legado}     ${Parcelamento_Maximo}      ${Deliveries}

REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM PERMITE PAGAMENTO DE PRODUTOS MARKETPLACE ATIVO E M1 ATIVO E COMBINAR COM O MESMO TIPO DE PAGAMENTO ATIVO
    [Template]                                                               Adicionar um tipo de pagamento com PERMITE PAGAMENTO DE PRODUTOS MARKETPLACE ATIVO E M1 ATIVO E COMBINAR COM O MESMO TIPO DE PAGAMENTO ATIVO via api
    [Tags]                                                                   ADICIONAR_TIPO_DE_PAGAMENTO_MKT_M1_MESMO_TIPO    ADICIONAR_TIPO_DE_PAGAMENTO_PIPE        
    ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_exibicao_na_promocao}    ${Permite_Parcelamento_Desativado}     ${Bandeira_Desativado}		${Tipo_De_Pagamento_Faturado_Desativado}		${Permite_BIN_De_Cartoes_Desativado}		${Permite_Pagamento_De_produtos_Marktplace_Ativado}		${Permite_Pagamento_De_Produtos_M1_Ativado}		${Permite_Pagamento_De_Produtos_Click&Collect_Desativado}		${Restringir_Pagamento_Por_Tipo_De_Entrega_Desativado}		${Permite_Combinar_Com_O_Mesmo_Tipo_De_Pagamento_Ativado}		${Permite_Dar_Destaque_Na_Promocao_Desativado}      ${De_para_o_legado}     ${Parcelamento_Maximo}

REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM PERMITE DAR DESTAQUE NA PROMOÇÃO ATIVO E PAGAMENTO FATURADO
    [Template]                                                               Adicionar um tipo de pagamento com permissao de dar destaque na promocao e pagamento faturado via api
    [Tags]                                                                   ADICIONAR_TIPO_DE_PAGAMENTO_DESTAQUE_PROMO_FATURADO    ADICIONAR_TIPO_DE_PAGAMENTO_PIPE          
    ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_exibicao_na_promocao}    ${Permite_Parcelamento_Desativado}     ${Bandeira_Desativado}		${Tipo_De_Pagamento_Faturado_Ativado}		${Permite_BIN_De_Cartoes_Desativado}		${Permite_Pagamento_De_produtos_Marktplace_Desativado}		${Permite_Pagamento_De_Produtos_M1_Desativado}		${Permite_Pagamento_De_Produtos_Click&Collect_Desativado}		${Restringir_Pagamento_Por_Tipo_De_Entrega_Desativado}		${Permite_Combinar_Com_O_Mesmo_Tipo_De_Pagamento_Desativado}		${Permite_Dar_Destaque_Na_Promocao_Ativado}      ${De_para_o_legado}     ${Parcelamento_Maximo}

REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM PERMITE PARCELAMENTO ATIVO E COM BANDEIRA ATIVO
    [Template]                                                               Adicionar um tipo de pagamento com permissao de parcelamento e bandeira ativo via api
    [Tags]                                                                   ADICIONAR_TIPO_DE_PAGAMENTO_PARCELAMENTO_BADEIRA    ADICIONAR_TIPO_DE_PAGAMENTO_PIPE          
    ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_exibicao_na_promocao}    ${Permite_Parcelamento_Ativado}     ${Bandeira_Ativado}		${Tipo_De_Pagamento_Faturado_Desativado}		${Permite_BIN_De_Cartoes_Desativado}		${Permite_Pagamento_De_produtos_Marktplace_Desativado}		${Permite_Pagamento_De_Produtos_M1_Desativado}		${Permite_Pagamento_De_Produtos_Click&Collect_Desativado}		${Restringir_Pagamento_Por_Tipo_De_Entrega_Desativado}		${Permite_Combinar_Com_O_Mesmo_Tipo_De_Pagamento_Desativado}		${Permite_Dar_Destaque_Na_Promocao_Desativado}      ${De_para_o_legado}     ${Parcelamento_Maximo}

REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM PERMITE PARCELAMENTO ATIVO E COM BANDEIRA ATIVO E COM PERMITE BIN DE CARTÕES ATIVO
    [Template]                                                               Adicionar um tipo de pagamento com permissao de parcelamento e bandeira ativo e com permissao de bin de cartoes via api
    [Tags]                                                                   ADICIONAR_TIPO_DE_PAGAMENTO_PARCELAMENTO_BADEIRA_BIN    ADICIONAR_TIPO_DE_PAGAMENTO_PIPE          
    ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_exibicao_na_promocao}    ${Permite_Parcelamento_Ativado}     ${Bandeira_Ativado}		${Tipo_De_Pagamento_Faturado_Desativado}		${Permite_BIN_De_Cartoes_Ativado}		${Permite_Pagamento_De_produtos_Marktplace_Desativado}		${Permite_Pagamento_De_Produtos_M1_Desativado}		${Permite_Pagamento_De_Produtos_Click&Collect_Desativado}		${Restringir_Pagamento_Por_Tipo_De_Entrega_Desativado}		${Permite_Combinar_Com_O_Mesmo_Tipo_De_Pagamento_Desativado}		${Permite_Dar_Destaque_Na_Promocao_Desativado}      ${De_para_o_legado}     ${Parcelamento_Maximo}

REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM PERMITE PARCELAMENTO ATIVO E COM BANDEIRA ATIVO E COM PERMITE BIN DE CARTÕES ATIVO E COM PERMITE COMBINAR COM O MESMO TIPO DE PAGAMENTO ATIVO
    [Template]                                                               Adicionar um tipo de pagamento com permissao de parcelamento e bandeira ativo e com permissao de bin de cartoes e com permite combinar mesmo tipo de pagamento via api
    [Tags]                                                                   ADICIONAR_TIPO_DE_PAGAMENTO_PARCELAMENTO_BADEIRA_BIN_MESMO_TIPO    ADICIONAR_TIPO_DE_PAGAMENTO_PIPE          
    ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_exibicao_na_promocao}    ${Permite_Parcelamento_Ativado}     ${Bandeira_Ativado}		${Tipo_De_Pagamento_Faturado_Desativado}		${Permite_BIN_De_Cartoes_Ativado}		${Permite_Pagamento_De_produtos_Marktplace_Desativado}		${Permite_Pagamento_De_Produtos_M1_Desativado}		${Permite_Pagamento_De_Produtos_Click&Collect_Desativado}		${Restringir_Pagamento_Por_Tipo_De_Entrega_Desativado}		${Permite_Combinar_Com_O_Mesmo_Tipo_De_Pagamento_Ativado}		${Permite_Dar_Destaque_Na_Promocao_Desativado}      ${De_para_o_legado}     ${Parcelamento_Maximo}

REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM PERMITE PARCELAMENTO ATIVO E COM BANDEIRA ATIVO E COM PERMITE COMBINAR COM O MESMO TIPO DE PAGAMENTO ATIVO
    [Template]                                                               Adicionar um tipo de pagamento com permissao de parcelamento e bandeira ativo e com permite combinar mesmo tipo de pagamento via api
    [Tags]                                                                   ADICIONAR_TIPO_DE_PAGAMENTO_PARCELAMENTO_BADEIRA_MESMO_TIPO    ADICIONAR_TIPO_DE_PAGAMENTO_PIPE          
    ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_exibicao_na_promocao}    ${Permite_Parcelamento_Ativado}     ${Bandeira_Ativado}		${Tipo_De_Pagamento_Faturado_Desativado}		${Permite_BIN_De_Cartoes_Desativado}		${Permite_Pagamento_De_produtos_Marktplace_Desativado}		${Permite_Pagamento_De_Produtos_M1_Desativado}		${Permite_Pagamento_De_Produtos_Click&Collect_Desativado}		${Restringir_Pagamento_Por_Tipo_De_Entrega_Desativado}		${Permite_Combinar_Com_O_Mesmo_Tipo_De_Pagamento_Ativado}		${Permite_Dar_Destaque_Na_Promocao_Desativado}      ${De_para_o_legado}     ${Parcelamento_Maximo}
