Documentation    A disponibilidade acontece quando o consumidor consegue comprar o produto numa loja online, ou seja, o item tem em estoque. 
...              Porém, se aparece aquela mensagem “Avise-me quando o produto chegar” ou ao algo similar, significa que a loja não tem o produto em estoque, 
...              pois o consumidor não consegue efetuar a compra.


*** Settings ***				
Resource    ../../resources/services.robot


*** Test Cases ***	
Aumentar o estoque dos produtos   
    [Tags]                                                       INCREMENTAR_ESTOQUE
    #Chamada                                                     #Produto              #Quantidade         
    Aumentar estoque dos produtos Fastshop                       AEMQUE2BZABCOB        444