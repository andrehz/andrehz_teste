Documentation    A disponibilidade acontece quando o consumidor consegue comprar o produto numa loja online, ou seja, o item tem em estoque. 
...              Porém, se aparece aquela mensagem “Avise-me quando o produto chegar” ou ao algo similar, significa que a loja não tem o produto em estoque, 
...              pois o consumidor não consegue efetuar a compra.


*** Settings ***				
Resource    ../../resources/services.robot

*** Variables ***
${sku}           BRBRE57AK2
${valor1}        949.00            
${legacyid1}     5119       
${valor2}        1000.00
${legacyid2}     5004       
${valor3}        1000.00   
${legacyid3}     5010
${valor4}        1000.00    
${legacyid4}     5017   


*** Test Cases ***	
Preço e Promoção: Simulação de Promoção                  
    [Tags]                                                       PRECO_PROMOCAO_API
    #Chamada                                                     #Produto              #Quantidade         
    Preço e Promoção: Simulação de Promoção                      ${sku}    ${valor1}    ${legacyid1}    ${valor2}    ${legacyid2}    ${valor3}    ${legacyid3}    ${valor4}    ${legacyid4}
