*** Settings ***				
Documentation      FASTSHOP - CLICK COLLECT    
...                Realizar uma compra Click Colect com sucesso via API.

Resource    ../../resources/services.robot

*** Variables ***
${Cpf}                   34591867803    #49170536252
${Senha}                 Testando123        
${Sku}                   SGSMA515FZPTOB       
${Estado}                SP
${Cidade}                S%C3%A3o%20%20Paulo       
${BranchId}              01
${BranchName}            ZAKI NARCHI
${BranchAddress}         AV ZAKI NARCHI 1650
${ValordoProduto}        1423.75 
${CartaodeCredito0}      4220619003385567
${CartaodeCredito}       5555555555555557   
${CartaodeCredito2}      5447318879391031 
${CartaodeCredito3}      4220619003385567
${Mes_CartaodeCredito}   07   
${Ano_CartaodeCredito}   2023 
${Cvv_CartaodeCredito}   123 

*** Test Cases ***	
REALIZAR UMA COMPRA COM SUCESSO VIA CLICKCOLLECT SELECIONANDO ESTADO E CIDADE
    [Template]                                                               Click Collect selecionando Estado e Cidade Api
    [Tags]                                                                   COMPRACLICKCOLLECTCOMSUCESSO          
    ${Cpf}    ${Senha}    ${Sku}    ${Estado}    ${Cidade}    ${BranchId}    ${BranchName}    ${BranchAddress}    ${ValordoProduto}    ${CartaodeCredito0}    ${Mes_CartaodeCredito}   ${Ano_CartaodeCredito}    ${Cvv_CartaodeCredito} 