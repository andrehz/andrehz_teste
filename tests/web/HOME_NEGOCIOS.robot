*** Settings ***
Documentation      FASTSHOP - HOME NEGOCIOS
...                Verificar se os componentes obrigatorios definidos pelo time de negócios são exibidos aos clientes.
...                Serão validados duas vezes por semana nas reuniões de homologações que ocorrem as terças e quintas feiras.


Resource           ../../resources/base.robot
Suite Setup        Start Session
Suite Teardown     End Session
Test Teardown      End Test

*** Variables ***
${AMBIENTE}                             www        #webapp2-feature    #webapp2-feature #webapp2-develop #webapp2-qa 
${TEMPO_PARA_ENCONTRAR_ELEMENTO}        5
*** Test Cases ***
HOME NEGÓCIOS
    [Template]                          VALIDAR SE TODOS OS COMPONENTES OBRIGATORIOS PELO TIME DE NEGÓCIOS SÃO EXIBIDOS AOS CLIENTES NA HOME

    [Tags]                              HOME_NEGOCIOS2
    #Sku                                                                             
    ${AMBIENTE}

*** Keywords ***
VALIDAR SE TODOS OS COMPONENTES OBRIGATORIOS PELO TIME DE NEGÓCIOS SÃO EXIBIDOS AOS CLIENTES NA HOME
    [Arguments]                                      ${AMBIENTE}  
                        
    ENTRAR COM OPÇÃO DE AMBIENTE DA HOME FASTSHOP E ACEITAR OS COOKIES 
    INICIO DO TESTE:    
    1 - VALIDAR COMPONENTES DO HEADER DA PÁGINA DE PRODUÇÃO
    2 - VALIDAR COMPONENTES DA PARTE INFERIOR DA PÁGINA DE PRODUÇÃO 
    FIM DO TESTE OK