*** Settings ***
Documentation           FAST SHOP - Entrega no mesmo dia
...                     Dado que estou na PDP do Produto
...                     Quando informo o CEP
...                     E clico no botão Aplicar
...                     Então são exibidas as opções de entrega incluindo No mesmo dia/No dia Seguinte, o horário e valor


Resource           ../../resources/base.robot
Suite Setup        Start Session
Suite Teardown     End Session

*** Variables ***
${AMBIENTE}                         webapp2-feature
${CPF_MESMO_DIA}                    82373075008     #07636040034
${CPF_DIA_SEGUINTE}                 35799063058
${SENHA}                            12345678
${SKU}                              BRBRA08AE_PRD       #LGCV7011TC4_PRD     #BRBSR10AB_PRD
${CEP_MESMO_DIA}                    02118010
${CEP_DIA_SEGUINTE}                 18119372        #18119000
${DESCRICAO_MESMO_DIA}              No Mesmo Dia
${DESCRICAO_DIA_SEGUINTE}           No Dia Seguinte
${PRAZO_MESMO_DIA}                  Até 22h
${PRAZO_DIA_SEGUINTE}               Amanhã até 22h
${ALERTA_ATENCAO_MESMO_DIA}         Atenção\nPara uma melhor experiência e conforto, não esqueça de verificar se será possível receber o pedido até o horário limite estipulado de entrega até 22h.\nOK, ENTENDI
${ALERTA_ATENCAO_DIA_SEGUINTE}      Atenção\nPara uma melhor experiência e conforto, não esqueça de verificar se será possível receber o pedido até o horário limite estipulado de entrega até amanhã 22h.\nOK, ENTENDI
${IMPORTANTE_MESMO_DIA}             Importante: O seu pedido pode ser entregue até 22h, verifique possíveis restrições para recebimento no horário acima informado.
${CARRINHO_ENTREGA}                 Entrega 1 de 1
${NUMERO_CARTAO}                    4220612154786956
${NOME_TITULAR}                     Teste Teste
${DATA_VALIDADE}                    10/23
${CVV}                              083

*** Test Cases ***
Entrega no mesmo dia - PDP
    [Template]              PDP COM ENTREGA NO MESMO DIA
    [Tags]                  MESMO_DIA_PDP      MESMO_DIA_PIPE
    ${AMBIENTE}     ${SKU}      ${CEP_MESMO_DIA}        ${DESCRICAO_MESMO_DIA}      ${PRAZO_MESMO_DIA}

Entrega no dia seguinte - PDP
    [Template]              PDP COM ENTREGA NO DIA SEGUINTE
    [Tags]                  DIA_SEGUINTE_PDP    MESMO_DIA_PIPE
    ${AMBIENTE}     ${SKU}      ${CEP_DIA_SEGUINTE}     ${DESCRICAO_DIA_SEGUINTE}       ${PRAZO_DIA_SEGUINTE}

Entrega no mesmo dia - Carrinho
    [Template]              CARRINHO COM ENTREGA NO MESMO DIA
    [Tags]                  MESMO_DIA_CARRINHO      MESMO_DIA_PIPE
    ${AMBIENTE}     ${CPF_MESMO_DIA}        ${SENHA}        ${SKU}      ${DESCRICAO_MESMO_DIA}      ${PRAZO_MESMO_DIA}      ${ALERTA_ATENCAO_MESMO_DIA}

Entrega no dia seguinte - Carrinho
    [Template]              CARRINHO COM ENTREGA NO DIA SEGUINTE
    [Tags]                  DIA_SEGUINTE_CARRINHO       MESMO_DIA_PIPE
    ${AMBIENTE}     ${CPF_DIA_SEGUINTE}        ${SENHA}        ${SKU}      ${DESCRICAO_DIA_SEGUINTE}      ${PRAZO_DIA_SEGUINTE}     ${ALERTA_ATENCAO_DIA_SEGUINTE}

Entrega no mesmo dia - Frete
    [Template]              FRETE COM ENTREGA NO MESMO DIA
    [Tags]                  MESMO_DIA_FRETE       MESMO_DIA_PIPE
    ${AMBIENTE}     ${CPF_MESMO_DIA}        ${SENHA}        ${SKU}      ${DESCRICAO_MESMO_DIA}      ${PRAZO_MESMO_DIA}      ${ALERTA_ATENCAO_MESMO_DIA}

Entrega no dia seguinte - Frete
    [Template]              FRETE COM ENTREGA NO DIA SEGUINTE
    [Tags]                  DIA_SEGUINTE_FRETE       MESMO_DIA_PIPE
    ${AMBIENTE}     ${CPF_DIA_SEGUINTE}        ${SENHA}        ${SKU}      ${DESCRICAO_DIA_SEGUINTE}      ${PRAZO_DIA_SEGUINTE}     ${ALERTA_ATENCAO_DIA_SEGUINTE}

Entrega no mesmo dia - Compra  
    [Template]              COMPRA COM ENTREGA NO MESMO DIA
    [Tags]                  MESMO_DIA_COMPRA       MESMO_DIA_PIPE
    ${AMBIENTE}     ${CPF_MESMO_DIA}        ${SENHA}        ${SKU}      ${DESCRICAO_MESMO_DIA}      ${PRAZO_MESMO_DIA}      ${ALERTA_ATENCAO_MESMO_DIA}     ${NUMERO_CARTAO}        ${NOME_TITULAR}     ${DATA_VALIDADE}        ${CVV}      ${IMPORTANTE_MESMO_DIA} 


*** Keywords ***
PDP COM ENTREGA NO MESMO DIA
    [Arguments]     ${AMBIENTE}     ${SKU}      ${CEP_MESMO_DIA}        ${DESCRICAO_MESMO_DIA}      ${PRAZO_MESMO_DIA}
    Dado que estou na PDP do Produto
    Quando informo o CEP com entrega no mesmo dia
    E clico no botão Aplicar
    Então são exibidas as opções de entrega incluindo no mesmo dia, o horário e valor

PDP COM ENTREGA NO DIA SEGUINTE
    [Arguments]     ${AMBIENTE}     ${SKU}      ${CEP_DIA_SEGUINTE}     ${DESCRICAO_DIA_SEGUINTE}       ${PRAZO_DIA_SEGUINTE}
    Dado que estou na PDP do Produto
    Quando informo o CEP com entrega no dia seguinte 
    E clico no botão Aplicar
    Então são exibidas as opções de entrega incluindo no dia seguinte, o horário e valor

CARRINHO COM ENTREGA NO MESMO DIA
    [Arguments]     ${AMBIENTE}     ${CPF_MESMO_DIA}        ${SENHA}        ${SKU}      ${DESCRICAO_MESMO_DIA}      ${PRAZO_MESMO_DIA}      ${ALERTA_ATENCAO_MESMO_DIA}
    Dado que realizo login com CPF no mesmo dia
    E verifico se o carrinho está vazio
    E adiciono o produto ao carrinho
    E acesso o carrinho
    Quando eu altero o tipo de entrega para no mesmo dia
    Então o alerta de atenção no mesmo dia é exibido
    E a opção de entrega é atualizada para no mesmo dia

CARRINHO COM ENTREGA NO DIA SEGUINTE
    [Arguments]     ${AMBIENTE}     ${CPF_DIA_SEGUINTE}        ${SENHA}        ${SKU}      ${DESCRICAO_DIA_SEGUINTE}      ${PRAZO_DIA_SEGUINTE}     ${ALERTA_ATENCAO_DIA_SEGUINTE}
    Dado que realizo login com CPF no dia seguinte
    E verifico se o carrinho está vazio
    E adiciono o produto ao carrinho
    E acesso o carrinho
    Quando eu altero o tipo de entrega para no dia seguinte
    Então o alerta de atenção no dia seguinte é exibido
    E a opção de entrega é atualizada para no dia seguinte

FRETE COM ENTREGA NO MESMO DIA
    [Arguments]     ${AMBIENTE}     ${CPF_MESMO_DIA}        ${SENHA}        ${SKU}      ${DESCRICAO_MESMO_DIA}      ${PRAZO_MESMO_DIA}      ${ALERTA_ATENCAO_MESMO_DIA}
    Dado que realizo login com CPF no mesmo dia
    E verifico se o carrinho está vazio
    E adiciono o produto ao carrinho
    E acesso o carrinho
    E continuo com a compra até a tela de frete
    Quando eu seleciono o tipo de entrega para no mesmo dia
    Então o alerta de atenção no mesmo dia é exibido

FRETE COM ENTREGA NO DIA SEGUINTE
    [Arguments]     ${AMBIENTE}     ${CPF_DIA_SEGUINTE}        ${SENHA}        ${SKU}      ${DESCRICAO_DIA_SEGUINTE}      ${PRAZO_DIA_SEGUINTE}     ${ALERTA_ATENCAO_DIA_SEGUINTE}
    Dado que realizo login com CPF no dia seguinte
    E verifico se o carrinho está vazio
    E adiciono o produto ao carrinho
    E acesso o carrinho
    E continuo com a compra até a tela de frete
    Quando eu seleciono o tipo de entrega para no dia seguinte
    Então o alerta de atenção no dia seguinte é exibido

COMPRA COM ENTREGA NO MESMO DIA
    [Arguments]     ${AMBIENTE}     ${CPF_MESMO_DIA}        ${SENHA}        ${SKU}      ${DESCRICAO_MESMO_DIA}      ${PRAZO_MESMO_DIA}      ${ALERTA_ATENCAO_MESMO_DIA}     ${NUMERO_CARTAO}        ${NOME_TITULAR}     ${DATA_VALIDADE}        ${CVV}      ${IMPORTANTE_MESMO_DIA} 
    Dado que realizo login com CPF no mesmo dia
    E verifico se o carrinho está vazio
    E adiciono o produto ao carrinho
    E acesso o carrinho
    E continuo com a compra até a tela de frete
    E seleciono o tipo de entrega para no mesmo dia
    E confirmo o alerta de atenção no mesmo dia
    E continuo com a compra até a forma de pagamento
    Quando eu finalizo a compra com cartão de crédito
    Então é exibida a finalização do pedido com a mensagem de entrega no mesmo dia