*** Settings ***
Documentation      FASTSHOP - CLICK COLLCTION    
...                Mudança no layout atual da seleção de endereço no fluxo de retirada em loja pela PDP.
...                Visando melhorar a experiência do usuário o time de UX desenvolveu um novo layout no fluxo da retirada em loja. 
...                Na tela de busca por lojas próximas haverá uma nova disposição dos componentes e a alteração da mensagem disposta atualmente


Resource           ../../resources/base.robot
Suite Setup        Start Session
Suite Teardown     End Session
Test Teardown      End Test

*** Variables ***
${AMBIENTE}                             webapp2-qa     #webapp2-feature    #webapp2-feature #webapp2-develop #webapp2-qa #www
${SKU}                                  WAQP252110CVDB     #I4LIGH10TCNZ    #SGSMA515FZPTOB      
${NOME_DO_ESTADO_BUSCA_CLICK}           9: Object  
${NOME_DA_CIDADE_BUSCA_CLICK}           14: Object
${CPF_AMBIENTE}                         34591867803    #60531429806
${SENHA_AMBIENTE}                       Testando123
${NUMERO_DO_CC_AMBIENTE}                4220612154786956 
${DATA_VALIDADE_AMBIENTE}               1023
${CVV_DO_CC_AMBIENTE}                   083 
${CPF_QRCODE}                           44178878883
${SENHA_QRCODE}                         Teste123
${NUMERO_PEDIDO}                        CA0085464
${DATA_HORA_DISPONIVEL_RETIRADA}        10/01/2022 às 16:52:16
${TITULO_GERAR_QRCODE}                  Gerar seu QR Code  
${DESCRICAO_GERAR_QRCODE}               Para sua segurança vamos fazer uma verificação para termos certeza de que você realizou esse pedido e irá retirar em uma de nossas lojas.\nATENÇÃO: Encaminhamos para o seu e-mail cadastrado na Fast Shop uma confirmação de segurança enviada pelo Datatrust@clear.sale, com título “Validação QRCode”, que ficará ativa por um período de 50 minutos. Não esqueça de verificar sua caixa de entrada, spam e lixeira.
${PERGUNTA_GERAR_QRCODE}                Você recebeu nosso e-mail e fez a confirmação? 
${BOTAO_CONFIRMAR_GERAR_QRCODE}         SIM, FIZ A CONFIRMAÇÃO 
${BOTAO_NAO_RECEBI_GERAR_QRCODE}        Não recebi 
${ENVIAR_NOVAMENTE}                     Pronto! Enviamos novamente, não esqueça de verificar sua caixa de entrada, spam e lixeira. 
${TITULO_ERRO_QRCODE}                   Ocorreu um erro inesperado
${DESCRICAO_ERRO_QRCODE}                Identificamos que você não fez a confirmação.\nDeseja tentar novamente?
${BOTAO_REENVIAR}                       SIM, REENVIAR E-MAIL
${DESCRICAO_QRCODE}                     Aqui está o QR Code para retirada do seu pedido. Orientamos que baixe seu QR CODE ou tire um print, caso saia da sua conta terá que gerar novamente.
${INFORMACOES_IMPORTANTE_QRCODE}        Informações importantes\n\n• Para retirada leve em mãos o QR CODE + DOCUMENTO COM FOTO\n• Caso tenha feito sua compra com cartão físico a validação para retirada poderá ser feita com o próprio\n• Verifique o horário de funcionamento da loja escolhida\n• Seu QR CODE não ficará mais visível após a utilização\n• O seu pedido ficará em loja por um prazo de até 15 dias, após o período entraremos em contato com você
${BOTAO_DOWNLOAD_QRCODE}                FAZER DOWNLOAD


*** Test Cases ***
CLICK COLLECTION
    [Template]                  FUNCIONALIDADES DO BOTÃO RETIRAR EM LOJA NA PDP  

    [Tags]                      CLICK_COLLECT_PDP
    #Ambiente                   #Produto                                                                             
    ${AMBIENTE}                 ${SKU}   

CLICK COLLECTION
    [Template]                  REALIZAR UMA COMPRA RETIRAR EM LOJA COM SUCESSO  

    [Tags]                      CLICK_COLLECT       PIPECLICKCOLLECT
    #Ambiente                   #Produto        #Nome do estado para compra           #CPF               #Senha               #Cartão de Crédito            #Data de Validade do CC          #CVV do Cartão 
    ${AMBIENTE}                 ${SKU}          ${NOME_DO_ESTADO_BUSCA_CLICK}         ${CPF_AMBIENTE}    ${SENHA_AMBIENTE}    ${NUMERO_DO_CC_AMBIENTE}      ${DATA_VALIDADE_AMBIENTE}        ${CVV_DO_CC_AMBIENTE}       

CLICK COLLECTION LIBERAR QR CODE
    [Template]                  LIBERAR QR CODE
    [Tags]                      CLICK_COLLECT_QRCODE        PIPECLICKCOLLECT

    #Ambiente           #CPF                    #Senha                  #Pedido                     #Data e hora
    ${AMBIENTE}         ${CPF_QRCODE}           ${SENHA_QRCODE}         ${NUMERO_PEDIDO}            ${DATA_HORA_DISPONIVEL_RETIRADA}        ${TITULO_GERAR_QRCODE}		${DESCRICAO_GERARQRCODE}		${PERGUNTA_GERAR_QRCODE}		${BOTAO_CONFIRMAR_GERAR_QRCODE}		${BOTAO_NAO_RECEBI_GERAR_QRCODE}        ${ENVIAR_NOVAMENTE}     ${TITULO_ERRO_QRCODE}       ${DESCRICAO_ERRO_QRCODE}        ${BOTAO_REENVIAR}       ${DESCRICAO_QRCODE}     ${INFORMACOES_IMPORTANTE_QRCODE}        ${BOTAO_DOWNLOAD_QRCODE}

*** Keywords ***
FUNCIONALIDADES DO BOTÃO RETIRAR EM LOJA NA PDP 
    [Arguments]                                      ${AMBIENTE}    ${busca_produto}      
                        
    TELA DE PRODUTOS ACEITANDO OS COOKIES - ${busca_produto}             
    INICIO DO TESTE:    
    1 - VERIFICAR SE O BOTÃO RETIRAR EM LOJA É EXIBIDO NA TELA DE PRODUTOS E CLICKAR NO ELEMENTO
  #  2 - VERIFICAR SE AO REALIZAR UMA BUSCA NO CAMPO CEP UM ICONE VERDE É EXIBIDO AO USUARIO
  #  2 - VALIDAR COMPONENTES DA PARTE INFERIOR DA PÁGINA DE PRODUÇÃO 
    FIM DO TESTE OK

REALIZAR UMA COMPRA RETIRAR EM LOJA COM SUCESSO  
    [Arguments]                                      ${AMBIENTE}    ${busca_produto}  ${NOME_DO_ESTADO_BUSCA_CLICK}    ${CPF_AMBIENTE}    ${SENHA_AMBIENTE}     ${NUMERO_DO_CC_AMBIENTE}      ${DATA_VALIDADE_AMBIENTE}        ${CVV_DO_CC_AMBIENTE}

    TELA DE PRODUTOS ACEITANDO OS COOKIES - ${busca_produto}             
    INICIO DO TESTE:    
    1 - VERIFICAR SE O BOTÃO RETIRAR EM LOJA É EXIBIDO NA TELA DE PRODUTOS E CLICKAR NO ELEMENTO
    2 - ESCOLHER A OPÇÃO BUSCA POR CIDADE E SELECIONAR UMA LOJA 
    3 - REALIZAR LOGIN E SEGUIR PARA O CARRINHO NO MODO CLICK COLLECTION
    4 - PAGAMENTO COM O CARTÃO DE CRÉDITO NO CLICK_COLLECTION
    FIM DO TESTE OK

LIBERAR QR CODE
    [Arguments]             ${AMBIENTE}         ${CPF_QRCODE}           ${SENHA_QRCODE}         ${NUMERO_PEDIDO}            ${DATA_HORA_DISPONIVEL_RETIRADA}        ${TITULO_GERAR_QRCODE}		${DESCRICAO_GERARQRCODE}		${PERGUNTA_GERAR_QRCODE}		${BOTAO_CONFIRMAR_GERAR_QRCODE}		${BOTAO_NAO_RECEBI_GERAR_QRCODE}        ${ENVIAR_NOVAMENTE}     ${TITULO_ERRO_QRCODE}       ${DESCRICAO_ERRO_QRCODE}        ${BOTAO_REENVIAR}       ${DESCRICAO_QRCODE}     ${INFORMACOES_IMPORTANTE_QRCODE}        ${BOTAO_DOWNLOAD_QRCODE}

    ENTRAR COM OPÇÃO DE AMBIENTE DA HOME FASTSHOP E ACEITAR OS COOKIES
    ACESSAR TELA DE LOGIN COM CPF E SENHA - ${CPF_QRCODE} ${SENHA_QRCODE}
    ACESSAR TELA DE PEDIDOS E IR ATÉ O PEDIDO ${NUMERO_PEDIDO}
    CLICAR EM LIBERAR QR CODE
    CLICAR EM NÃO RECEBI
    CLICAR EM SIM, FIZ A CONFIRMAÇÃO SEM TER FEITO A CONFIRMAÇÃO
    CLICAR EM SIM, FIZ A CONFIRMAÇÃO

    