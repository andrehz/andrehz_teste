*** Settings ***
Documentation      FASTSHOP - Alteração senha E-MAIL
...                Assim que o cliente inserir seu código de verificação recebido por SMS ou clicar no link recebido em seu e-mail, avançará para alterar sua senha (tela 1* e-mail e tela adicional* sms). Para que o cliente consiga cadastrar sua nova senha ele deverá atingir no mínimo uma nota média das dicas fornecidas pela Fast Shop, enquanto essa nota não for atingida o botão "salvar nova senha" ficará inativo (tela 1/2*). Caso o cliente tente clicar nos campos inativos mostraremos uma mensagem de alerta (tela 3*). Dado que o cliente alcançou uma nota média, o botão "salvar nova senha" passará a ficar ativo para prosseguir (tela 4*). O cliente também ficará livre para tornar a sua senha ainda melhor ao nível forte, mas fica a sua escolha (tela 5*). Após a alteração de senha o cliente é informado e poderá retornar a home já logado dado que clicou em "Continuar navegando" 


Resource           ../../resources/base.robot
Suite Setup        Start Session
Suite Teardown     End Session
Test Teardown      End Test

*** Variables ***
${AMBIENTE}                     webapp2-qa    #webapp2-feature #webapp2-develop #webapp2-qa #www
${NOVA_SENHA}                   Minh@SenhaResetada123 



*** Test Cases ***
HOME NEGÓCIOS
    [Template]                  VALIDAR SE O CLIENTE CONSEGUE ALTERAR A SENHA POR EMAIL

    [Tags]                      ALTERAR_SENHA_EMAIL
    #Ambiente                   #CPF            #Nova Senha                                                                             
    ${AMBIENTE}                                 ${NOVA_SENHA}

*** Keywords ***
VALIDAR SE O CLIENTE CONSEGUE ALTERAR A SENHA POR EMAIL
    [Arguments]                                      ${AMBIENTE}            ${NOVA_SENHA}
    
    TELA RESET DE SENHA ACEITADO COOKIES
    CADASTRAR RESET DE SENHA DE UM NOVO CLIENTE NO SITE DA FASTSHOP
    TELA LOGIN COM RESET DE SENHA ACEITADO COOKIES
    TELA DO RESET DE SENHA COM CPF
    TELA DE RESET DE SENHA ESCOLHER A OPÇÃO E-MAIL
    INICIO DO TESTE:    
    #1 - VALIDAR COMPONENTES DO HEADER DA PÁGINA DE PRODUÇÃO
    #2 - VALIDAR COMPONENTES DA PARTE INFERIOR DA PÁGINA DE PRODUÇÃO 
    FIM DO TESTE OK