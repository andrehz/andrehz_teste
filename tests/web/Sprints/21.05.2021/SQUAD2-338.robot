*** Settings ***
Documentation   Descrição:
...				Como Fast Shop, quero disponibilizar para os Assinantes Prime na Página da Minha Assinatura, informações sobre como utilizar cada benefícios.
...				Critérios de Aceitação:
...				Dado que o cliente é um Assinante Prime e esta logado, quando estiver na página da minha assinatura,   então visualizará o bloco "benefícios de seu plano" aberto, com todos os benefícios elegíveis ao Plano ( Prime ou Plus).
...				        1.1. Dado que o cliente esta na página da minha assinatura, visualizando as opções de benefícios do seu plano, quando desejar visualizar maiores detalhes de cada benefícios, então deverá ter a opção "Saiba Mais" para que ele possa clicar sobre a frase e visualizar o detalhamento de cada beneficio.
...				https://xd.adobe.com/view/1a5af23b-40c2-4294-a0f8-519b1af1fa37-4f09/screen/3416a30b-6b91-4653-82d9-e2e0db762d5c/
...				2. Dado que o cliente clicou sobre a frase "Saiba Mais", então abrirá um modal no formato de perguntas e respostas, aparecendo apenas os títulos de cada benefícios com setas para abertura das respostas.
...				Os dados para preencher o modal devem ser pegos pela URL:
...				https://fastshopwr-a.akamaihd.net/json/benefits/android-benefits.json
...				Obs: Caso o cliente deseja visualizar as respostas de cada benefícios, deverá clicar na seta ao lado do titulo do beneficio.
...				        Caso ele queira fechar as respostas, deverá clicar novamente na seta ao lado do titulo.
...				https://xd.adobe.com/view/1a5af23b-40c2-4294-a0f8-519b1af1fa37-4f09/screen/79cfa66a-e632-4068-80bd-8e84f5dcd4e8/
...				Importante:
...				a) Dentro do titulo do beneficio ( Suporte Técnico Presencial) deverá conter um link na frase "listas de lojas físicas" quando o cliente clicar sobre este link, deverá abrir no navegador o arquivo em PDf com a listagem das lojas.
...				Link para direcionamento do documento:
...				https://documentcloud.adobe.com/link/track?uri=urn%3Aaaid%3Ascds%3AUS%3A67f388c0-4133-4e37-b381-783e334ab95e#pageNum=1
...				 b) Dentro do titulo do beneficio ( Serviço de Instalação) deverá conter um link na frase "relação dos produtos elegíveis" quando o cliente clicar sobre este link, deverá abrir no navegador o arquivo em PDf com a listagem dos produtos.
...				Link para direcionamento do documento:
...				https://documentcloud.adobe.com/link/track?uri=urn:aaid:scds:US:e7f6484e-93a9-45ee-89cf-cd4857e80d6b#pageNum=1
...				Conteúdo dos textos de cada beneficio:
...				Ofertas exclusivas
...				Assinantes Fast Prime e Fast Prime Plus têm acesso a ofertas exclusivas em produtos selecionados, vendidos e entregues pela Fast Shop através do site, app, lojas físicas e televendas. Os produtos elegíveis, estarão sinalizados com o selo [PRIME]
...				 Frete grátis ou com desconto
...				Frete Grátis ou com 10% de desconto: entrega em até 7 dias úteis. Consulte a disponibilidade do benefício para o seu CEP. Válido para itens vendidos e entregues pela Fast Shop.
...				Entrega Ultra Fast grátis
...				O serviço Ultra Fast é válido para produtos selecionados. Ele depende de prévia contratação e sua disponibilidade está sujeita à localização do endereço de entrega, ao estoque das lojas participantes e ao dia e horário da compra.
...				A opção de entrega aparecerá no site e no aplicativo no fechamento do pedido, nas compras realizadas de segunda a sexta, das 10h às 18h.
...				Importante: o prazo de 2 horas inicia a partir da aprovação do pagamento. Você receberá um e-mail informando que o pagamento foi concluído.
...				Suporte técnico remoto
...				Com o serviço de Suporte Técnico Remoto, nossos especialistas te auxiliam na instalação, atualização de softwares ou aplicativos, transferência de dados e backup, configuração em redes sem fio e outros. Acesse o serviço por um de nossos canais de atendimento:
...				Telefone 0800 000.3690
...				Chat fshero.com.br/chat
...				E-mail hero@fs.com.br
...				Whatsapp 11 9 4342.8056
...				Funcionamento das 8h às 23h (on-line) e das 23h01 às 7h59 (caixa postal e atendimento automatizado).
...				Suporte técnico presencial
...				Os Assinantes Fast Prime Plus, possuem o benefício do Suporte Técnico de forma presencial (sem agendamento prévio).
...				Consulte a lista de lojas que oferecem o serviço
...				Atendimento Personalizado no SAC e no Televendas
...				Assinantes Fast Prime e Fast Prime Plus possuem o atendimento de uma equipe especializada e exclusiva no SAC e no Televendas para a compra de produtos, esclarecimento de dúvidas, orientações e solução de problemas.
...				Para acessar o serviço ligue para 11 3003.3278 ou 0800 010 3278 e informe seu CPF. Você será automaticamente direcionado para a Central de Atendimento especializada.
...				Serviço de Instalação
...				Um serviço completo, que deixa tudo pronto para você aproveitar. Nas localidades onde o serviço é disponibilizado e durante o prazo de vigência do plano de assinatura, assinantes Fast Prime Plus têm direito a 2 serviços de instalação de produtos novos grátis, ao longo do período de 12 meses.
...				O prazo para utilização e execução das instalações gratuitas é de 12 meses contados do dia da contratação do plano de assinatura do programa de benefícios, na modalidade Fast Prime Plus. Assim, caso o assinante não utilize as 2 instalações gratuitas a que tinha direito no período de 12 meses, estas não serão cumulativas para o próximo período, ainda que ocorra a renovação automática do plano. Produtos de saldo e produtos vendidos e entregues por lojas parceiras também não são elegíveis ao presente benefício.
...				Consulte os produtos elegíveis ao serviço e as localidades atendidas


Resource           ../../../../resources/base.robot
Suite Setup        Start Session
Suite Teardown     End Session
Test Teardown      End Test

*** Variables ***
${CPF_PRIME}                81573692077
${CPF_COMUM}                83045972179
${SENHA_AMBIENTE}           12345678 
${SKU_AMBIENTE}             EXDB84X1
${QUANTIDADE_AMBIENTE}      1  
*** Test Cases ***
REGRESSÃO - ABRIR MODAL
    [Template]                  Regressão: Validar se quando o usuario clicar no botao um modal vai abrir (QA) 

    [Tags]                      SQUAD2-338
    #CPF                        #Senha                      #Sku                #Quantidade                                                       
    ${CPF_PRIME}                ${SENHA_AMBIENTE}           ${SKU_AMBIENTE}     ${QUANTIDADE_AMBIENTE}     

REGRESSÃO - VALIDAR O COMPORTAMENTO PARA O CLIENTE PRIME 
    [Template]                  Regressão: Validar o comportamento para cliente prime (QA)

    [Tags]                      SQUAD2-338
    #CPF                        #Senha                      #Sku                #Quantidade                                                       
    ${CPF_PRIME}                ${SENHA_AMBIENTE}           ${SKU_AMBIENTE}     ${QUANTIDADE_AMBIENTE}     

REGRESSÃO - VALIDAR O COMPORTAMENTO PARA O CLIENTE NÃO PRIME 
    [Template]                  Regressão: Validar o comportamento para clientes NAO prime (QA)

    [Tags]                      SQUAD2-338
    #CPF                        #Senha                      #Sku                #Quantidade                                                       
    ${CPF_COMUM}                ${SENHA_AMBIENTE}           ${SKU_AMBIENTE}     ${QUANTIDADE_AMBIENTE}              


*** Keywords ***
Regressão: Validar se quando o usuario clicar no botao um modal vai abrir (QA) 
    [Arguments]                                                        ${cpf}        ${senha}        ${busca_produto}            ${quantidade_produto}    

    BOOT INICIAL                                                       ${cpf}        ${senha}        ${busca_produto}                            
    TELA DA MINHA ASSINATURA PRIME ACEITANDO OS COOKIES - ${busca_produto}      
    TELA LOGIN COM CPF E SENHA CORRETOS - ${cpf} ${senha}
    INICIO DO TESTE:    
    VALIDAR SE QUANDO O USUARIO CLICAR NO BOTÃO O MODAL VAI ABRIR
    FIM DO TESTE                                                        ${cpf}        ${senha}        ${busca_produto}

Regressão: Validar o comportamento para cliente prime (QA)
    [Arguments]                                                        ${cpf}        ${senha}        ${busca_produto}            ${quantidade_produto}    

    BOOT INICIAL                                                       ${cpf}        ${senha}        ${busca_produto}                            
    TELA DA MINHA ASSINATURA PRIME ACEITANDO OS COOKIES - ${busca_produto}      
    TELA LOGIN COM CPF E SENHA CORRETOS - ${cpf} ${senha}
    INICIO DO TESTE:    
    VALIDAR O COMPORTAMENTO PARA O CLIENTE PRIME
    FIM DO TESTE                                                      ${cpf}        ${senha}        ${busca_produto}             

Regressão: Validar o comportamento para clientes NAO prime (QA)
    [Arguments]                                                        ${cpf}        ${senha}        ${busca_produto}            ${quantidade_produto}    
    
    BOOT INICIAL                                                       ${cpf}        ${senha}        ${busca_produto}                            
    TELA DA MINHA ASSINATURA PRIME ACEITANDO OS COOKIES - ${busca_produto}      
    TELA LOGIN COM CPF E SENHA CORRETOS - ${cpf} ${senha}
    INICIO DO TESTE:    
    VALIDAR O COMPORTAMENTO PARA O CLIENTE NÃO PRIME
    FIM DO TESTE                                                      ${cpf}        ${senha}        ${busca_produto}             