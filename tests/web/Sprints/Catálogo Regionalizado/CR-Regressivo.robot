*** Settings ***
Documentation      FASTSHOP - Catálogo Regionalizado 
...                Criar o componente de localização do cliente na home para mostrarmos as ofertas e entregas específicas para a região fornecida. 
...                Objetivo: Deve ser aplicado o tratamento para o prime de tudo que for mudado.
...                Comportamento: 
...                Carregamento feito em todas as páginas do carrinho para trás conforme alteração de CEP
...                O cliente que não tem o armazenamento da última compra feita mostraremos o endereço principal (shipping adress).
...                COMPONENTE: ficará localizado no header abaixo da busca com a indicação de "Enviando para". A área de clique vai desde o ícone até a seta. 
...                Localização: Cliente entrará no site com a localização feita de forma automática mostrando envio para seu último endereço de compra (shipping adress) ou principal.
...                Ponto de atenção: preferível último endereço de compra mas backend apontou dificuldade nesse comportamento e que inicialmente trariam o principal (dar visibilidade da possibilidade definida).
...                Escolha de outro endereço: dado que o cliente clica no componente, é aberto uma bottom sheet mostrando seus endereços salvos e o endereço que estiver pré selecionado de forma automática no inicio da sua navegação estará marcado e os demais sem preenchimento. O cliente poderá alterar o endereço clicando no desejado, quando selecionado e clicado em "Aplicar endereço" a bottom sheet se fecha sozinha. 
...                Novo endereço não salvo: dado que o cliente queira pesquisar ofertas em outro endereço que ainda não esteja salvo, poderá aplicar novo CEP através do "Informar outro CEP" e caso não saiba,  terá a opção do "Não sei meu CEP". Ponto de atenção: essa bottom sheet e comportamento já é existente de quando o cliente deseja alterar o endereço no carrinho, deverá se replicado. 


Resource           ../../../../resources/base.robot
Suite Setup        Start Session
Suite Teardown     End Session
Test Teardown      End Test

*** Variables ***
${CEP_VALIDO}               07110040
${CEP_INVALIDO}             00000000 
${NOME_DA_RUA}              RUA MARIA DE CASTRO MESQUITA
${NOME_DA_RUA_INVALIDO}     RUA ERRADA
${NOME_DA_CIDADE}           GUARULHOS
${NOME_DO_ESTADO}           SP
${INFORMAR_CPF}             498.520.371-88
${INFORMAR_SENHA}           12345678 


*** Test Cases ***
REGRESSÃO - VALIDAR SE É EXIBIDO A MENSAGEM: "ESCOLHA O SEU CEP" PARA UM CLIENTE NÃO LOGADO
    [Template]                  Regressão: VALIDAR SE É EXIBIDO A MENSAGEM: "ESCOLHA O SEU CEP" PARA UM CLIENTE NÃO LOGADO

    [Tags]                      CATALOGO1        CATALOGREGIONALIZADO
    #CPF                                                                              
    ${CEP_VALIDO}             

REGRESSÃO - VALIDAR SE AO CLICAR EM INFORME O SEU CEP É EXIBIDO O MODAL AO CLIENTE
    [Template]                  Regressão: VALIDAR SE AO CLICAR EM INFORME O SEU CEP É EXIBIDO O MODAL AO CLIENTE

    [Tags]                      CATALOGO2        CATALOGREGIONALIZADO
    #CPF                                                                          
    ${CEP_VALIDO}             

REGRESSÃO - VALIDAR SE AO INFORMAR UM CEP VALIDO O SITE DA FAST SHOP INFORMA AS MELHORES OFERTAS PARA A REGIÃO 
    [Template]                  Regressão: VALIDAR SE AO INFORMAR UM CEP VALIDO O SITE DA FAST SHOP INFORMA AS MELHORES OFERTAS PARA A REGIÃO

    [Tags]                      CATALOGO3        CATALOGREGIONALIZADO
    #CPF                                                                             
    ${CEP_VALIDO}

REGRESSÃO - VALIDAR SE AO INFORMAR UM CEP INVALIDO O SITE RETORNA A MENSAGEM DE CEP NÃO LOCALIZADO
    [Template]                  Regressão: VALIDAR SE AO INFORMAR UM CEP INVALIDO O SITE RETORNA A MENSAGEM DE CEP NÃO LOCALIZADO

    [Tags]                      CATALOGO4        CATALOGREGIONALIZADO
    #CPF                                                                             
    ${CEP_INVALIDO}    

REGRESSÃO - VALIDAR SE O FLUXO NÃO SEI O MEU CEP ESTÁ BUSCANDO DE FORMA CORRETA O ENDEREÇO DO CLIENTE
    [Template]                  Regressão: VALIDAR SE O FLUXO NÃO SEI O MEU CEP ESTÁ BUSCANDO DE FORMA CORRETA O ENDEREÇO DO CLIENTE

    [Tags]                      CATALOGO5        CATALOGREGIONALIZADO
                                                  
    ${CEP_VALIDO}               ${NOME_DA_RUA}     ${NOME_DA_CIDADE}        ${NOME_DO_ESTADO}  

Regressão: VALIDAR SE AO CLIENTE INFORMAR UM ENDEREÇO INCORRETO O SITE INFORMA CEP NÃO LOCALIZADO
    [Template]                  Regressão: VALIDAR SE AO CLIENTE INFORMAR UM ENDEREÇO INCORRETO O SITE INFORMA CEP NÃO LOCALIZADO

    [Tags]                      CATALOGO6        CATALOGREGIONALIZADO
                                                      
    ${CEP_VALIDO}               ${NOME_DA_RUA_INVALIDO}     ${NOME_DA_CIDADE}        ${NOME_DO_ESTADO}           

Regressão: VALIDAR SE AO REALIZAR A PARTIR DO BOTÃO FAÇA O LOGIN NO MODAL O CLIENTE VISUALIZA SEUS ENDEREÇOS SALVOS
    [Template]                  Regressão: VALIDAR SE AO REALIZAR A PARTIR DO BOTÃO FAÇA O LOGIN NO MODAL O CLIENTE VISUALIZA SEUS ENDEREÇOS SALVOS

    [Tags]                      CATALOGO7        CATALOGREGIONALIZADO

    ${CEP_VALIDO}               ${NOME_DA_RUA_INVALIDO}    ${NOME_DA_CIDADE}    ${NOME_DO_ESTADO}    ${INFORMAR_CPF}    ${INFORMAR_SENHA}

*** Keywords ***
Regressão: VALIDAR SE É EXIBIDO A MENSAGEM: "ESCOLHA O SEU CEP" PARA UM CLIENTE NÃO LOGADO
    [Arguments]                                                        ${CEP_VALIDO}             
                                                    
    ENTRAR NA HOME FASTSHOP E ACEITAR OS COOKIES 
    INICIO DO TESTE:    
    VALIDAR SE É EXIBIDO A MENSAGEM: "INFORME O SEU CEP" PARA UM CLIENTE NÃO LOGADO
    FIM DO TESTE OK                    

Regressão: VALIDAR SE AO CLICAR EM INFORME O SEU CEP É EXIBIDO O MODAL AO CLIENTE
    [Arguments]                                                        ${CEP_VALIDO}
                           
    ENTRAR NA HOME FASTSHOP E ACEITAR OS COOKIES 
    INICIO DO TESTE:    
    VALIDAR SE AO CLICAR EM INFORME O SEU CEP É EXIBIDO O MODAL AO CLIENTE
    FIM DO TESTE OK                    

Regressão: VALIDAR SE AO INFORMAR UM CEP VALIDO O SITE DA FAST SHOP INFORMA AS MELHORES OFERTAS PARA A REGIÃO
    [Arguments]                                                        ${CEP_VALIDO}                                                        

    ENTRAR NA HOME FASTSHOP E ACEITAR OS COOKIES 
    INICIO DO TESTE:
    VALIDAR SE AO INFORMAR UM CEP VALIDO O SITE DA FAST SHOP INFORMA AS MELHORES OFERTAS PARA A REGIÃO ${CEP_VALIDO}
    FIM DO TESTE OK                    

Regressão: VALIDAR SE AO INFORMAR UM CEP INVALIDO O SITE RETORNA A MENSAGEM DE CEP NÃO LOCALIZADO
    [Arguments]                                                        ${CEP_VALIDO}                                                        

    ENTRAR NA HOME FASTSHOP E ACEITAR OS COOKIES 
    INICIO DO TESTE:
    VALIDAR AO INFORMAR UM CEP INVALIDO O SITE EXIBE UMA MENSAGEM DE ERRO ${CEP_INVALIDO}
    FIM DO TESTE OK


Regressão: VALIDAR SE O FLUXO NÃO SEI O MEU CEP ESTÁ BUSCANDO DE FORMA CORRETA O ENDEREÇO DO CLIENTE
    [Arguments]                                                        ${CEP_VALIDO}    ${NOME_DA_RUA}    ${NOME_DA_CIDADE}    ${NOME_DO_ESTADO}                                                        

    ENTRAR NA HOME FASTSHOP E ACEITAR OS COOKIES 
    INICIO DO TESTE:
    VALIDAR SE O FLUXO NÃO SEI O MEU CEP ESTÁ BUSCANDO DE FORMA CORRETA O ENDEREÇO DO CLIENTE ${CEP_VALIDO} ${NOME_DA_RUA} ${NOME_DA_CIDADE} ${NOME_DO_ESTADO}
    FIM DO TESTE OK   

Regressão: VALIDAR SE AO CLIENTE INFORMAR UM ENDEREÇO INCORRETO O SITE INFORMA CEP NÃO LOCALIZADO
    [Arguments]                                                        ${CEP_VALIDO}    ${NOME_DA_RUA_INVALIDO}    ${NOME_DA_CIDADE}    ${NOME_DO_ESTADO}                                                        

    ENTRAR NA HOME FASTSHOP E ACEITAR OS COOKIES 
    INICIO DO TESTE:
    VALIDAR SE AO CLIENTE INFORMAR UM ENDEREÇO INCORRETO O SITE INFORMA CEP NÃO LOCALIZADO ${CEP_VALIDO} ${NOME_DA_RUA_INVALIDO} ${NOME_DA_CIDADE} ${NOME_DO_ESTADO} ${INFORMAR_CPF} ${INFORMAR_SENHA}
    FIM DO TESTE OK   

Regressão: VALIDAR SE AO REALIZAR A PARTIR DO BOTÃO FAÇA O LOGIN NO MODAL O CLIENTE VISUALIZA SEUS ENDEREÇOS SALVOS
    [Arguments]                                                        ${CEP_VALIDO}    ${NOME_DA_RUA_INVALIDO}    ${NOME_DA_CIDADE}    ${NOME_DO_ESTADO}    ${INFORMAR_CPF}    ${INFORMAR_SENHA}                                                        

    ENTRAR NA HOME FASTSHOP E ACEITAR OS COOKIES 
    INICIO DO TESTE:
    VALIDAR SE AO REALIZAR A PARTIR DO BOTÃO FAÇA O LOGIN NO MODAL O CLIENTE VISUALIZA SEUS ENDEREÇOS SALVOS ${CEP_VALIDO} ${NOME_DA_RUA_INVALIDO} ${NOME_DA_CIDADE} ${NOME_DO_ESTADO} ${INFORMAR_CPF} ${INFORMAR_SENHA}
    FIM DO TESTE OK 