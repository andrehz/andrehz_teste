*** Settings ***
Library	    Collections			
Library	    RequestsLibrary
Library     OperatingSystem
Library     lib/cortar_endereco.py
Library     DateTime
Resource    Helpers.robot
Resource    base.robot

*** Variables ***
${base_url}     https://apiqa.fastshop.com.br
                #https://apiqa.fastshop.com.br
                #https://apiqa.fastshop.com.br
                #https://www.fastshop.com.br]
${cpf_api}      94186183031                          


*** Keywords ***
### /sessions
Post Session
    [Arguments]     ${payload}

    &{headers}=	    Create Dictionary	Content-Type=application/json

    ${resp}=	    Post Request     FastShop      /fastshop-qa/wcs/resources/v1/auth/login     data=${payload}     headers=${headers}
    [return]        ${resp}
    Log             ${payload} 
    Log             ${headers}
    Log             ${resp} 


Logar na FastShop
    [Arguments]                         ${cpf_api}              ${senha_api}    ${resp.status_code_page}               
    ${payload}=                         Convert To Json         {"document":"${cpf_api}","password":"${senha_api}"}
    ${resp}=	                        Post Session            ${payload}
    Should Be Equal As Strings          ${resp.status_code}     ${resp.status_code_page} 


Visualizar o carrinho FASTSHOP
   
    [Arguments]                         ${cpf_api}          ${senha_api}               
    ${payload}=                         Convert To Json     {"document":"${cpf_api}","password":"${senha_api}"}
    Log                                 ${payload}
    ${resp}=                            Post Session        ${payload}
    Should Be Equal As Strings          ${resp.status_code}     201
    ${WCToken}=                         Convert To String   ${resp.json()['WCToken']}
    
    Log                                 ${payload}
    ${WCTrustedToken}=                  Convert To String   ${resp.json()['WCTrustedToken']}

    Log                                 ${WCTrustedToken}
    ${userID}=                          Convert To String   ${resp.json()['userID']}

    Log                                 ${userID}
    &{headers}=	                        Create Dictionary	storeId=${userID}       WCToken=${WCToken}       WCTrustedToken=${WCTrustedToken}        Connection=Connection       User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36       Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp      
    ${resp}=	                        Get Request         FastShop      /v1/checkout/cart/self     data=${payload}     headers=${headers}     
    
    Should Be Equal As Strings          ${resp.status_code}     200
    

Deletar um produto do carrinho FastShop                  
    [Arguments]                         ${cpf}    ${senha}    ${busca_produto} 
    
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json     {"document":"${cpf}","password":"${senha}"}
    Log                                 ${payload}
    ${resp}=                            Post Session        ${payload}
    Should Be Equal As Strings          ${resp.status_code}     201
    ${WCToken}=                         Convert To String   ${resp.json()['WCToken']}
    
    Log                                 ${payload}
    ${WCTrustedToken}=                  Convert To String   ${resp.json()['WCTrustedToken']}

    Log                                 ${WCTrustedToken}
    ${userID}=                          Convert To String   ${resp.json()['userID']}

    Log                                 ${userID}
    &{headers}=	                        Create Dictionary	storeId=${userID}       WCToken=${WCToken}               WCTrustedToken=${WCTrustedToken}        Connection=Connection       User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36       Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp      
    ${resp}=	                          Delete Request         FastShop      /fastshop-qa/checkout/cart/${busca_produto}    data=${payload}     headers=${headers}     
    Log to console                      ${resp}        
 

Deletar dois produtos do carrinho FastShop
   
    [Arguments]                         ${cpf}          ${senha}        ${busca_produto}    ${busca_produto_dois}               
    ${payload}=                         Convert To Json     {"document":"${cpf}","password":"${senha}"}
    Log                                 ${payload}
    ${resp}=                            Post Session        ${payload}
    Should Be Equal As Strings          ${resp.status_code}     201
    ${WCToken}=                         Convert To String   ${resp.json()['WCToken']}
    
    Log                                 ${payload}
    ${WCTrustedToken}=                  Convert To String   ${resp.json()['WCTrustedToken']}

    Log                                 ${WCTrustedToken}
    ${userID}=                          Convert To String   ${resp.json()['userID']}

    Log                                 ${userID}
    &{headers}=	                        Create Dictionary	storeId=${userID}       WCToken=${WCToken}               WCTrustedToken=${WCTrustedToken}        Connection=Connection       User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36       Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp      
    ${resp}=	                          Delete Request         FastShop      /v1/checkout/cart/${busca_produto}    data=${payload}     headers=${headers}     
    Log to console                      ${resp}        
    &{headers}=	                        Create Dictionary	storeId=${userID}       WCToken=${WCToken}               WCTrustedToken=${WCTrustedToken}        Connection=Connection       User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36       Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp      
    ${resp}=	                          Delete Request         FastShop      /v1/checkout/cart/${busca_produto_dois}    data=${payload}     headers=${headers}     
    Log to console                      ${resp}   
  



  #Wishlist
Inserir um produto em favoritos no carrinho FASTSHOP
   
    [Arguments]                         ${cpf_api}          ${senha_api}            ${busca_produto}        ${resp.status_code_page}                       
    ${payload}=                         Convert To Json     {"document":"${cpf_api}","password":"${senha_api}"}
    Log                                 ${payload}
    ${resp}=                            Post Session        ${payload}
    Should Be Equal As Strings          ${resp.status_code}     201
    ${WCToken}=                         Convert To String   ${resp.json()['WCToken']}
    
    Log                                 ${payload}
    ${WCTrustedToken}=                  Convert To String   ${resp.json()['WCTrustedToken']}

    Log                                 ${WCTrustedToken}
    ${userID}=                          Convert To String   ${resp.json()['userID']}

    Log                                 ${userID}
    &{headers}=	                        Create Dictionary	storeId=${userID}       WCToken=${WCToken}       WCTrustedToken=${WCTrustedToken}        Connection=Connection       User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36       Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp      
    ${payload}=                         Convert To Json     [{"sku":"${busca_produto}","quantity":1,"url":"/p/d/JBLGO2PTOB/iphone-11-branco-com-tela-de-61-4g-64-gb-e-camera-de-12-mp-mwlu2bra"}]
    Log                                 ${payload}

    ${resp}=	                        Post Request            FastShop       /v1/checkout/cart/wishList?deleteCartItem=false        data=${payload}     headers=${headers}     
    Should Be Equal As Strings          ${resp.status_code}     ${resp.status_code_page}

    &{headers}=	                        Create Dictionary	    storeId=${userID}       WCToken=${WCToken}               WCTrustedToken=${WCTrustedToken}        Connection=Connection       User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36       Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp      
    ${resp}=	                        Delete Request          FastShop      /v1/checkout/cart/wishList/${busca_produto}    data=${payload}     headers=${headers}     
    Log to console                      ${resp}   
  
#UPDATES
Atualizar o endereço do usuario1
    [Arguments]                         ${cpf_api}          ${senha_api}              ${resp.status_code_page}                               
    ${payload}=                         Convert To Json     {"document":"${cpf_api}","password":"${senha_api}"}
    Log                                 ${payload}
    ${resp}=                            Post Session        ${payload}
    Should Be Equal As Strings          ${resp.status_code}     201
    ${WCToken}=                         Convert To String   ${resp.json()['WCToken']}
    
    Log                                 ${payload}
    ${WCTrustedToken}=                  Convert To String   ${resp.json()['WCTrustedToken']}

    Log                                 ${WCTrustedToken}
    ${userID}=                          Convert To String   ${resp.json()['userID']}

    Log                                 ${userID}
    &{headers}=	                        Create Dictionary	storeId=${userID}       WCToken=${WCToken}       WCTrustedToken=${WCTrustedToken}        Connection=Connection       User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36       Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp      
    ${payload}=                         Convert To Json     {"name":"Rua Maria De Castro Mesquita 84","nickName":"API-TESTE-FASTSHOP","zipCode":"07110040","streetName":"R R MARIA DE CASTRO MESQUITA","number":54,"complement":"Rua Maria De Castro ","district":"Rua Maria De Castro Mesquita 8","city":"GUARULHOS","state":"SP","telephone":"(11) 98748-8174","housingType":"1","country":"Brasil","cellphone":"(11) 98748-8174"}
    Log                                 ${payload}
    ${resp}=	                        Post Request             FastShop      /fastshop-qa/wcs/resources/v1/customer/addresses/create     data=${payload}                headers=${headers}
    Should Be Equal As Strings          ${resp.status_code}     ${resp.status_code_page}
    Log to console                      "Endereco cadastrado" ${resp}  

    &{headers}=	                        Create Dictionary	    storeId=${userID}       WCToken=${WCToken}               WCTrustedToken=${WCTrustedToken}        Connection=Connection       User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36       Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp    
    ${payload}=                         Convert To Json         {}
    Log to console                      ${payload}  
    ${resp}=	                        Get Request          FastShop      /fastshop-qa/wcs/resources//v1/customer/addresses        data=${payload}     headers=${headers}     
    ${addressId}=                       Convert To String   ${resp.json()['addresses']}
    Log to console                      ${addressId}
    ${addressId2}=                      cortar_addressId     ${addressId}
    Log to console                      "Numero ID endereço" ${addressId2}

    ${resp}=	                        Put Request          FastShop      /fastshop-qa//checkout/cart/updateAddress/${addressId2}        data=${payload}     headers=${headers}     
    Should Be Equal As Strings          ${resp.status_code}  200


#UPDATES
1Atualizar o endereço do usuario
    [Arguments]                         ${cpf_api}          ${senha_api}              ${resp.status_code_page}                           
    ${payload}=                         Convert To Json     {"document":"${cpf_api}","password":"${senha_api}"}
    Log                                 ${payload}
    ${resp}=                            Post Session        ${payload}
    Should Be Equal As Strings          ${resp.status_code}     201
    ${WCToken}=                         Convert To String   ${resp.json()['WCToken']}
    
    Log                                 ${payload}
    ${WCTrustedToken}=                  Convert To String   ${resp.json()['WCTrustedToken']}

    Log                                 ${WCTrustedToken}
    ${userID}=                          Convert To String   ${resp.json()['userID']}

    Log                                 ${userID}
    &{headers}=	                        Create Dictionary	storeId=${userID}       WCToken=${WCToken}       WCTrustedToken=${WCTrustedToken}        Connection=Connection       User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36       Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp      
    ${payload}=                         Convert To Json     {"name":"Rua Maria De Castro Mesquita 84","nickName":"API -TESTE","zipCode":"07110040","streetName":"R R MARIA DE CASTRO MESQUITA","number":54,"complement":"Rua Maria De Castro ","district":"Rua Maria De Castro Mesquita 8","city":"GUARULHOS","state":"SP","telephone":"(11) 98748-8174","housingType":"1","country":"Brasil","cellphone":"(11) 98748-8174"}

    &{headers}=	                        Create Dictionary	    storeId=${userID}       WCToken=${WCToken}               WCTrustedToken=${WCTrustedToken}        Connection=Connection       User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36       Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp    
    ${payload}=                         Convert To Json         {}
    Log to console                      ${payload}  
    ${resp}=	                        Get Request          FastShop      /fastshop-qa/wcs/resources//v1/customer/addresses        data=${payload}     headers=${headers}     
    ${addressId}=                       Convert To String    ${resp.json()['addresses']}
    ${addressId2}=                      cortar_addressId     ${addressId}
    Log to console                      ${addressId2}
          
    
    ${resp}=	                        Put Request          FastShop      /fastshop-qa//checkout/cart/updateAddress/${addressId2}           data=${payload}     headers=${headers}     
    Should Be Equal As Strings          ${resp.status_code}  200


### create
Cadastro criar um usuario com sucesso
    [Arguments]                         #${resp.status_code_page}  
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${CPF}                              FakerLibrary.cpf  
    ${NOMEFAKE}                         FakerLibrary.Name
    ${TELEFONEFAKE}                     FakerLibrary.Phone Number
    ${PALAVRAFAKE}                      FakerLibrary.Word 
    ${PASSWORDFAKE}                     FakerLibrary.Password 
    ${NUMEROFAKE}                       FakerLibrary.Random Number
    &{headers}=	                        Create Dictionary	Connection=Connection       User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36       Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp      
    ${CPF_VALIDO}=                      Convert To String   ${CPF[0:3]}${CPF[4:7]}${CPF[8:11]}${CPF[12:]}
    #${EMAIL_VALIDO}=                    Convert To String   yimege4109@tmednews.com
    ${EMAIL_VALIDO}=                    Convert To String   ${PALAVRAFAKE}_${NUMEROFAKE}_hz@fastshopteste.com.br 
    ${payload}=                         Convert To Json     {"document":"${CPF_VALIDO}","name":"${NOMEFAKE}","birthday":"1985-06-03","gender":"Male","tradeName":"","companyName":"","ie":"","telephone":"(11) 987488195","cellphone":"(11) 987488195","email":"${EMAIL_VALIDO}","password":"12345678","receiveEmailFast":true,"receiveSMSFast":true,"receiveEmailOthers":true,"cpf":"${CPF_VALIDO}","emailVerify":"${EMAIL_VALIDO}","passwordVerify":"12345678","zipCode":"07110040","streetName":"R R MARIA DE CASTRO MESQUITA","number":"84","complement":"56","district":"JARDIM SAO PAULO","city":"GUARULHOS","state":"SP","housingType":"2","country":"Brasil"}
    Log                                 ${payload}

    ${resp}=	                        Post Request            FastShop       /fastshop-qa/wcs/resources/v1/customer/create       data=${payload}     headers=${headers}     
    
    Log to console                      ${resp.status_code}
    Log to console                      ${payload}
    Log to console                      ******************CPF DO USUARIO: ${CPF_VALIDO}
    Log to console                      ******************
    Log to console                      ******************
    Log to console                      ${EMAIL_VALIDO}
    Log                                 ${CPF_VALIDO}
    Set Suite Variable                  ${CPF_VALIDO}
    Log                                 ${EMAIL_VALIDO}
    Should Be Equal As Strings          ${resp.status_code}       201

Cadastro reset de senha criar um usuario com email temporario com sucesso
    [Arguments]                         ${resp.status_code_page}  
    #Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${CPF}                              FakerLibrary.cpf  
    ${NOMEFAKE}                         FakerLibrary.Name
    ${TELEFONEFAKE}                     FakerLibrary.Phone Number
    ${PALAVRAFAKE}                      FakerLibrary.Word 
    ${PASSWORDFAKE}                     FakerLibrary.Password 
    ${NUMEROFAKE}                       FakerLibrary.Random Number
    &{headers}=	                        Create Dictionary	Connection=Connection       User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36       Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp      
    ${CPF_VALIDO}=                      Convert To String   ${CPF[0:3]}${CPF[4:7]}${CPF[8:11]}${CPF[12:]}
    ${EMAIL_VALIDO}=                    Convert To String   ${EMAIL_TEMPORARIO}
    ${payload}=                         Convert To Json     {"document":"${CPF_VALIDO}","name":"${NOMEFAKE}","birthday":"1985-06-03","gender":"Male","tradeName":"","companyName":"","ie":"","telephone":"(11) 987488195","cellphone":"(11) 987488195","email":"${EMAIL_TEMPORARIO}","password":"12345678","receiveEmailFast":true,"receiveSMSFast":true,"receiveEmailOthers":true,"cpf":"${CPF_VALIDO}","emailVerify":"${EMAIL_TEMPORARIO}","passwordVerify":"12345678","zipCode":"07110040","streetName":"R R MARIA DE CASTRO MESQUITA","number":"84","complement":"56","district":"JARDIM SAO PAULO","city":"GUARULHOS","state":"SP","housingType":"2","country":"Brasil"}
    Log                                 ${payload}

    ${resp}=	                        Post Request            FastShop       /fastshop-qa/wcs/resources/v1/customer/create       data=${payload}     headers=${headers}     
    
    Log to console                      ${resp.status_code}
    Log to console                      ${payload}
    Log to console                      ******************CPF DO USUARIO: ${CPF_VALIDO}
    Log to console                      ******************
    Log to console                      ******************
    Log to console                      ${EMAIL_VALIDO}
    Log                                 ${CPF_VALIDO}
    Log                                 ${EMAIL_VALIDO}
    Should Be Equal As Strings          ${resp.status_code}       ${resp.status_code_page}

Criar uma conta prime plus
    ###Login

    ${payload}=                         Convert To Json         {"document":"${CPF_VALIDO}","password":"12345678"}
    Log                                 ${payload}
    ${resp}=                            Sprints Post Session    ${payload}
    Should Be Equal As Strings          ${resp.status_code}     201
    ${WCToken}=                         Convert To String       ${resp.json()['WCToken']}
    
    Log                                 ${payload}
    ${WCTrustedToken}=                  Convert To String       ${resp.json()['WCTrustedToken']}

    Log                                 ${WCTrustedToken}
    ${userID}=                          Convert To String       ${resp.json()['userID']}
    Log                                 ${userID}

    ###Criar o Prime Plus
    &{headers}=	                        Create Dictionary	    Connection=keep-alive       WCTrustedToken=${WCTrustedToken}        User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36    userAgent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36   WCToken=${WCToken}      Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp       storeId=10151      ip=172.84.04                                                                    
    ${payload}=                         Convert To Json         {"email":"${PALAVRAFAKE}_${NUMEROFAKE}_hz@fastshopteste.com.br","planId":"17914","productId":"72905","cardNumber":"5555555555555557","holderName":"teste teste","expireDate":"01/2022","document":"${CPF_VALIDO}","cvv":"123"} 
    ${resp}=	                        Post Request            FastShop       /prime/subscription       data=${payload}     headers=${headers}     
    Log to console                      ${resp.status_code}
    Log to console                      ${payload}
    Should Be Equal As Strings          ${resp.status_code}     200


CPF INVÁLIDO - Cadastro criar um usuario com dados inválidos
    [Arguments]                         ${resp.status_code_page}  
    ${CPF}                              FakerLibrary.cpf  
    ${NOMEFAKE}                         FakerLibrary.Name
    ${TELEFONEFAKE}                     FakerLibrary.Phone Number
    ${PALAVRAFAKE}                      FakerLibrary.Word 
    ${PASSWORDFAKE}                     FakerLibrary.Password 
    &{headers}=	                        Create Dictionary	Connection=Connection       User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36       Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp      
    ${payload}=                         Convert To Json     {"document":"${CPF}","name":"${NOMEFAKE}","birthday":"1985-06-03","gender":"Male","tradeName":"","companyName":"","ie":"","telephone":"(11) 987488195","cellphone":"(11) 987488195","email":"${PALAVRAFAKE}@fastshopteste.com","password":"${PASSWORDFAKE}","receiveEmailFast":true,"receiveSMSFast":true,"receiveEmailOthers":true,"cpf":"${CPF}","emailVerify":"${PALAVRAFAKE}@fastshopteste.com","passwordVerify":"${PASSWORDFAKE}","zipCode":"07110040","streetName":"R R MARIA DE CASTRO MESQUITA","number":"84","complement":"56","district":"JARDIM SAO PAULO","city":"GUARULHOS","state":"SP","housingType":"2","country":"Brasil"}
    Log                                 ${payload}

    ${resp}=	                        Post Request            FastShop       /fastshop-qa/wcs/resources/v1/customer/create       data=${payload}     headers=${headers}     
    Log to console                      ${payload}
    Log to console                      ${resp.status_code}
    Should Be Equal As Strings          ${resp.status_code}         ${resp.status_code_page}

FORMULÁRIO EM BRANCO - Cadastro criar um usuario com dados inválidos
    [Arguments]                         ${resp.status_code_page}  
    ${CPF}                              FakerLibrary.cpf  
    ${NOMEFAKE}                         FakerLibrary.Name
    ${TELEFONEFAKE}                     FakerLibrary.Phone Number
    ${PALAVRAFAKE}                      FakerLibrary.Word 
    ${PASSWORDFAKE}                     FakerLibrary.Password 
    &{headers}=	                        Create Dictionary	Connection=Connection       User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36       Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp      
    ${payload}=                         Convert To Json     {"document":"${CPF[0:3]}${CPF[4:7]}${CPF[8:11]}${CPF[12:]}","name":"","birthday":"","gender":"","tradeName":"","companyName":"","ie":"","telephone":"","cellphone":"","email":"","password":"","receiveEmailFast":true,"receiveSMSFast":true,"receiveEmailOthers":true,"cpf":"${CPF[0:3]}${CPF[4:7]}${CPF[8:11]}${CPF[12:]}","emailVerify":"","passwordVerify":"","zipCode":"07110040","streetName":"R R MARIA DE CASTRO MESQUITA","number":"84","complement":"56","district":"JARDIM SAO PAULO","city":"GUARULHOS","state":"SP","housingType":"2","country":"Brasil"}
    Log                                 ${payload}

    ${resp}=	                        Post Request            FastShop       /fastshop-qa/wcs/resources/v1/customer/create       data=${payload}     headers=${headers}     
    Log to console                      ${payload}
    Log to console                      ${resp.status_code}
    Should Be Equal As Strings          ${resp.status_code}         ${resp.status_code_page}


FORMULÁRIO EM BRANCO DADOS DE ENTREGA - Cadastro criar um usuario com dados inválidos
    [Arguments]                         ${resp.status_code_page}  
    ${CPF}                              FakerLibrary.cpf  
    ${NOMEFAKE}                         FakerLibrary.Name
    ${TELEFONEFAKE}                     FakerLibrary.Phone Number
    ${PALAVRAFAKE}                      FakerLibrary.Word 
    ${PASSWORDFAKE}                     FakerLibrary.Password 
    &{headers}=	                        Create Dictionary	Connection=Connection       User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36       Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp      
    ${payload}=                         Convert To Json     {"document":"${CPF[0:3]}${CPF[4:7]}${CPF[8:11]}${CPF[12:]}","name":"${NOMEFAKE}","birthday":"1985-06-03","gender":"Male","tradeName":"","companyName":"","ie":"","telephone":"(11) 987488195","cellphone":"(11) 987488195","email":"${PALAVRAFAKE}@fastshopteste.com","password":"${PASSWORDFAKE}","receiveEmailFast":true,"receiveSMSFast":true,"receiveEmailOthers":true,"cpf":"${CPF[0:3]}${CPF[4:7]}${CPF[8:11]}${CPF[12:]}","emailVerify":"${PALAVRAFAKE}@fastshopteste.com","passwordVerify":"${PASSWORDFAKE}","zipCode":"","streetName":"","number":"","complement":"","district":"","city":"","state":"","housingType":"","country":""}
    Log                                 ${payload}

    ${resp}=	                        Post Request            FastShop       /fastshop-qa/wcs/resources/v1/customer/create       data=${payload}     headers=${headers}     
    Log to console                      ${payload}
    Log to console                      ${resp.status_code}
    Should Be Equal As Strings          ${resp.status_code}         ${resp.status_code_page}

APENAS PRIMEIRO NOME - Cadastro criar um usuario com dados inválidos
    [Arguments]                         ${resp.status_code_page}  
    ${CPF}                              FakerLibrary.cpf  
    ${NOMEFAKE}                         FakerLibrary.Name
    ${TELEFONEFAKE}                     FakerLibrary.Phone Number
    ${PALAVRAFAKE}                      FakerLibrary.Word 
    ${PASSWORDFAKE}                     FakerLibrary.Password 
    &{headers}=	                        Create Dictionary	Connection=Connection       User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36       Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp      
    ${payload}=                         Convert To Json     {"document":"${CPF[0:3]}${CPF[4:7]}${CPF[8:11]}${CPF[12:]}","name":"ANDRE","birthday":"1985-06-03","gender":"Male","tradeName":"","companyName":"","ie":"","telephone":"(11) 987488195","cellphone":"(11) 987488195","email":"${PALAVRAFAKE}@fastshopteste.com","password":"${PASSWORDFAKE}","receiveEmailFast":true,"receiveSMSFast":true,"receiveEmailOthers":true,"cpf":"${CPF[0:3]}${CPF[4:7]}${CPF[8:11]}${CPF[12:]}","emailVerify":"${PALAVRAFAKE}@fastshopteste.com","passwordVerify":"${PASSWORDFAKE}","zipCode":"07110040","streetName":"R R MARIA DE CASTRO MESQUITA","number":"84","complement":"56","district":"JARDIM SAO PAULO","city":"GUARULHOS","state":"SP","housingType":"2","country":"Brasil"}
    Log                                 ${payload}

    ${resp}=	                        Post Request            FastShop       /fastshop-qa/wcs/resources/v1/customer/create       data=${payload}     headers=${headers}     
    Log to console                      ${payload}
    Log to console                      ${resp.status_code}
    Should Be Equal As Strings          ${resp.status_code}         ${resp.status_code_page}

DATA INVÁLIDA - Cadastro criar um usuario com dados inválidos
    [Arguments]                         ${resp.status_code_page}  
    ${CPF}                              FakerLibrary.cpf  
    ${NOMEFAKE}                         FakerLibrary.Name
    ${TELEFONEFAKE}                     FakerLibrary.Phone Number
    ${PALAVRAFAKE}                      FakerLibrary.Word 
    ${PASSWORDFAKE}                     FakerLibrary.Password 
    &{headers}=	                        Create Dictionary	Connection=Connection       User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36       Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp      
    ${payload}=                         Convert To Json     {"document":"${CPF[0:3]}${CPF[4:7]}${CPF[8:11]}${CPF[12:]}","name":"${NOMEFAKE}","birthday":"0000-06-03","gender":"Male","tradeName":"","companyName":"","ie":"","telephone":"(11) 987488195","cellphone":"(11) 987488195","email":"${PALAVRAFAKE}@fastshopteste.com","password":"${PASSWORDFAKE}","receiveEmailFast":true,"receiveSMSFast":true,"receiveEmailOthers":true,"cpf":"${CPF[0:3]}${CPF[4:7]}${CPF[8:11]}${CPF[12:]}","emailVerify":"${PALAVRAFAKE}@fastshopteste.com","passwordVerify":"${PASSWORDFAKE}","zipCode":"07110040","streetName":"R R MARIA DE CASTRO MESQUITA","number":"84","complement":"56","district":"JARDIM SAO PAULO","city":"GUARULHOS","state":"SP","housingType":"2","country":"Brasil"}
    Log                                 ${payload}

    ${resp}=	                        Post Request            FastShop       /fastshop-qa/wcs/resources/v1/customer/create       data=${payload}     headers=${headers}     
    Log to console                      ${payload}
    Log to console                      ${resp.status_code}
    Should Be Equal As Strings          ${resp.status_code}         ${resp.status_code_page}

TELEFONE INCOMPLETO - Cadastro criar um usuario com dados inválidos
    [Arguments]                         ${resp.status_code_page}  
    ${CPF}                              FakerLibrary.cpf  
    ${NOMEFAKE}                         FakerLibrary.Name
    ${TELEFONEFAKE}                     FakerLibrary.Phone Number
    ${PALAVRAFAKE}                      FakerLibrary.Word 
    ${PASSWORDFAKE}                     FakerLibrary.Password 
    &{headers}=	                        Create Dictionary	Connection=Connection       User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36       Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp      
    ${payload}=                         Convert To Json     {"document":"${CPF[0:3]}${CPF[4:7]}${CPF[8:11]}${CPF[12:]}","name":"${NOMEFAKE}","birthday":"1985-06-03","gender":"Male","tradeName":"","companyName":"","ie":"","telephone":"(11) 9874","cellphone":"(11) 9874","email":"${PALAVRAFAKE}@fastshopteste.com","password":"${PASSWORDFAKE}","receiveEmailFast":true,"receiveSMSFast":true,"receiveEmailOthers":true,"cpf":"${CPF[0:3]}${CPF[4:7]}${CPF[8:11]}${CPF[12:]}","emailVerify":"${PALAVRAFAKE}@fastshopteste.com","passwordVerify":"${PASSWORDFAKE}","zipCode":"07110040","streetName":"R R MARIA DE CASTRO MESQUITA","number":"84","complement":"56","district":"JARDIM SAO PAULO","city":"GUARULHOS","state":"SP","housingType":"2","country":"Brasil"}
    Log                                 ${payload}

    ${resp}=	                        Post Request            FastShop       /fastshop-qa/wcs/resources/v1/customer/create       data=${payload}     headers=${headers}     
    Log to console                      ${payload}
    Log to console                      ${resp.status_code}
    Should Be Equal As Strings          ${resp.status_code}         ${resp.status_code_page}

E-MAIL INVÁLIDO - Cadastro criar um usuario com dados inválidos
    [Arguments]                         ${resp.status_code_page}  
    ${CPF}                              FakerLibrary.cpf  
    ${NOMEFAKE}                         FakerLibrary.Name
    ${TELEFONEFAKE}                     FakerLibrary.Phone Number
    ${PALAVRAFAKE}                      FakerLibrary.Word 
    ${PASSWORDFAKE}                     FakerLibrary.Password 
    &{headers}=	                        Create Dictionary	Connection=Connection       User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36       Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp      
    ${payload}=                         Convert To Json     {"document":"${CPF[0:3]}${CPF[4:7]}${CPF[8:11]}${CPF[12:]}","name":"${NOMEFAKE}","birthday":"1985-06-03","gender":"Male","tradeName":"","companyName":"","ie":"","telephone":"(11) 987488174","cellphone":"(11) 987488174","email":"${PALAVRAFAKE}fastshopteste.com","password":"${PASSWORDFAKE}","receiveEmailFast":true,"receiveSMSFast":true,"receiveEmailOthers":true,"cpf":"${CPF[0:3]}${CPF[4:7]}${CPF[8:11]}${CPF[12:]}","emailVerify":"${PALAVRAFAKE}fastshopteste.com","passwordVerify":"${PASSWORDFAKE}","zipCode":"07110040","streetName":"R R MARIA DE CASTRO MESQUITA","number":"84","complement":"56","district":"JARDIM SAO PAULO","city":"GUARULHOS","state":"SP","housingType":"2","country":"Brasil"}
    Log                                 ${payload}

    ${resp}=	                        Post Request            FastShop       /fastshop-qa/wcs/resources/v1/customer/create       data=${payload}     headers=${headers}     
    Log to console                      ${payload}
    Log to console                      ${resp.status_code}
    Should Be Equal As Strings          ${resp.status_code}         ${resp.status_code_page}

SENHA MENOR QUE 8 CARACTERES - Cadastro criar um usuario com dados inválidos
    [Arguments]                         ${resp.status_code_page}  
    ${CPF}                              FakerLibrary.cpf  
    ${NOMEFAKE}                         FakerLibrary.Name
    ${TELEFONEFAKE}                     FakerLibrary.Phone Number
    ${PALAVRAFAKE}                      FakerLibrary.Word 
    ${PASSWORDFAKE}                     FakerLibrary.Password 
    &{headers}=	                        Create Dictionary	Connection=Connection       User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36       Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp      
    ${payload}=                         Convert To Json     {"document":"${CPF[0:3]}${CPF[4:7]}${CPF[8:11]}${CPF[12:]}","name":"${NOMEFAKE}","birthday":"1985-06-03","gender":"Male","tradeName":"","companyName":"","ie":"","telephone":"(11) 987488195","cellphone":"(11) 987488195","email":"${PALAVRAFAKE}@fastshopteste.com","password":"123","receiveEmailFast":true,"receiveSMSFast":true,"receiveEmailOthers":true,"cpf":"${CPF[0:3]}${CPF[4:7]}${CPF[8:11]}${CPF[12:]}","emailVerify":"${PALAVRAFAKE}@fastshopteste.com","passwordVerify":"123","zipCode":"07110040","streetName":"R R MARIA DE CASTRO MESQUITA","number":"84","complement":"56","district":"JARDIM SAO PAULO","city":"GUARULHOS","state":"SP","housingType":"2","country":"Brasil"}
    Log                                 ${payload}

    ${resp}=	                        Post Request            FastShop       /fastshop-qa/wcs/resources/v1/customer/create       data=${payload}     headers=${headers}     
    Log to console                      ${payload}
    Log to console                      ${resp.status_code}
    Should Be Equal As Strings          ${resp.status_code}         ${resp.status_code_page}

CEP INCOMPLETO - Cadastro criar um usuario com dados inválidos
    [Arguments]                         ${resp.status_code_page}  
    ${CPF}                              FakerLibrary.cpf  
    ${NOMEFAKE}                         FakerLibrary.Name
    ${TELEFONEFAKE}                     FakerLibrary.Phone Number
    ${PALAVRAFAKE}                      FakerLibrary.Word 
    ${PASSWORDFAKE}                     FakerLibrary.Password 
    &{headers}=	                        Create Dictionary	Connection=Connection       User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36       Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp      
    ${payload}=                         Convert To Json     {"document":"${CPF[0:3]}${CPF[4:7]}${CPF[8:11]}${CPF[12:]}","name":"${NOMEFAKE}","birthday":"1985-06-03","gender":"Male","tradeName":"","companyName":"","ie":"","telephone":"(11) 987488195","cellphone":"(11) 987488195","email":"${PALAVRAFAKE}@fastshopteste.com","password":"${PASSWORDFAKE}","receiveEmailFast":true,"receiveSMSFast":true,"receiveEmailOthers":true,"cpf":"${CPF[0:3]}${CPF[4:7]}${CPF[8:11]}${CPF[12:]}","emailVerify":"${PALAVRAFAKE}@fastshopteste.com","passwordVerify":"${PASSWORDFAKE}","zipCode":"071","streetName":"R R MARIA DE CASTRO MESQUITA","number":"84","complement":"56","district":"JARDIM SAO PAULO","city":"GUARULHOS","state":"SP","housingType":"2","country":"Brasil"}
    Log                                 ${payload}

    ${resp}=	                        Post Request            FastShop       /fastshop-qa/wcs/resources/v1/customer/create       data=${payload}     headers=${headers}     
    Log to console                      ${payload}
    Log to console                      ${resp.status_code}
    Should Be Equal As Strings          ${resp.status_code}         ${resp.status_code_page}

CEP INVÁLIDO - Cadastro criar um usuario com dados inválidos
    [Arguments]                         ${resp.status_code_page}  
    ${CPF}                              FakerLibrary.cpf  
    ${NOMEFAKE}                         FakerLibrary.Name
    ${TELEFONEFAKE}                     FakerLibrary.Phone Number
    ${PALAVRAFAKE}                      FakerLibrary.Word 
    ${PASSWORDFAKE}                     FakerLibrary.Password 
    &{headers}=	                        Create Dictionary	Connection=Connection       User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36       Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp      
    ${payload}=                         Convert To Json     {"document":"${CPF[0:3]}${CPF[4:7]}${CPF[8:11]}${CPF[12:]}","name":"${NOMEFAKE}","birthday":"1985-06-03","gender":"Male","tradeName":"","companyName":"","ie":"","telephone":"(11) 987488195","cellphone":"(11) 987488195","email":"${PALAVRAFAKE}@fastshopteste.com","password":"${PASSWORDFAKE}","receiveEmailFast":true,"receiveSMSFast":true,"receiveEmailOthers":true,"cpf":"${CPF[0:3]}${CPF[4:7]}${CPF[8:11]}${CPF[12:]}","emailVerify":"${PALAVRAFAKE}@fastshopteste.com","passwordVerify":"${PASSWORDFAKE}","zipCode":"00000000","streetName":"R R MARIA DE CASTRO MESQUITA","number":"84","complement":"56","district":"JARDIM SAO PAULO","city":"GUARULHOS","state":"SP","housingType":"2","country":"Brasil"}
    Log                                 ${payload}

    ${resp}=	                        Post Request            FastShop       /fastshop-qa/wcs/resources/v1/customer/create       data=${payload}     headers=${headers}     
    Log to console                      ${payload}
    Log to console                      ${resp.status_code}
    Should Be Equal As Strings          ${resp.status_code}         ${resp.status_code_page}


COMPLEMENTO MAIOR QUE 20 CARACTERES - Cadastro criar um usuario com dados inválidos
    [Arguments]                         ${resp.status_code_page}  
    ${CPF}                              FakerLibrary.cpf  
    ${NOMEFAKE}                         FakerLibrary.Name
    ${TELEFONEFAKE}                     FakerLibrary.Phone Number
    ${PALAVRAFAKE}                      FakerLibrary.Word 
    ${PASSWORDFAKE}                     FakerLibrary.Password 
    &{headers}=	                        Create Dictionary	Connection=Connection       User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36       Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp      
    ${payload}=                         Convert To Json     {"document":"${CPF[0:3]}${CPF[4:7]}${CPF[8:11]}${CPF[12:]}","name":"${NOMEFAKE}","birthday":"1985-06-03","gender":"Male","tradeName":"","companyName":"","ie":"","telephone":"(11) 987488195","cellphone":"(11) 987488195","email":"${PALAVRAFAKE}@fastshopteste.com","password":"${PASSWORDFAKE}","receiveEmailFast":true,"receiveSMSFast":true,"receiveEmailOthers":true,"cpf":"${CPF[0:3]}${CPF[4:7]}${CPF[8:11]}${CPF[12:]}","emailVerify":"${PALAVRAFAKE}@fastshopteste.com","passwordVerify":"${PASSWORDFAKE}","zipCode":"07110040","streetName":"R R MARIA DE CASTRO MESQUITA","number":"84","complement":"1234567891011121314151617181920","district":"JARDIM SAO PAULO","city":"GUARULHOS","state":"SP","housingType":"2","country":"Brasil"}
    Log                                 ${payload}

    ${resp}=	                        Post Request            FastShop       /fastshop-qa/wcs/resources/v1/customer/create       data=${payload}     headers=${headers}     
    Log to console                      ${payload}
    Log to console                      ${resp.status_code}
    Should Be Equal As Strings          ${resp.status_code}         ${resp.status_code_page}

CEP MAIOR QUE 8 CARACTERES - Cadastro criar um usuario com dados inválidos
    [Arguments]                         ${resp.status_code_page}  
    ${CPF}                              FakerLibrary.cpf  
    ${NOMEFAKE}                         FakerLibrary.Name
    ${TELEFONEFAKE}                     FakerLibrary.Phone Number
    ${PALAVRAFAKE}                      FakerLibrary.Word 
    ${PASSWORDFAKE}                     FakerLibrary.Password 
    &{headers}=	                        Create Dictionary	Connection=Connection       User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36       Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp      
    ${payload}=                         Convert To Json     {"document":"${CPF[0:3]}${CPF[4:7]}${CPF[8:11]}${CPF[12:]}","name":"${NOMEFAKE}","birthday":"1985-06-03","gender":"Male","tradeName":"","companyName":"","ie":"","telephone":"(11) 987488195","cellphone":"(11) 987488195","email":"${PALAVRAFAKE}@fastshopteste.com","password":"${PASSWORDFAKE}","receiveEmailFast":true,"receiveSMSFast":true,"receiveEmailOthers":true,"cpf":"${CPF[0:3]}${CPF[4:7]}${CPF[8:11]}${CPF[12:]}","emailVerify":"${PALAVRAFAKE}@fastshopteste.com","passwordVerify":"${PASSWORDFAKE}","zipCode":"0000000000000000","streetName":"R R MARIA DE CASTRO MESQUITA","number":"84","complement":"56","district":"JARDIM SAO PAULO","city":"GUARULHOS","state":"SP","housingType":"2","country":"Brasil"}
    Log                                 ${payload}

    ${resp}=	                        Post Request            FastShop       /fastshop-qa/wcs/resources/v1/customer/create       data=${payload}     headers=${headers}     
    Log to console                      ${payload}
    Log to console                      ${resp.status_code}
    Should Be Equal As Strings          ${resp.status_code}         ${resp.status_code_page}


Cadastro criar uma empresa CNPJ
    [Arguments]                         ${CNPJ}                     ${resp.status_code_page}  
    ${CPF}                              FakerLibrary.cpf  
    ${NOMEFAKE}                         FakerLibrary.Name
    ${TELEFONEFAKE}                     FakerLibrary.Phone Number
    ${PALAVRAFAKE}                      FakerLibrary.Word 
    ${PASSWORDFAKE}                     FakerLibrary.Password 
    &{headers}=	                        Create Dictionary	Connection=Connection       User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36       Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp      
    ${payload}=                         Convert To Json     {"document":"${CNPJ}","name":"${NOMEFAKE}","birthday":"","gender":"","tradeName":"${PALAVRAFAKE}","companyName":"${PALAVRAFAKE}","ie":"andre","telephone":"(11) 98748-8174","cellphone":"(11) 98748-8174","email":"${PALAVRAFAKE}@fastshopteste.com","password":"${PASSWORDFAKE}","receiveEmailFast":true,"receiveSMSFast":true,"receiveEmailOthers":true,"cnpj":"${CNPJ}","emailVerify":"${PALAVRAFAKE}@fastshopteste.com","passwordVerify":"${PASSWORDFAKE}","zipCode":"07110040","streetName":"R R MARIA DE CASTRO MESQUITA","number":"55","complement":"aa","district":"JARDIM SAO PAULO","city":"GUARULHOS","state":"SP","housingType":"1","country":"Brasil"}
    Log                                 ${payload}

    ${resp}=	                        Post Request            FastShop       /fastshop-qa/wcs/resources/v1/customer/create       data=${payload}     headers=${headers}     
    Log to console                      ${payload}
    Log to console                      ${resp.status_code}
    Should Be Equal As Strings          ${resp.status_code}     ${resp.status_code_page}

   
#HOME
Acessar a home da Fastshop e realizar uma busca de um produto
    [Arguments]                         ${busca_produto}        ${resp.status_code_page}  
    Create Session	                    FastShop	            https://cfagfast.fastshop.com.br/
    &{headers}=	                        Create Dictionary	    Connection=Connection               authority=cfagfast.fastshop.com.br               accept=application/json, text/plain, */*"   User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36     origin=http://webapp2-qa.fastshop.com.br    sec-fetch-site=cross-site   sec-fetch-mode=cors sec-fetch-dest=empty    referer=http://webapp2-qa.fastshop.com.br/web/s/SGQN55Q60RAB    accept-language=pt-BR,pt;q=0.9,en;q=0.8,en-US;q=0.7 cache-control=no-cache
    ${resp}=	                        Get Request             FastShop                            products?products=${busca_produto}_PRD           headers=${headers}     
    Log to console                      ${resp}
    Log to console                      ${resp.status_code}
    Should Be Equal As Strings          ${resp.status_code}     ${resp.status_code_page}

#Acessar a home da Fastshop logar e realizar uma busca de um produto


#Cart
Efetuar uma compra com pagamento via boleto
   
    [Arguments]                         ${cpf_api}          ${senha_api}            ${busca_produto}        ${resp.status_code_page} 
                          
    ${payload}=                         Convert To Json         {"document":"${cpf_api}","password":"${senha_api}"}
    Log                                 ${payload}
    ${resp}=                            Post Session            ${payload}
    Should Be Equal As Strings          ${resp.status_code}     201
    Log to console                      "#0-Log com sucsso" >>>>>>>${resp.status_code}   
    ${WCToken}=                         Convert To String       ${resp.json()['WCToken']}
    Log                                 ${payload}
    ${WCTrustedToken}=                  Convert To String       ${resp.json()['WCTrustedToken']}
    Log                                 ${WCTrustedToken}
    ${userID}=                          Convert To String       ${resp.json()['userID']}
    Log                                 ${userID}
    &{headers}=	                        Create Dictionary	    Connection=keep-alive       WCTrustedToken=${WCTrustedToken}        User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36    userAgent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36   WCToken=${WCToken}      Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp       storeId=10151      ip=172.84.04                                                                    
    ${payload}=                         Convert To Json         {"sku":"${busca_produto}","quantity":1}
    Log                                 ${payload}
    #1-ADICIONAR PRODUTO NO CARRINHO FASTSHOP
    ${resp}=	                        Post Request            FastShop                    /v1/checkout/cart/items                     data=${payload}     headers=${headers}     
    Should Be Equal As Strings          ${resp.status_code}     ${resp.status_code_page}
    Log to console                      "#1-ADICIONAR PRODUTO NO CARRINHO FASTSHOP >>>>>>>"${resp.status_code}  
    #2-DELETAR PRODUTO NO CARRINHO FASTSHOP
    ${resp}=	                        Delete Request            FastShop                    /fastshop-qa/checkout/cart/${busca_produto}                     data=${payload}     headers=${headers}     
    Should Be Equal As Strings          ${resp.status_code}     ${resp.status_code_page}
    Log to console                      "#2-DELETAR PRODUTO NO CARRINHO FASTSHOP >>>>>>>"${resp.status_code}  
    #3-ADICIONAR PRODUTO NO CARRINHO FASTSHOP
    ${resp}=	                        Post Request            FastShop                    /v1/checkout/cart/items                     data=${payload}     headers=${headers}     
    Should Be Equal As Strings          ${resp.status_code}     ${resp.status_code_page}
    Log to console                      "#3-ADICIONAR PRODUTO NO CARRINHO FASTSHOP >>>>>>>"${resp.status_code}             
    #4-Visualizar carrinho FASTSHOP
    ${resp}=	                        Get Request             FastShop      /v1/checkout/cart/self     data=${payload}                headers=${headers}
    Should Be Equal As Strings          ${resp.status_code}     200
    Log to console                      "#4-Visualizar carrinho FASTSHOP >>>>>>>"${resp.status_code}   
    #5-Alterando a quantidade no carrinho FASTSHOP
    #${payload}=                         Convert To Json        {"quantity":1,"sku":"${busca_produto}"}
    ${resp}=	                        Put Request             FastShop      /fastshop-qa/checkout/cart/     data=${payload}                headers=${headers}
    Should Be Equal As Strings          ${resp.status_code}     200
    Log to console                      "5-#Alterando a quantidade no carrinho FASTSHOP >>>>>>>"${resp.status_code}   
    #6-Inserir serviço suporte técnico
    ${payload}=                         Convert To Json        [{"quantity":1,"sku":"GY00HELPDESK","productSku":"${busca_produto}","type":"DIGISUPPORT","isClickCollect":false}]
    ${resp}=	                        Post Request            FastShop      /fastshop-qa/checkout/cart/addListItem     data=${payload}                headers=${headers}
    Should Be Equal As Strings          ${resp.status_code}     200
    Log to console                      "6-#Inserir serviço suporte técnico >>>>>>>"${resp.status_code}   
    ${payload}=                          Convert To Json         {}
    #7-Deletar serviço de suporte tecnico no carrinho FASTSHOP
    ${payload}=                         Convert To Json        [{"sku":"GY00HELPDESK"}]
    ${resp}=	                        Delete Request             FastShop      /fastshop-qa/checkout/cart/items         data=${payload}                headers=${headers}
    Should Be Equal As Strings          ${resp.status_code}     200
    Log to console                      "7-#Deletar serviço de suporte tecnico no carrinho FASTSHOP >>>>>>>"${resp.status_code}   
    #8-Inserir serviço garantia estendida
    ${payload}=                         Convert To Json        [{"quantity":1,"sku":"GXAEMWHJ2BZACN-${busca_produto}","productSku":"${busca_produto}","type":"WARRANTY","isClickCollect":false}]
    ${resp}=	                        Post Request            FastShop      /fastshop-qa/checkout/cart/addListItem     data=${payload}                headers=${headers}
    Should Be Equal As Strings          ${resp.status_code}     200
    Log to console                      "8-#Inserir serviço garantia estendida >>>>>>>"${resp.status_code}   
    ${payload}=                          Convert To Json         {}
    #9-Deletar serviço de garantia estendida carrinho FASTSHOP
    ${payload}=                         Convert To Json        [{"sku":"GXAEMWHJ2BZACN-${busca_produto}"}]
    ${resp}=	                        Delete Request         FastShop      /fastshop-qa/checkout/cart/items    data=${payload}                headers=${headers}
    Should Be Equal As Strings          ${resp.status_code}     200
    Log to console                      "9-#Deletar serviço de garantia estendida carrinho FASTSHOP >>>>>>>"${resp.status_code}   
    ${payload}=                          Convert To Json         {}
    #10-Inserir serviço de seguro
    ${payload}=                         Convert To Json        [{"quantity":1,"sku":"GSAEMWHJ2BZACN-${busca_produto}","productSku":"${busca_produto}","type":"THEFT","isClickCollect":false}]
    ${resp}=	                        Post Request            FastShop      /fastshop-qa/checkout/cart/addListItem     data=${payload}                headers=${headers}
    Should Be Equal As Strings          ${resp.status_code}     200
    Log to console                      "10-#Inserir serviço de seguro >>>>>>>"${resp.status_code}   
    ${payload}=                          Convert To Json         {}
    #11-Deletar serviço de seguro carrinho FASTSHOP
    ${payload}=                         Convert To Json        [{"sku":"GXAEMWHJ2BZACN-${busca_produto}"}]
    ${resp}=	                        Delete Request             FastShop      /fastshop-qa/checkout/cart/items    data=${payload}                headers=${headers}
    Should Be Equal As Strings          ${resp.status_code}     200
    Log to console                      "11-#Deletar serviço de seguro carrinho FASTSHOP >>>>>>>"${resp.status_code}   
    ${payload}=                          Convert To Json         {}
    #12-Quantidade no carrinho FASTSHOP
    ${resp}=	                        Get Request             FastShop      /v1/checkout/cart/summaryOfQuantity                       data=${payload}     headers=${headers}
    Should Be Equal As Strings          ${resp.status_code}     200
    Log to console                       "#12-Quantidade no carrinho FASTSHOP >>>>>>>"${resp.status_code}   
    #13-Quantidade maxima no carrinho FASTSHOP 
    ${resp}=	                        Get Request             FastShop      /v1/checkout/parametrization/findByName/QTD_ITENS_MAX     data=${payload}     headers=${headers}
    Should Be Equal As Strings          ${resp.status_code}     200
    Log to console                      "#13-Quantidade maxima no carrinho FASTSHOP >>>>>>>"${resp.status_code}   
    ${payload}=                          Convert To Json         {}
    #ENDEREÇO >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    #15-Visualizar carrinho FASTSHOP 
    ${resp}=	                        Get Request             FastShop      /v1/checkout/cart/self     data=${payload}                headers=${headers}
    Should Be Equal As Strings          ${resp.status_code}     200
    Log to console                      "#15-Visualizar carrinho FASTSHOP >>>>>>>"${resp.status_code}   
    ${payload}=                          Convert To Json         {}   
    #16-Verificar o CEP 
    ${resp}=	                        Get Request             FastShop      /fastshop-qa/wcs/resources//v1/customer/searchAddress/07110-040     data=${payload}                headers=${headers}
    Should Be Equal As Strings          ${resp.status_code}     200
    Log to console                      "#Verificar o CEP >>>>>>>"${resp.status_code}   
    ${payload}=                          Convert To Json         {}
    #17-Criar um endereço do cliente no carrinho FASTSHOP 
    ${payload}=                         Convert To Json         {"name":"Rua Maria De Castro Mesquita 84","nickName":"API-TESTE-FASTSHOP","zipCode":"07110040","streetName":"R R MARIA DE CASTRO MESQUITA","number":54,"complement":"Rua Maria De Castro ","district":"Rua Maria De Castro Mesquita 8","city":"GUARULHOS","state":"SP","telephone":"(11) 98748-8174","housingType":"1","country":"Brasil","cellphone":"(11) 98748-8174"}
    Log                                 ${payload}
    ${resp}=	                        Post Request            FastShop      /fastshop-qa/wcs/resources/v1/customer/addresses/create     data=${payload}                headers=${headers}
    Should Be Equal As Strings          ${resp.status_code}     204
    Log to console                      "#17-Criar um endereço do cliente no carrinho FASTSHOP  >>>>>>>"${resp.status_code}  
    ${payload}=                          Convert To Json         {}    
    #18-Criar um endereço do cliente no carrinho FASTSHOP 
    ${payload}=                         Convert To Json         {"name":"Rua Maria De Castro Mesquita 84","nickName":"API-TESTE-FASTSHOP","zipCode":"07110040","streetName":"R R MARIA DE CASTRO MESQUITA","number":54,"complement":"Rua Maria De Castro ","district":"Rua Maria De Castro Mesquita 8","city":"GUARULHOS","state":"SP","telephone":"(11) 98748-8174","housingType":"1","country":"Brasil","cellphone":"(11) 98748-8174"}
    Log                                 ${payload}
    ${resp}=	                        Post Request            FastShop      /fastshop-qa/wcs/resources/v1/customer/addresses/create     data=${payload}                headers=${headers}
    Should Be Equal As Strings          ${resp.status_code}     204
    Log to console                      "#18-Criar um endereço do cliente no carrinho FASTSHOP  >>>>>>>"${resp.status_code}  
    ${payload}=                          Convert To Json         {}    
    #19-Visualizar carrinho FASTSHOP 
    ${resp}=	                        Get Request             FastShop      /v1/checkout/cart/self     data=${payload}                headers=${headers}
    Should Be Equal As Strings          ${resp.status_code}     200
    Log to console                      "#19-Visualizar carrinho FASTSHOP >>>>>>>"${resp.status_code}   
    ${payload}=                          Convert To Json         {}   
    #20-Atualizar o endereço do cliente no carrinho FASTSHOP 
    ${resp}=	                        Get Request              FastShop      /fastshop-qa/wcs/resources//v1/customer/addresses        data=${payload}     headers=${headers}     
    ${addressId}=                       Convert To String        ${resp.json()['addresses']}
    Log to console                      ${addressId}
    ${addressId2}=                      cortar_addressId         ${addressId}
    Log to console                      "Numero ID endereço"     ${addressId2}
    #21-DELETAR ENDEREÇO NO CARRINHO FASTSHOP
    ${resp}=	                        Delete Request            FastShop                    /fastshop-qa/wcs/resources/v1/customer/addresses/delete/${addressId2}                     data=${payload}     headers=${headers}     
    Should Be Equal As Strings          ${resp.status_code}       400
    Log to console                      "#21-DELETAR ENDEREÇO NO CARRINHO FASTSHOP >>>>>>>"${resp.status_code}  
    #22-Atualizar o endereço do cliente no carrinho FASTSHOP 
    ${resp}=	                        Get Request              FastShop      /fastshop-qa/wcs/resources//v1/customer/addresses        data=${payload}     headers=${headers}     
    ${addressId}=                       Convert To String        ${resp.json()['addresses']}
    Log to console                      ${addressId}
    ${addressId2}=                      cortar_addressId         ${addressId}
    Log to console                      Numero ID endereço ${addressId2}
    #23-PUT Atualizar o endereço do cliente no carrinho FASTSHOP 
    ${resp}=	                        Put Request              FastShop      /fastshop-qa//checkout/cart/updateAddress/${addressId2}        data=${payload}     headers=${headers}     
    Should Be Equal As Strings          ${resp.status_code}      200
    Log to console                      "#23-PUT Atualizar o endereço do cliente no carrinho FASTSHOP   >>>>>>>"${resp.status_code}   
    ${payload}=                          Convert To Json         {}   
    #Entrega Normal, agendada ou economica >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    #24-Visualizar carrinho FASTSHOP 
    ${resp}=	                        Get Request             FastShop      /v1/checkout/cart/self     data=${payload}                headers=${headers}
    Should Be Equal As Strings          ${resp.status_code}     200
    Log to console                      "#24-Visualizar carrinho FASTSHOP >>>>>>>"${resp.status_code}   
    ${payload}=                         Convert To Json         {}
    #25-Atualizar o cep 
    ${resp}=	                        Get Request             FastShop      /fastshop-qa/wcs/resources//v1/customer/searchAddress/07110-040     data=${payload}                headers=${headers}
    Should Be Equal As Strings          ${resp.status_code}     200
    Log to console                      "#25-Atualizar o cep >>>>>>>"${resp.status_code}   
    ${payload}=                          Convert To Json         {}
    #26-Endereço do cliente no carrinho FASTSHOP 
    ${resp}=	                        Get Request             FastShop     /fastshop-qa/wcs/resources//v1/customer/addresses                       data=${payload}     headers=${headers}
    Should Be Equal As Strings          ${resp.status_code}     200
    Log to console                      "#26-Endereço do cliente no carrinho FASTSHOP >>>>>>>"${resp.status_code}  
    ${payload}=                          Convert To Json         {} 
    #27-PUT Atualizar para entrega economica FASTSHOP 
    ${payload}=                         Convert To Json        [{"deliveryCode":"FAST-01-33","shipModeName":"Economica","addressId":"${addressId2}","sku":null}]
    ${resp}=	                        Put Request              FastShop      /fastshop-qa/checkout/cart/shippingMode/            data=${payload}     headers=${headers}     
    Should Be Equal As Strings          ${resp.status_code}      200
    Log to console                      "#27-PUT Atualizar para entrega economica FASTSHOP    >>>>>>>"${resp.status_code}   
    ${payload}=                          Convert To Json         {}   
    #28-PUT Atualizar para entrega agendada FASTSHOP         
    ${datadehoje}=                      datadehoje
    ${datadehoje2}=                     Convert To String       ${datadehoje}
    ${payload}=                         Convert To Json        [{"deliveryCode":"FAST-01-33","shipModeName":"Agendada","periodDescription":"MANHA","availableDate":"${datadehoje2[0:10]}T00:00:00","addressId":"${addressId2}","sku":null}]
    ${resp}=	                        Put Request              FastShop      /fastshop-qa/checkout/cart/shippingMode/            data=${payload}     headers=${headers}     
    Should Be Equal As Strings          ${resp.status_code}      200
    Log to console                      "#28-PUT Atualizar para entrega agendada FASTSHOP    >>>>>>>"${resp.status_code}   
    ${payload}=                          Convert To Json         {} 
    #Serviços instalar >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    #29-Visualizar carrinho FASTSHOP 
    ${resp}=	                        Get Request             FastShop      /v1/checkout/cart/self     data=${payload}                headers=${headers}
    Should Be Equal As Strings          ${resp.status_code}     200
    Log to console                      "#29-Visualizar carrinho FASTSHOP >>>>>>>"${resp.status_code}   
    ${payload}=                          Convert To Json         {} 
    #30-Instalação de serviços no carrinho FASTSHOP 
    ${resp}=	                         Get Request             FastShop      /v2/installation-service/searchEligibility?skuProducts=${buscaproduto}&zipCode=02550000&platform=WEB     data=${payload}     headers=${headers}
    Should Be Equal As Strings          ${resp.status_code}      200
    Log to console                      "#30-Instalação de serviços no carrinho FASTSHOP  >>>>>>>"${resp.status_code}   
    ${payload}=                          Convert To Json         {} 
    #31-Serviços no carrinho FASTSHOP
    ${resp}=	                         Get Request             FastShop      /fastshop-qa/wcs/resources/v5/products/services/byPartNumber/${busca_produto}     data=${payload}     headers=${headers}
    Should Be Equal As Strings          ${resp.status_code}      200
    Log to console                      "#31-Serviços no carrinho FASTSHOP >>>>>>>"${resp.status_code}   
    ${payload}=                          Convert To Json         {}
    #PAGAMENTO >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    #32-OBTER FORMAS DE PAGAMENTO NO CARRINHO FASTSHOP
    ${resp}=	                        Get Request             FastShop      /fastshop-qa/checkout/cart/payments     data=${payload}     headers=${headers}
    ${paymentMethods}=                  Convert To String                       ${resp.json()['paymentMethods']}
    #Log to console                      ${paymentMethods}
    ${Value_cartao_de_credito}=         cortar_valor_cartao_de_credito                   ${paymentMethods}
    ${Value_multiplos_cartoes}=         cortar_valor_multiplos_cartao_de_credito         ${paymentMethods}
    ${Value_boleto}=                    cortar_valor_boleto                              ${paymentMethods}
    ${Value_lista_casamento}=           cortar_valor_lista_casamento                     ${paymentMethods}
    Log to console                      Valor a pagar em Multiplos Cartões de Crédito: ${Value_multiplos_cartoes}
    Log to console                      Valor a pagar no Cartão: ${Value_cartao_de_credito}
    Log to console                      Valor a pagar no Boleto: ${Value_boleto}
    Log to console                      Valor a pagar no Lista de casamento: ${Value_lista_casamento}
    Should Be Equal As Strings          ${resp.status_code}      200
    Log to console                      "#32-OBTER FORMAS DE PAGAMENTO NO CARRINHO FASTSHOP >>>>>>>"${resp.status_code}   
    ${payload}=                          Convert To Json         {}
    #33-ESCOLHER BOLETO EM FORMAS DE PAGAMENTO NO CARRINHO FASTSHOP
    ${payload}=                         Convert To Json         [{"paymentMethodId":"5007","paymentMethodName":"Boleto Bradesco","paymentTypeDescription":"Boleto","paymentTypeName":"Boleto","value":${Value_boleto},"installmentNumber":null}]
    ${resp}=	                        Post Request            FastShop           /fastshop-qa/checkout/cart/payments                     data=${payload}     headers=${headers}     
    Should Be Equal As Strings          ${resp.status_code}     ${resp.status_code_page}
    Log to console                      "#33-ESCOLHER BOLETO EM FORMAS DE PAGAMENTO NO CARRINHO FASTSHOP >>>>>>>"${resp.status_code}
    ${payload}=                          Convert To Json         {}  
    #34-ESCOLHER CARTÃO DE CRÉDITO EM FORMAS DE PAGAMENTO NO CARRINHO FASTSHOP
    ${payload}=                         Convert To Json         [{"paymentMethodId":"5010","paymentMethodName":"Cartão de Crédito Amex 15x","paymentTypeDescription":"Adicionar Cartão de Crédito","paymentTypeName":"Cartão de Crédito","cardNumber":"5161541224057189","tokenId":null,"value":null,"installmentNumber":null}]
    ${resp}=	                        Post Request            FastShop           /fastshop-qa/checkout/cart/payments                     data=${payload}     headers=${headers}     
    Should Be Equal As Strings          ${resp.status_code}     ${resp.status_code_page}
    Log to console                      "#34-ESCOLHER CARTÃO DE CRÉDITO EM FORMAS DE PAGAMENTO NO CARRINHO FASTSHOP >>>>>>>"${resp.status_code}
    ${payload}=                          Convert To Json         {}  
    #35-ESCOLHER MULTIPLOS CARTÕES DE CRÉDITO EM FORMAS DE PAGAMENTO NO CARRINHO FASTSHOP
    ${payload}=                         Convert To Json         [{"paymentMethodId":"5004","paymentMethodName":"Cartão de Crédito somente 1x","paymentTypeDescription":"em 1x no Cartão","paymentTypeName":"Cartão de Crédito somente 1x","installmentNumber":1,"value":10}]
    ${resp}=	                        Post Request            FastShop           /fastshop-qa/checkout/cart/payments                     data=${payload}     headers=${headers}     
    Should Be Equal As Strings          ${resp.status_code}     ${resp.status_code_page}
    Log to console                      "#35-ESCOLHER MULTIPLOS CARTÕES DE CRÉDITO EM FORMAS DE PAGAMENTO NO CARRINHO FASTSHOP  >>>>>>>"${resp.status_code}
    ${payload}=                          Convert To Json         {}      
    #36-ESCOLHER LISTA DE CASAMENTO EM FORMAS DE PAGAMENTO NO CARRINHO FASTSHOP
    ${payload}=                         Convert To Json         [{"paymentMethodId":"5108","paymentMethodName":"Crédito de Lista de Casamento","paymentTypeDescription":"Crédito de Lista de Casamento","paymentTypeName":"Crédito de Lista de Casamento","value":${Value_lista_casamento},"installmentNumber":null}]
    ${resp}=	                        Post Request            FastShop           /fastshop-qa/checkout/cart/payments                     data=${payload}     headers=${headers}     
    Should Be Equal As Strings          ${resp.status_code}     ${resp.status_code_page}
    Log to console                      "#36-ESCOLHER LISTA DE CASAMENTO EM FORMAS DE PAGAMENTO NO CARRINHO FASTSHOP >>>>>>>"${resp.status_code}
    ${payload}=                          Convert To Json         {}  
    #36-PAGAR POR BOLETO
    ${payload}=                         Convert To Json         [{"paymentMethodId":"5007","paymentMethodName":"Boleto Bradesco","paymentTypeDescription":"Boleto","paymentTypeName":"Boleto","value":${Value_boleto},"installmentNumber":null,"cardNumber":null,"expirationMonth":null,"expirationYear":null,"securityCode":null,"creditCardOwner":null,"document":null,"registeredCard":null,"tokenId":null}]
    ${resp}=	                        Post Request            FastShop           /fastshop-qa/checkout/cart/sendPayments                     data=${payload}     headers=${headers}     
    Should Be Equal As Strings          ${resp.status_code}     ${resp.status_code_page}
    Log to console                      "#36-PAGAR POR BOLETO >>>>>>>"${resp.status_code}
    ${payload}=                         Convert To Json         {}  

Verificar preço em produção ${busca_produto} ${priceTag} 
    &{headers}=	                        Create Dictionary	    Connection=keep-alive               User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36    userAgent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36         Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp       storeId=10151      ip=172.84.04                                                                    
    ${payload}=                         Convert To Json         {"sku":"${busca_produto}","quantity":1}
    Log                                 ${payload}
    
    #GET PRODUTO
    ${resp}=	                        Get Request             FastShop      /wcs/resources/v2/products/byPartNumber/${busca_produto}     data=${payload}                headers=${headers}
    Should Be Equal As Strings          ${resp.status_code}                  200
    Should Be Equal As Strings          ${resp.json()['priceTag']}           ${priceTag}
    Log to console                      "######################################################################################################################################################################"*PREÇO - PRODUÇÃO: ${resp.json()['priceTag']} PREÇO NA PLANILHA: ${priceTag} - SKU: ${busca_produto} FASTSHOP ###################################################################################################################################################################### 
    Log                                 "######################################################################################################################################################################"*PREÇO - PRODUÇÃO: ${resp.json()['priceTag']} PREÇO NA PLANILHA: ${priceTag} - SKU: ${busca_produto} FASTSHOP ###################################################################################################################################################################### 


E altero a quantidade dos produtos via api ${} ${} ${busca_produto} a quantidade no carrinho FASTSHOP
    Create Session	                    FastShop	            https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json         {"document":"${cpf}","password":"${senha}"}
    Log                                 ${payload}
    ${resp}=                            Post Session            ${payload}
    Should Be Equal As Strings          ${resp.status_code}     201
    Log to console                      "#0-Log com sucsso" >>>>>>>${resp.status_code}   
    ${WCToken}=                         Convert To String       ${resp.json()['WCToken']}
    Log                                 ${payload}
    ${WCTrustedToken}=                  Convert To String       ${resp.json()['WCTrustedToken']}
    Log                                 ${WCTrustedToken}
    ${userID}=                          Convert To String       ${resp.json()['userID']}
    Log                                 ${userID}
    &{headers}=	                        Create Dictionary	    Connection=keep-alive       WCTrustedToken=${WCTrustedToken}        User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36    userAgent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36   WCToken=${WCToken}      Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp       storeId=10151      ip=172.84.04                                                                    
    ${payload}=                         Convert To Json         {"sku":"${busca_produto}","quantity":1}
    Log                                 ${payload}
    
    
    #${payload}=                         Convert To Json        {"quantity":1,"sku":"${busca_produto}"}
    ${resp}=	                        Put Request             FastShop      /fastshop-qa/checkout/cart/     data=${payload}                headers=${headers}
    Should Be Equal As Strings          ${resp.status_code}     200
    Log to console                      "5-#Alterando a quantidade no carrinho FASTSHOP >>>>>>>"${resp.status_code}   


E Deleto a quantidade dos produtos via api ${} ${} ${busca_produto} a quantidade no carrinho FASTSHOP
    Create Session	                    FastShop	            ${BASE_URL_API}
    ${payload}=                         Convert To Json         {"document":"${cpf}","password":"${senha}"}
    Log                                 ${payload}
    ${resp}=                            Post Session            ${payload}
    Should Be Equal As Strings          ${resp.status_code}     201
    Log to console                      "#0-Log com sucsso" >>>>>>>${resp.status_code}   
    ${WCToken}=                         Convert To String       ${resp.json()['WCToken']}
    Log                                 ${payload}
    ${WCTrustedToken}=                  Convert To String       ${resp.json()['WCTrustedToken']}
    Log                                 ${WCTrustedToken}
    ${userID}=                          Convert To String       ${resp.json()['userID']}
    Log                                 ${userID}
    &{headers}=	                        Create Dictionary	    Connection=keep-alive       WCTrustedToken=${WCTrustedToken}        User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36    userAgent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36   WCToken=${WCToken}      Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp       storeId=10151      ip=172.84.04                                                                    
    ${payload}=                         Convert To Json         {"sku":"${busca_produto}","quantity":1}

    ${resp}=	                        Delete Request            FastShop                    /fastshop-qa/checkout/cart/${busca_produto}                     data=${payload}     headers=${headers}     
    Should Be Equal As Strings          ${resp.status_code}     ${resp.status_code_page}
    Log to console                      "#2-DELETAR PRODUTO NO CARRINHO FASTSHOP >>>>>>>"${resp.status_code}  

#CUPONS
Inserir um cupum e verificar o seu status code
    [Arguments]                         ${busca_produto}                  ${cupons}                    ${status}          ${mensagem_recebida}       
    Log to console                      TESTE COM O CUPOM: ${cupons} 
    &{headers}=	                        Create Dictionary	    Connection=keep-alive               User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36    userAgent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36         Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp       storeId=10151      ip=172.84.04                                                                    
    ${payload}=                         Convert To Json         { "channel": "webapp", "cpf": "19644209036", "houseNumber": "12", "store": "fastshop", "zipCode": "06850120", "partner": null, "couponCodes": [ "${cupons}" ], "items": [ { "freightValue": 9.90, "offerPrice": 3679.13, "taxFee": null, "marketPlaceText": null, "sku": "${busca_produto}", "typeDelivery": "Normal", "quantity": 1, "fulfillmentCenter": "14", "street": null, "campaign": null, "promotions": [ { "tag": "Aniversário Fastshop", "methodPayment": null, "description": null, "sku": null, "value": 3679.13, "paymentTypeName": null, "paymentMethodName": null, "isPromotionApplied": true } ], "isClickCollect": false, "isM1": false, "isGift": false, "isMarketPlace": false, "isPlan": false, "isService": false } ], "selectedPayments": [], "selectedCard": { "bin": 555566, "maxPayments": 0, "paymentMethodId": null, "paymentMethodName": null, "paymentTypeName": null, "value": null, "installmentNumber": null } }
    Log                                 ${payload}
    
    #GET PRODUTO
    ${resp}=	                        Post Request             FastShop      /price-management/api/v1/price-promotion/price-cart?     data=${payload}                headers=${headers}
    Should Be Equal As Strings          ${resp.status_code}                    ${status}
    ${description}=                     Convert To String                      ${resp.json()}
    Should Contain	                    ${description}                         ${mensagem_recebida}
    Log to console                      Mensagem recebida está na resposta: "${mensagem_recebida}"
    Log to console                      Status: "${resp.status_code}"
    #Should Be Equal As Strings          ${resp.json()['priceTag']}           ${priceTag}
    #Log to console                      "*PREÇO - PRODUÇÃO: ${resp.json()['priceTag']} PREÇO NA PLANILHA: ${priceTag} - SKU: ${busca_produto} FASTSHOP 
    #Log                                 "*PREÇO - PRODUÇÃO: ${resp.json()['priceTag']} PREÇO NA PLANILHA: ${priceTag} - SKU: ${busca_produto} FASTSHOP 

Deletar um produto do carrinho FastShop ${cpf} ${senha} ${busca_produto}                  
    
    ${payload}=                         Convert To Json     {"document":"${cpf}","password":"${senha}"}
    Log                                 ${payload}
    ${resp}=                            Post Session        ${payload}
    Should Be Equal As Strings          ${resp.status_code}     201
    ${WCToken}=                         Convert To String   ${resp.json()['WCToken']}
    
    Log                                 ${payload}
    ${WCTrustedToken}=                  Convert To String   ${resp.json()['WCTrustedToken']}

    Log                                 ${WCTrustedToken}
    ${userID}=                          Convert To String   ${resp.json()['userID']}

    Log                                 ${userID}
    &{headers}=	                        Create Dictionary	storeId=${userID}       WCToken=${WCToken}               WCTrustedToken=${WCTrustedToken}        Connection=Connection       User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36       Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp      
    ${resp}=	                          Delete Request          FastShop      /v1/checkout/cart/${busca_produto}    data=${payload}     headers=${headers}     
    Log to console                      ${resp}        
 

 Aumentar estoque dos produtos Fastshop                  
    [Arguments]                         ${sku_produto}     ${quantidade_produto} 
    
    ${d}=    get time
    log      {d}       
   
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json     {"isClickCollect":null,"pickupStore":null,"dateTime":"${d}","SKU":"${sku_produto}","DistributionCenterID":"33","Quantity":"${quantidade_produto}","JustInTime":"N","Datetime":"${d}","FulfillmentCenter":null}
    Log                                 ${payload}

    &{headers}=	                        Create Dictionary	 Connection=Connection       User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36       Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1      
    ${resp}=	                        Post Request         FastShop      /fastshop-qa/incrementalstock    data=${payload}     headers=${headers}     
    Log to console                      ${resp}  
    Log to console                      ${resp.reason}
    Log to console                      Data e Hora certa da adição ao estoque: ${d}  
    Log to console                      Quantidade total: ${quantidade_produto} 
    Log to console                      ${resp.content}
    Should Be Equal As Strings          ${resp.status_code}     200







#Click Collect
### Verificar Estado e Cidade
Verificar se o produto está disponibel em um estado cidade
    [Arguments]                         ${SKU_AMBIENTE_API}     ${ESTADO_API}    ${CODIGO_CIDADE_API}
    Create Session	                    FastShop	            https://apiqa.fastshop.com.br     
    ${payload}=                         Convert To Json         {}
    &{headers}=	                        Create Dictionary	    Content-Type=application/json

    ${resp}=	                        Get Request             FastShop      /fastshop-qa/v1/clickcollect/inventory/availabilityByState?sku=${SKU_AMBIENTE_API}&State=${ESTADO_API}&city=${CODIGO_CIDADE_API}     data=${payload}     headers=${headers}
    [return]                            ${resp}
    Log                                 ${resp} 
    Should Be Equal As Strings          ${resp.status_code}     200


#Click Collect
### Compra completa














### Compra completa
Click Collect selecionando Estado e Cidade Api
    [Arguments]                         ${CPF_AMBIENTE}    ${SENHA_AMBIENTE}    ${SKU_AMBIENTE_API}    ${ESTADO_API}    ${CODIGO_CIDADE_API}    ${BRANCHID}    ${BRANCHNAME}    ${BRANCHADRESS}    ${VALORDOSKU}    ${CARTAODECREDITOCLICKCOLLECT}    ${MESCARTAODECREDITOCLICKCOLLECT}    ${ANOCARTAODECREDITOCLICKCOLLECT}    ${CVVCARTAODECREDITOCLICKCOLLECT}
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json     {"document":"${CPF_AMBIENTE}","password":"${SENHA_AMBIENTE}"}
    Log                                 ${payload}

    &{headers}=	                        Create Dictionary	Content-Type=application/json

    ${resp}=	                        Post Request     FastShop      /fastshop-qa/wcs/resources/v1/auth/login     data=${payload}     headers=${headers}
    [return]                            ${resp}
    Log                                 ${payload} 
    Log                                 ${headers}
    Log                                 ${resp} 
    Should Be Equal As Strings          ${resp.status_code}     201
#Retirar Tokens
    ${WCToken}=                         Convert To String   ${resp.json()['WCToken']}
    Log                                 ${payload}
    ${WCTrustedToken}=                  Convert To String   ${resp.json()['WCTrustedToken']}
    Log                                 ${WCTrustedToken}
    ${userID}=                          Convert To String   ${resp.json()['userID']}
#Criar novos Headers e Body
    &{headers}=	                        Create Dictionary	    storeId=10151       WCToken=${WCToken}       WCTrustedToken=${WCTrustedToken}           Connection=Connection       User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36       Content-Type=application/json       Accept=application/json, text/plain, */*        checkout-api-version=v1     channel=webapp      
#TESTE 1 - Entrar na PDP do produto via API
    Log to console                      TESTE 1 - Entrar na PDP do produto via API
    ${payload}=                         Convert To Json         {}
    ${resp}=	                        Get Request             FastShop    /fastshop-qa/wcs/resources/v1/categories/breadcrumb/${SKU_AMBIENTE_API}     data=${payload}     headers=${headers}     
    Should Be Equal As Strings          ${resp.status_code}     200
    Log to console                      TESTE 1 OK - Entrar na PDP do produto via API
#TESTE 2 - Selecionar Estado de São Paulo para Click Collect
    Log to console                      TESTE 2 - Selecionar Estado de São Paulo para Click Collect
    ${payload}=                         Convert To Json         {}
    ${resp}=	                        Get Request             FastShop    /fastshop-qa/wcs/resources//v1/geonode/city/90     data=${payload}     headers=${headers}     
    Should Be Equal As Strings          ${resp.status_code}     200
    Log to console                      TESTE 2 OK - Selecionar Estado de São Paulo para Click Collect
    Log to console                      TESTE 2 - Verificar se existe estoque do produto na cidade selecionada
    ${payload}=                         Convert To Json         {}
    ${resp}=	                        Get Request             FastShop    /fastshop-qa/v1/clickcollect/inventory/availabilityByState?sku=${SKU_AMBIENTE_API}&State=${ESTADO_API}&city=${CODIGO_CIDADE_API}    data=${payload}     headers=${headers}     
    Should Be Equal As Strings          ${resp.status_code}     200
    Log to console                      TESTE 2 OK - Verificar se existe estoque do produto na cidade selecionada
#TESTE 3 - Escolher loja para retirar o produto
    Log to console                      TESTE 3 - Selecionar Estado de São Paulo para Click Collect
    ${payload}=                         Convert To Json          {"quantity":1,"sku":"${SKU_AMBIENTE_API}","isClickCollect":true,"pickupStore":{"name":"Solicite documento e confira com a Nota Fiscal","document":"11111111111","branchId":"${BRANCHID}","branchName":"${BRANCHNAME}","branchAddress":"${BRANCHADRESS}"}}
    ${resp}=	                        Post Request             FastShop    /fastshop-qa/checkout/cart/items     data=${payload}     headers=${headers}     
    Should Be Equal As Strings          ${resp.status_code}      200
    Log to console                      TESTE 3 OK - Selecionar Estado de São Paulo para Click Collect
#TESTE 4 -Escolher forma de pagamento
    Log to console                      TESTE 4 - Escolher forma de pagamento: Verificar Quantidade
    ${payload}=                         Convert To Json         {}
    ${resp}=	                        Get Request             FastShop     /fastshop-qa/checkout/cart/summaryOfQuantity     data=${payload}     headers=${headers}     
    Should Be Equal As Strings          ${resp.status_code}     200
    Log to console                      TESTE 4 OK - Escolher forma de pagamento: erificar Quantidade
    
    Log to console                      TESTE 4 - Escolher forma de pagamento: Carrinho Self
    ${payload}=                         Convert To Json         {}
    ${resp}=	                        Get Request             FastShop     /fastshop-qa/checkout/cart/self                  data=${payload}     headers=${headers}     
    Should Be Equal As Strings          ${resp.status_code}     200
    Log to console                      TESTE 4 OK - Carrinho Self
    
    Log to console                      TESTE 4 - Escolher forma de pagamento: Pagamento no carrinho
    ${payload}=                         Convert To Json         {}
    ${resp}=	                        Get Request             FastShop     /fastshop-qa/checkout/cart/payments              data=${payload}     headers=${headers}     
    Should Be Equal As Strings          ${resp.status_code}     200
    Log to console                      TESTE 4 OK - Pagamento no carrinho
#TESTE 5 -Selecionar Compra a vista via Cartão de Crédito no Click Collect
    Log to console                      TESTE 5 - Selecionar Compra a vista via Cartão de Crédito no Click Collect: Installments
    ${payload}=                         Convert To Json          [{"paymentMethodId":"5010","paymentMethodName":"Cartão de Crédito 12x","paymentTypeDescription":"Cartão de Crédito","paymentTypeName":"Cartão de Crédito","cardNumber":"${CARTAODECREDITOCLICKCOLLECT}","tokenId":null,"value":null,"installmentNumber":null}]
    ${resp}=	                        Post Request             FastShop    /fastshop-qa/checkout/cart/installments          data=${payload}     headers=${headers}     
    Should Be Equal As Strings          ${resp.status_code}      200
    Log to console                      TESTE 5 OK - SelecSelecionar Compra a vista via Cartão de Crédito no Click Collect: Installments

    Log to console                      TESTE 5 - Selecionar Compra a vista via Cartão de Crédito no Click Collect
    ${payload}=                         Convert To Json          [{"paymentMethodId":"5010","paymentMethodName":"Cartão de Crédito 12x","paymentTypeDescription":"Cartão de Crédito","paymentTypeName":"Cartão de Crédito","installmentNumber":1,"value":${VALORDOSKU}}]
    ${resp}=	                        Post Request             FastShop    /fastshop-qa/checkout/cart/payments        data=${payload}     headers=${headers}     
    Should Be Equal As Strings          ${resp.status_code}      200
    Log to console                      TESTE 5 OK - Selecionar Compra a vista via Cartão de Crédito no Click Collect
#TESTE 6 -Compra concluida com Sucesso via cartão de Crédito no Click Collect
    Log to console                      TESTE 6 - Compra concluida com Sucesso via cartão de Crédito no Click Collect
    ${payload}=                         Convert To Json          [{"paymentMethodId":"5010","paymentMethodName":"Cartão de Crédito 12x","paymentTypeDescription":"Cartão de Crédito","paymentTypeName":"Cartão de Crédito","value":${VALORDOSKU},"installmentNumber":1,"cardNumber":"${CARTAODECREDITOCLICKCOLLECT}","expirationMonth":"${MESCARTAODECREDITOCLICKCOLLECT}","expirationYear":"${ANOCARTAODECREDITOCLICKCOLLECT}","securityCode":"${CVVCARTAODECREDITOCLICKCOLLECT}","creditCardOwner":"Teste Teste","document":"${CPF_AMBIENTE}","registeredCard":null,"saveCard":false,"tokenId":null}]
    ${resp}=	                        Post Request             FastShop    /fastshop-qa/checkout/cart/sendPayments    data=${payload}     headers=${headers}     
    Should Be Equal As Strings          ${resp.status_code}      200
    Log to console                      TESTE 6 OK - Compra concluida com Sucesso via cartão de Crédito no Click Collect






    

#REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO
Adicionar um tipo de pagamento via api
    [Arguments]                         ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_Exibicao_na_Promocao}    ${Permite_Parcelamento}		${Bandeira}		${Tipo_de_Pagamento_Faturado}		${Permite_BIN_de_Cartões}		${Permite_pagamento_de_produtos_Marktplace}		${Permite_pagamento_de_produtos_M1}		${Permite_pagamento_de_produtos_Click&Collect}		${Restringir_pagamento_por_Tipo_de_Entrega}		${Permite_combinar_com_o_mesmo_tipo_de_Pagamento}		${Permite_dar_destaque_na_promoção}		${De_para_o_legado}     ${Parcelamento_Maximo}
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json     {"id":null,"name":"${Nome}","description":"${Descricao}","presentationOrder":${Ordem_da_Exibicao_na_Promocao},"allowInstallment":${Permite_Parcelamento},"allowFlag":${Bandeira},"billedPayment":false,"allowbilledPayment":${Tipo_de_Pagamento_Faturado},"billedPaymentDsp":false,"installmentAllowedDsp":false,"flagDsp":false,"legacyId":"${De_para_o_legado}","installments":[],"allowBin":${Permite_BIN_de_Cartões},"allowMarketplace":${Permite_pagamento_de_produtos_Marktplace},"allowM1":${Permite_pagamento_de_produtos_M1},"allowClickCollect":${Permite_pagamento_de_produtos_Click&Collect},"allowDeliveryType":${Restringir_pagamento_por_Tipo_de_Entrega},"allowCombineSameType":${Permite_combinar_com_o_mesmo_tipo_de_Pagamento},"deliveriesTypes":[],"deliveries":"","installmentMax":"","allowHighLightPromotion":${Permite_dar_destaque_na_promoção}}
    Log                                 ${payload}

    &{headers}=	                        Create Dictionary	Content-Type=application/json    Connection=keep-alive    Authorization=${Authorizationapi}

    ${resp}=	                        Post Request     FastShop      /price-management/api/v1/payment-type/         data=${payload}     headers=${headers}
    [return]                            ${resp}
    Log to console                      ${payload} 
    Log to console                      ${headers}
    Log to console                      ${resp} 
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}
    ${ID_Api_Adicionar_tipo_de_pg}=     Convert To String     ${resp.json()['id']}
    Log to console                      ${ID_Api_Adicionar_tipo_de_pg}
    Log to console                      Adicionar um tipo de pagamento via api foi executado com sucesso
    ${resp}=	                        Delete Request        FastShop      /price-management/api/v1/payment-type/${ID_Api_Adicionar_tipo_de_pg}         data=${payload}     headers=${headers}
    Log to console                      O Item ${ID_Api_Adicionar_tipo_de_pg} foi deletado com sucesso.      
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code} 

#REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM PERSMISSÃO DE PARCELAMENTO
Adicionar um tipo de pagamento com permissao de parcelamento via api
    [Arguments]                         ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_Exibicao_na_Promocao}    ${Permite_Parcelamento}		${Bandeira}		${Tipo_de_Pagamento_Faturado}		${Permite_BIN_de_Cartões}		${Permite_pagamento_de_produtos_Marktplace}		${Permite_pagamento_de_produtos_M1}		${Permite_pagamento_de_produtos_Click&Collect}		${Restringir_pagamento_por_Tipo_de_Entrega}		${Permite_combinar_com_o_mesmo_tipo_de_Pagamento}		${Permite_dar_destaque_na_promoção}		${De_para_o_legado}     ${Parcelamento_Maximo}
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json     {"id":null,"name":"${Nome}","description":"${Descricao}","presentationOrder":${Ordem_da_Exibicao_na_Promocao},"allowInstallment":${Permite_Parcelamento},"allowFlag":${Bandeira},"billedPayment":false,"allowbilledPayment":${Tipo_de_Pagamento_Faturado},"billedPaymentDsp":false,"installmentAllowedDsp":false,"flagDsp":false,"legacyId":"${De_para_o_legado}","installments":[],"allowBin":${Permite_BIN_de_Cartões},"allowMarketplace":${Permite_pagamento_de_produtos_Marktplace},"allowM1":${Permite_pagamento_de_produtos_M1},"allowClickCollect":${Permite_pagamento_de_produtos_Click&Collect},"allowDeliveryType":${Restringir_pagamento_por_Tipo_de_Entrega},"allowCombineSameType":${Permite_combinar_com_o_mesmo_tipo_de_Pagamento},"deliveriesTypes":[],"deliveries":"","installmentMax":"","allowHighLightPromotion":${Permite_dar_destaque_na_promoção}}
    Log                                 ${payload}

    &{headers}=	                        Create Dictionary	Content-Type=application/json    Connection=keep-alive    Authorization=${Authorizationapi}

    ${resp}=	                        Post Request     FastShop      /price-management/api/v1/payment-type/         data=${payload}     headers=${headers}
    [return]                            ${resp}
    Log to console                      ${payload} 
    Log to console                      ${headers}
    Log to console                      ${resp} 
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}
    ${ID_Api_Adicionar_tipo_de_pg}=     Convert To String     ${resp.json()['id']}
    Log to console                      ${ID_Api_Adicionar_tipo_de_pg}
    Log to console                      Adicionar um tipo de pagamento com permissao de parcelamento via api foi executado com sucesso 
    ${resp}=	                        Delete Request        FastShop      /price-management/api/v1/payment-type/${ID_Api_Adicionar_tipo_de_pg}         data=${payload}     headers=${headers}
    Log to console                      O Item ${ID_Api_Adicionar_tipo_de_pg} foi deletado com sucesso.      
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}


#REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM BANDEIRA
Adicionar um tipo de pagamento com bandeira via api
    [Arguments]                         ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_Exibicao_na_Promocao}    ${Permite_Parcelamento}		${Bandeira}		${Tipo_de_Pagamento_Faturado}		${Permite_BIN_de_Cartões}		${Permite_pagamento_de_produtos_Marktplace}		${Permite_pagamento_de_produtos_M1}		${Permite_pagamento_de_produtos_Click&Collect}		${Restringir_pagamento_por_Tipo_de_Entrega}		${Permite_combinar_com_o_mesmo_tipo_de_Pagamento}		${Permite_dar_destaque_na_promoção}		${De_para_o_legado}     ${Parcelamento_Maximo}
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json     {"id":null,"name":"${Nome}","description":"${Descricao}","presentationOrder":${Ordem_da_Exibicao_na_Promocao},"allowInstallment":${Permite_Parcelamento},"allowFlag":${Bandeira},"billedPayment":false,"allowbilledPayment":${Tipo_de_Pagamento_Faturado},"billedPaymentDsp":false,"installmentAllowedDsp":false,"flagDsp":false,"legacyId":"${De_para_o_legado}","installments":[],"allowBin":${Permite_BIN_de_Cartões},"allowMarketplace":${Permite_pagamento_de_produtos_Marktplace},"allowM1":${Permite_pagamento_de_produtos_M1},"allowClickCollect":${Permite_pagamento_de_produtos_Click&Collect},"allowDeliveryType":${Restringir_pagamento_por_Tipo_de_Entrega},"allowCombineSameType":${Permite_combinar_com_o_mesmo_tipo_de_Pagamento},"deliveriesTypes":[],"deliveries":"","installmentMax":"","allowHighLightPromotion":${Permite_dar_destaque_na_promoção}}
    Log                                 ${payload}

    &{headers}=	                        Create Dictionary	Content-Type=application/json    Connection=keep-alive    Authorization=${Authorizationapi}

    ${resp}=	                        Post Request     FastShop      /price-management/api/v1/payment-type/         data=${payload}     headers=${headers}
    [return]                            ${resp}
    Log to console                      ${payload} 
    Log to console                      ${headers}
    Log to console                      ${resp} 
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}
    ${ID_Api_Adicionar_tipo_de_pg}=     Convert To String     ${resp.json()['id']}
    Log to console                      ${ID_Api_Adicionar_tipo_de_pg}
    Log to console                      Adicionar um tipo de pagamento com bandeira via api foi executado com sucesso 
    ${resp}=	                        Delete Request        FastShop      /price-management/api/v1/payment-type/${ID_Api_Adicionar_tipo_de_pg}         data=${payload}     headers=${headers}
    Log to console                      O Item ${ID_Api_Adicionar_tipo_de_pg} foi deletado com sucesso.      
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}


#REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM TIPO DE PAGAMENTO FATURADO
Adicionar um tipo de pagamento com tipo de pagamento faturado via api
    [Arguments]                         ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_Exibicao_na_Promocao}    ${Permite_Parcelamento}		${Bandeira}		${Tipo_de_Pagamento_Faturado}		${Permite_BIN_de_Cartões}		${Permite_pagamento_de_produtos_Marktplace}		${Permite_pagamento_de_produtos_M1}		${Permite_pagamento_de_produtos_Click&Collect}		${Restringir_pagamento_por_Tipo_de_Entrega}		${Permite_combinar_com_o_mesmo_tipo_de_Pagamento}		${Permite_dar_destaque_na_promoção}		${De_para_o_legado}     ${Parcelamento_Maximo}
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json     {"id":null,"name":"${Nome}","description":"${Descricao}","presentationOrder":${Ordem_da_Exibicao_na_Promocao},"allowInstallment":${Permite_Parcelamento},"allowFlag":${Bandeira},"billedPayment":false,"allowbilledPayment":${Tipo_de_Pagamento_Faturado},"billedPaymentDsp":false,"installmentAllowedDsp":false,"flagDsp":false,"legacyId":"${De_para_o_legado}","installments":[],"allowBin":${Permite_BIN_de_Cartões},"allowMarketplace":${Permite_pagamento_de_produtos_Marktplace},"allowM1":${Permite_pagamento_de_produtos_M1},"allowClickCollect":${Permite_pagamento_de_produtos_Click&Collect},"allowDeliveryType":${Restringir_pagamento_por_Tipo_de_Entrega},"allowCombineSameType":${Permite_combinar_com_o_mesmo_tipo_de_Pagamento},"deliveriesTypes":[],"deliveries":"","installmentMax":"","allowHighLightPromotion":${Permite_dar_destaque_na_promoção}}
    Log                                 ${payload}

    &{headers}=	                        Create Dictionary	Content-Type=application/json    Connection=keep-alive    Authorization=${Authorizationapi}

    ${resp}=	                        Post Request     FastShop      /price-management/api/v1/payment-type/         data=${payload}     headers=${headers}
    [return]                            ${resp}
    Log to console                      ${payload} 
    Log to console                      ${headers}
    Log to console                      ${resp} 
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}
    ${ID_Api_Adicionar_tipo_de_pg}=     Convert To String     ${resp.json()['id']}
    Log to console                      ${ID_Api_Adicionar_tipo_de_pg}
    Log to console                      Adicionar um tipo de pagamento com tipo de pagamento faturado via api foi executado com sucesso 
    ${resp}=	                        Delete Request        FastShop      /price-management/api/v1/payment-type/${ID_Api_Adicionar_tipo_de_pg}         data=${payload}     headers=${headers}
    Log to console                      O Item ${ID_Api_Adicionar_tipo_de_pg} foi deletado com sucesso.      
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}


#REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM PERMISSÃO BIN DE CARTÕES
Adicionar um tipo de pagamento com permissao de bin de cartoes via api
    [Arguments]                         ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_Exibicao_na_Promocao}    ${Permite_Parcelamento}		${Bandeira}		${Tipo_de_Pagamento_Faturado}		${Permite_BIN_de_Cartões}		${Permite_pagamento_de_produtos_Marktplace}		${Permite_pagamento_de_produtos_M1}		${Permite_pagamento_de_produtos_Click&Collect}		${Restringir_pagamento_por_Tipo_de_Entrega}		${Permite_combinar_com_o_mesmo_tipo_de_Pagamento}		${Permite_dar_destaque_na_promoção}		${De_para_o_legado}     ${Parcelamento_Maximo}
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json     {"id":null,"name":"${Nome}","description":"${Descricao}","presentationOrder":${Ordem_da_Exibicao_na_Promocao},"allowInstallment":${Permite_Parcelamento},"allowFlag":${Bandeira},"billedPayment":false,"allowbilledPayment":${Tipo_de_Pagamento_Faturado},"billedPaymentDsp":false,"installmentAllowedDsp":false,"flagDsp":false,"legacyId":"${De_para_o_legado}","installments":[],"allowBin":${Permite_BIN_de_Cartões},"allowMarketplace":${Permite_pagamento_de_produtos_Marktplace},"allowM1":${Permite_pagamento_de_produtos_M1},"allowClickCollect":${Permite_pagamento_de_produtos_Click&Collect},"allowDeliveryType":${Restringir_pagamento_por_Tipo_de_Entrega},"allowCombineSameType":${Permite_combinar_com_o_mesmo_tipo_de_Pagamento},"deliveriesTypes":[],"deliveries":"","installmentMax":"","allowHighLightPromotion":${Permite_dar_destaque_na_promoção}}
    Log                                 ${payload}

    &{headers}=	                        Create Dictionary	Content-Type=application/json    Connection=keep-alive    Authorization=${Authorizationapi}

    ${resp}=	                        Post Request     FastShop      /price-management/api/v1/payment-type/         data=${payload}     headers=${headers}
    [return]                            ${resp}
    Log to console                      ${payload} 
    Log to console                      ${headers}
    Log to console                      ${resp} 
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}
    ${ID_Api_Adicionar_tipo_de_pg}=     Convert To String     ${resp.json()['id']}
    Log to console                      ${ID_Api_Adicionar_tipo_de_pg}
    Log to console                      Adicionar um tipo de pagamento com permissao de bin de cartoes via api foi executado com sucesso 
    ${resp}=	                        Delete Request        FastShop      /price-management/api/v1/payment-type/${ID_Api_Adicionar_tipo_de_pg}         data=${payload}     headers=${headers}
    Log to console                      O Item ${ID_Api_Adicionar_tipo_de_pg} foi deletado com sucesso.      
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}


#REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM PERMISSÃO DE PAGAMENTO DE PRODUTOS MARKTPLACE
Adicionar um tipo de pagamento com permissao de pagamento de produtos marktplcace via api
    [Arguments]                         ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_Exibicao_na_Promocao}    ${Permite_Parcelamento}		${Bandeira}		${Tipo_de_Pagamento_Faturado}		${Permite_BIN_de_Cartões}		${Permite_pagamento_de_produtos_Marktplace}		${Permite_pagamento_de_produtos_M1}		${Permite_pagamento_de_produtos_Click&Collect}		${Restringir_pagamento_por_Tipo_de_Entrega}		${Permite_combinar_com_o_mesmo_tipo_de_Pagamento}		${Permite_dar_destaque_na_promoção}		${De_para_o_legado}     ${Parcelamento_Maximo}
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json     {"id":null,"name":"${Nome}","description":"${Descricao}","presentationOrder":${Ordem_da_Exibicao_na_Promocao},"allowInstallment":${Permite_Parcelamento},"allowFlag":${Bandeira},"billedPayment":false,"allowbilledPayment":${Tipo_de_Pagamento_Faturado},"billedPaymentDsp":false,"installmentAllowedDsp":false,"flagDsp":false,"legacyId":"${De_para_o_legado}","installments":[],"allowBin":${Permite_BIN_de_Cartões},"allowMarketplace":${Permite_pagamento_de_produtos_Marktplace},"allowM1":${Permite_pagamento_de_produtos_M1},"allowClickCollect":${Permite_pagamento_de_produtos_Click&Collect},"allowDeliveryType":${Restringir_pagamento_por_Tipo_de_Entrega},"allowCombineSameType":${Permite_combinar_com_o_mesmo_tipo_de_Pagamento},"deliveriesTypes":[],"deliveries":"","installmentMax":"","allowHighLightPromotion":${Permite_dar_destaque_na_promoção}}
    Log                                 ${payload}

    &{headers}=	                        Create Dictionary	Content-Type=application/json    Connection=keep-alive    Authorization=${Authorizationapi}

    ${resp}=	                        Post Request     FastShop      /price-management/api/v1/payment-type/         data=${payload}     headers=${headers}
    [return]                            ${resp}
    Log to console                      ${payload} 
    Log to console                      ${headers}
    Log to console                      ${resp} 
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}
    ${ID_Api_Adicionar_tipo_de_pg}=     Convert To String     ${resp.json()['id']}
    Log to console                      ${ID_Api_Adicionar_tipo_de_pg}    
    Log to console                      Adicionar um tipo de pagamento com permissao de pagamento de produtos marktplcace via api foi executado com sucesso
    ${resp}=	                        Delete Request        FastShop      /price-management/api/v1/payment-type/${ID_Api_Adicionar_tipo_de_pg}         data=${payload}     headers=${headers}
    Log to console                      O Item ${ID_Api_Adicionar_tipo_de_pg} foi deletado com sucesso.      
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}


#REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM PERMISSÃO DE PAGAMENTO DE PRODUTOS M1
Adicionar um tipo de pagamento com permissao de pagamento de produtos m1 via api
    [Arguments]                         ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_Exibicao_na_Promocao}    ${Permite_Parcelamento}		${Bandeira}		${Tipo_de_Pagamento_Faturado}		${Permite_BIN_de_Cartões}		${Permite_pagamento_de_produtos_Marktplace}		${Permite_pagamento_de_produtos_M1}		${Permite_pagamento_de_produtos_Click&Collect}		${Restringir_pagamento_por_Tipo_de_Entrega}		${Permite_combinar_com_o_mesmo_tipo_de_Pagamento}		${Permite_dar_destaque_na_promoção}		${De_para_o_legado}     ${Parcelamento_Maximo}
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json     {"id":null,"name":"${Nome}","description":"${Descricao}","presentationOrder":${Ordem_da_Exibicao_na_Promocao},"allowInstallment":${Permite_Parcelamento},"allowFlag":${Bandeira},"billedPayment":false,"allowbilledPayment":${Tipo_de_Pagamento_Faturado},"billedPaymentDsp":false,"installmentAllowedDsp":false,"flagDsp":false,"legacyId":"${De_para_o_legado}","installments":[],"allowBin":${Permite_BIN_de_Cartões},"allowMarketplace":${Permite_pagamento_de_produtos_Marktplace},"allowM1":${Permite_pagamento_de_produtos_M1},"allowClickCollect":${Permite_pagamento_de_produtos_Click&Collect},"allowDeliveryType":${Restringir_pagamento_por_Tipo_de_Entrega},"allowCombineSameType":${Permite_combinar_com_o_mesmo_tipo_de_Pagamento},"deliveriesTypes":[],"deliveries":"","installmentMax":"","allowHighLightPromotion":${Permite_dar_destaque_na_promoção}}
    Log                                 ${payload}

    &{headers}=	                        Create Dictionary	Content-Type=application/json    Connection=keep-alive    Authorization=${Authorizationapi}

    ${resp}=	                        Post Request     FastShop      /price-management/api/v1/payment-type/         data=${payload}     headers=${headers}
    [return]                            ${resp}
    Log to console                      ${payload} 
    Log to console                      ${headers}
    Log to console                      ${resp} 
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}
    ${ID_Api_Adicionar_tipo_de_pg}=     Convert To String     ${resp.json()['id']}
    Log to console                      ${ID_Api_Adicionar_tipo_de_pg}
    Log to console                      Adicionar um tipo de pagamento com permissao de pagamento de produtos m1 via api foi executado com sucesso 
    ${resp}=	                        Delete Request        FastShop      /price-management/api/v1/payment-type/${ID_Api_Adicionar_tipo_de_pg}         data=${payload}     headers=${headers}
    Log to console                      O Item ${ID_Api_Adicionar_tipo_de_pg} foi deletado com sucesso.      
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}


#REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM PERMISSÃO DE PAGAMENTO DE PRODUTOS CLICK&COLLECT
Adicionar um tipo de pagamento com permissao de pagamento de produtos clickcollect via api
    [Arguments]                         ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_Exibicao_na_Promocao}    ${Permite_Parcelamento}		${Bandeira}		${Tipo_de_Pagamento_Faturado}		${Permite_BIN_de_Cartões}		${Permite_pagamento_de_produtos_Marktplace}		${Permite_pagamento_de_produtos_M1}		${Permite_pagamento_de_produtos_Click&Collect}		${Restringir_pagamento_por_Tipo_de_Entrega}		${Permite_combinar_com_o_mesmo_tipo_de_Pagamento}		${Permite_dar_destaque_na_promoção}		${De_para_o_legado}     ${Parcelamento_Maximo}
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json     {"id":null,"name":"${Nome}","description":"${Descricao}","presentationOrder":${Ordem_da_Exibicao_na_Promocao},"allowInstallment":${Permite_Parcelamento},"allowFlag":${Bandeira},"billedPayment":false,"allowbilledPayment":${Tipo_de_Pagamento_Faturado},"billedPaymentDsp":false,"installmentAllowedDsp":false,"flagDsp":false,"legacyId":"${De_para_o_legado}","installments":[],"allowBin":${Permite_BIN_de_Cartões},"allowMarketplace":${Permite_pagamento_de_produtos_Marktplace},"allowM1":${Permite_pagamento_de_produtos_M1},"allowClickCollect":${Permite_pagamento_de_produtos_Click&Collect},"allowDeliveryType":${Restringir_pagamento_por_Tipo_de_Entrega},"allowCombineSameType":${Permite_combinar_com_o_mesmo_tipo_de_Pagamento},"deliveriesTypes":[],"deliveries":"","installmentMax":"","allowHighLightPromotion":${Permite_dar_destaque_na_promoção}}
    Log                                 ${payload}

    &{headers}=	                        Create Dictionary	Content-Type=application/json    Connection=keep-alive    Authorization=${Authorizationapi}

    ${resp}=	                        Post Request     FastShop      /price-management/api/v1/payment-type/         data=${payload}     headers=${headers}
    [return]                            ${resp}
    Log to console                      ${payload} 
    Log to console                      ${headers}
    Log to console                      ${resp} 
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}
    ${ID_Api_Adicionar_tipo_de_pg}=     Convert To String     ${resp.json()['id']}
    Log to console                      ${ID_Api_Adicionar_tipo_de_pg}
    Log to console                      Adicionar um tipo de pagamento com permissao de pagamento de produtos clickcollect via api foi executado com sucesso 
    ${resp}=	                        Delete Request        FastShop      /price-management/api/v1/payment-type/${ID_Api_Adicionar_tipo_de_pg}         data=${payload}     headers=${headers}
    Log to console                      O Item ${ID_Api_Adicionar_tipo_de_pg} foi deletado com sucesso.      
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}


#REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM RESTRIÇÃO DE PAGAMENTO POR TIPO DE ENTREGA
Adicionar um tipo de pagamento com restricao de pagamento por tipo de entrega via api
    [Arguments]                         ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_Exibicao_na_Promocao}    ${Permite_Parcelamento}		${Bandeira}		${Tipo_de_Pagamento_Faturado}		${Permite_BIN_de_Cartões}		${Permite_pagamento_de_produtos_Marktplace}		${Permite_pagamento_de_produtos_M1}		${Permite_pagamento_de_produtos_Click&Collect}		${Restringir_pagamento_por_Tipo_de_Entrega}		${Permite_combinar_com_o_mesmo_tipo_de_Pagamento}		${Permite_dar_destaque_na_promoção}		${De_para_o_legado}     ${Parcelamento_Maximo}      ${Deliveries}
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json     {"id":null,"name":"${Nome}","description":"${Descricao}","presentationOrder":${Ordem_da_Exibicao_na_Promocao},"allowInstallment":${Permite_Parcelamento},"allowFlag":${Bandeira},"billedPayment":false,"allowbilledPayment":${Tipo_de_Pagamento_Faturado},"billedPaymentDsp":false,"installmentAllowedDsp":false,"flagDsp":false,"legacyId":"${De_para_o_legado}","installments":[],"allowBin":${Permite_BIN_de_Cartões},"allowMarketplace":${Permite_pagamento_de_produtos_Marktplace},"allowM1":${Permite_pagamento_de_produtos_M1},"allowClickCollect":${Permite_pagamento_de_produtos_Click&Collect},"allowDeliveryType":${Restringir_pagamento_por_Tipo_de_Entrega},"allowCombineSameType":${Permite_combinar_com_o_mesmo_tipo_de_Pagamento},"deliveriesTypes":["${Deliveries}"],"deliveries":"","installmentMax":"","allowHighLightPromotion":${Permite_dar_destaque_na_promoção}}
    Log                                 ${payload}

    &{headers}=	                        Create Dictionary	Content-Type=application/json    Connection=keep-alive    Authorization=${Authorizationapi}

    ${resp}=	                        Post Request     FastShop      /price-management/api/v1/payment-type/         data=${payload}     headers=${headers}
    [return]                            ${resp}
    Log to console                      ${payload} 
    Log to console                      ${headers}
    Log to console                      ${resp} 
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}
    ${ID_Api_Adicionar_tipo_de_pg}=     Convert To String     ${resp.json()['id']}
    Log to console                      ${ID_Api_Adicionar_tipo_de_pg}
    Log to console                      Adicionar um tipo de pagamento com restricao de pagamento por tipo de entrega via api foi executado com sucesso 
    ${resp}=	                        Delete Request        FastShop      /price-management/api/v1/payment-type/${ID_Api_Adicionar_tipo_de_pg}         data=${payload}     headers=${headers}
    Log to console                      O Item ${ID_Api_Adicionar_tipo_de_pg} foi deletado com sucesso.      
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}

#REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM PERMISSÃO DE COMBINAR COM O MESMO TIPO DE PAGAMENTO
Adicionar um tipo de pagamento com permissao de combinar mesmo tipo de pagamento via api
    [Arguments]                         ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_Exibicao_na_Promocao}    ${Permite_Parcelamento}		${Bandeira}		${Tipo_de_Pagamento_Faturado}		${Permite_BIN_de_Cartões}		${Permite_pagamento_de_produtos_Marktplace}		${Permite_pagamento_de_produtos_M1}		${Permite_pagamento_de_produtos_Click&Collect}		${Restringir_pagamento_por_Tipo_de_Entrega}		${Permite_combinar_com_o_mesmo_tipo_de_Pagamento}		${Permite_dar_destaque_na_promoção}		${De_para_o_legado}     ${Parcelamento_Maximo}
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json     {"id":null,"name":"${Nome}","description":"${Descricao}","presentationOrder":${Ordem_da_Exibicao_na_Promocao},"allowInstallment":${Permite_Parcelamento},"allowFlag":${Bandeira},"billedPayment":false,"allowbilledPayment":${Tipo_de_Pagamento_Faturado},"billedPaymentDsp":false,"installmentAllowedDsp":false,"flagDsp":false,"legacyId":"${De_para_o_legado}","installments":[],"allowBin":${Permite_BIN_de_Cartões},"allowMarketplace":${Permite_pagamento_de_produtos_Marktplace},"allowM1":${Permite_pagamento_de_produtos_M1},"allowClickCollect":${Permite_pagamento_de_produtos_Click&Collect},"allowDeliveryType":${Restringir_pagamento_por_Tipo_de_Entrega},"allowCombineSameType":${Permite_combinar_com_o_mesmo_tipo_de_Pagamento},"deliveriesTypes":[],"deliveries":"","installmentMax":"","allowHighLightPromotion":${Permite_dar_destaque_na_promoção}}
    Log                                 ${payload}

    &{headers}=	                        Create Dictionary	Content-Type=application/json    Connection=keep-alive    Authorization=${Authorizationapi}

    ${resp}=	                        Post Request     FastShop      /price-management/api/v1/payment-type/         data=${payload}     headers=${headers}
    [return]                            ${resp}
    Log to console                      ${payload} 
    Log to console                      ${headers}
    Log to console                      ${resp} 
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}
    ${ID_Api_Adicionar_tipo_de_pg}=     Convert To String     ${resp.json()['id']}
    Log to console                      ${ID_Api_Adicionar_tipo_de_pg}
    Log to console                      Adicionar um tipo de pagamento com permissao de combinar mesmo tipo de pagamento via api foi executado com sucesso 
    ${resp}=	                        Delete Request        FastShop      /price-management/api/v1/payment-type/${ID_Api_Adicionar_tipo_de_pg}         data=${payload}     headers=${headers}
    Log to console                      O Item ${ID_Api_Adicionar_tipo_de_pg} foi deletado com sucesso.      
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}


#REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM PERMISSÃO DE DAR DESTAQUE NA PROMOÇÃO
Adicionar um tipo de pagamento com permissao de dar destaque na promocao via api
    [Arguments]                         ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_Exibicao_na_Promocao}    ${Permite_Parcelamento}		${Bandeira}		${Tipo_de_Pagamento_Faturado}		${Permite_BIN_de_Cartões}		${Permite_pagamento_de_produtos_Marktplace}		${Permite_pagamento_de_produtos_M1}		${Permite_pagamento_de_produtos_Click&Collect}		${Restringir_pagamento_por_Tipo_de_Entrega}		${Permite_combinar_com_o_mesmo_tipo_de_Pagamento}		${Permite_dar_destaque_na_promoção}		${De_para_o_legado}     ${Parcelamento_Maximo}
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json     {"id":null,"name":"${Nome}","description":"${Descricao}","presentationOrder":${Ordem_da_Exibicao_na_Promocao},"allowInstallment":${Permite_Parcelamento},"allowFlag":${Bandeira},"billedPayment":false,"allowbilledPayment":${Tipo_de_Pagamento_Faturado},"billedPaymentDsp":false,"installmentAllowedDsp":false,"flagDsp":false,"legacyId":"${De_para_o_legado}","installments":[],"allowBin":${Permite_BIN_de_Cartões},"allowMarketplace":${Permite_pagamento_de_produtos_Marktplace},"allowM1":${Permite_pagamento_de_produtos_M1},"allowClickCollect":${Permite_pagamento_de_produtos_Click&Collect},"allowDeliveryType":${Restringir_pagamento_por_Tipo_de_Entrega},"allowCombineSameType":${Permite_combinar_com_o_mesmo_tipo_de_Pagamento},"deliveriesTypes":[],"deliveries":"","installmentMax":"","allowHighLightPromotion":${Permite_dar_destaque_na_promoção}}
    Log                                 ${payload}

    &{headers}=	                        Create Dictionary	Content-Type=application/json    Connection=keep-alive    Authorization=${Authorizationapi}

    ${resp}=	                        Post Request     FastShop      /price-management/api/v1/payment-type/         data=${payload}     headers=${headers}
    [return]                            ${resp}
    Log to console                      ${payload} 
    Log to console                      ${headers}
    Log to console                      ${resp} 
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}
    ${ID_Api_Adicionar_tipo_de_pg}=     Convert To String     ${resp.json()['id']}
    Log to console                      ${ID_Api_Adicionar_tipo_de_pg}
    Log to console                      Adicionar um tipo de pagamento com permissao de dar destaque na promocao via api foi executado com sucesso 
    ${resp}=	                        Delete Request        FastShop      /price-management/api/v1/payment-type/${ID_Api_Adicionar_tipo_de_pg}         data=${payload}     headers=${headers}
    Log to console                      O Item ${ID_Api_Adicionar_tipo_de_pg} foi deletado com sucesso.      
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}

#REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM BIN DE CARTÃO E MKTPLACE
Adicionar um tipo de pagamento com permissao de bin de cartoes e Produtos de Marktplcace via api
    [Arguments]                         ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_Exibicao_na_Promocao}    ${Permite_Parcelamento}		${Bandeira}		${Tipo_de_Pagamento_Faturado}		${Permite_BIN_de_Cartões}		${Permite_pagamento_de_produtos_Marktplace}		${Permite_pagamento_de_produtos_M1}		${Permite_pagamento_de_produtos_Click&Collect}		${Restringir_pagamento_por_Tipo_de_Entrega}		${Permite_combinar_com_o_mesmo_tipo_de_Pagamento}		${Permite_dar_destaque_na_promoção}		${De_para_o_legado}     ${Parcelamento_Maximo}
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json     {"id":null,"name":"${Nome}","description":"${Descricao}","presentationOrder":${Ordem_da_Exibicao_na_Promocao},"allowInstallment":${Permite_Parcelamento},"allowFlag":${Bandeira},"billedPayment":false,"allowbilledPayment":${Tipo_de_Pagamento_Faturado},"billedPaymentDsp":false,"installmentAllowedDsp":false,"flagDsp":false,"legacyId":"${De_para_o_legado}","installments":[],"allowBin":${Permite_BIN_de_Cartões},"allowMarketplace":${Permite_pagamento_de_produtos_Marktplace},"allowM1":${Permite_pagamento_de_produtos_M1},"allowClickCollect":${Permite_pagamento_de_produtos_Click&Collect},"allowDeliveryType":${Restringir_pagamento_por_Tipo_de_Entrega},"allowCombineSameType":${Permite_combinar_com_o_mesmo_tipo_de_Pagamento},"deliveriesTypes":[],"deliveries":"","installmentMax":"","allowHighLightPromotion":${Permite_dar_destaque_na_promoção}}
    Log                                 ${payload}

    &{headers}=	                        Create Dictionary	Content-Type=application/json    Connection=keep-alive    Authorization=${Authorizationapi}

    ${resp}=	                        Post Request     FastShop      /price-management/api/v1/payment-type/         data=${payload}     headers=${headers}
    [return]                            ${resp}
    Log to console                      ${payload} 
    Log to console                      ${headers}
    Log to console                      ${resp} 
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}
    ${ID_Api_Adicionar_tipo_de_pg}=     Convert To String     ${resp.json()['id']}
    Log to console                      ${ID_Api_Adicionar_tipo_de_pg}
    Log to console                      Adicionar um tipo de pagamento com permissao de bin de cartoes e Produtos de Marktplcace via api foi executado com sucesso 
    ${resp}=	                        Delete Request        FastShop      /price-management/api/v1/payment-type/${ID_Api_Adicionar_tipo_de_pg}         data=${payload}     headers=${headers}
    Log to console                      O Item ${ID_Api_Adicionar_tipo_de_pg} foi deletado com sucesso.      
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}

#REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM PERMITE PAGAMENTO DE PRODUTOS CLICK&COLLECT PERMITE PAGAMENTO DE PRODUTOS MARKETPLACE ATIVO e Restringir pagamento por Tipo de Entrega ATIVO
Adicionar um tipo de pagamento com CLICK & COLLECT E RESTRINGE TIPO DE ENTREGA via api
    [Arguments]                         ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_Exibicao_na_Promocao}    ${Permite_Parcelamento}		${Bandeira}		${Tipo_de_Pagamento_Faturado}		${Permite_BIN_de_Cartões}		${Permite_pagamento_de_produtos_Marktplace}		${Permite_pagamento_de_produtos_M1}		${Permite_pagamento_de_produtos_Click&Collect}		${Restringir_pagamento_por_Tipo_de_Entrega}		${Permite_combinar_com_o_mesmo_tipo_de_Pagamento}		${Permite_dar_destaque_na_promoção}		${De_para_o_legado}     ${Parcelamento_Maximo}      ${Deliveries}
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json     {"id":null,"name":"${Nome}","description":"${Descricao}","presentationOrder":${Ordem_da_Exibicao_na_Promocao},"allowInstallment":${Permite_Parcelamento},"allowFlag":${Bandeira},"billedPayment":false,"allowbilledPayment":${Tipo_de_Pagamento_Faturado},"billedPaymentDsp":false,"installmentAllowedDsp":false,"flagDsp":false,"legacyId":"${De_para_o_legado}","installments":[],"allowBin":${Permite_BIN_de_Cartões},"allowMarketplace":${Permite_pagamento_de_produtos_Marktplace},"allowM1":${Permite_pagamento_de_produtos_M1},"allowClickCollect":${Permite_pagamento_de_produtos_Click&Collect},"allowDeliveryType":${Restringir_pagamento_por_Tipo_de_Entrega},"allowCombineSameType":${Permite_combinar_com_o_mesmo_tipo_de_Pagamento},"deliveriesTypes":["${Deliveries}"],"deliveries":"","installmentMax":"","allowHighLightPromotion":${Permite_dar_destaque_na_promoção}}
    Log                                 ${payload}

    &{headers}=	                        Create Dictionary	Content-Type=application/json    Connection=keep-alive    Authorization=${Authorizationapi}

    ${resp}=	                        Post Request     FastShop      /price-management/api/v1/payment-type/         data=${payload}     headers=${headers}
    [return]                            ${resp}
    Log to console                      ${payload} 
    Log to console                      ${headers}
    Log to console                      ${resp} 
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}
    ${ID_Api_Adicionar_tipo_de_pg}=     Convert To String     ${resp.json()['id']}
    Log to console                      ${ID_Api_Adicionar_tipo_de_pg}
    Log to console                      Adicionar um tipo de pagamento com CLICK & COLLECT E RESTRINGE TIPO DE ENTREGA via api foi executado com sucesso 
    ${resp}=	                        Delete Request        FastShop      /price-management/api/v1/payment-type/${ID_Api_Adicionar_tipo_de_pg}         data=${payload}     headers=${headers}
    Log to console                      O Item ${ID_Api_Adicionar_tipo_de_pg} foi deletado com sucesso.      
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}

#REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM PERMITE PAGAMENTO DE PRODUTOS MARKETPLACE ATIVO E M1 ATIVO E COMBINAR COM O MESMO TIPO DE PAGAMENTO ATIVO
Adicionar um tipo de pagamento com PERMITE PAGAMENTO DE PRODUTOS MARKETPLACE ATIVO E M1 ATIVO E COMBINAR COM O MESMO TIPO DE PAGAMENTO ATIVO via api
    [Arguments]                         ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_Exibicao_na_Promocao}    ${Permite_Parcelamento}		${Bandeira}		${Tipo_de_Pagamento_Faturado}		${Permite_BIN_de_Cartões}		${Permite_pagamento_de_produtos_Marktplace}		${Permite_pagamento_de_produtos_M1}		${Permite_pagamento_de_produtos_Click&Collect}		${Restringir_pagamento_por_Tipo_de_Entrega}		${Permite_combinar_com_o_mesmo_tipo_de_Pagamento}		${Permite_dar_destaque_na_promoção}		${De_para_o_legado}     ${Parcelamento_Maximo}
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json     {"id":null,"name":"${Nome}","description":"${Descricao}","presentationOrder":${Ordem_da_Exibicao_na_Promocao},"allowInstallment":${Permite_Parcelamento},"allowFlag":${Bandeira},"billedPayment":false,"allowbilledPayment":${Tipo_de_Pagamento_Faturado},"billedPaymentDsp":false,"installmentAllowedDsp":false,"flagDsp":false,"legacyId":"${De_para_o_legado}","installments":[],"allowBin":${Permite_BIN_de_Cartões},"allowMarketplace":${Permite_pagamento_de_produtos_Marktplace},"allowM1":${Permite_pagamento_de_produtos_M1},"allowClickCollect":${Permite_pagamento_de_produtos_Click&Collect},"allowDeliveryType":${Restringir_pagamento_por_Tipo_de_Entrega},"allowCombineSameType":${Permite_combinar_com_o_mesmo_tipo_de_Pagamento},"deliveriesTypes":[],"deliveries":"","installmentMax":"","allowHighLightPromotion":${Permite_dar_destaque_na_promoção}}
    Log                                 ${payload}

    &{headers}=	                        Create Dictionary	Content-Type=application/json    Connection=keep-alive    Authorization=${Authorizationapi}

    ${resp}=	                        Post Request     FastShop      /price-management/api/v1/payment-type/         data=${payload}     headers=${headers}
    [return]                            ${resp}
    Log to console                      ${payload} 
    Log to console                      ${headers}
    Log to console                      ${resp} 
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}
    ${ID_Api_Adicionar_tipo_de_pg}=     Convert To String     ${resp.json()['id']}
    Log to console                      ${ID_Api_Adicionar_tipo_de_pg}
    Log to console                      Adicionar um tipo de pagamento com PERMITE PAGAMENTO DE PRODUTOS MARKETPLACE ATIVO E M1 ATIVO E COMBINAR COM O MESMO TIPO DE PAGAMENTO ATIVO via api
    ${resp}=	                        Delete Request        FastShop      /price-management/api/v1/payment-type/${ID_Api_Adicionar_tipo_de_pg}         data=${payload}     headers=${headers}
    Log to console                      O Item ${ID_Api_Adicionar_tipo_de_pg} foi deletado com sucesso.      
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}

#REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM PERMITE DAR DESTAQUE NA PROMOÇÃO ATIVO E PAGAMENTO FATURADO
Adicionar um tipo de pagamento com permissao de dar destaque na promocao e pagamento faturado via api
    [Arguments]                         ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_Exibicao_na_Promocao}    ${Permite_Parcelamento}		${Bandeira}		${Tipo_de_Pagamento_Faturado}		${Permite_BIN_de_Cartões}		${Permite_pagamento_de_produtos_Marktplace}		${Permite_pagamento_de_produtos_M1}		${Permite_pagamento_de_produtos_Click&Collect}		${Restringir_pagamento_por_Tipo_de_Entrega}		${Permite_combinar_com_o_mesmo_tipo_de_Pagamento}		${Permite_dar_destaque_na_promoção}		${De_para_o_legado}     ${Parcelamento_Maximo}
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json     {"id":null,"name":"${Nome}","description":"${Descricao}","presentationOrder":${Ordem_da_Exibicao_na_Promocao},"allowInstallment":${Permite_Parcelamento},"allowFlag":${Bandeira},"billedPayment":false,"allowbilledPayment":${Tipo_de_Pagamento_Faturado},"billedPaymentDsp":false,"installmentAllowedDsp":false,"flagDsp":false,"legacyId":"${De_para_o_legado}","installments":[],"allowBin":${Permite_BIN_de_Cartões},"allowMarketplace":${Permite_pagamento_de_produtos_Marktplace},"allowM1":${Permite_pagamento_de_produtos_M1},"allowClickCollect":${Permite_pagamento_de_produtos_Click&Collect},"allowDeliveryType":${Restringir_pagamento_por_Tipo_de_Entrega},"allowCombineSameType":${Permite_combinar_com_o_mesmo_tipo_de_Pagamento},"deliveriesTypes":[],"deliveries":"","installmentMax":"","allowHighLightPromotion":${Permite_dar_destaque_na_promoção}}
    Log                                 ${payload}

    &{headers}=	                        Create Dictionary	Content-Type=application/json    Connection=keep-alive    Authorization=${Authorizationapi}

    ${resp}=	                        Post Request     FastShop      /price-management/api/v1/payment-type/         data=${payload}     headers=${headers}
    [return]                            ${resp}
    Log to console                      ${payload} 
    Log to console                      ${headers}
    Log to console                      ${resp} 
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}
    ${ID_Api_Adicionar_tipo_de_pg}=     Convert To String     ${resp.json()['id']}
    Log to console                      ${ID_Api_Adicionar_tipo_de_pg}
    Log to console                      Adicionar um tipo de pagamento com permissao de dar destaque na promocao e pagamento faturado via api
    ${resp}=	                        Delete Request        FastShop      /price-management/api/v1/payment-type/${ID_Api_Adicionar_tipo_de_pg}         data=${payload}     headers=${headers}
    Log to console                      O Item ${ID_Api_Adicionar_tipo_de_pg} foi deletado com sucesso.      
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}

#REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM PERMITE PARCELAMENTO ATIVO E BANDEIRA ATIVO
Adicionar um tipo de pagamento com permissao de parcelamento e bandeira ativo via api
    [Arguments]                         ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_Exibicao_na_Promocao}    ${Permite_Parcelamento}		${Bandeira}		${Tipo_de_Pagamento_Faturado}		${Permite_BIN_de_Cartões}		${Permite_pagamento_de_produtos_Marktplace}		${Permite_pagamento_de_produtos_M1}		${Permite_pagamento_de_produtos_Click&Collect}		${Restringir_pagamento_por_Tipo_de_Entrega}		${Permite_combinar_com_o_mesmo_tipo_de_Pagamento}		${Permite_dar_destaque_na_promoção}		${De_para_o_legado}     ${Parcelamento_Maximo}
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json     {"id":null,"name":"${Nome}","description":"${Descricao}","presentationOrder":${Ordem_da_Exibicao_na_Promocao},"allowInstallment":${Permite_Parcelamento},"allowFlag":${Bandeira},"billedPayment":false,"allowbilledPayment":${Tipo_de_Pagamento_Faturado},"billedPaymentDsp":false,"installmentAllowedDsp":false,"flagDsp":false,"legacyId":"${De_para_o_legado}","installments":[],"allowBin":${Permite_BIN_de_Cartões},"allowMarketplace":${Permite_pagamento_de_produtos_Marktplace},"allowM1":${Permite_pagamento_de_produtos_M1},"allowClickCollect":${Permite_pagamento_de_produtos_Click&Collect},"allowDeliveryType":${Restringir_pagamento_por_Tipo_de_Entrega},"allowCombineSameType":${Permite_combinar_com_o_mesmo_tipo_de_Pagamento},"deliveriesTypes":[],"deliveries":"","installmentMax":"","allowHighLightPromotion":${Permite_dar_destaque_na_promoção}}
    Log                                 ${payload}

    &{headers}=	                        Create Dictionary	Content-Type=application/json    Connection=keep-alive    Authorization=${Authorizationapi}

    ${resp}=	                        Post Request     FastShop      /price-management/api/v1/payment-type/         data=${payload}     headers=${headers}
    [return]                            ${resp}
    Log to console                      ${payload} 
    Log to console                      ${headers}
    Log to console                      ${resp} 
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}
    ${ID_Api_Adicionar_tipo_de_pg}=     Convert To String     ${resp.json()['id']}
    Log to console                      ${ID_Api_Adicionar_tipo_de_pg}
    Log to console                      Adicionar um tipo de pagamento com permissao de parcelamento e bandeira ativo via api
    ${resp}=	                        Delete Request        FastShop      /price-management/api/v1/payment-type/${ID_Api_Adicionar_tipo_de_pg}         data=${payload}     headers=${headers}
    Log to console                      O Item ${ID_Api_Adicionar_tipo_de_pg} foi deletado com sucesso.      
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}

#REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM PERMITE PARCELAMENTO E BANDEIRA ATIVO E PERMITE BIN DE CARTÕES
Adicionar um tipo de pagamento com permissao de parcelamento e bandeira ativo e com permissao de bin de cartoes via api
    [Arguments]                         ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_Exibicao_na_Promocao}    ${Permite_Parcelamento}		${Bandeira}		${Tipo_de_Pagamento_Faturado}		${Permite_BIN_de_Cartões}		${Permite_pagamento_de_produtos_Marktplace}		${Permite_pagamento_de_produtos_M1}		${Permite_pagamento_de_produtos_Click&Collect}		${Restringir_pagamento_por_Tipo_de_Entrega}		${Permite_combinar_com_o_mesmo_tipo_de_Pagamento}		${Permite_dar_destaque_na_promoção}		${De_para_o_legado}     ${Parcelamento_Maximo}
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json     {"id":null,"name":"${Nome}","description":"${Descricao}","presentationOrder":${Ordem_da_Exibicao_na_Promocao},"allowInstallment":${Permite_Parcelamento},"allowFlag":${Bandeira},"billedPayment":false,"allowbilledPayment":${Tipo_de_Pagamento_Faturado},"billedPaymentDsp":false,"installmentAllowedDsp":false,"flagDsp":false,"legacyId":"${De_para_o_legado}","installments":[],"allowBin":${Permite_BIN_de_Cartões},"allowMarketplace":${Permite_pagamento_de_produtos_Marktplace},"allowM1":${Permite_pagamento_de_produtos_M1},"allowClickCollect":${Permite_pagamento_de_produtos_Click&Collect},"allowDeliveryType":${Restringir_pagamento_por_Tipo_de_Entrega},"allowCombineSameType":${Permite_combinar_com_o_mesmo_tipo_de_Pagamento},"deliveriesTypes":[],"deliveries":"","installmentMax":"","allowHighLightPromotion":${Permite_dar_destaque_na_promoção}}
    Log                                 ${payload}

    &{headers}=	                        Create Dictionary	Content-Type=application/json    Connection=keep-alive    Authorization=${Authorizationapi}

    ${resp}=	                        Post Request     FastShop      /price-management/api/v1/payment-type/         data=${payload}     headers=${headers}
    [return]                            ${resp}
    Log to console                      ${payload} 
    Log to console                      ${headers}
    Log to console                      ${resp} 
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}
    ${ID_Api_Adicionar_tipo_de_pg}=     Convert To String     ${resp.json()['id']}
    Log to console                      ${ID_Api_Adicionar_tipo_de_pg}
    Log to console                      Adicionar um tipo de pagamento com permissao de parcelamento e bandeira ativo e com permite bin de cartoes via api
    ${resp}=	                        Delete Request        FastShop      /price-management/api/v1/payment-type/${ID_Api_Adicionar_tipo_de_pg}         data=${payload}     headers=${headers}
    Log to console                      O Item ${ID_Api_Adicionar_tipo_de_pg} foi deletado com sucesso.      
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}

#REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM PERMITE PARCELAMENTO E BANDEIRA ATIVO E PERMITE BIN DE CARTÕES E PERMITE COMBINAR O MESMO TIPO DE PAGAMENTO
Adicionar um tipo de pagamento com permissao de parcelamento e bandeira ativo e com permissao de bin de cartoes e com permite combinar mesmo tipo de pagamento via api
    [Arguments]                         ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_Exibicao_na_Promocao}    ${Permite_Parcelamento}		${Bandeira}		${Tipo_de_Pagamento_Faturado}		${Permite_BIN_de_Cartões}		${Permite_pagamento_de_produtos_Marktplace}		${Permite_pagamento_de_produtos_M1}		${Permite_pagamento_de_produtos_Click&Collect}		${Restringir_pagamento_por_Tipo_de_Entrega}		${Permite_combinar_com_o_mesmo_tipo_de_Pagamento}		${Permite_dar_destaque_na_promoção}		${De_para_o_legado}     ${Parcelamento_Maximo}
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json     {"id":null,"name":"${Nome}","description":"${Descricao}","presentationOrder":${Ordem_da_Exibicao_na_Promocao},"allowInstallment":${Permite_Parcelamento},"allowFlag":${Bandeira},"billedPayment":false,"allowbilledPayment":${Tipo_de_Pagamento_Faturado},"billedPaymentDsp":false,"installmentAllowedDsp":false,"flagDsp":false,"legacyId":"${De_para_o_legado}","installments":[],"allowBin":${Permite_BIN_de_Cartões},"allowMarketplace":${Permite_pagamento_de_produtos_Marktplace},"allowM1":${Permite_pagamento_de_produtos_M1},"allowClickCollect":${Permite_pagamento_de_produtos_Click&Collect},"allowDeliveryType":${Restringir_pagamento_por_Tipo_de_Entrega},"allowCombineSameType":${Permite_combinar_com_o_mesmo_tipo_de_Pagamento},"deliveriesTypes":[],"deliveries":"","installmentMax":"","allowHighLightPromotion":${Permite_dar_destaque_na_promoção}}
    Log                                 ${payload}

    &{headers}=	                        Create Dictionary	Content-Type=application/json    Connection=keep-alive    Authorization=${Authorizationapi}

    ${resp}=	                        Post Request     FastShop      /price-management/api/v1/payment-type/         data=${payload}     headers=${headers}
    [return]                            ${resp}
    Log to console                      ${payload} 
    Log to console                      ${headers}
    Log to console                      ${resp} 
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}
    ${ID_Api_Adicionar_tipo_de_pg}=     Convert To String     ${resp.json()['id']}
    Log to console                      ${ID_Api_Adicionar_tipo_de_pg}
    Log to console                      Adicionar um tipo de pagamento com permissao de parcelamento e bandeira ativo e com permite bin de cartoes e com permite combinar memso tipo de pagamento via api
    ${resp}=	                        Delete Request        FastShop      /price-management/api/v1/payment-type/${ID_Api_Adicionar_tipo_de_pg}         data=${payload}     headers=${headers}
    Log to console                      O Item ${ID_Api_Adicionar_tipo_de_pg} foi deletado com sucesso.      
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}

#REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM PERMITE PARCELAMENTO E BANDEIRA ATIVO E PERMITE COMBINAR O MESMO TIPO DE PAGAMENTO
Adicionar um tipo de pagamento com permissao de parcelamento e bandeira ativo e com permite combinar mesmo tipo de pagamento via api
    [Arguments]                         ${Authorizationapi}    ${Status_Code}    ${Nome}    ${Descricao}    ${Ordem_da_Exibicao_na_Promocao}    ${Permite_Parcelamento}		${Bandeira}		${Tipo_de_Pagamento_Faturado}		${Permite_BIN_de_Cartões}		${Permite_pagamento_de_produtos_Marktplace}		${Permite_pagamento_de_produtos_M1}		${Permite_pagamento_de_produtos_Click&Collect}		${Restringir_pagamento_por_Tipo_de_Entrega}		${Permite_combinar_com_o_mesmo_tipo_de_Pagamento}		${Permite_dar_destaque_na_promoção}		${De_para_o_legado}     ${Parcelamento_Maximo}
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json     {"id":null,"name":"${Nome}","description":"${Descricao}","presentationOrder":${Ordem_da_Exibicao_na_Promocao},"allowInstallment":${Permite_Parcelamento},"allowFlag":${Bandeira},"billedPayment":false,"allowbilledPayment":${Tipo_de_Pagamento_Faturado},"billedPaymentDsp":false,"installmentAllowedDsp":false,"flagDsp":false,"legacyId":"${De_para_o_legado}","installments":[],"allowBin":${Permite_BIN_de_Cartões},"allowMarketplace":${Permite_pagamento_de_produtos_Marktplace},"allowM1":${Permite_pagamento_de_produtos_M1},"allowClickCollect":${Permite_pagamento_de_produtos_Click&Collect},"allowDeliveryType":${Restringir_pagamento_por_Tipo_de_Entrega},"allowCombineSameType":${Permite_combinar_com_o_mesmo_tipo_de_Pagamento},"deliveriesTypes":[],"deliveries":"","installmentMax":"","allowHighLightPromotion":${Permite_dar_destaque_na_promoção}}
    Log                                 ${payload}

    &{headers}=	                        Create Dictionary	Content-Type=application/json    Connection=keep-alive    Authorization=${Authorizationapi}

    ${resp}=	                        Post Request     FastShop      /price-management/api/v1/payment-type/         data=${payload}     headers=${headers}
    [return]                            ${resp}
    Log to console                      ${payload} 
    Log to console                      ${headers}
    Log to console                      ${resp} 
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}
    ${ID_Api_Adicionar_tipo_de_pg}=     Convert To String     ${resp.json()['id']}
    Log to console                      ${ID_Api_Adicionar_tipo_de_pg}
    Log to console                      Adicionar um tipo de pagamento com permissao de parcelamento e bandeira ativo e com permite combinar memso tipo de pagamento via api
    ${resp}=	                        Delete Request        FastShop      /price-management/api/v1/payment-type/${ID_Api_Adicionar_tipo_de_pg}         data=${payload}     headers=${headers}
    Log to console                      O Item ${ID_Api_Adicionar_tipo_de_pg} foi deletado com sucesso.      
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}





#REALIZAR ADIÇÃO DA BANDEIRA MASTERCARD COM URL
Adicionar bandeira mastercard com url via api
    [Arguments]                         ${Authorizationapi}    ${Status_Code}    ${Nome}    ${De_Para_Legado}     ${URL_Imagem}
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json     {"name":"${Nome}","legacyFromTo":"${De_Para_Legado}","id":null,"urlImage":"${URL_Imagem}"}
    Log                                 ${payload}

    &{headers}=	                        Create Dictionary	Content-Type=application/json    Connection=keep-alive    Authorization=${Authorizationapi}

    ${resp}=	                        Post Request     FastShop      /price-management/api/v1/flag/         data=${payload}     headers=${headers}
    [return]                            ${resp}
    Log to console                      ${payload} 
    Log to console                      ${headers}
    Log to console                      ${resp} 
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}
    ${ID_Api_Adicionar_Bandeira}=       Convert To String     ${resp.json()['id']}
    Log to console                      ${ID_Api_Adicionar_Bandeira}
    Log to console                      Adicionar bandeira mastercard com url via api
    ${resp}=	                        Delete Request        FastShop      /price-management/api/v1/flag/${ID_Api_Adicionar_Bandeira}         data=${payload}     headers=${headers}
    Log to console                      O Item ${ID_Api_Adicionar_Bandeira} foi deletado com sucesso.      
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}

#REALIZAR ADIÇÃO DA BANDEIRA MASTERCARD SEM URL
Adicionar bandeira mastercard sem url via api
    [Arguments]                         ${Authorizationapi}    ${Status_Code}    ${Nome}    ${De_Para_Legado}
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json     {"name":"${Nome}","legacyFromTo":"${De_Para_Legado}","id":null,"urlImage":null}
    Log                                 ${payload}

    &{headers}=	                        Create Dictionary	Content-Type=application/json    Connection=keep-alive    Authorization=${Authorizationapi}

    ${resp}=	                        Post Request     FastShop      /price-management/api/v1/flag/         data=${payload}     headers=${headers}
    [return]                            ${resp}
    Log to console                      ${payload} 
    Log to console                      ${headers}
    Log to console                      ${resp} 
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}
    ${ID_Api_Adicionar_Bandeira}=       Convert To String     ${resp.json()['id']}
    Log to console                      ${ID_Api_Adicionar_Bandeira}
    Log to console                      Adicionar bandeira mastercard sem url via api
    ${resp}=	                        Delete Request        FastShop      /price-management/api/v1/flag/${ID_Api_Adicionar_Bandeira}         data=${payload}     headers=${headers}
    Log to console                      O Item ${ID_Api_Adicionar_Bandeira} foi deletado com sucesso.      
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}

#REALIZAR ADIÇÃO DA BANDEIRA VISA COM URL
Adicionar bandeira visa com url via api
    [Arguments]                         ${Authorizationapi}    ${Status_Code}    ${Nome}    ${De_Para_Legado}     ${URL_Imagem}
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json     {"name":"${Nome}","legacyFromTo":"${De_Para_Legado}","id":null,"urlImage":"${URL_Imagem}"}
    Log                                 ${payload}

    &{headers}=	                        Create Dictionary	Content-Type=application/json    Connection=keep-alive    Authorization=${Authorizationapi}

    ${resp}=	                        Post Request     FastShop      /price-management/api/v1/flag/         data=${payload}     headers=${headers}
    [return]                            ${resp}
    Log to console                      ${payload} 
    Log to console                      ${headers}
    Log to console                      ${resp} 
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}
    ${ID_Api_Adicionar_Bandeira}=       Convert To String     ${resp.json()['id']}
    Log to console                      ${ID_Api_Adicionar_Bandeira}
    Log to console                      Adicionar bandeira visa com url via api
    ${resp}=	                        Delete Request        FastShop      /price-management/api/v1/flag/${ID_Api_Adicionar_Bandeira}         data=${payload}     headers=${headers}
    Log to console                      O Item ${ID_Api_Adicionar_Bandeira} foi deletado com sucesso.      
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}

#REALIZAR ADIÇÃO DA BANDEIRA VISA SEM URL
Adicionar bandeira visa sem url via api
    [Arguments]                         ${Authorizationapi}    ${Status_Code}    ${Nome}    ${De_Para_Legado}
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json     {"name":"${Nome}","legacyFromTo":"${De_Para_Legado}","id":null,"urlImage":null}
    Log                                 ${payload}

    &{headers}=	                        Create Dictionary	Content-Type=application/json    Connection=keep-alive    Authorization=${Authorizationapi}

    ${resp}=	                        Post Request     FastShop      /price-management/api/v1/flag/         data=${payload}     headers=${headers}
    [return]                            ${resp}
    Log to console                      ${payload} 
    Log to console                      ${headers}
    Log to console                      ${resp} 
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}
    ${ID_Api_Adicionar_Bandeira}=       Convert To String     ${resp.json()['id']}
    Log to console                      ${ID_Api_Adicionar_Bandeira}
    Log to console                      Adicionar bandeira visa sem url via api
    ${resp}=	                        Delete Request        FastShop      /price-management/api/v1/flag/${ID_Api_Adicionar_Bandeira}         data=${payload}     headers=${headers}
    Log to console                      O Item ${ID_Api_Adicionar_Bandeira} foi deletado com sucesso.      
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}

#REALIZAR ADIÇÃO DA BANDEIRA AMEX COM URL
Adicionar bandeira amex com url via api
    [Arguments]                         ${Authorizationapi}    ${Status_Code}    ${Nome}    ${De_Para_Legado}     ${URL_Imagem}
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json     {"name":"${Nome}","legacyFromTo":"${De_Para_Legado}","id":null,"urlImage":"${URL_Imagem}"}
    Log                                 ${payload}

    &{headers}=	                        Create Dictionary	Content-Type=application/json    Connection=keep-alive    Authorization=${Authorizationapi}

    ${resp}=	                        Post Request     FastShop      /price-management/api/v1/flag/         data=${payload}     headers=${headers}
    [return]                            ${resp}
    Log to console                      ${payload} 
    Log to console                      ${headers}
    Log to console                      ${resp} 
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}
    ${ID_Api_Adicionar_Bandeira}=       Convert To String     ${resp.json()['id']}
    Log to console                      ${ID_Api_Adicionar_Bandeira}
    Log to console                      Adicionar bandeira amex com url via api
    ${resp}=	                        Delete Request        FastShop      /price-management/api/v1/flag/${ID_Api_Adicionar_Bandeira}         data=${payload}     headers=${headers}
    Log to console                      O Item ${ID_Api_Adicionar_Bandeira} foi deletado com sucesso.      
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}

#REALIZAR ADIÇÃO DA BANDEIRA AMEX SEM URL
Adicionar bandeira amex sem url via api
    [Arguments]                         ${Authorizationapi}    ${Status_Code}    ${Nome}    ${De_Para_Legado}
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json     {"name":"${Nome}","legacyFromTo":"${De_Para_Legado}","id":null,"urlImage":null}
    Log                                 ${payload}

    &{headers}=	                        Create Dictionary	Content-Type=application/json    Connection=keep-alive    Authorization=${Authorizationapi}

    ${resp}=	                        Post Request     FastShop      /price-management/api/v1/flag/         data=${payload}     headers=${headers}
    [return]                            ${resp}
    Log to console                      ${payload} 
    Log to console                      ${headers}
    Log to console                      ${resp} 
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}
    ${ID_Api_Adicionar_Bandeira}=       Convert To String     ${resp.json()['id']}
    Log to console                      ${ID_Api_Adicionar_Bandeira}
    Log to console                      Adicionar bandeira amex sem url via api
    ${resp}=	                        Delete Request        FastShop      /price-management/api/v1/flag/${ID_Api_Adicionar_Bandeira}         data=${payload}     headers=${headers}
    Log to console                      O Item ${ID_Api_Adicionar_Bandeira} foi deletado com sucesso.      
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}

#REALIZAR ADIÇÃO DA BANDEIRA ELO COM URL
Adicionar bandeira elo com url via api
    [Arguments]                         ${Authorizationapi}    ${Status_Code}    ${Nome}    ${De_Para_Legado}     ${URL_Imagem}
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json     {"name":"${Nome}","legacyFromTo":"${De_Para_Legado}","id":null,"urlImage":"${URL_Imagem}"}
    Log                                 ${payload}

    &{headers}=	                        Create Dictionary	Content-Type=application/json    Connection=keep-alive    Authorization=${Authorizationapi}

    ${resp}=	                        Post Request     FastShop      /price-management/api/v1/flag/         data=${payload}     headers=${headers}
    [return]                            ${resp}
    Log to console                      ${payload} 
    Log to console                      ${headers}
    Log to console                      ${resp} 
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}
    ${ID_Api_Adicionar_Bandeira}=       Convert To String     ${resp.json()['id']}
    Log to console                      ${ID_Api_Adicionar_Bandeira}
    Log to console                      Adicionar bandeira elo com url via api
    ${resp}=	                        Delete Request        FastShop      /price-management/api/v1/flag/${ID_Api_Adicionar_Bandeira}         data=${payload}     headers=${headers}
    Log to console                      O Item ${ID_Api_Adicionar_Bandeira} foi deletado com sucesso.      
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}

#REALIZAR ADIÇÃO DA BANDEIRA ELO SEM URL
Adicionar bandeira elo sem url via api
    [Arguments]                         ${Authorizationapi}    ${Status_Code}    ${Nome}    ${De_Para_Legado}
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json     {"name":"${Nome}","legacyFromTo":"${De_Para_Legado}","id":null,"urlImage":null}
    Log                                 ${payload}

    &{headers}=	                        Create Dictionary	Content-Type=application/json    Connection=keep-alive    Authorization=${Authorizationapi}

    ${resp}=	                        Post Request     FastShop      /price-management/api/v1/flag/         data=${payload}     headers=${headers}
    [return]                            ${resp}
    Log to console                      ${payload} 
    Log to console                      ${headers}
    Log to console                      ${resp} 
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}
    ${ID_Api_Adicionar_Bandeira}=       Convert To String     ${resp.json()['id']}
    Log to console                      ${ID_Api_Adicionar_Bandeira}
    Log to console                      Adicionar bandeira elo sem url via api
    ${resp}=	                        Delete Request        FastShop      /price-management/api/v1/flag/${ID_Api_Adicionar_Bandeira}         data=${payload}     headers=${headers}
    Log to console                      O Item ${ID_Api_Adicionar_Bandeira} foi deletado com sucesso.      
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}

#REALIZAR ADIÇÃO DA BANDEIRA DINERS COM URL
Adicionar bandeira diners com url via api
    [Arguments]                         ${Authorizationapi}    ${Status_Code}    ${Nome}    ${De_Para_Legado}     ${URL_Imagem}
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json     {"name":"${Nome}","legacyFromTo":"${De_Para_Legado}","id":null,"urlImage":"${URL_Imagem}"}
    Log                                 ${payload}

    &{headers}=	                        Create Dictionary	Content-Type=application/json    Connection=keep-alive    Authorization=${Authorizationapi}

    ${resp}=	                        Post Request     FastShop      /price-management/api/v1/flag/         data=${payload}     headers=${headers}
    [return]                            ${resp}
    Log to console                      ${payload} 
    Log to console                      ${headers}
    Log to console                      ${resp} 
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}
    ${ID_Api_Adicionar_Bandeira}=       Convert To String     ${resp.json()['id']}
    Log to console                      ${ID_Api_Adicionar_Bandeira}
    Log to console                      Adicionar bandeira diners com url via api
    ${resp}=	                        Delete Request        FastShop      /price-management/api/v1/flag/${ID_Api_Adicionar_Bandeira}         data=${payload}     headers=${headers}
    Log to console                      O Item ${ID_Api_Adicionar_Bandeira} foi deletado com sucesso.      
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}

#REALIZAR ADIÇÃO DA BANDEIRA DINERS SEM URL
Adicionar bandeira diners sem url via api
    [Arguments]                         ${Authorizationapi}    ${Status_Code}    ${Nome}    ${De_Para_Legado}
    Create Session	                    FastShop	        https://apiqa.fastshop.com.br
    ${payload}=                         Convert To Json     {"name":"${Nome}","legacyFromTo":"${De_Para_Legado}","id":null,"urlImage":null}
    Log                                 ${payload}

    &{headers}=	                        Create Dictionary	Content-Type=application/json    Connection=keep-alive    Authorization=${Authorizationapi}

    ${resp}=	                        Post Request     FastShop      /price-management/api/v1/flag/         data=${payload}     headers=${headers}
    [return]                            ${resp}
    Log to console                      ${payload} 
    Log to console                      ${headers}
    Log to console                      ${resp} 
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}
    ${ID_Api_Adicionar_Bandeira}=       Convert To String     ${resp.json()['id']}
    Log to console                      ${ID_Api_Adicionar_Bandeira}
    Log to console                      Adicionar bandeira diners sem url via api
    ${resp}=	                        Delete Request        FastShop      /price-management/api/v1/flag/${ID_Api_Adicionar_Bandeira}         data=${payload}     headers=${headers}
    Log to console                      O Item ${ID_Api_Adicionar_Bandeira} foi deletado com sucesso.      
    Should Be Equal As Strings          ${resp.status_code}   ${Status_Code}





#REALIZAR ADIÇÃO DE GRUPO DE TABELA DE VENDA COM UMA TABELA DE VENDA E COM PREÇO MÍNIMO DE VENDA
Adicionar grupo de tabela de venda com uma tabela e com preco minimo de venda
    [Arguments]                                 ${Authorizationapi}     ${Status_Code}      ${Nome}     ${Descricao}        ${Preco_Minimo_Venda}       ${Tabelas_De_Vendas}
    Create Session                              FastShop            https://apiqa.fastshop.com.br
    ${payload}=                                 Convert to Json      {"name":"${Nome}","id":null,"description":"${Descricao}","pmvRequired":${Preco_Minimo_Venda},"paymentGroupSalesTable":["${Tabelas_De_Vendas}"]}
    Log                                         ${payload}

    &{headers}=	                                Create Dictionary	Content-Type=application/json    Connection=keep-alive    Authorization=${Authorizationapi}

    ${resp}=                                    Post Request        FastShop        /price-management/api/v1/group-sales-table/     data=${payload}     headers=${headers}
    [return]                                    ${resp}
    Log to console                              ${payload}
    Log to console                              ${headers}
    Log to console                              ${resp}
    Should Be Equal As Strings                  ${resp.status_code}     ${Status_Code}
    ${ID_Api_Adicionar_Grupo_Tabela_Venda}=     Convert To String       ${resp.json()['id']}
    Log to console                              ${ID_Api_Adicionar_Grupo_Tabela_Venda}
    Log to console                              Grupo de tabela de venda com uma tabela e com PMV adicionado com sucesso via api.
    ${resp}=                                    Delete Request      FastShop        /price-management/api/v1/group-sales-table/${ID_Api_Adicionar_Grupo_Tabela_Venda}       data=${payload}     headers=${headers}
    Log to console                              O item ${ID_Api_Adicionar_Grupo_Tabela_Venda} foi deletado com sucesso.
    Should Be Equal As Strings                  ${resp.status_code}     ${Status_Code}

#REALIZAR ADIÇÃO DE GRUPO DE TABELA DE VENDA COM UMA TABELA DE VENDA E SEM PREÇO MÍNIMO DE VENDA
Adicionar grupo de tabela de venda com uma tabela e sem preco minimo de venda
    [Arguments]                                 ${Authorizationapi}     ${Status_Code}      ${Nome}     ${Descricao}        ${Preco_Minimo_Venda}       ${Tabelas_De_Vendas}
    Create Session                              FastShop            https://apiqa.fastshop.com.br
    ${payload}=                                 Convert to Json      {"name":"${Nome}","id":null,"description":"${Descricao}","pmvRequired":${Preco_Minimo_Venda},"paymentGroupSalesTable":["${Tabelas_De_Vendas}"]}
    Log                                         ${payload}

    &{headers}=                                 Create Dictionary       Content-Type=application/json       Connection=keep-alive       Authorization=${Authorizationapi}

    ${resp}=                                    Post Request        FastShop        /price-management/api/v1/group-sales-table/     data=${payload}     headers=${headers}
    [return]                                    ${resp}
    Log to console                              ${payload}
    Log to console                              ${headers}
    Log to console                              ${resp}
    Should Be Equal As Strings                  ${resp.status_code}     ${Status_Code}
    ${ID_Api_Adicionar_Grupo_Tabela_Venda}=     Convert To String       ${resp.json()['id']}
    Log to console                              ${ID_Api_Adicionar_Grupo_Tabela_Venda}
    Log to console                              Grupo de tabela de venda com uma tabela e sem PMV adicionado com sucesso via api.
    ${resp}=                                    Delete Request      FastShop        /price-management/api/v1/group-sales-table/${ID_Api_Adicionar_Grupo_Tabela_Venda}       data=${payload}     headers=${headers}
    Log to console                              O item ${ID_Api_Adicionar_Grupo_Tabela_Venda} foi deletado com sucesso.
    Should Be Equal As Strings                  ${resp.status_code}     ${Status_Code}

#REALIZAR ADIÇÃO DE GRUPO DE TABELA DE VENDA COM DUAS TABELAS DE VENDA E COM PREÇO MÍNIMO DE VENDA
Adicionar grupo de tabela de venda com duas tabelas e com preco minimo de venda
    [Arguments]                                 ${Authorizationapi}     ${Status_Code}      ${Nome}     ${Descricao}        ${Preco_Minimo_Venda}       ${Tabelas_De_Vendas}
    Create Session                              FastShop            https://apiqa.fastshop.com.br
    ${payload}=                                 Convert to Json      {"name":"${Nome}","id":null,"description":"${Descricao}","pmvRequired":${Preco_Minimo_Venda},"paymentGroupSalesTable":["${Tabelas_De_Vendas}"]}
    Log                                         ${payload}

    &{headers}=	                                Create Dictionary	Content-Type=application/json    Connection=keep-alive    Authorization=${Authorizationapi}

    ${resp}=                                    Post Request        FastShop        /price-management/api/v1/group-sales-table/     data=${payload}     headers=${headers}
    [return]                                    ${resp}
    Log to console                              ${payload}
    Log to console                              ${headers}
    Log to console                              ${resp}
    Should Be Equal As Strings                  ${resp.status_code}     ${Status_Code}
    ${ID_Api_Adicionar_Grupo_Tabela_Venda}=     Convert To String       ${resp.json()['id']}
    Log to console                              ${ID_Api_Adicionar_Grupo_Tabela_Venda}
    Log to console                              Grupo de tabela de venda com duas tabelas e com PMV adicionado com sucesso via api.
    ${resp}=                                    Delete Request      FastShop        /price-management/api/v1/group-sales-table/${ID_Api_Adicionar_Grupo_Tabela_Venda}       data=${payload}     headers=${headers}
    Log to console                              O item ${ID_Api_Adicionar_Grupo_Tabela_Venda} foi deletado com sucesso.
    Should Be Equal As Strings                  ${resp.status_code}     ${Status_Code}

#REALIZAR ADIÇÃO DE GRUPO DE TABELA DE VENDA COM DUAS TABELAS DE VENDA E SEM PREÇO MÍNIMO DE VENDA
Adicionar grupo de tabela de venda com duas tabelas e sem preco minimo de venda
    [Arguments]                                 ${Authorizationapi}     ${Status_Code}      ${Nome}     ${Descricao}        ${Preco_Minimo_Venda}       ${Tabelas_De_Vendas}
    Create Session                              FastShop            https://apiqa.fastshop.com.br
    ${payload}=                                 Convert to Json      {"name":"${Nome}","id":null,"description":"${Descricao}","pmvRequired":${Preco_Minimo_Venda},"paymentGroupSalesTable":["${Tabelas_De_Vendas}"]}
    Log                                         ${payload}

    &{headers}=                                 Create Dictionary       Content-Type=application/json       Connection=keep-alive       Authorization=${Authorizationapi}

    ${resp}=                                    Post Request        FastShop        /price-management/api/v1/group-sales-table/     data=${payload}     headers=${headers}
    [return]                                    ${resp}
    Log to console                              ${payload}
    Log to console                              ${headers}
    Log to console                              ${resp}
    Should Be Equal As Strings                  ${resp.status_code}     ${Status_Code}
    ${ID_Api_Adicionar_Grupo_Tabela_Venda}=     Convert To String       ${resp.json()['id']}
    Log to console                              ${ID_Api_Adicionar_Grupo_Tabela_Venda}
    Log to console                              Grupo de tabela de venda com duas tabelas e sem PMV adicionado com sucesso via api.
    ${resp}=                                    Delete Request      FastShop        /price-management/api/v1/group-sales-table/${ID_Api_Adicionar_Grupo_Tabela_Venda}       data=${payload}     headers=${headers}
    Log to console                              O item ${ID_Api_Adicionar_Grupo_Tabela_Venda} foi deletado com sucesso.
    Should Be Equal As Strings                  ${resp.status_code}     ${Status_Code}

#REALIZAR ADIÇÃO DE GRUPO DE TABELA DE VENDA COM TRÊS TABELAS DE VENDA E COM PREÇO MÍNIMO DE VENDA
Adicionar grupo de tabela de venda com tres tabelas e com preco minimo de venda
    [Arguments]                                 ${Authorizationapi}     ${Status_Code}      ${Nome}     ${Descricao}        ${Preco_Minimo_Venda}       ${Tabelas_De_Vendas}
    Create Session                              FastShop            https://apiqa.fastshop.com.br
    ${payload}=                                 Convert to Json      {"name":"${Nome}","id":null,"description":"${Descricao}","pmvRequired":${Preco_Minimo_Venda},"paymentGroupSalesTable":["${Tabelas_De_Vendas}"]}
    Log                                         ${payload}

    &{headers}=	                                Create Dictionary	Content-Type=application/json    Connection=keep-alive    Authorization=${Authorizationapi}

    ${resp}=                                    Post Request        FastShop        /price-management/api/v1/group-sales-table/     data=${payload}     headers=${headers}
    [return]                                    ${resp}
    Log to console                              ${payload}
    Log to console                              ${headers}
    Log to console                              ${resp}
    Should Be Equal As Strings                  ${resp.status_code}     ${Status_Code}
    ${ID_Api_Adicionar_Grupo_Tabela_Venda}=     Convert To String       ${resp.json()['id']}
    Log to console                              ${ID_Api_Adicionar_Grupo_Tabela_Venda}
    Log to console                              Grupo de tabela de venda com tres tabelas e com PMV adicionado com sucesso via api.
    ${resp}=                                    Delete Request      FastShop        /price-management/api/v1/group-sales-table/${ID_Api_Adicionar_Grupo_Tabela_Venda}       data=${payload}     headers=${headers}
    Log to console                              O item ${ID_Api_Adicionar_Grupo_Tabela_Venda} foi deletado com sucesso.
    Should Be Equal As Strings                  ${resp.status_code}     ${Status_Code}

#REALIZAR ADIÇÃO DE GRUPO DE TABELA DE VENDA COM TRÊS TABELAS DE VENDA E SEM PREÇO MÍNIMO DE VENDA
Adicionar grupo de tabela de venda com tres tabelas e sem preco minimo de venda
    [Arguments]                                 ${Authorizationapi}     ${Status_Code}      ${Nome}     ${Descricao}        ${Preco_Minimo_Venda}       ${Tabelas_De_Vendas}
    Create Session                              FastShop            https://apiqa.fastshop.com.br
    ${payload}=                                 Convert to Json      {"name":"${Nome}","id":null,"description":"${Descricao}","pmvRequired":${Preco_Minimo_Venda},"paymentGroupSalesTable":["${Tabelas_De_Vendas}"]}
    Log                                         ${payload}

    &{headers}=                                 Create Dictionary       Content-Type=application/json       Connection=keep-alive       Authorization=${Authorizationapi}

    ${resp}=                                    Post Request        FastShop        /price-management/api/v1/group-sales-table/     data=${payload}     headers=${headers}
    [return]                                    ${resp}
    Log to console                              ${payload}
    Log to console                              ${headers}
    Log to console                              ${resp}
    Should Be Equal As Strings                  ${resp.status_code}     ${Status_Code}
    ${ID_Api_Adicionar_Grupo_Tabela_Venda}=     Convert To String       ${resp.json()['id']}
    Log to console                              ${ID_Api_Adicionar_Grupo_Tabela_Venda}
    Log to console                              Grupo de tabela de venda com tres tabelas e sem PMV adicionado com sucesso via api.
    ${resp}=                                    Delete Request      FastShop        /price-management/api/v1/group-sales-table/${ID_Api_Adicionar_Grupo_Tabela_Venda}       data=${payload}     headers=${headers}
    Log to console                              O item ${ID_Api_Adicionar_Grupo_Tabela_Venda} foi deletado com sucesso.
    Should Be Equal As Strings                  ${resp.status_code}     ${Status_Code}





#REALIZAR ADIÇÃO DE UM IDENTIFICADOR DE LOJA VIRTUAL
Adicionar identificador de loja virtual
    [Arguments]                                     ${AuthorizationApi}     ${Status_Code_Adicionar}        ${Status_Code_Excluir}      ${Nome}     ${Descricao}
    Create Session                                  FastShop        https://apiqa.fastshop.com.br
    ${payload}=                                     Convert to Json     {"id":null,"name":"${Nome}","description":"${Descricao}"}
    Log                                             ${payload}

    &{headers}=                                     Create Dictionary       Content-Type=application/json       Connection=keep-alive       Authorization=${Authorizationapi}

    ${resp}=                                        Post Request        FastShop        /price-management/api/v1/identifier/        data=${payload}     headers=${headers}
    [return]                                        ${resp}
    Log to console                                  ${payload}
    Log to console                                  ${headers}
    Log to console                                  ${resp}
    Should Be Equal As Strings                      ${resp.status_code}     ${Status_Code_Adicionar}
    ${ID_Api_Adicionar_Identificador_LV}=           Convert To String       ${resp.json()['id']}
    Log to console                                  ${ID_Api_Adicionar_Identificador_LV}
    Log to console                                  Identificador de loja virtual adicionado com sucesso via api.
    ${resp}=                                        Delete Request      FastShop        /price-management/api/v1/identifier/${ID_Api_Adicionar_Identificador_LV}        data=${payload}     headers=${headers}
    Log to console                                  O item ${ID_Api_Adicionar_Identificador_LV} foi deletado com sucesso.
    Should Be Equal As Strings                      ${resp.status_code}     ${Status_Code_Excluir}





#REALIZAR UMA ADIÇÃO DE DE-PARA DE LOJA E TABELA DE VENDA COM NOME E TABELA IGUAIS COM DOIS NÚMEROS
Adicionar de-para de loja e tabela de venda com nome e tabela iguais com dois numeros
    [Arguments]                                     ${AuthorizationApi}     ${Status_Code_Adicionar}        ${Status_Code_Excluir}      ${Nome_Tabela}      ${Descricao}
    Create Session                                  FastShop       https://apiqa.fastshop.com.br
    ${payload}=                                     Convert to Json        {"id":null,"name":"${Nome_Tabela}","description":"${Descricao}","saleTable":"${Nome_Tabela}"}
    Log                                             ${payload}

    &{headers}=                                     Create Dictionary      Content-Type=application/json       Connection=keep-alive       Authorization=${AuthorizationApi}

    ${resp}=                                        Post Request       FastShop        /price-management/api/v1/store/     data=${payload}     headers=${headers}
    [return]                                        ${resp}
    Log to console                                  ${payload}
    Log to console                                  ${headers}
    Log to console                                  ${resp}
    Should Be Equal As Strings                      ${resp.status_code}     ${Status_Code_Adicionar}
    ${ID_Api_Adicionar_De_Para_Loja_Tabela}=        Convert To String      ${resp.json()['id']}
    Log to console                                  ${ID_Api_Adicionar_De_Para_Loja_Tabela}
    Log to console                                  De-para de loja e tabela de venda com nome e tabela iguais com dois numeros adiconado com sucesso.
    ${resp}=                                        Delete Request      Fastshop        /price-management/api/v1/store/${ID_Api_Adicionar_De_Para_Loja_Tabela}      data=${payload}     headers=${headers}
    Log to console                                  O item ${ID_Api_Adicionar_De_Para_Loja_Tabela} foi deletado com sucesso.
    Should Be Equal As Strings                      ${resp.status_code}     ${Status_Code_Excluir}

#REALIZAR UMA ADIÇÃO DE DE-PARA DE LOJA E TABELA DE VENDA COM NOME E TABELA IGUAIS COM DUAS LETRAS
Adicionar de-para de loja e tabela de venda com nome e tabela iguais com duas letras
    [Arguments]                                     ${AuthorizationApi}     ${Status_Code_Adicionar}        ${Status_Code_Excluir}      ${Nome_Tabela}      ${Descricao}
    Create Session                                  FastShop       https://apiqa.fastshop.com.br
    ${payload}=                                     Convert to Json        {"id":null,"name":"${Nome_Tabela}","description":"${Descricao}","saleTable":"${Nome_Tabela}"}
    Log                                             ${payload}

    &{headers}=                                     Create Dictionary      Content-Type=application/json       Connection=keep-alive       Authorization=${AuthorizationApi}

    ${resp}=                                        Post Request       FastShop        /price-management/api/v1/store/     data=${payload}     headers=${headers}
    [return]                                        ${resp}
    Log to console                                  ${payload}
    Log to console                                  ${headers}
    Log to console                                  ${resp}
    Should Be Equal As Strings                      ${resp.status_code}     ${Status_Code_Adicionar}
    ${ID_Api_Adicionar_De_Para_Loja_Tabela}=        Convert To String      ${resp.json()['id']}
    Log to console                                  ${ID_Api_Adicionar_De_Para_Loja_Tabela}
    Log to console                                  De-para de loja e tabela de venda com nome e tabela iguais com duas letras adiconado com sucesso.
    ${resp}=                                        Delete Request      Fastshop        /price-management/api/v1/store/${ID_Api_Adicionar_De_Para_Loja_Tabela}      data=${payload}     headers=${headers}
    Log to console                                  O item ${ID_Api_Adicionar_De_Para_Loja_Tabela} foi deletado com sucesso.
    Should Be Equal As Strings                      ${resp.status_code}     ${Status_Code_Excluir}

#REALIZAR UMA ADIÇÃO DE DE-PARA DE LOJA E TABELA DE VENDA COM NOME E TABELA IGUAIS COM NÚMERO E LETRA
Adicionar de-para de loja e tabela de venda com nome e tabela iguais com numero e letra
    [Arguments]                                     ${AuthorizationApi}     ${Status_Code_Adicionar}        ${Status_Code_Excluir}      ${Nome_Tabela}      ${Descricao}
    Create Session                                  FastShop       https://apiqa.fastshop.com.br
    ${payload}=                                     Convert to Json        {"id":null,"name":"${Nome_Tabela}","description":"${Descricao}","saleTable":"${Nome_Tabela}"}
    Log                                             ${payload}

    &{headers}=                                     Create Dictionary      Content-Type=application/json       Connection=keep-alive       Authorization=${AuthorizationApi}

    ${resp}=                                        Post Request       FastShop        /price-management/api/v1/store/     data=${payload}     headers=${headers}
    [return]                                        ${resp}
    Log to console                                  ${payload}
    Log to console                                  ${headers}
    Log to console                                  ${resp}
    Should Be Equal As Strings                      ${resp.status_code}     ${Status_Code_Adicionar}
    ${ID_Api_Adicionar_De_Para_Loja_Tabela}=        Convert To String      ${resp.json()['id']}
    Log to console                                  ${ID_Api_Adicionar_De_Para_Loja_Tabela}
    Log to console                                  De-para de loja e tabela de venda com nome e tabela iguais com numero e letra adiconado com sucesso.
    ${resp}=                                        Delete Request      Fastshop        /price-management/api/v1/store/${ID_Api_Adicionar_De_Para_Loja_Tabela}      data=${payload}     headers=${headers}
    Log to console                                  O item ${ID_Api_Adicionar_De_Para_Loja_Tabela} foi deletado com sucesso.
    Should Be Equal As Strings                      ${resp.status_code}     ${Status_Code_Excluir}

#REALIZAR UMA ADIÇÃO DE DE-PARA DE LOJA E TABELA DE VENDA COM NOME E TABELA IGUAIS COM LETRA E NÚMERO
Adicionar de-para de loja e tabela de venda com nome e tabela iguais com letra e numero
    [Arguments]                                     ${AuthorizationApi}     ${Status_Code_Adicionar}        ${Status_Code_Excluir}      ${Nome_Tabela}      ${Descricao}
    Create Session                                  FastShop       https://apiqa.fastshop.com.br
    ${payload}=                                     Convert to Json        {"id":null,"name":"${Nome_Tabela}","description":"${Descricao}","saleTable":"${Nome_Tabela}"}
    Log                                             ${payload}

    &{headers}=                                     Create Dictionary      Content-Type=application/json       Connection=keep-alive       Authorization=${AuthorizationApi}

    ${resp}=                                        Post Request       FastShop        /price-management/api/v1/store/     data=${payload}     headers=${headers}
    [return]                                        ${resp}
    Log to console                                  ${payload}
    Log to console                                  ${headers}
    Log to console                                  ${resp}
    Should Be Equal As Strings                      ${resp.status_code}     ${Status_Code_Adicionar}
    ${ID_Api_Adicionar_De_Para_Loja_Tabela}=        Convert To String      ${resp.json()['id']}
    Log to console                                  ${ID_Api_Adicionar_De_Para_Loja_Tabela}
    Log to console                                  De-para de loja e tabela de venda com nome e tabela iguais com letra e numero adiconado com sucesso.
    ${resp}=                                        Delete Request      Fastshop        /price-management/api/v1/store/${ID_Api_Adicionar_De_Para_Loja_Tabela}      data=${payload}     headers=${headers}
    Log to console                                  O item ${ID_Api_Adicionar_De_Para_Loja_Tabela} foi deletado com sucesso.
    Should Be Equal As Strings                      ${resp.status_code}     ${Status_Code_Excluir}

#REALIZAR UMA ADIÇÃO DE DE-PARA DE LOJA E TABELA DE VENDA COM TABELA DE VENDA COM DOIS NÚMEROS
Adicionar de-para de loja e tabela de venda com tabela de venda com dois numeros
    [Arguments]                                     ${AuthorizationApi}     ${Status_Code_Adicionar}        ${Status_Code_Excluir}      ${Nome}     ${Descricao}        ${Nome_Tabela}
    Create Session                                  FastShop       https://apiqa.fastshop.com.br
    ${payload}=                                     Convert to Json        {"id":null,"name":"${Nome}","description":"${Descricao}","saleTable":"${Nome_Tabela}"}
    Log                                             ${payload}

    &{headers}=                                     Create Dictionary      Content-Type=application/json       Connection=keep-alive       Authorization=${AuthorizationApi}

    ${resp}=                                        Post Request       FastShop        /price-management/api/v1/store/     data=${payload}     headers=${headers}
    [return]                                        ${resp}
    Log to console                                  ${payload}
    Log to console                                  ${headers}
    Log to console                                  ${resp}
    Should Be Equal As Strings                      ${resp.status_code}     ${Status_Code_Adicionar}
    ${ID_Api_Adicionar_De_Para_Loja_Tabela}=        Convert To String      ${resp.json()['id']}
    Log to console                                  ${ID_Api_Adicionar_De_Para_Loja_Tabela}
    Log to console                                  De-para de loja e tabela de venda com tabela de venda com dois numeros adiconado com sucesso.
    ${resp}=                                        Delete Request      Fastshop        /price-management/api/v1/store/${ID_Api_Adicionar_De_Para_Loja_Tabela}      data=${payload}     headers=${headers}
    Log to console                                  O item ${ID_Api_Adicionar_De_Para_Loja_Tabela} foi deletado com sucesso.
    Should Be Equal As Strings                      ${resp.status_code}     ${Status_Code_Excluir}

#REALIZAR UMA ADIÇÃO DE DE-PARA DE LOJA E TABELA DE VENDA COM TABELA DE VENDA COM DUAS LETRAS
Adicionar de-para de loja e tabela de venda com tabela de venda com duas letras
    [Arguments]                                     ${AuthorizationApi}     ${Status_Code_Adicionar}        ${Status_Code_Excluir}      ${Nome}     ${Descricao}        ${Nome_Tabela}
    Create Session                                  FastShop       https://apiqa.fastshop.com.br
    ${payload}=                                     Convert to Json        {"id":null,"name":"${Nome}","description":"${Descricao}","saleTable":"${Nome_Tabela}"}
    Log                                             ${payload}

    &{headers}=                                     Create Dictionary      Content-Type=application/json       Connection=keep-alive       Authorization=${AuthorizationApi}

    ${resp}=                                        Post Request       FastShop        /price-management/api/v1/store/     data=${payload}     headers=${headers}
    [return]                                        ${resp}
    Log to console                                  ${payload}
    Log to console                                  ${headers}
    Log to console                                  ${resp}
    Should Be Equal As Strings                      ${resp.status_code}     ${Status_Code_Adicionar}
    ${ID_Api_Adicionar_De_Para_Loja_Tabela}=        Convert To String      ${resp.json()['id']}
    Log to console                                  ${ID_Api_Adicionar_De_Para_Loja_Tabela}
    Log to console                                  De-para de loja e tabela de venda com tabela de venda com duas letras adiconado com sucesso.
    ${resp}=                                        Delete Request      Fastshop        /price-management/api/v1/store/${ID_Api_Adicionar_De_Para_Loja_Tabela}      data=${payload}     headers=${headers}
    Log to console                                  O item ${ID_Api_Adicionar_De_Para_Loja_Tabela} foi deletado com sucesso.
    Should Be Equal As Strings                      ${resp.status_code}     ${Status_Code_Excluir}

#REALIZAR UMA ADIÇÃO DE DE-PARA DE LOJA E TABELA DE VENDA COM TABELA DE VENDA COM NÚMERO E LETRA
Adicionar de-para de loja e tabela de venda com tabela de venda com numero e letra
    [Arguments]                                     ${AuthorizationApi}     ${Status_Code_Adicionar}        ${Status_Code_Excluir}      ${Nome}     ${Descricao}        ${Nome_Tabela}
    Create Session                                  FastShop       https://apiqa.fastshop.com.br
    ${payload}=                                     Convert to Json        {"id":null,"name":"${Nome}","description":"${Descricao}","saleTable":"${Nome_Tabela}"}
    Log                                             ${payload}

    &{headers}=                                     Create Dictionary      Content-Type=application/json       Connection=keep-alive       Authorization=${AuthorizationApi}

    ${resp}=                                        Post Request       FastShop        /price-management/api/v1/store/     data=${payload}     headers=${headers}
    [return]                                        ${resp}
    Log to console                                  ${payload}
    Log to console                                  ${headers}
    Log to console                                  ${resp}
    Should Be Equal As Strings                      ${resp.status_code}     ${Status_Code_Adicionar}
    ${ID_Api_Adicionar_De_Para_Loja_Tabela}=        Convert To String      ${resp.json()['id']}
    Log to console                                  ${ID_Api_Adicionar_De_Para_Loja_Tabela}
    Log to console                                  De-para de loja e tabela de venda com tabela de venda com numero e letra adiconado com sucesso.
    ${resp}=                                        Delete Request      Fastshop        /price-management/api/v1/store/${ID_Api_Adicionar_De_Para_Loja_Tabela}      data=${payload}     headers=${headers}
    Log to console                                  O item ${ID_Api_Adicionar_De_Para_Loja_Tabela} foi deletado com sucesso.
    Should Be Equal As Strings                      ${resp.status_code}     ${Status_Code_Excluir}

#REALIZAR UMA ADIÇÃO DE DE-PARA DE LOJA E TABELA DE VENDA COM TABELA DE VENDA COM LETRA E NÚMERO
Adicionar de-para de loja e tabela de venda com tabela de venda com letra e numero
    [Arguments]                                     ${AuthorizationApi}     ${Status_Code_Adicionar}        ${Status_Code_Excluir}      ${Nome}     ${Descricao}        ${Nome_Tabela}
    Create Session                                  FastShop       https://apiqa.fastshop.com.br
    ${payload}=                                     Convert to Json        {"id":null,"name":"${Nome}","description":"${Descricao}","saleTable":"${Nome_Tabela}"}
    Log                                             ${payload}

    &{headers}=                                     Create Dictionary      Content-Type=application/json       Connection=keep-alive       Authorization=${AuthorizationApi}

    ${resp}=                                        Post Request       FastShop        /price-management/api/v1/store/     data=${payload}     headers=${headers}
    [return]                                        ${resp}
    Log to console                                  ${payload}
    Log to console                                  ${headers}
    Log to console                                  ${resp}
    Should Be Equal As Strings                      ${resp.status_code}     ${Status_Code_Adicionar}
    ${ID_Api_Adicionar_De_Para_Loja_Tabela}=        Convert To String      ${resp.json()['id']}
    Log to console                                  ${ID_Api_Adicionar_De_Para_Loja_Tabela}
    Log to console                                  De-para de loja e tabela de venda com tabela de venda com letra e numero adiconado com sucesso.
    ${resp}=                                        Delete Request      Fastshop        /price-management/api/v1/store/${ID_Api_Adicionar_De_Para_Loja_Tabela}      data=${payload}     headers=${headers}
    Log to console                                  O item ${ID_Api_Adicionar_De_Para_Loja_Tabela} foi deletado com sucesso.
    Should Be Equal As Strings                      ${resp.status_code}     ${Status_Code_Excluir}





#REALIZAR UMA ADIÇÃO DE TIPO DE PAGAMENTO COM PERMITE DAR DESTAQUE NA PROMOÇÃO ATIVO E PAGAMENTO FATURADO
Verificar disponibilidade do Sku via api
    [Arguments]                         ${Sku_Produto_Regionalizado}    ${Cep_do_Cliente}    ${Status_Code}   
    Create Session	                    FastShop	        https://api.fastshop.com.br
    ${payload}=                         Convert To Json     {}
    Log                                 ${payload}

    &{headers}=	                        Create Dictionary	Content-Type=application/json    Connection=keep-alive


    ${resp}=	                        Get Request     FastShop      /regionalized-catalog/v1/catalog/byPartNumber?partNumber=${Sku_Produto_Regionalizado}&zipCode=${Cep_do_Cliente}         data=${payload}     headers=${headers}
    Log to console                      **************** API RETORNOU STATUS: '${resp.status_code}'
    Should Be Equal As Strings          ${resp.status_code}         ${Status_Code}
    ${Resultado_Regionalizado}=         Convert To String           ${resp.json()['regionalizedResult']}
    ${Market_Place}=                    Convert To String           ${resp.json()['marketPlace']}
    ${Market_Place_Texto}=              Convert To String           ${resp.json()['marketPlaceText']}
    ${Disponivel_Regiao}=               Convert To String           ${resp.json()['isRegionAvailable']}    
    ${Compra_restrita}=                 Convert To String           ${resp.json()['isSaleRestrictionToBuy']}
    ${Visualizar_Compra_restrita}=      Convert To String           ${resp.json()['isSaleRestrictionToView']}
    ${Posa}=                            Convert To String           ${resp.json()['isPosa']}
    Log to console                      **************** Resultado_Regionalizado: '${Resultado_Regionalizado}' 
    Log to console                      **************** Disponivel_Regiao: '${Disponivel_Regiao}' 
    Should Be Equal As Strings          ${resp.status_code}          ${Status_Code}
    Should Be Equal As Strings          ${Resultado_Regionalizado}   ${Status_Resultado_Regionalizado}
    Should Be Equal As Strings          ${Disponivel_Regiao}         ${Status_Disponivel_Regiao}
    Log to console                      """"""""""""""""" Verificar API do Shipping
    ${resp}=	                        Get Request                  FastShop      /public/api/shipping/product?sku=${Sku_Produto_Regionalizado}&zipCode=${Cep_do_Cliente}&primePlanId=&price=100         data=${payload}     headers=${headers}
    Should Be Equal As Strings          ${resp.status_code}          ${Status_Code}   
    ${Data_Base}=                       Convert To String            ${resp.json()['dateBase']}
    ${Cidade_De_Entrega}=               Convert To String            ${resp.json()['deliveryCity']}
    ${Estado_De_Entrega}=               Convert To String            ${resp.json()['deliveryState']}
    ${Status_De_Erro}=                  Convert To String            ${resp.json()['errorCode']}    
    ${Mensagem_De_Erro}=                Convert To String            ${resp.json()['errorMessage']}
    ${Log_Shipping}=                    Convert To String            ${resp.json()['logs']}
    Log to console                      **************** Entrega na Cidade: '${Cidade_De_Entrega}' 
    Log to console                      **************** Mensagem de Erro: '${Mensagem_De_Erro}' 
    Should Be Equal As Strings          ${resp.status_code}          ${Status_Code}
    Log to console                      **************** API RETORNOU STATUS: ${resp.status_code}
