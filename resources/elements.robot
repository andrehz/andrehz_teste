*** Settings ***
Resource         ../resources/setup_hz.robot

*** Variables ***
#HOME
${ID_INBOX_BUSCA_HOME}                                id:search-box-id
${ID_BUTTON_ICONE_CARRINHO_HOME}                      //div[@class='cart-counter']  #id:cart-link-desk
${ID_BUTTON_ICONE_FAVORITOS_HOME}                     //div[@class='favorite-counter']   #id:favorite-link-desk
${ID_DIV_INFERIOR_DA_HOME_RODAPE}                     //div[@class='new-footer']    #id:ruleFoot
${ID_ICONE_SELO_PIX}                                  id:auto-selo-pix
${ID_ICONE_SELO_SECTIGO}                              id:auto-selo-sectigo
${ID_ICONE_PAGAMENTO_VISA_HOME}                       id:auto-visa
${ID_ICONE_PAGAMENTO_MASTER_HOME}                     id:auto-mastercard
${ID_ICONE_PAGAMENTO_AMEX_HOME}                       id:auto-american-express
${ID_ICONE_PAGAMENTO_DINNERS_HOME}                    id:auto-diners-club
${ID_ICONE_PAGAMENTO_ELO_HOME}                        id:auto-elo
${ID_ICONE_PAGAMENTO_BOLETO_HOME}                     id:auto-boleto
${ID_ICONE_PAGAMENTO_PIX_HOME}                        id:auto-selo-pix
${ID_ICONE_HOME_APP_GOOGLE_PLAY}                      id:auto-google-play
${ID_ICONE_HOME_APP_STORE}                            id:auto-app-atore
${ID_ICONE_SELO_EBIT_HOME}                            id:auto-selo-ebit
${ID_ICONE_SELO_RECLAME_AQUI_HOME}                    id:auto-selo-reclame-aqui
${ID_ICONE_SELO_COMPRE_E_CONFIE_HOME}                 id:auto-selo-compre-confie

#BUSCA PRODUTO
${ID_ACEITAR_COOKIES}                                 id:onetrust-accept-btn-handler
${INPUT_BUSCA_PRODUTO}                                id:inpHeaderSearch
${SPAN_LUPA_BUSCA_PRODUTO}                            id:btnHeaderSearch
${BUTTON_SELECIONAR_SERVICO}                          //button[contains(text(), 'Selecionar')]
${H2_NOME_DO_PRODUTO}                             	  //div[@class='info-subtitle']//..//span     #id:deliveredBy
${H1_NOME_DO_PRODUTO}                                 //div[@class='content']//..//h1
${A_ADICIONAR_NOVO_ENDERECO}                          //button[@class='btn btn-new-address']  
${P_NAO_SEI_MEU_ENDERECO}                             //p[contains(text(), 'Não sei meu CEP')]
${INPUT_CEP}                                          //input[@formcontrolname='zipcode']
${BUTTON_APLICAR_CEP}                                 //button[@type='submit']
${BUTTON_CONHECA_NOSSO_FRETE}                         //button[contains(text(), 'Conheça nosso frete')]
${INPUT_DIGITE_SEU_CEP}                               //input[@placeholder='Digite seu CEP']
${ID_ICONE_ADICIONAR_AO_CARRINHO}                     id:cart-link-desk     #id:addToCartIcon       
${BUTTON_ADICIONAR_AO_CARRINHO}                       id:addToCart      #//button[contains(text(), 'Adicionar ao carrinho')]
${SPAN_ADICIONAR_AO_CARRINHO}                         //span[contains(text(), 'Adicionar ao carrinho')]
${SPAN_CUPOM_CARRINHO}                                //span[@class='coupon-placeholder']
${BUTTON_CONTINUAR_COMPRA_VERDE}                      (//span[contains(text(), ' Subtotal ')]//..//button[contains(text(), 'Continuar compra')])[last()]
${BUTTON_CONTINUAR_VERDE_ENDERECO}                    (//button[contains(text(), 'Continuar')])[last()]   
${DIV_QUANDO_RECEBER}                                 //h1[contains(text(), 'Quando deseja receber?')]  
${H1_ADICIONAR_SERVICO}                               //h1[contains(text(), 'Deseja adicionar algum serviço?')] 
${H1_COMO_DESEJA_PAGAR}                               //h1[contains(text(), 'Como deseja pagar?')] 
${DIV_ADD_CARTAO_DE_CREDITO}                          //div[contains(text(), ' Adicionar Cartão de Crédito ')] 
${LABEL_PAGAR_VIA_BOLETO}                             //div[contains(text(), 'Boleto')]//..//..//label
${LABEL_PAGAR_VIA_PIX}                                //div[contains(text(), 'Boleto')]//..//..//label
${LABEL_PAGAR_VIA_LISTA_DE_CASAMENTO}                 //div[contains(text(), 'Crédito de Lista de Casamento')]//..//..//label
${LABEL_PAGAR_VIA_CARTAO_DE_CREDITO}                  //div[contains(text(), 'Cartão de Crédito')]//..//..//label
${LABEL_PAGAR_COM_DOIS_CARTOES_DE_CREDITO}            //div[contains(text(), ' 2 Cartões de Crédito ')]//..//..//label
${BUTTON_CONTINUAR_VERDE_DADOS_PESSOAIS}              (//button[contains(text(), 'Continuar')])[last()]   
${BUTTON_CONTINUAR_VERDE_FRETE}                       (//button[contains(text(), 'Continuar')])[last()]
${BUTTON_CONTINUAR_VERDE_SERVICOS}                    (//button[contains(text(), 'Continuar')])[last()]  
${BUTTON_CONTINUAR_VERDE_PAGAMENTO}                   (//button[contains(text(), 'Continuar')])[last()] 
${BUTTON_CONTINUAR_VERDE_ADD_CARTAO}                  (//button[contains(text(), 'Continuar')])[last()]  
${BUTTON_CONTINUAR_VERDE_FINALIZAR_COMPRA}            (//button[contains(text(), 'Finalizar compra')])[last()] 
${BUTTON_CONTINUAR_VERDE_CONTINUAR_PARCELAMENTO}      (//button[contains(text(), 'Continuar para parcelamento')])[last()] 
${DIV_DESCRICAO_DO_PAGAMENTO}                         //div[@class='invoice-description']
${CARRINHO_URL}                                       http://webapp2-qa.fastshop.com.br/web/checkout-v2/carrinho  
${DIV_COMPRA_CONCLUIDA}                               //div[contains(text(), 'Você receberá no e-mail')]  
${A_BOTÃO_COMPRA_CONCLUIDA_OK_ENTENDI}                //a[@class='btn btn-success']
${DIV_EXCLUIR_FAVORITOS}                              //div[@class='delete icon-trash theme-third-color']        #//div[@class='delete']  
${DIV_EXCLUIR_PRODUTO}                                //div[@class='remove icon-trash theme-third-color']        #//div[@class='remove']  
${P_CARRINHO_VAZIO}                                   //p[@class='alert-text'][contains(text(), 'Seu carrinho está vazio.')]
${DIV_NOME_DO_PRODUTO}                                //div[@class='row']//span[1]
${LABEL_PERIODO_DO_SEGURO}                            //span[contains(text(), '+')]//..//..//label  
${BUTTON_APLICAR_SEGURO}                              //button[contains(text(), 'APLICAR')]        
${DIV_LOADER_ESPERA}                                  //div[@class='loader']
${DIV_BUSCA_PRODUTO_INFORMACOES_PRECO}                //div[@class='grid-box-descripion']
${DIV_MOVER_FAVORITOS}                                //div[@class='sendToWishlist icon-send-to-favorite theme-third-color']
${SPAN_FECHAR_EBIT}                                    id:btFechar

#MINHA ASSINATURA
${ID_SAIBA_MAIS_MINHA_ASSINATURA}                     id:auto-benefits-button
${DIV_CLASS_BENEFICIOS_PRIME}                         //div[@class='benefits']
${DIV_CLASS_NOTIFICACAO_NAO_PRIME}                    //div[@class='notification']

#CATÁLOGO REGIONALIZADO
${ID_HOME_INFORME_SEU_CEP}                            //div[@class='circle-icon']  #id:auto-user-region-no-logged-desk
${ID_IMPUT_PROCURAR_CEP}                              id:auto-my-address-v2-zipcode
${ID_CEP_CONFIRMAR_BUSCA}                             id:auto-apply-zipcode
${ID_CEP_NAO_SEI_MEU_CEP}                             id:auto-my-address-v2-search-zipcode
${ID_CEP_FACA_SEU_LOGIN}                              id:auto-go-to-login
${ID_IMPUT_NOME_DA_RUA_NAO_SEI_CEP}                   id:auto-search-address-street-name
${ID_IMPUT_NOME_DA_CIDADE_NAO_SEI_CEP}                id:auto-search-address-city
${ID_IMPUT_NOME_DO_ESTADO_NAO_SEI_CEP}                id:auto-search-address-state
${ID_CEP_CONFIRMAR_BUSCA_NAO_SEI_MEU_CEP}             id:auto-search-address-btn-search
${ID_CONFIRMA_ENDERECO_BUSCA_NAO_SEI_CEP}             id:auto-search-address-text
${ID_NAO_SEI_CEP_CONFIRMAR_BUSCA}                     id:auto-search-address-btn-apply
${ID_CEP_EXIBIDO_AO_CLIENTE_CR}                       id:auto-user-region-description-desk
${ID_DIV_CEP_NAO_LOCALIZADO}                          //div[contains(text(), 'CEP não localizado.')]   
${P_AVISO_DE_CEP_NAO_ENCONTRADO}                      //p[@class='error-message ng-star-inserted']

#RESET DE SENHA
${DIV_ESQUECI_MINHA_SENHA_NO_LOGIN}                   //div[@class='forgotPassword-link']
${ID_EMAIL_TEMPORARIO}                                id:email-input
${BUTTON_VERDE_CONTINUAR_RECUPERAR_SENHA}             //button[@class='btn-green recover']
${LABEL_BUTTON_EMAIL_RECUPERAR_SENHA}                 //label[@for='e-mail']
${BUTTON_VERDE_RECUPERAR_SENHA}                       //button[@class='btn-green recover']
${DIV_CONFIRMAÇÃO_DE_ENVIO_EMAIL_SENHA}               //div[@class='btn-to-go ng-star-inserted'] 
${TD_EMAIL_DE_RECUPERAR_A_SENHA}                      //td[contains(text(), 'FastShop')]
${A_CONTEUDO_URL_EMAIL_RECUPERAR_A_SENHA}             //a[contains(text(), 'www.fastshop.com.br')]   
${ID_RECUPERAR_SENHA}                                 id:password
${ID_RECUPERAR_SENHA_CONFIRMAÇÃO}                     id:password_confirmation
${BUTTON_CONFIRMAR_VERDE}                             //button[contains(text(), 'Continuar')]    #//button[@class='btn btn-green']   
${BUTTON_CONFIRMAR_VERDE_CLICK}                       //button[contains(text(), 'ENTRAR')]    #//button[@class='btn btn-green']   

#PDP
${ID_ICONE_CHATBLA_PDP}                               id:auto-talk-seller  
${P_MENSAGEM_CHATBLA_PDP}                             //P[@class='description-talk']
${ID_HOTSITE_CHATBLA_APP}                             id:_landing_app_redirect
${ID_BOTAO_CLICK_COLLECTION_PDP}                      id:auto-click-collect-button  
${ID_AUMENTAR_A_QUANTIDADE_PDP}                       id:auto-add-product-quantity
${ID_COMPRAR_AGORA}                                   id:buyNow
${DIV_PRECO_PRODUTO_PDP}                              (//div[@class='content'])[last()]
${DIV_MODAL_RETIRAR_EM_LOJA}                          //div[@class='modal-content']   
${DIV_CEP_MODAL_RETIRAR_EM_LOJA}                      //div[contains(text(), 'CEP')]
${DIV_CIDADE_MODAL_RETIRAR_EM_LOJA}                   //div[contains(text(), 'Cidade')]
${SELECT_BUSCA_ESTADO_CLICK_CLOCTION}                 //select[@formcontrolname='state']
${SELECT_BUSCA_CIDADE_CLICK_CLOCTION}                 //select[@formcontrolname='city']
${BOTÃO_CONFIRMAR_BUSCA_ESTADO_CLICK_COLLECTION}      //button[contains(text(), 'Continuar')]
${LABEL_CEP_BUSCA_RETIRAR_EM_LOJA}                    //span[contains(text(), 'CEP')]
${IMPUT_CEP_CLICK_COLLECTION}                         //input[@type='tel'] 
${I_ICONE_VERIFICAÇÃO_VERDE_RETIRAR_EM_LOJA}          //i[@class='validation-icon']   
${DIV_PRODUTO_DISPONIVEL_EM_LOJA_CICK_COLLECTON}      //div[contains(text(), ' Disponível ')]
${BUTTON_APLICAR_CICK_COLLECTON}                      //button[contains(text(), 'Aplicar')]
${BUTTON_CONTINUAR_MODAL_INFO_CICK_COLLECTON}         //button[contains(text(), 'Continuar')]

#PAGAMENTO
${DIV_NOME_DO_TITULAR_DO_CARTAO}                      //div[contains(text(), 'NOME DO TITULAR')]  
${INPUT_NUMERO_CARTAO_DE_CREDITO}                     id:cardNumber                     
${INPUT_NOME_TITULAR_DO_CARTAO}                       id:holderName
${INPUT_VALIDADE_DO_CARTAO}                           id:expireDate
${INPUT_CPF_TITULAR_CARTAO}                           id:document    
${DIV_PAGAMENTO_CARTAO_JUROS}                         //div[contains(text(), ' Juros de')] 
${DIV_PARCELAMENTO_CAARTAO_DE_CREDITO}                id:optionSelected
${DIV_PAGAMENTO_PARCELAMENTO}                         //div[@class='select-items'] 
${DIV_PAGAMENTO_CVV}                                  //input[@formcontrolname='cvv']
${INPUT_CVV}                                          id:cvv
${LABEL_PAGAMENTO_PARCELADO}                          //div[@class='box-virtual-select']
${DIV_PAGAMENTO_LISTA_1x_NO_CARTÃO}                   //div[contains(text(), '1x no cartão')]  #//div[contains(text(), 'À vista')]  
${DIV_PAGAMENTO_LISTA_A_VISTA}                        //div[contains(text(), 'À vista')]
${DIV_PAGAMENTO_PARCELADO}                            //div[contains(text(), '3x iguais*')]  
${DIV_ICONE_CONDICAO_ESPECIAL}                        //div[contains(text(), 'Condição especial')]  
${DIV_PAGAMENTO_CONDICAO_ESPECIAL_1x}                 //div[contains(text(), '1x')]             
${DIV_PAGAMENTO_LISTA_1x}                             //div[contains(text(), '1x')]   
${DIV_PAGAMENTO_LISTA_2x}                             //div[contains(text(), '2x iguais')]    
${DIV_PAGAMENTO_LISTA_3x}                             //div[contains(text(), '3x iguais')]  
${DIV_NUMERO_DA_ORDEM_DE_COMPRA}                      //div[@class='order-number']

#PIX
${ID_IMAGE_LOGOPIX_PAGAMENTO}                         id:pixLogoImage
${ID_IMAGE_NOTA_DINHEIRO_ICONE}                       //img[@id='pixLogoImage']//..//..//..//img[@id='signimage']
${RADIO_BUTTON_PIX}                                   //img[@id='pixLogoImage']//..//..//..//label
${DIV_DESCRICAO_DA_COMPRA_COM_PIX}                    //div[@class='main-description']
${DIV_RESUMO_FORMA_DE_PAGAMENTO_PIX}                  //div[@class='summary payment-section']//div[contains(text(), ' Pix ')] 
${ID_LOGO_PIX_TELA_PAGAMENTO}                         id:logoPix
${ID_ICONE_CIFRAO_TELA_PAGAMENTO}                     id:money
${SPAN_CONDICAO_ESPECIAL_COM_PIX}                     //span[@id='tagPromotionValue'][contains(text(), 'no pix')] 
${DIV_COMPRA_FINALIZADA_SUCESSO_PIX}                  //div[@class='highlight']
${ID_CHAVE_PIX}                                       id:pixKey
${ID_QR_CODE}                                         id:qrCode
${ID_BOTAO_COPIAR_CHAVE_PIX}                          id:copyPixKey
${DIV_NOTIFICAÇÃO_CHAVE_COPIADA_COM_SUCESSO}          //div[@class='notification']
${DIV_CAMPO_CHAVE_PIX}                                //div[@class='pix']

#BOLETO
${RADIO_BUTTON_BOLETO}                                //img[@id='pixLogoImage']//..//..//..//label
${DIV_TELA_CONFIRMACAO_BOLETO}                        //div[@class='invoice-title'][contains(text(), 'Boleto')] 
${DIV_NUMERO_CODIGO_BARRAS_BOLETO}                    id:auto-invoice-data    #//div[@class='number']
${DIV_CAMPO_NUMERO_CODIGO_BARRAS_BOLETO}              //div[@class='invoice-code']

#2 cartões
${LABEL_PAGAR_DOIS_CARTOES_DE_CREDITO}                //div[contains(text(), '2 Cartões de Crédito')]//..//..//label
${LABEL_VALOR_PRIMEIRO_CARTAO}                        //input[@formcontrolname='valueToPay']
${DIV_SEGUNDO_CARTAO}                                 //div[@class='subtitle'][contains(text(), ' 2° cartão ')] 
${BUTTON_IR_PARA_O_SEGUNDO_CARTAO}                    (//button[contains(text(), 'Ir para o 2° cartão')])[last()]
#Lista de casamento
${INPUT_VALOR_DA_LISTA}                                //input[@formcontrolname='valueToPay']
${SPAN_VALOR_SUBTOTAL}                                 //span[@class='subtotal-value']

#CUPOM
${INPUT_CUPOM_CARRINHO}                                //input[@formcontrolname='code']
${BUTTON_CONFIRMAR_CUPOM_CARRINHO}                     id:coupon_btn
${P_MENSAGEM_CUPOM_ADICIONADO_COM_SUCESSO}             //div[@class='notification']/./p[contains(text(), 'Cupom aplicado aos produtos elegíveis.')] 
${DIV_ICONE_DELETAR_CUPOM}                             //div[@class='coupon-remove']
${P_MENSAGEM_CUPOM_EXCLUIDO_DO_CARRINHO}               //div[@class='notification']/./p[contains(text(), 'Cupom excluído do carrinho.')] 
${BASE_URL_API}                                        https://apiqa.fastshop.com.br

#SERVIÇOS
${DIV_SUPORTE_TECNICO}                                //div[@class='service-name']//span[contains(text(), 'Suporte técnico')]//..//..//div[@class='checkbox']  
${DIV_PROTEÇÃO_DIGITAL}                                //div[@class='service-name']//span[contains(text(), 'Proteção Digital')]//..//..//div[@class='checkbox']
${DIV_SEGURO}                                         //div[@class='service-name']//span[contains(text(), 'Seguro')][1]//..//..//div[@class='checkbox']   
${DIV_SEGURO_QUEBRA_ACIDENTAL}                        //div[@class='service-name']//span[contains(text(), 'Seguro e Quebra Acidental')]//..//..//div[@class='checkbox']  
${DIV_GARANTIA_ESTENDIDA}                             //div[@class='service-name']//span[contains(text(), 'Garantia Estendida')]//..//..//div[@class='checkbox']  
${DIV_INSTALAÇÃO}                                     //div[@class='service-name']//span[contains(text(), 'Instalação e orientação de uso')]//..//..//div[@class='checkbox']  
${BUTTON_SERVICO_REMOVE}                              //span[@class='icon icon-trash theme-third-color']        #//div[@class='box-service']//button[@class='remove space']
${BUTTON_APLICAR_SERVICO}                             //button[contains(text(), 'APLICAR')]
${BUTTON_ALTERAR_ENTREGA}                             //button[contains(text(), 'Alterar')]
${BUTTON_APLICAR_ENTREGA}                             //button[@class='apply theme-first-btn']
${BUTTON_OK_ALERTA_ATENCAO}                           //button[contains(text(), 'OK, ENTENDI')]
${BUTTON_REMOVE}                                      //button[@class='remove']
${LI_QUANTIDADE_2}                                    //li[contains(text(), '2')] 
${LI_QUANTIDADE_3}                                    //li[contains(text(), '3')] 
${LI_QUANTIDADE_4}                                    //li[contains(text(), '4')] 
${LI_QUANTIDADE_5}                                    //div[contains(text(), '5')]        #//li[contains(text(), '5')]      

#ENDEREÇO
${INPUT_ENDEREÇO_NOME_DESTINATARIO}                   //input[@formcontrolname='name']
${ID_ENDEREÇO_CEP}                                    id:zipCode
${ID_ENDEREÇO_NOME_DA_RUA}                            id:streetName
${ID_ENDEREÇO_NUMERO}                                 id:number
${ID_ENDEREÇO_COMPLEMENTO}                            id:complement
${ID_ENDEREÇO_BAIRRO}                                 id:district
${ID_ENDEREÇO_CIDADE}                                 id:city
${ID_ENDEREÇO_ESTADO}                                 //select[@formcontrolname='state']
${ID_ENDEREÇO_TIPO_CASA}                              //select[@formcontrolname='housingType']
${ID_ENDEREÇO_TELEFONE}                               id:telephone
${BUTTON_SALVAR_ENDERÇO}                              //button[contains(text(), ' Adicionar ')]

### Login
${ID_LOGIN_CPF}                                       id:document
${ID_LOGIN_SENHA}                                     id:password
${BUTTON_CONFIRMAR_VERDE}                             //button[contains(text(), 'ENTRAR')]    #//button[@class='btn btn-green']   
${BUTTON_ENTRAR_VERDE}                                //button[contains(text(), 'ENTRAR')]
${A_NOME_CLIENTE_LOGADO}                              //a[contains(text(), 'Daniel')] 
${P_MENSSAGEM_ERRO_LOGIN}                             //p[@class='err-msg']
${DIV_CPF_INVALIDO}                                   //div[@class='invalid-feedback']
${P_MENSSAGEM_ERRO_LOGIN}                             //p[contains(text(), ' Entre ou ')]
${A_LOGOUT}                                           //a[@class='logout']
${DIV_CAMPO_OBRIGATORIO}                              //div[@class='invalid-feedback']

### FAVORITOS
${BUTTON_LISTA_DE_FAVORITOS}                          //div[@class='user']//a[@class='favorite-icon']
${URL_FAVORITOS}                                      http://webapp2-qa.fastshop.com.br/web/checkout-v2/favoritos
${BUTTON_FAVORITOS}                                   //button[@class='btn-wish-list']

### CLICK&COLLECT QR CODE
${BUTTON_SIM_FIZ_CONFIRMACAO}                         //button[@class='btn btn-primary theme-first-btn']
${BUTTON_NAO_RECEBI}                                  //button[@class='btn btn-link theme-first-link-color']
${BUTTON_REENVIAR_EMAIL}                              //button[@class='btn btn-primary theme-first-btn']
${BUTTON_FAZER_DOWNLOAD}                              //button[@class='btn btn-primary theme-first-btn']

### Cadastro API
${URL_CADASTRO_API}                         http://www.fastshop.com.br/web/checkout-v2/cadastro      #http://webapp2-qa.fastshop.com.br/web/checkout-v2/cadastro      #https://www.fastshop.com.br/web/checkout-v2/cadastro  
${url}                                      http://webapp2-qa.fastshop.com.br/web/checkout-v2/cadastro
${check_informe_Cpf_nao_cadastro}           id:document
${check_informe_Cpf}                        id:inputUser
${check_botao_verde_continuar}              class:btn-green
${check_label_senha_na_fastshop}            //label[contains(text(),'Senha na Fast Shop')]
${check_informe_senha}                      id:current-password
${check_botao_verde_continuar_comprando}    class:btn-success
${check_nome_andre}                         //span[contains(text(),'ANDRE')]
${check_mensagem_vermelha}                  //div[@class='invalid-feedback']
${check_mensagem_span_critica_vermelha}     class:invalid-feedback
${check_campo_cpf}                          id:document
${check_cadastro_nome}                      id:name
${check_cadastro_idade}                     id:birthday
${check_cadastro_sexo}                      //span[contains(text(),'Sexo')]//..//select
${check_cadastro_telefone}                  id:telephone
${check_cadastro_email}                     id:email
${check_cadastro_senha}                     id:password
${check_cadastro_confirmacao_senha}         id:password_confirmation
#${check_cadastro_recebe_email}              id:receiveEmailFast
#${check_cadastro_recebe_sms}                id:receiveSMSFast
${check_cadastro_cep}                       id:zipCode
${check_cadastro_rua}                       id:streetName
${check_cadastro_numero_casa}               id:number
${check_cadastro_bairro}                    id:district
${check_cadastro_cidade}                    id:city  
${check_cadastro_complemento}               id:complement
${check_cadastro_select_imovel}             //span[contains(text(),'Tipo de Imóvel')]//..//select
${check_icone_validador_verde_cep}          //span[contains(text(),'CEP')]//..//i[@class='validation-icon']          
${check_icone_validador_verde_rua}          //span[contains(text(),'Endereço')]//..//i[@class='validation-icon'] 
${check_icone_validador_verde_casa}         //span[contains(text(),'Número')]//..//i[@class='validation-icon'] 
${check_icone_validador_verde_bairro}       //span[contains(text(),'Bairro')]//..//i[@class='validation-icon'] 
${check_icone_validador_verde_cidade}       //span[contains(text(),'Cidade')]//..//i[@class='validation-icon']  
${check_icone_validador_verde_estado}       //span[contains(text(),'Estado')]//..//i[@class='validation-icon']
${check_icone_validador_verde_imovel}       //span[contains(text(),'Tipo de Imóvel')]//..//i[@class='validation-icon']         
${check_botao_verde_cadastrar}              //button[contains(text(), 'Cadastrar')]
${check_cadastro_notificacao_email}         //span[contains(text(),'Email')]//..//span[@class='slider']
${check_cadastro_notificacao_sms}           //span[contains(text(),'SMS')]//..//span[@class='slider']
${check_cadastro_notificacao_pushapp}       //span[contains(text(),'Push no App')]//..//span[@class='slider']
${check_cadastro_notificacao_telefone}      //span[contains(text(),'Telefone')]//..//span[@class='slider']
${check_cadastro_notificacao_whatsapp}      //span[contains(text(),'WhatsApp')]//..//span[@class='slider']
${check_cadastro_notificacao_webpush}       //span[contains(text(),'Web Push')]//..//span[@class='slider']
${check_aceite_termos}                      //label[contains(text(),'Declaro que li e aceito os')]//..//input[@class='checkbox-blue']
${check_termos_condicao_venda}              xpath=//a[@href="https://fastshopwr-a.akamaihd.net/pdf/termos_e_condicoes_de_venda.pdf"]
${check_termos_privacidade}                 xpath=//a[@href="https://fastshopwr-a.akamaihd.net/pdf/politica_de_privacidade.pdf"]
${check_termos_veracidade}                  xpath=//a[@href="https://fastshopwr-a.akamaihd.net/pdf/termos_de_veracidade.pdf"]
