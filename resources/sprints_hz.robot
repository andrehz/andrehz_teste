*** Settings ***
Resource    base.robot
Resource    Helpers.robot
Resource    services.robot
Library	    Collections			
Library	    RequestsLibrary
Library     OperatingSystem

*** Keywords ***  
###################### FASTSHOP - ENTRAR NA HOME
ENTRAR NA HOME FASTSHOP E ACEITAR OS COOKIES 
    Delete All Cookies
    Go To                               ${BASE_URL}
    Wait Until Element Is Visible       ${ID_ACEITAR_COOKIES}                       30
        Sleep                           1    
    Click Element	                    ${ID_ACEITAR_COOKIES}
        Sleep                           1
    Log to console                      Dado - OK    "###TELA INICIAL"

ENTRAR NA HOME DE PRODUÇÃO DA FASTSHOP E ACEITAR OS COOKIES 
    Delete All Cookies
    Go To                               ${BASE_PRODUÇÃO_URL}
    Wait Until Element Is Visible       ${ID_ACEITAR_COOKIES}                       30
        Sleep                           1    
    Click Element	                    ${ID_ACEITAR_COOKIES}
        Sleep                           1
    Log to console                      Dado - OK    "###TELA INICIAL DE PRODUÇÃO"

ENTRAR NA FEATURE HOME FASTSHOP E ACEITAR OS COOKIES 
    Delete All Cookies
    Go To                               ${BASE_FEATURE_URL}
    Wait Until Element Is Visible       ${ID_ACEITAR_COOKIES}                       30
        Sleep                           1
    Click Element	                    ${ID_ACEITAR_COOKIES}
        Sleep                           1
    Log to console                      Dado - OK    "###TELA INICIAL FEATURE"

ENTRAR COM OPÇÃO DE AMBIENTE DA HOME FASTSHOP E ACEITAR OS COOKIES 
    Delete All Cookies
    Go To                               http://${AMBIENTE}.fastshop.com.br/web/
    Wait Until Element Is Visible       ${ID_ACEITAR_COOKIES}                       30
        Sleep                           1
    Click Element	                    ${ID_ACEITAR_COOKIES}
        Sleep                           1
    Log to console                      Dado - OK    "###TELA INICIAL FEATURE"

TELA DA MINHA ASSINATURA PRIME ACEITANDO OS COOKIES - ${busca_produto}             
    Delete All Cookies
    Go To                               ${BASE_URL}web/prime/minha-assinatura
    Wait Until Element Is Visible       ${ID_ACEITAR_COOKIES}                       60
        Sleep                           1
    Click Element	                    ${ID_ACEITAR_COOKIES}
    Log to console                      @TELA DE MINHA ASSINATURA ACESSADA COM SUCESSO, COOKIES FORAM ACEITOS.
    Capture Page Screenshot
        Sleep                           1

TELA DE PAGAMENTO ACEITANDO OS COOKIES              
    Delete All Cookies
    Go To                               ${BASE_URL}web/checkout-v2/pagamento
    Wait Until Element Is Visible       ${ID_ACEITAR_COOKIES}                       30
        Sleep                           1
    Click Element	                    ${ID_ACEITAR_COOKIES}
    Log to console                      @TELA DE PAGAMENTO ACESSADA COM SUCESSO. COOKIES FORAM ACEITOS, REDIRECIONANDO PARA O LOGIN.
    Capture Page Screenshot
        Sleep                           1

TELA DE BUSCA DE PRODUTOS ACEITANDO OS COOKIES - ${busca_produto}             
    #Delete All Cookies
    Go To                               ${BASE_URL}web/s/${busca_produto}
    Wait Until Element Is Visible       //div[@class='grid-box-image']                       15  #${ID_ACEITAR_COOKIES}                       60
    #Click Element	                    ${ID_ACEITAR_COOKIES}
    Log to console                      @TELA DE BUSCA DE PRODUTOS ACESSADA COM SUCESSO, COOKIES FORAM ACEITOS.
    Wait Until Element Is Visible       ${ID_ICONE_CHATBLA_PDP}
        Sleep                           3
    #Wait Until Element Is Visible       ${ID_ACEITAR_COOKIES}                       60   
    Execute javascript                  document.body.style.zoom="70%"



TELA DE PRODUTOS ACEITANDO OS COOKIES - ${busca_produto}             
    Delete All Cookies
    Go To                               ${BASE_URL}web/p/d/${busca_produto}/teste
    Wait Until Element Is Visible       ${ID_ACEITAR_COOKIES}                       60
        Sleep                           1
    Click Element	                    ${ID_ACEITAR_COOKIES}
    Log to console                      @TELA DE PRODUTOS ACESSADA COM SUCESSO, COOKIES FORAM ACEITOS.
    Capture Page Screenshot
        Sleep                           1

TELA DE PRODUTOS ADICIONANDO UM PRODUTO AO CARRINHO E ACEITANDO OS COOKIES - ${busca_produto}
    Delete All Cookies
    Go To                               ${BASE_URL}web/p/d/${busca_produto}/teste
    Wait Until Element Is Visible       ${ID_ACEITAR_COOKIES}                       60
        Sleep                           1
    Click Element	                    ${ID_ACEITAR_COOKIES}
    Log to console                      @TELA DE PRODUTOS ACESSADA COM SUCESSO, COOKIES FORAM ACEITOS.
    Capture Page Screenshot
        Sleep                           1
    Wait Until Element Is Visible       ${ID_ICONE_ADICIONAR_AO_CARRINHO}           20
    ${PRODUTO}=                         Get Text                                    ${H1_NOME_DO_PRODUTO}
    Set Global Variable                 ${PRODUTO}
    Log                                 ${PRODUTO}     
    Element Should Contain              ${H2_NOME_DO_PRODUTO}                       ${busca_produto} 
    Click Element	                    ${ID_ICONE_ADICIONAR_AO_CARRINHO} 
    Log to console                      Sku do teste: ${busca_produto}  
    Capture Page Screenshot


TELA LOGIN COM CPF E SENHA CORRETOS - ${cpf} ${senha}
    Wait Until Element Is Visible       ${ID_LOGIN_CPF}                             30
    Input Text	                        ${ID_LOGIN_CPF}                             ${cpf}
    Input Text	                        ${ID_LOGIN_SENHA}                           ${senha}
    Click Element                       ${BUTTON_CONFIRMAR_VERDE} 
    Capture Page Screenshot
    Log to console                      CPF DO TESTE: ${cpf} 
    Log to console                      SENHA: ${senha}
    Log to console                      @LOGIN EFETUADO COM SUCESSO

#RESET DE SENHA
TELA RESET DE SENHA ACEITADO COOKIES
    Go To                               https://www.invertexto.com/gerador-email-temporario
    Location Should Be	                https://www.invertexto.com/gerador-email-temporario
    Wait Until Element Is Visible       ${ID_EMAIL_TEMPORARIO} 
    ${EMAIL_TEMPORARIO}=                Get Value                                      ${ID_EMAIL_TEMPORARIO}
    Set Suite Variable                  ${EMAIL_TEMPORARIO}
    Log to console                      ************EMAIL: ${EMAIL_TEMPORARIO}
    Open Browser	                    about:blank        	chrome     alias=second
    Location Should Be	                about:blank		
    #Switch Browser	                    1	                # index	
    #Capture Page Screenshot
    #Switch Browser	                    second	            # alias	
    Capture Page Screenshot

TELA LOGIN COM RESET DE SENHA ACEITADO COOKIES
    Go To                               http://${AMBIENTE}.fastshop.com.br/web/checkout-v2/login
    Wait Until Element Is Visible       ${ID_LOGIN_CPF}                             30
    Input Text	                        ${ID_LOGIN_CPF}                             ${CPF_VALIDO}
    Click Element                       ${DIV_ESQUECI_MINHA_SENHA_NO_LOGIN}
    Capture Page Screenshot
    Log to console                      ************EMAIL: ${EMAIL_TEMPORARIO}
    Log to console                      CPF DO TESTE: ${CPF_VALIDO} 
    Log to console                      @RESET DE SENHA INICIADO
    #Switch Browser	                    1	                # index	
    Capture Page Screenshot

TELA DO RESET DE SENHA COM CPF
    SLEEP   900

    Wait Until Element Is Visible       ${ID_LOGIN_CPF}                             30
    Input Text	                        ${ID_LOGIN_CPF}                             ${CPF_VALIDO}
    Click Element                       ${BUTTON_VERDE_CONTINUAR_RECUPERAR_SENHA}
    Log to console                      @CPF INSERIDO E CLICK NO BOTÃO PARA INICIAR O PROCESSO DE RECUPERAR A SENHA

TELA DE RESET DE SENHA ESCOLHER A OPÇÃO E-MAIL
    Wait Until Element Is Visible       ${LABEL_BUTTON_EMAIL_RECUPERAR_SENHA}       30
    Click Element                       ${LABEL_BUTTON_EMAIL_RECUPERAR_SENHA}
    Click Element                       ${BUTTON_VERDE_RECUPERAR_SENHA}
    Wait Until Element Is Visible       ${DIV_CONFIRMAÇÃO_DE_ENVIO_EMAIL_SENHA}     30  
    Log to console                      @RESET DE SENHA COM EMAIL DISPARADO PARA O EMAIL TEMPORARIO
    Capture Page Screenshot

TELA EMAIL TEMPORARIO
    Wait Until Element Is Visible       ${TD_EMAIL_DE_RECUPERAR_A_SENHA}            30
    Click Element                       ${TD_EMAIL_DE_RECUPERAR_A_SENHA}
    Log to console                      @EMAIL CHEGOU NA CAIXA DE ENTRADA, EFETUADO O CLICK PARA EXIBIR O CONTEUDO DO EMAIL
    Capture Page Screenshot
    

#Pix
TELA DE PAGAMENTO COM O PIX
    Wait Until Element Is Visible       ${RADIO_BUTTON_PIX}                          60
    Log to console                      RADIO BUTTON DE SELEÇÃO DO PIX ESTÁ PRESENTE NA TELA DE PAGAMENTOS
    Click Element                       ${RADIO_BUTTON_PIX}
    Log to console                      BOTÃO CONTINUAR ESTÁ HABILITADO
    #Click Element                       ${BUTTON_CONTINUAR_VERDE_PAGAMENTO}     
    Log to console                      @SELECIONADO PAGAMENTO COM O PIX
    Capture Page Screenshot

TELA DE INSTRUÇÃO PARA CONFIRMAÇÃO COM O PIX
    Wait Until Element Is Visible       ${RADIO_BUTTON_PIX}                          60
    Log to console                      RADIO BUTTON DE SELEÇÃO DO PIX ESTÁ PRESENTE NA TELA DE PAGAMENTOS
    Click Element                       ${RADIO_BUTTON_PIX}
    Log to console                      BOTÃO CONTINUAR ESTÁ HABILITADO
    Click Element                       ${BUTTON_CONTINUAR_VERDE_PAGAMENTO}     
    Log to console                      @SELECIONADO PAGAMENTO COM O PIX E AVANÇO DE TELA NO BOTÃO CONTINUAR
    Capture Page Screenshot
    Wait Until Element Is Visible       ${ID_LOGO_PIX_TELA_PAGAMENTO}                 60
    Wait Until Element Is Visible       ${ID_ICONE_CIFRAO_TELA_PAGAMENTO} 
    Log to console                      @TELA DE INSTRUÇÃO DE PAGAMENTO COM O PIX
    Capture Page Screenshot

TELA DE FINALIZAÇÃO COM O PIX
    Wait Until Element Is Visible       ${RADIO_BUTTON_PIX}                          60
    Log to console                      RADIO BUTTON DE SELEÇÃO DO PIX ESTÁ PRESENTE NA TELA DE PAGAMENTOS
    Click Element                       ${RADIO_BUTTON_PIX}
    Log to console                      BOTÃO CONTINUAR ESTÁ HABILITADO
    Click Element                       ${BUTTON_CONTINUAR_VERDE_PAGAMENTO}     
    Log to console                      @SELECIONADO PAGAMENTO COM O PIX E AVANÇO DE TELA NO BOTÃO CONTINUAR
    Capture Page Screenshot
    Wait Until Element Is Visible       ${ID_LOGO_PIX_TELA_PAGAMENTO}                 60
    Wait Until Element Is Visible       ${ID_ICONE_CIFRAO_TELA_PAGAMENTO} 
    Log to console                      @TELA DE INSTRUÇÃO DE PAGAMENTO COM O PIX E AVANÇO DE TELA NO BOTÃO CONTINUAR
    Capture Page Screenshot
    Click Element                       ${BUTTON_CONTINUAR_VERDE_FINALIZAR_COMPRA}     
    Wait Until Element Is Visible       ${ID_CHAVE_PIX}                               60            PROBLEMAS COM O PAGAMENTO PIX                         
    ${CODIGO_CHAVE_PIX}=                Get Text                                      ${ID_CHAVE_PIX}
    Capture Element Screenshot          ${DIV_CAMPO_CHAVE_PIX}
    Capture Element Screenshot          ${ID_QR_CODE}
    Log to console                      VALOR DA CAHVE PIX é: ${CODIGO_CHAVE_PIX}
     Wait Until Element Is Visible      ${DIV_NUMERO_DA_ORDEM_DE_COMPRA}              60    
    Capture Element Screenshot	        ${DIV_NUMERO_DA_ORDEM_DE_COMPRA}
    Capture Page Screenshot             
    ${NUMERO_DA_ORDEM}=                 Get Text                                       ${DIV_NUMERO_DA_ORDEM_DE_COMPRA}
    Log to console                      ${NUMERO_DA_ORDEM}
    Log to console                      @SELECIONADO PAGAMENTO COM O PIX E AVANÇO DE TELA NO BOTÃO CONTINUAR

#Boleto
TELA DE PAGAMENTO COM O BOLETO
    Wait Until Element Is Visible       ${LABEL_PAGAR_VIA_BOLETO}                     60
    Log to console                      RADIO BUTTON DE SELEÇÃO DO BOLETO ESTÁ PRESENTE NA TELA DE PAGAMENTOS
    Click Element                       ${LABEL_PAGAR_VIA_BOLETO}
    Log to console                      BOTÃO CONTINUAR ESTÁ HABILITADO
    #Click Element                       ${BUTTON_CONTINUAR_VERDE_PAGAMENTO}     
    Log to console                      @SELECIONADO PAGAMENTO COM O BOLETO 
    Capture Page Screenshot

TELA DE INSTRUÇÃO PARA CONFIRMAÇÃO COM O BOLETO
    Wait Until Element Is Visible       ${LABEL_PAGAR_VIA_BOLETO}                     60
    Log to console                      RADIO BUTTON DE SELEÇÃO DO BOLETO ESTÁ PRESENTE NA TELA DE PAGAMENTOS
    Click Element                       ${LABEL_PAGAR_VIA_BOLETO}
    Log to console                      BOTÃO CONTINUAR ESTÁ HABILITADO
    Click Element                       ${BUTTON_CONTINUAR_VERDE_PAGAMENTO}     
    Log to console                      @SELECIONADO PAGAMENTO COM O BOLETO E AVANÇO DE TELA NO BOTÃO CONTINUAR
    Capture Page Screenshot
    Wait Until Element Is Visible       ${DIV_TELA_CONFIRMACAO_BOLETO}                60
    Log to console                      @TELA DE INSTRUÇÃO DE PAGAMENTO COM O BOLETO

TELA DE FINALIZAÇÃO COM O BOLETO
    Wait Until Element Is Visible       ${LABEL_PAGAR_VIA_BOLETO}                     60
    Log to console                      RADIO BUTTON DE SELEÇÃO DO BOLETO ESTÁ PRESENTE NA TELA DE PAGAMENTOS
    Click Element                       ${LABEL_PAGAR_VIA_BOLETO}
    Log to console                      BOTÃO CONTINUAR ESTÁ HABILITADO
    Click Element                       ${BUTTON_CONTINUAR_VERDE_PAGAMENTO}     
    Log to console                      @SELECIONADO PAGAMENTO COM O BOLETO E AVANÇO DE TELA NO BOTÃO CONTINUAR
    Capture Page Screenshot
    Wait Until Element Is Visible       ${DIV_TELA_CONFIRMACAO_BOLETO}                60
    Log to console                      @TELA DE INSTRUÇÃO DE PAGAMENTO COM O BOLETO E AVANÇO DE TELA NO BOTÃO CONTINUAR
    Capture Page Screenshot
    Click Element                       ${BUTTON_CONTINUAR_VERDE_FINALIZAR_COMPRA}   
    Wait Until Element Is Visible       ${DIV_NUMERO_DA_ORDEM_DE_COMPRA}              120   
    Capture Element Screenshot	        ${DIV_CAMPO_NUMERO_CODIGO_BARRAS_BOLETO}
    Capture Page Screenshot              
    ${CODIGO_DE_BARRAS_BOLETO}=         Get Text                                      ${DIV_NUMERO_CODIGO_BARRAS_BOLETO}
    Log to console                      VALOR BOLETO é: ${CODIGO_DE_BARRAS_BOLETO}
    Capture Element Screenshot	        ${DIV_NUMERO_DA_ORDEM_DE_COMPRA}
    Capture Page Screenshot             
    ${NUMERO_DA_ORDEM}=                 Get Text                                                 ${DIV_NUMERO_DA_ORDEM_DE_COMPRA}
    Log to console                      ${NUMERO_DA_ORDEM}
    Log to console                      @SELECIONADO PAGAMENTO COM O BOLETO E AVANÇO DE TELA NO BOTÃO CONTINUAR


#Um Cartão
TELA INSERIR OS DADOS DO CARTÃO DE CRÉDITO
    Wait Until Element Is Visible       ${LABEL_PAGAR_VIA_CARTAO_DE_CREDITO}                     60
    Log to console                      RADIO BUTTON DE SELEÇÃO DO CARTÃO DE CRÉDITO ESTÁ PRESENTE NA TELA DE PAGAMENTOS
    Click Element                       ${LABEL_PAGAR_VIA_CARTAO_DE_CREDITO}
    Log to console                      BOTÃO CONTINUAR ESTÁ HABILITADO
    Log to console                      @SELECIONADO PAGAMENTO COM O CARTÃO DE CRÉDITO 
    Capture Page Screenshot

TELA INSERIR OS DADOS DO CARTÃO DE CRÉDITO - ${cpf} ${numero_cartao_de_credito}
    Wait Until Element Is Visible       ${LABEL_PAGAR_VIA_CARTAO_DE_CREDITO}                     60
    Log to console                      RADIO BUTTON DE SELEÇÃO DO CARTÃO DE CRÉDITO ESTÁ PRESENTE NA TELA DE PAGAMENTOS
    Click Element                       ${LABEL_PAGAR_VIA_CARTAO_DE_CREDITO}
    Log to console                      BOTÃO CONTINUAR ESTÁ HABILITADO
    Click Element                       ${BUTTON_CONTINUAR_VERDE_PAGAMENTO}     
    Log to console                      @SELECIONADO PAGAMENTO COM O CARTÃO DE CRÉDITO E AVANÇO DE TELA NO BOTÃO CONTINUAR
    Capture Page Screenshot
    Wait Until Element Is Visible       ${INPUT_NUMERO_CARTAO_DE_CREDITO}                        60
    Log to console                      @TELA DE PGAMENTO COM O CARTÃO DE CRÉDITO, DADOS DO CARTÃO PARA SEREM INSERIDOS.
    Capture Page Screenshot            
    Input Text                          ${INPUT_NUMERO_CARTAO_DE_CREDITO}                        ${numero_cartao_de_credito}       
    Log to console                      NÚMERO DO CARTÃO DE CRÉDITO É: ${numero_cartao_de_credito}          
    Input Text                          ${INPUT_NOME_TITULAR_DO_CARTAO}                          WILLIAM MOORE HZ                                                              
    Log to console                      NOME DO TITULAR DO CARTÃO DE CRÉDITO É: WILLIAM MOORE HZ    
    Input Text                          ${INPUT_VALIDADE_DO_CARTAO}                                ${validade_do_cartao_de_credito}                             
    Log to console                      VALIDADE DO CARTÃO DE CRÉDITO É: ${validade_do_cartao_de_credito} 
    Input Text                          ${INPUT_CPF_TITULAR_CARTAO}                                ${cpf}                          
    Log to console                      CPF DO TITULAR DO CARTÃO DE CRÉDITO É: ${cpf}       
    Log to console                      @SELECIONADO PAGAMENTO COM O CARTÃO DE CRÉDITOS, DADOS INSERIDOS COM SUCESSO


TELA DE PARCELAMENTO COM O CARTÃO DE CRÉDITO - ${cpf} ${numero_cartao_de_credito} ${cvv_cartao_de_credito}
    Wait Until Element Is Visible       ${LABEL_PAGAR_VIA_CARTAO_DE_CREDITO}                     60
    Log to console                      RADIO BUTTON DE SELEÇÃO DO CARTÃO DE CRÉDITO ESTÁ PRESENTE NA TELA DE PAGAMENTOS
    Click Element                       ${LABEL_PAGAR_VIA_CARTAO_DE_CREDITO}
    Log to console                      BOTÃO CONTINUAR ESTÁ HABILITADO
    Click Element                       ${BUTTON_CONTINUAR_VERDE_PAGAMENTO}     
    Log to console                      @SELECIONADO PAGAMENTO COM O CARTÃO DE CRÉDITO E AVANÇO DE TELA NO BOTÃO CONTINUAR
    Capture Page Screenshot
    Wait Until Element Is Visible       ${INPUT_NUMERO_CARTAO_DE_CREDITO}                        60
    Log to console                      @TELA DE PGAMENTO COM O CARTÃO DE CRÉDITO, DADOS DO CARTÃO PARA SEREM INSERIDOS.
    Capture Page Screenshot            
    Input Text                          ${INPUT_NUMERO_CARTAO_DE_CREDITO}                        ${numero_cartao_de_credito}       
    Log to console                      NÚMERO DO CARTÃO DE CRÉDITO É: ${numero_cartao_de_credito}          
    Input Text                          ${INPUT_NOME_TITULAR_DO_CARTAO}                          WILLIAM MOORE HZ                                                              
    Log to console                      NOME DO TITULAR DO CARTÃO DE CRÉDITO É: WILLIAM MOORE HZ    
    Input Text                          ${INPUT_VALIDADE_DO_CARTAO}                              ${validade_do_cartao_de_credito}                             
    Log to console                      VALIDADE DO CARTÃO DE CRÉDITO É: ${validade_do_cartao_de_credito} 
    Input Text                          ${INPUT_CPF_TITULAR_CARTAO}                              ${cpf}                          
    Log to console                      CPF DO TITULAR DO CARTÃO DE CRÉDITO É: ${cpf}       
    Log to console                      @SELECIONADO PAGAMENTO COM O CARTÃO DE CRÉDITOS, DADOS INSERIDOS COM SUCESSO
    Click Element                       ${BUTTON_CONTINUAR_VERDE_CONTINUAR_PARCELAMENTO}
    Wait Until Element Is Visible       ${DIV_PARCELAMENTO_CAARTAO_DE_CREDITO}                   60
    Click Element                       ${DIV_PARCELAMENTO_CAARTAO_DE_CREDITO}
    Wait Until Element Is Visible       ${DIV_PAGAMENTO_LISTA_A_VISTA}                           60                         
    Click Element                       ${DIV_PAGAMENTO_LISTA_A_VISTA} 
    Log to console                      @SELECIONADO O VALOR A VISTA            
    Wait Until Element Is Not Visible   ${DIV_LOADER_ESPERA}                                     60            
    Input Text                          ${INPUT_CVV}                                             ${cvv_cartao_de_credito}  
    Capture Page Screenshot
    Log to console                      @PARCELA E CVV: ${cvv_cartao_de_credito} INSERIDOS

TELA DE FINALIZAÇÃO COM O CARTÃO DE CRÉDITO - ${cpf} ${numero_cartao_de_credito} ${validade_do_cartao_de_credito} ${cvv_cartao_de_credito}
    Wait Until Element Is Visible       ${LABEL_PAGAR_VIA_CARTAO_DE_CREDITO}                     60
    Log to console                      RADIO BUTTON DE SELEÇÃO DO CARTÃO DE CRÉDITO ESTÁ PRESENTE NA TELA DE PAGAMENTOS
    Click Element                       ${LABEL_PAGAR_VIA_CARTAO_DE_CREDITO}
    Log to console                      BOTÃO CONTINUAR ESTÁ HABILITADO
    Click Element                       ${BUTTON_CONTINUAR_VERDE_PAGAMENTO}     
    Log to console                      @SELECIONADO PAGAMENTO COM O CARTÃO DE CRÉDITO E AVANÇO DE TELA NO BOTÃO CONTINUAR
    Capture Page Screenshot
    Wait Until Element Is Visible       ${INPUT_NUMERO_CARTAO_DE_CREDITO}                        60
    Log to console                      @TELA DE PGAMENTO COM O CARTÃO DE CRÉDITO, DADOS DO CARTÃO PARA SEREM INSERIDOS.
    Capture Page Screenshot            
    Input Text                          ${INPUT_NUMERO_CARTAO_DE_CREDITO}                        ${numero_cartao_de_credito}       
    Log to console                      NÚMERO DO CARTÃO DE CRÉDITO É: ${numero_cartao_de_credito}          
    Input Text                          ${INPUT_NOME_TITULAR_DO_CARTAO}                          WILLIAM MOORE HZ                                                              
    Log to console                      NOME DO TITULAR DO CARTÃO DE CRÉDITO É: WILLIAM MOORE HZ    
    Input Text                          ${INPUT_VALIDADE_DO_CARTAO}                              ${validade_do_cartao_de_credito}                             
    Log to console                      VALIDADE DO CARTÃO DE CRÉDITO É: ${validade_do_cartao_de_credito} 
    Input Text                          ${INPUT_CPF_TITULAR_CARTAO}                              ${cpf}                          
    Log to console                      CPF DO TITULAR DO CARTÃO DE CRÉDITO É: ${cpf}       
    Log to console                      @SELECIONADO PAGAMENTO COM O CARTÃO DE CRÉDITOS, DADOS INSERIDOS COM SUCESSO
    Click Element                       ${BUTTON_CONTINUAR_VERDE_CONTINUAR_PARCELAMENTO}
    Wait Until Element Is Visible       ${DIV_PARCELAMENTO_CAARTAO_DE_CREDITO}                   60
    Click Element                       ${DIV_PARCELAMENTO_CAARTAO_DE_CREDITO}
    Wait Until Element Is Visible       ${DIV_PAGAMENTO_LISTA_A_VISTA}                           60                         
    Click Element                       ${DIV_PAGAMENTO_LISTA_A_VISTA} 
    Log to console                      @SELECIONADO O VALOR A VISTA            
    Wait Until Element Is Not Visible   ${DIV_LOADER_ESPERA}                                     60            
    Input Text                          ${INPUT_CVV}                                             ${cvv_cartao_de_credito}  
    Capture Page Screenshot
    Click Element                       ${BUTTON_CONTINUAR_VERDE_FINALIZAR_COMPRA}   
    Wait Until Element Is Visible       ${DIV_NUMERO_DA_ORDEM_DE_COMPRA}                         60    
    Capture Element Screenshot	        ${DIV_NUMERO_DA_ORDEM_DE_COMPRA}
    Capture Page Screenshot             
    ${NUMERO_DA_ORDEM}=                 Get Text                                                 ${DIV_NUMERO_DA_ORDEM_DE_COMPRA}
    Log to console                      ${NUMERO_DA_ORDEM}
    Log to console                      @PARCELA E CVV: ${cvv_cartao_de_credito} INSERIDOS, BOTÃO DE FINALIZAR A COMPRA SELECIONADO PARA AVANÇAR DE TELA

#Dois cartões
TELA PAGAMENTO COM DOIS CARTÕES DE CRÉDITO
    Wait Until Element Is Visible       ${LABEL_PAGAR_COM_DOIS_CARTOES_DE_CREDITO}               60
    Log to console                      RADIO BUTTON DE SELEÇÃO DE DOIS CARTÕES DE CRÉDITO ESTÁ PRESENTE NA TELA DE PAGAMENTOS
    Click Element                       ${LABEL_PAGAR_COM_DOIS_CARTOES_DE_CREDITO}
    Log to console                      BOTÃO CONTINUAR ESTÁ HABILITADO
    Click Element                       ${BUTTON_CONTINUAR_VERDE_PAGAMENTO}     
    Log to console                      @SELECIONADO PAGAMENTO COM O CARTÃO DE CRÉDITO E AVANÇO DE TELA NO BOTÃO CONTINUAR
    Capture Page Screenshot



TELA INSERIR OS DADOS COM DOIS CARTÕES DE CRÉDITO - ${cpf} ${numero_cartao_de_credito_um} ${validade_do_cartao_de_credito_um} ${cvv_cartao_de_credito_um} ${valor_cartao_de_credito_um}
    Wait Until Element Is Visible       ${LABEL_PAGAR_COM_DOIS_CARTOES_DE_CREDITO}               60
    Log to console                      RADIO BUTTON DE SELEÇÃO DE DOIS CARTÕES DE CRÉDITO ESTÁ PRESENTE NA TELA DE PAGAMENTOS
    Click Element                       ${LABEL_PAGAR_COM_DOIS_CARTOES_DE_CREDITO}
    Log to console                      BOTÃO CONTINUAR ESTÁ HABILITADO
    Click Element                       ${BUTTON_CONTINUAR_VERDE_PAGAMENTO}     
    Log to console                      @SELECIONADO PAGAMENTO COM DOIS CARTÕES DE CRÉDITO E AVANÇO DE TELA NO BOTÃO CONTINUAR
    Capture Page Screenshot
    Wait Until Element Is Visible      ${LABEL_VALOR_PRIMEIRO_CARTAO}                            60
    Log to console                      @TELA DE PGAMENTO COM DOIS CARTÕES DE CRÉDITO, DADOS DO CARTÃO PARA SEREM INSERIDOS.
    Capture Page Screenshot            
    Input Text                          ${LABEL_VALOR_PRIMEIRO_CARTAO}                           ${valor_cartao_de_credito_um}       
    Log to console                      VALOR NO PRIMEIRO CARTÃO DE CRÉDITO É: ${valor_cartao_de_credito_um}          
    Input Text                          ${INPUT_NUMERO_CARTAO_DE_CREDITO}                        ${numero_cartao_de_credito_um}       
    Log to console                      NÚMERO DO CARTÃO DE CRÉDITO É: ${numero_cartao_de_credito_um}          
    Input Text                          ${INPUT_NOME_TITULAR_DO_CARTAO}                          WILLIAM MOORE DOIS HZ                                                              
    Log to console                      NOME DO TITULAR DO CARTÃO DE CRÉDITO É: WILLIAM MOORE DOIS HZ    
    Input Text                          ${INPUT_VALIDADE_DO_CARTAO}                              ${validade_do_cartao_de_credito_um}                            
    Log to console                      VALIDADE DO CARTÃO DE CRÉDITO É: ${validade_do_cartao_de_credito_um} 
    Input Text                          ${INPUT_CPF_TITULAR_CARTAO}                              ${cpf}                          
    Log to console                      CPF DO TITULAR DO CARTÃO DE CRÉDITO É: ${cpf}       
    Log to console                      @SELECIONADO PAGAMENTO COM O CARTÃO DE CRÉDITOS, DADOS INSERIDOS COM SUCESSO


TELA PARCELAMENTO DO PRIMEIRO CARTÃO PAGANDO COM DOIS CARTÕES DE CRÉDITO - ${cpf} ${numero_cartao_de_credito_um} ${validade_do_cartao_de_credito_um} ${cvv_cartao_de_credito_um} ${valor_cartao_de_credito_um}
    Wait Until Element Is Visible       ${LABEL_PAGAR_COM_DOIS_CARTOES_DE_CREDITO}               60
    Log to console                      RADIO BUTTON DE SELEÇÃO DE DOIS CARTÕES DE CRÉDITO ESTÁ PRESENTE NA TELA DE PAGAMENTOS
    Click Element                       ${LABEL_PAGAR_COM_DOIS_CARTOES_DE_CREDITO}
    Log to console                      BOTÃO CONTINUAR ESTÁ HABILITADO
    Click Element                       ${BUTTON_CONTINUAR_VERDE_PAGAMENTO}     
    Log to console                      @SELECIONADO PAGAMENTO COM DOIS CARTÕES DE CRÉDITO E AVANÇO DE TELA NO BOTÃO CONTINUAR
    Capture Page Screenshot
    Wait Until Element Is Visible      ${LABEL_VALOR_PRIMEIRO_CARTAO}                            60
    Log to console                      @TELA DE PGAMENTO COM DOIS CARTÕES DE CRÉDITO, DADOS DO CARTÃO PARA SEREM INSERIDOS.
    Capture Page Screenshot            
    Input Text                          ${LABEL_VALOR_PRIMEIRO_CARTAO}                           ${valor_cartao_de_credito_um}       
    Log to console                      VALOR NO PRIMEIRO CARTÃO DE CRÉDITO É: ${valor_cartao_de_credito_um}          
    Input Text                          ${INPUT_NUMERO_CARTAO_DE_CREDITO}                        ${numero_cartao_de_credito_um}       
    Log to console                      NÚMERO DO CARTÃO DE CRÉDITO É: ${numero_cartao_de_credito_um}          
    Input Text                          ${INPUT_NOME_TITULAR_DO_CARTAO}                          WILLIAM MOORE DOIS HZ                                                              
    Log to console                      NOME DO TITULAR DO CARTÃO DE CRÉDITO É: WILLIAM MOORE DOIS HZ    
    Input Text                          ${INPUT_VALIDADE_DO_CARTAO}                              ${validade_do_cartao_de_credito_um}                            
    Log to console                      VALIDADE DO CARTÃO DE CRÉDITO É: ${validade_do_cartao_de_credito_um} 
    Input Text                          ${INPUT_CPF_TITULAR_CARTAO}                              ${cpf}                          
    Log to console                      CPF DO TITULAR DO CARTÃO DE CRÉDITO É: ${cpf}       
    Click Element                       ${BUTTON_CONTINUAR_VERDE_CONTINUAR_PARCELAMENTO}
    Log to console                      @SELECIONADO PAGAMENTO COM O CARTÃO DE CRÉDITOS, DADOS INSERIDOS COM SUCESSO E AVANÇO PARA TELA DE PARCELAMENTO
    Wait Until Element Is Visible       ${DIV_PARCELAMENTO_CAARTAO_DE_CREDITO}                   60
    Click Element                       ${DIV_PARCELAMENTO_CAARTAO_DE_CREDITO}
    Wait Until Element Is Visible       ${DIV_PAGAMENTO_LISTA_A_VISTA}                           60                         
    Click Element                       ${DIV_PAGAMENTO_LISTA_A_VISTA} 
    Log to console                      @SELECIONADO O VALOR A VISTA            
    Wait Until Element Is Not Visible   ${DIV_LOADER_ESPERA}                                     60            
    Input Text                          ${INPUT_CVV}                                             ${cvv_cartao_de_credito_um}  
    Capture Page Screenshot
    Log to console                      @PARCELA E CVV: ${cvv_cartao_de_credito_um} INSERIDOS
    Log to console                      @SELECIONADO PAGAMENTO COM O CARTÃO DE CRÉDITOS, DADOS INSERIDOS COM SUCESSO E AVANÇO PARA TELA DE PARCELAMENTO


TELA INSERIR OS DADOS DO SEGUNDO CARTÃO PAGANDO COM DOIS CARTÕES DE CRÉDITO - ${cpf} ${numero_cartao_de_credito_um} ${validade_do_cartao_de_credito_um} ${cvv_cartao_de_credito_um} ${valor_cartao_de_credito_um} ${numero_cartao_de_credito_dois} ${validade_do_cartao_de_credito_dois} ${cvv_cartao_de_credito_dois}
    Wait Until Element Is Visible       ${LABEL_PAGAR_COM_DOIS_CARTOES_DE_CREDITO}               60
    Log to console                      RADIO BUTTON DE SELEÇÃO DE DOIS CARTÕES DE CRÉDITO ESTÁ PRESENTE NA TELA DE PAGAMENTOS
    Click Element                       ${LABEL_PAGAR_COM_DOIS_CARTOES_DE_CREDITO}
    Log to console                      BOTÃO CONTINUAR ESTÁ HABILITADO
    Click Element                       ${BUTTON_CONTINUAR_VERDE_PAGAMENTO}     
    Log to console                      @SELECIONADO PAGAMENTO COM DOIS CARTÕES DE CRÉDITO E AVANÇO DE TELA NO BOTÃO CONTINUAR
    Capture Page Screenshot
    Wait Until Element Is Visible      ${LABEL_VALOR_PRIMEIRO_CARTAO}                            60
    Log to console                      @TELA DE PGAMENTO COM DOIS CARTÕES DE CRÉDITO, DADOS DO CARTÃO PARA SEREM INSERIDOS.
    Capture Page Screenshot            
    Input Text                          ${LABEL_VALOR_PRIMEIRO_CARTAO}                           ${valor_cartao_de_credito_um}       
    Log to console                      VALOR NO PRIMEIRO CARTÃO DE CRÉDITO É: ${valor_cartao_de_credito_um}          
    Input Text                          ${INPUT_NUMERO_CARTAO_DE_CREDITO}                        ${numero_cartao_de_credito_um}       
    Log to console                      NÚMERO DO CARTÃO DE CRÉDITO É: ${numero_cartao_de_credito_um}          
    Input Text                          ${INPUT_NOME_TITULAR_DO_CARTAO}                          WILLIAM MOORE DOIS HZ                                                              
    Log to console                      NOME DO TITULAR DO CARTÃO DE CRÉDITO É: WILLIAM MOORE DOIS HZ    
    Input Text                          ${INPUT_VALIDADE_DO_CARTAO}                              ${validade_do_cartao_de_credito_um}                            
    Log to console                      VALIDADE DO CARTÃO DE CRÉDITO É: ${validade_do_cartao_de_credito_um} 
    Input Text                          ${INPUT_CPF_TITULAR_CARTAO}                              ${cpf}                          
    Log to console                      CPF DO TITULAR DO CARTÃO DE CRÉDITO É: ${cpf}       
    Click Element                       ${BUTTON_CONTINUAR_VERDE_CONTINUAR_PARCELAMENTO}
    Log to console                      @SELECIONADO PAGAMENTO COM O CARTÃO DE CRÉDITOS, DADOS INSERIDOS COM SUCESSO E AVANÇO PARA TELA DE PARCELAMENTO
    Wait Until Element Is Visible       ${DIV_PARCELAMENTO_CAARTAO_DE_CREDITO}                   60
    Click Element                       ${DIV_PARCELAMENTO_CAARTAO_DE_CREDITO}
    Wait Until Element Is Visible       ${DIV_PAGAMENTO_LISTA_A_VISTA}                           60                         
    Click Element                       ${DIV_PAGAMENTO_LISTA_A_VISTA} 
    Log to console                      @SELECIONADO O VALOR A VISTA            
    Wait Until Element Is Not Visible   ${DIV_LOADER_ESPERA}                                     60            
    Input Text                          ${INPUT_CVV}                                             ${cvv_cartao_de_credito_um}  
    Capture Page Screenshot
    Log to console                      @PARCELA E CVV: ${cvv_cartao_de_credito_um} INSERIDOS
    Click Element                       ${BUTTON_IR_PARA_O_SEGUNDO_CARTAO}
    Log to console                      @DODOS DO PARCELAMENTO E CVV INSERIDOS, BOTÃO DE AVANÇAR PARA OS DADOS DO SEGUNDO CARTÃO.
    Wait Until Element Is Visible      ${LABEL_VALOR_PRIMEIRO_CARTAO}                            60
    Log to console                      VALOR NO SEGUNDO CARTÃO DE CRÉDITO É: ${valor_cartao_de_credito_um}          
    Input Text                          ${INPUT_NUMERO_CARTAO_DE_CREDITO}                        ${numero_cartao_de_credito_dois}       
    Log to console                      NÚMERO DO CARTÃO DE CRÉDITO É: ${numero_cartao_de_credito_dois}          
    Input Text                          ${INPUT_NOME_TITULAR_DO_CARTAO}                          WILLIAM MOORE DOIS HZ                                                              
    Log to console                      NOME DO TITULAR DO CARTÃO DE CRÉDITO É: WILLIAM MOORE DOIS HZ    
    Input Text                          ${INPUT_VALIDADE_DO_CARTAO}                              ${validade_do_cartao_de_credito_dois}                            
    Log to console                      VALIDADE DO CARTÃO DE CRÉDITO É: ${validade_do_cartao_de_credito_dois} 
    Input Text                          ${INPUT_CPF_TITULAR_CARTAO}                              ${cpf}                          
    Log to console                      CPF DO TITULAR DO CARTÃO DE CRÉDITO É: ${cpf}       
    Log to console                      @SELECIONADO PAGAMENTO COM O CARTÃO DE CRÉDITOS, DADOS INSERIDOS COM SUCESSO E AVANÇO PARA TELA DE PARCELAMENTO

TELA PARCELAMENTO DO SEGUNDO CARTÃO PAGANDO COM DOIS CARTÕES DE CRÉDITO - ${cpf} ${numero_cartao_de_credito_um} ${validade_do_cartao_de_credito_um} ${cvv_cartao_de_credito_um} ${valor_cartao_de_credito_um} ${numero_cartao_de_credito_dois} ${validade_do_cartao_de_credito_dois} ${cvv_cartao_de_credito_dois}
    Wait Until Element Is Visible       ${LABEL_PAGAR_COM_DOIS_CARTOES_DE_CREDITO}               60
    Log to console                      RADIO BUTTON DE SELEÇÃO DE DOIS CARTÕES DE CRÉDITO ESTÁ PRESENTE NA TELA DE PAGAMENTOS
    Click Element                       ${LABEL_PAGAR_COM_DOIS_CARTOES_DE_CREDITO}
    Log to console                      BOTÃO CONTINUAR ESTÁ HABILITADO
    Click Element                       ${BUTTON_CONTINUAR_VERDE_PAGAMENTO}     
    Log to console                      @SELECIONADO PAGAMENTO COM DOIS CARTÕES DE CRÉDITO E AVANÇO DE TELA NO BOTÃO CONTINUAR
    Capture Page Screenshot
    Wait Until Element Is Visible      ${LABEL_VALOR_PRIMEIRO_CARTAO}                            60
    Log to console                      @TELA DE PGAMENTO COM DOIS CARTÕES DE CRÉDITO, DADOS DO CARTÃO PARA SEREM INSERIDOS.
    Capture Page Screenshot            
    Input Text                          ${LABEL_VALOR_PRIMEIRO_CARTAO}                           ${valor_cartao_de_credito_um}       
    Log to console                      VALOR NO PRIMEIRO CARTÃO DE CRÉDITO É: ${valor_cartao_de_credito_um}          
    Input Text                          ${INPUT_NUMERO_CARTAO_DE_CREDITO}                        ${numero_cartao_de_credito_um}       
    Log to console                      NÚMERO DO CARTÃO DE CRÉDITO É: ${numero_cartao_de_credito_um}          
    Input Text                          ${INPUT_NOME_TITULAR_DO_CARTAO}                          WILLIAM MOORE DOIS HZ                                                              
    Log to console                      NOME DO TITULAR DO CARTÃO DE CRÉDITO É: WILLIAM MOORE DOIS HZ    
    Input Text                          ${INPUT_VALIDADE_DO_CARTAO}                              ${validade_do_cartao_de_credito_um}                            
    Log to console                      VALIDADE DO CARTÃO DE CRÉDITO É: ${validade_do_cartao_de_credito_um} 
    Input Text                          ${INPUT_CPF_TITULAR_CARTAO}                              ${cpf}                          
    Log to console                      CPF DO TITULAR DO CARTÃO DE CRÉDITO É: ${cpf}       
    Click Element                       ${BUTTON_CONTINUAR_VERDE_CONTINUAR_PARCELAMENTO}
    Log to console                      @SELECIONADO PAGAMENTO COM O CARTÃO DE CRÉDITOS, DADOS INSERIDOS COM SUCESSO E AVANÇO PARA TELA DE PARCELAMENTO
    Wait Until Element Is Visible       ${DIV_PARCELAMENTO_CAARTAO_DE_CREDITO}                   60
    Click Element                       ${DIV_PARCELAMENTO_CAARTAO_DE_CREDITO}
    Wait Until Element Is Visible       ${DIV_PAGAMENTO_LISTA_A_VISTA}                           60                         
    Click Element                       ${DIV_PAGAMENTO_LISTA_A_VISTA} 
    Log to console                      @SELECIONADO O VALOR A VISTA            
    Wait Until Element Is Not Visible   ${DIV_LOADER_ESPERA}                                     60            
    Input Text                          ${INPUT_CVV}                                             ${cvv_cartao_de_credito_um}  
    Capture Page Screenshot
    Log to console                      @PARCELA E CVV: ${cvv_cartao_de_credito_um} INSERIDOS
    Click Element                       ${BUTTON_IR_PARA_O_SEGUNDO_CARTAO}
    Log to console                      @DODOS DO PARCELAMENTO E CVV INSERIDOS, BOTÃO DE AVANÇAR PARA OS DADOS DO SEGUNDO CARTÃO.
    Wait Until Element Is Visible      ${LABEL_VALOR_PRIMEIRO_CARTAO}                            60
    Log to console                      VALOR NO SEGUNDO CARTÃO DE CRÉDITO É: ${valor_cartao_de_credito_um}          
    Input Text                          ${INPUT_NUMERO_CARTAO_DE_CREDITO}                        ${numero_cartao_de_credito_dois}       
    Log to console                      NÚMERO DO CARTÃO DE CRÉDITO É: ${numero_cartao_de_credito_dois}          
    Input Text                          ${INPUT_NOME_TITULAR_DO_CARTAO}                          WILLIAM MOORE DOIS HZ                                                              
    Log to console                      NOME DO TITULAR DO CARTÃO DE CRÉDITO É: WILLIAM MOORE DOIS HZ    
    Input Text                          ${INPUT_VALIDADE_DO_CARTAO}                              ${validade_do_cartao_de_credito_dois}                            
    Log to console                      VALIDADE DO CARTÃO DE CRÉDITO É: ${validade_do_cartao_de_credito_dois} 
    Input Text                          ${INPUT_CPF_TITULAR_CARTAO}                              ${cpf}                          
    Log to console                      CPF DO TITULAR DO CARTÃO DE CRÉDITO É: ${cpf}       
    Log to console                      @SELECIONADO PAGAMENTO COM O CARTÃO DE CRÉDITOS, DADOS INSERIDOS COM SUCESSO E AVANÇO PARA TELA DE PARCELAMENTO
    Click Element                       ${BUTTON_CONTINUAR_VERDE_CONTINUAR_PARCELAMENTO}
    Log to console                      @SELECIONADO PAGAMENTO COM O CARTÃO DE CRÉDITOS, DADOS INSERIDOS COM SUCESSO E AVANÇO PARA TELA DE PARCELAMENTO
    Wait Until Element Is Visible       ${DIV_PARCELAMENTO_CAARTAO_DE_CREDITO}                   60
    Click Element                       ${DIV_PARCELAMENTO_CAARTAO_DE_CREDITO}
    Wait Until Element Is Visible       ${DIV_PAGAMENTO_LISTA_A_VISTA}                           60                         
    Click Element                       ${DIV_PAGAMENTO_LISTA_A_VISTA} 
    Log to console                      @SELECIONADO O VALOR A VISTA            
    Wait Until Element Is Not Visible   ${DIV_LOADER_ESPERA}                                     60            
    Input Text                          ${INPUT_CVV}                                             ${cvv_cartao_de_credito_dois}  
    Capture Page Screenshot
    Log to console                      @PARCELA E CVV: ${cvv_cartao_de_credito_dois} INSERIDOS
    Log to console                      @SELECIONADO PAGAMENTO COM O CARTÃO DE CRÉDITOS, DADOS INSERIDOS COM SUCESSO E AVANÇO PARA TELA DE PARCELAMENTO

TELA DE FINALIZAÇÃO COM DOIS CARTÕES DE CRÉDITO - ${cpf} ${numero_cartao_de_credito_um} ${validade_do_cartao_de_credito_um} ${cvv_cartao_de_credito_um} ${valor_cartao_de_credito_um} ${numero_cartao_de_credito_dois} ${validade_do_cartao_de_credito_dois} ${cvv_cartao_de_credito_dois}
    Wait Until Element Is Visible       ${LABEL_PAGAR_COM_DOIS_CARTOES_DE_CREDITO}               60
    Log to console                      RADIO BUTTON DE SELEÇÃO DE DOIS CARTÕES DE CRÉDITO ESTÁ PRESENTE NA TELA DE PAGAMENTOS
    Click Element                       ${LABEL_PAGAR_COM_DOIS_CARTOES_DE_CREDITO}
    Log to console                      BOTÃO CONTINUAR ESTÁ HABILITADO
    Click Element                       ${BUTTON_CONTINUAR_VERDE_PAGAMENTO}     
    Log to console                      @SELECIONADO PAGAMENTO COM DOIS CARTÕES DE CRÉDITO E AVANÇO DE TELA NO BOTÃO CONTINUAR
    Capture Page Screenshot
    Wait Until Element Is Visible      ${LABEL_VALOR_PRIMEIRO_CARTAO}                            60
    Log to console                      @TELA DE PGAMENTO COM DOIS CARTÕES DE CRÉDITO, DADOS DO CARTÃO PARA SEREM INSERIDOS.
    Capture Page Screenshot            
    Input Text                          ${LABEL_VALOR_PRIMEIRO_CARTAO}                           ${valor_cartao_de_credito_um}       
    Log to console                      VALOR NO PRIMEIRO CARTÃO DE CRÉDITO É: ${valor_cartao_de_credito_um}          
    Input Text                          ${INPUT_NUMERO_CARTAO_DE_CREDITO}                        ${numero_cartao_de_credito_um}       
    Log to console                      NÚMERO DO CARTÃO DE CRÉDITO É: ${numero_cartao_de_credito_um}          
    Input Text                          ${INPUT_NOME_TITULAR_DO_CARTAO}                          WILLIAM MOORE DOIS HZ                                                              
    Log to console                      NOME DO TITULAR DO CARTÃO DE CRÉDITO É: WILLIAM MOORE DOIS HZ    
    Input Text                          ${INPUT_VALIDADE_DO_CARTAO}                              ${validade_do_cartao_de_credito_um}                            
    Log to console                      VALIDADE DO CARTÃO DE CRÉDITO É: ${validade_do_cartao_de_credito_um} 
    Input Text                          ${INPUT_CPF_TITULAR_CARTAO}                              ${cpf}                          
    Log to console                      CPF DO TITULAR DO CARTÃO DE CRÉDITO É: ${cpf}       
    Click Element                       ${BUTTON_CONTINUAR_VERDE_CONTINUAR_PARCELAMENTO}
    Log to console                      @SELECIONADO PAGAMENTO COM O CARTÃO DE CRÉDITOS, DADOS INSERIDOS COM SUCESSO E AVANÇO PARA TELA DE PARCELAMENTO
    Wait Until Element Is Visible       ${DIV_PARCELAMENTO_CAARTAO_DE_CREDITO}                   60
    Click Element                       ${DIV_PARCELAMENTO_CAARTAO_DE_CREDITO}
    Wait Until Element Is Visible       ${DIV_PAGAMENTO_LISTA_A_VISTA}                           60                         
    Click Element                       ${DIV_PAGAMENTO_LISTA_A_VISTA} 
    Log to console                      @SELECIONADO O VALOR A VISTA            
    Wait Until Element Is Not Visible   ${DIV_LOADER_ESPERA}                                     60            
    Input Text                          ${INPUT_CVV}                                             ${cvv_cartao_de_credito_um}  
    Capture Page Screenshot
    Log to console                      @PARCELA E CVV: ${cvv_cartao_de_credito_um} INSERIDOS
    Click Element                       ${BUTTON_IR_PARA_O_SEGUNDO_CARTAO}
    Log to console                      @DODOS DO PARCELAMENTO E CVV INSERIDOS, BOTÃO DE AVANÇAR PARA OS DADOS DO SEGUNDO CARTÃO.
    Wait Until Element Is Visible      ${LABEL_VALOR_PRIMEIRO_CARTAO}                            60
    Log to console                      VALOR NO SEGUNDO CARTÃO DE CRÉDITO É: ${valor_cartao_de_credito_um}         
    Input Text                          ${INPUT_NUMERO_CARTAO_DE_CREDITO}                        ${numero_cartao_de_credito_dois}       
    Log to console                      NÚMERO DO CARTÃO DE CRÉDITO É: ${numero_cartao_de_credito_dois}          
    Input Text                          ${INPUT_NOME_TITULAR_DO_CARTAO}                          WILLIAM MOORE DOIS HZ                                                              
    Log to console                      NOME DO TITULAR DO CARTÃO DE CRÉDITO É: WILLIAM MOORE DOIS HZ    
    Input Text                          ${INPUT_VALIDADE_DO_CARTAO}                              ${validade_do_cartao_de_credito_dois}                            
    Log to console                      VALIDADE DO CARTÃO DE CRÉDITO É: ${validade_do_cartao_de_credito_dois} 
    Input Text                          ${INPUT_CPF_TITULAR_CARTAO}                              ${cpf}                          
    Log to console                      CPF DO TITULAR DO CARTÃO DE CRÉDITO É: ${cpf}       
    Log to console                      @SELECIONADO PAGAMENTO COM O CARTÃO DE CRÉDITOS, DADOS INSERIDOS COM SUCESSO E AVANÇO PARA TELA DE PARCELAMENTO
    Click Element                       ${BUTTON_CONTINUAR_VERDE_CONTINUAR_PARCELAMENTO}
    Log to console                      @SELECIONADO PAGAMENTO COM O CARTÃO DE CRÉDITOS, DADOS INSERIDOS COM SUCESSO E AVANÇO PARA TELA DE PARCELAMENTO
    Wait Until Element Is Visible       ${DIV_PARCELAMENTO_CAARTAO_DE_CREDITO}                   60
    Click Element                       ${DIV_PARCELAMENTO_CAARTAO_DE_CREDITO}
    Wait Until Element Is Visible       ${DIV_PAGAMENTO_LISTA_A_VISTA}                           60                         
    Click Element                       ${DIV_PAGAMENTO_LISTA_A_VISTA} 
    Log to console                      @SELECIONADO O VALOR A VISTA            
    Wait Until Element Is Not Visible   ${DIV_LOADER_ESPERA}                                     60            
    Input Text                          ${INPUT_CVV}                                             ${cvv_cartao_de_credito_dois}  
    Capture Page Screenshot
    Log to console                      @PARCELA E CVV: ${cvv_cartao_de_credito_dois} INSERIDOS
    Log to console                      @SELECIONADO PARCELAMENTO AVANÇO PARA COMCLUIR A COMPRA
    Capture Page Screenshot
    Click Element                       ${BUTTON_CONTINUAR_VERDE_FINALIZAR_COMPRA}   
    Wait Until Element Is Visible       ${DIV_NUMERO_DA_ORDEM_DE_COMPRA}                         60    
    Capture Element Screenshot	        ${DIV_NUMERO_DA_ORDEM_DE_COMPRA}
    Capture Page Screenshot             
    ${NUMERO_DA_ORDEM}=                 Get Text                                                 ${DIV_NUMERO_DA_ORDEM_DE_COMPRA}
    Log to console                      ${NUMERO_DA_ORDEM}


INICIO DO TESTE:
    Sleep                               1	

1 - Verificar se é exibido o botão na cor verde
    Wait Until Element Is Visible       ${ID_LOGO_PIX_VERDE}               60
    Log to console                      LOGO PIX FOI EXIBIDO NA TELA DE PAGAMENTOS NA COR VERDE
    Capture Element Screenshot	        ${ID_LOGO_PIX_VERDE}
    Capture Page Screenshot

##PPE5
REQUISITO 1 - LOGO PIX É EXIBIDO NA TELA DE PAGAMENTOS
    Wait Until Element Is Visible       ${ID_IMAGE_LOGOPIX_PAGAMENTO}               60
    Log to console                      LOGO PIX FOI EXIBIDO NA TELA DE PAGAMENTOS
    Capture Element Screenshot	        ${ID_IMAGE_LOGOPIX_PAGAMENTO}
    Capture Page Screenshot
 
REQUISITO 2 - LOGO CIFRÃO É EXIBIDO AO LADO DO RADIO BUTTON NA TELA DE PAGAMENTOS
    Wait Until Element Is Visible       ${ID_IMAGE_NOTA_DINHEIRO_ICONE}             60
    Log to console                      ICONE DE CIFRÃO FOI EXIBIDO AO LADO DO RADIO BUTTON NA TELA DE PAGAMENTOS
    Capture Element Screenshot	        ${ID_IMAGE_NOTA_DINHEIRO_ICONE}
    Capture Page Screenshot

REQUISITO 3 - AO SELECIONAR O RADIO BUTTON O BOTÃO CONTINUAR DEVE SER HABILITADO
    Wait Until Element Is Visible       ${RADIO_BUTTON_PIX}                         60
    Log to console                      RADIO BUTTON DE SELEÇÃO DO PIX ESTÁ PRESENTE NA TELA DE PAGAMENTOS
    Element Should Be Disabled          ${BUTTON_CONTINUAR_VERDE_PAGAMENTO}                        
    Log to console                      BOTÃO CONTINUAR ESTÁ DESABILITADO
    Capture Element Screenshot	        ${BUTTON_CONTINUAR_VERDE_PAGAMENTO}
    Capture Page Screenshot
    Click Element                       ${RADIO_BUTTON_PIX}
    Element Should Be Enabled           ${BUTTON_CONTINUAR_VERDE_PAGAMENTO} 
    Log to console                      BOTÃO CONTINUAR ESTÁ HABILITADO
    Capture Element Screenshot	        ${BUTTON_CONTINUAR_VERDE_PAGAMENTO}
    Capture Page Screenshot

##PPE14
REQUISITO 1 - INTRUÇÕES DA COMPRA COM O PIX SÃO EXIBIDAS AO CLIENTE
    Wait Until Element Is Visible       ${DIV_DESCRICAO_DA_COMPRA_COM_PIX}                60
    Log to console                      DESCRIÇÃO DA COMPRA COM O PIX FOI EXIBIDA AO CLIENTE
    Capture Element Screenshot	        ${DIV_DESCRICAO_DA_COMPRA_COM_PIX}
    Capture Page Screenshot

REQUISITO 2 - NO RESUMO: FORMA DE PAGAMENTO PIX É EXIBIDO AO CLIENTE
    Wait Until Element Is Visible       ${DIV_RESUMO_FORMA_DE_PAGAMENTO_PIX}              60
    Log to console                      NO RESUMO: FORMA DE PAGAMENTO PIX É EXIBIDO AO CLIENTE
    Capture Element Screenshot	        ${DIV_RESUMO_FORMA_DE_PAGAMENTO_PIX}
    Capture Page Screenshot

REQUISITO 3 - EM PAGAMENTO: LOGO PIX E LOGO CIFRÃO SÃO EXIBIDOS AO CLIENTE
    Wait Until Element Is Visible       ${ID_LOGO_PIX_TELA_PAGAMENTO}                     60
    Wait Until Element Is Visible       ${ID_ICONE_CIFRAO_TELA_PAGAMENTO} 
    Log to console                      EM PAGAMENTO: LOGO PIX E LOGO CIFRÃO SÃO EXIBIDOS AO CLIENTE
    Capture Element Screenshot	        ${ID_LOGO_PIX_TELA_PAGAMENTO}
    Capture Element Screenshot          ${ID_ICONE_CIFRAO_TELA_PAGAMENTO} 
    Capture Page Screenshot

#PPE15
REQUISITO 1 - CONDIÇÃO ESPECIAL EM PAGAMENTO EXIBE A FRASE "COM O PIX"
    Wait Until Element Is Visible       ${SPAN_CONDICAO_ESPECIAL_COM_PIX}                  60
    Log to console                      NO RESUMO: FORMA DE PAGAMENTO PIX É EXIBIDO AO CLIENTE
    Capture Element Screenshot	        ${SPAN_CONDICAO_ESPECIAL_COM_PIX}
    Capture Page Screenshot

#PPE4
REQUISITO 1 - TELA DE PEDIDO FINALIZADO: "PEDIDO REALIZADO COM SUCESSO" É EXIBIDA AO CLIENTE
    Wait Until Element Is Visible       ${DIV_COMPRA_FINALIZADA_SUCESSO_PIX}              60
    Log to console                      TELA DE PEDIDO FINALIZADO: "PEDIDO REALIZADO COM SUCESSO" É EXIBIDO AO CLIENTE
    Capture Element Screenshot	        ${DIV_COMPRA_FINALIZADA_SUCESSO_PIX}   
    Capture Page Screenshot

REQUISITO 2 - CHAVE PIX 
    Wait Until Element Is Visible       ${ID_CHAVE_PIX}                                     60
    Log to console                      CHAVE PIX É EXIBIDA AO CLIENTE
    Capture Element Screenshot	        ${ID_CHAVE_PIX} 
    Capture Page Screenshot
REQUISITO 3 - QRCODE
    Wait Until Element Is Visible       ${ID_QR_CODE}                                       60
    Log to console                      QRCODE EXIBIDO AO CLIENTE
    Capture Element Screenshot	        ${ID_QR_CODE} 

REQUISITO 4 - NOTIFICAÇÃO DE CHAVE PIX COPIADA É EXIBIDA AO CLIENTE
    Wait Until Element Is Visible       ${ID_BOTAO_COPIAR_CHAVE_PIX}                        60  
    Capture Element Screenshot	        ${ID_BOTAO_COPIAR_CHAVE_PIX}
    Capture Page Screenshot
    Log to console                      BOTÃO COPIAR É ACIONADO OBTENDO A CHAVE PIX NO VALOR: 
    Click Element                       ${ID_BOTAO_COPIAR_CHAVE_PIX}   
    Wait Until Element Is Visible       ${DIV_NOTIFICAÇÃO_CHAVE_COPIADA_COM_SUCESSO}        60
    Log to console                      NOTIFICAÇÃO DE CHAVE PIX COPIADA É EXIBIDA AO CLIENTE
    Capture Element Screenshot	        ${DIV_NOTIFICAÇÃO_CHAVE_COPIADA_COM_SUCESSO}
    Capture Page Screenshot

##SQUAD2-321
VALIDAR SE AO CLICAR NA IMAGEM DO CHAT A DIV SE EXPANDE 
    Wait Until Element Is Visible       ${ID_ICONE_CHATBLA_PDP}                             60    
        Set Focus To Element            ${ID_ICONE_CHATBLA_PDP}
    Capture Element Screenshot	        ${ID_ICONE_CHATBLA_PDP} 
    Capture Page Screenshot
    Log to console                      ICONE DO CHATBLA É EXIBIDO AO CLIENTE
        Sleep                           11
    Click Element                       ${ID_ICONE_CHATBLA_PDP} 
    Log to console                      AO CLICAR NO ICONE DO CHATBLA É EXIBIDO AO CLIENTE 
    Capture Page Screenshot

VALIDAR SE AO EXPANDIR É EXIBIDO UMA MENSAGEM AO CLIENTE
    Wait Until Element Is Visible       ${ID_ICONE_CHATBLA_PDP}                             60    
        Set Focus To Element            ${ID_ICONE_CHATBLA_PDP}
    Capture Element Screenshot	        ${ID_ICONE_CHATBLA_PDP} 
    Capture Page Screenshot
    Log to console                      ICONE DO CHATBLA É EXIBIDO AO CLIENTE
        Sleep                           11
    Click Element                       ${ID_ICONE_CHATBLA_PDP} 
    Log to console                      AO CLICAR NO ICONE DO CHATBLA É EXIBIDO AO CLIENTE A MENSAGEM EXPANDIDA
    Capture Page Screenshot

VALIDAR SE AO CLICAR NA MENSAGEM O CLIENTE É REDIRECIONADO PARA A TELA APP
    Wait Until Element Is Visible       ${ID_ICONE_CHATBLA_PDP}                             60    
        Set Focus To Element            ${ID_ICONE_CHATBLA_PDP}
    Capture Element Screenshot	        ${ID_ICONE_CHATBLA_PDP} 
    Capture Page Screenshot
    Log to console                      ICONE DO CHATBLA É EXIBIDO AO CLIENTE
        Sleep                           2
    Click Element                       ${P_MENSAGEM_CHATBLA_PDP} 
        Sleep                           10
    #Wait Until Page Contains Element    ${ID_HOTSITE_CHATBLA_APP}                            60 
    Log to console                      AO CLICAR NA MENSAGEM DO CHATBLA CLIENTE É REDIRECIONADO A TELA APP
    Capture Page Screenshot

##Catálogo Regionalizado
VALIDAR SE É EXIBIDO A MENSAGEM: "INFORME O SEU CEP" PARA UM CLIENTE NÃO LOGADO
    Wait Until Element Is Visible       ${ID_HOME_INFORME_SEU_CEP}                          60    
        Set Focus To Element            ${ID_HOME_INFORME_SEU_CEP}
    Capture Element Screenshot	        ${ID_HOME_INFORME_SEU_CEP}
    Capture Page Screenshot
    Log to console                      INFORME O SEU CEP É EXIBIDO AO CLIENTE
    Capture Page Screenshot

VALIDAR SE AO CLICAR EM INFORME O SEU CEP É EXIBIDO O MODAL AO CLIENTE
    Wait Until Element Is Visible       ${ID_HOME_INFORME_SEU_CEP}                          60    
        Set Focus To Element            ${ID_HOME_INFORME_SEU_CEP}
    Capture Element Screenshot	        ${ID_HOME_INFORME_SEU_CEP}
    Capture Page Screenshot
    Click Element            	        ${ID_HOME_INFORME_SEU_CEP}
    Log to console                      INFORME O SEU CEP É EXIBIDO AO CLIENTE
    Wait Until Element Is Visible       ${ID_IMPUT_PROCURAR_CEP}                            60    
    Capture Element Screenshot	        ${ID_IMPUT_PROCURAR_CEP}
    Log to console                      PROCURAR CEP É EXIBIDO AO CLIENTE


VALIDAR SE AO INFORMAR UM CEP VALIDO O SITE DA FAST SHOP INFORMA AS MELHORES OFERTAS PARA A REGIÃO ${CEP_VALIDO}
    Wait Until Element Is Visible       ${ID_HOME_INFORME_SEU_CEP}                          60    
        Set Focus To Element            ${ID_HOME_INFORME_SEU_CEP}
    Capture Element Screenshot	        ${ID_HOME_INFORME_SEU_CEP}
    Capture Page Screenshot
    Click Element            	        ${ID_HOME_INFORME_SEU_CEP}
    Log to console                      INFORME O SEU CEP É EXIBIDO AO CLIENTE
    Wait Until Element Is Visible       ${ID_IMPUT_PROCURAR_CEP}                            60    
    Input Text	                        ${ID_IMPUT_PROCURAR_CEP}                            ${CEP_VALIDO}
    Capture Page Screenshot    
    Click Element            	        ${ID_CEP_CONFIRMAR_BUSCA}
    Log to console                      CEP DIGITADO E ACIONADO O BOTÃO DE PESQUISA
    Wait Until Element Is Visible       ${ID_CEP_EXIBIDO_AO_CLIENTE_CR}                     60  
    Capture Element Screenshot	        ${ID_CEP_EXIBIDO_AO_CLIENTE_CR}
    Capture Page Screenshot


VALIDAR AO INFORMAR UM CEP INVALIDO O SITE EXIBE UMA MENSAGEM DE ERRO ${CEP_INVALIDO}
    Wait Until Element Is Visible       ${ID_HOME_INFORME_SEU_CEP}                          60    
        Set Focus To Element            ${ID_HOME_INFORME_SEU_CEP}
    Capture Element Screenshot	        ${ID_HOME_INFORME_SEU_CEP}
    Capture Page Screenshot
    Click Element            	        ${ID_HOME_INFORME_SEU_CEP}
    Log to console                      INFORME O SEU CEP É EXIBIDO AO CLIENTE
    Wait Until Element Is Visible       ${ID_IMPUT_PROCURAR_CEP}                            60    
    Input Text	                        ${ID_IMPUT_PROCURAR_CEP}                            ${CEP_INVALIDO}
    Capture Page Screenshot    
    Click Element            	        ${ID_CEP_CONFIRMAR_BUSCA}
    Log to console                      CEP DIGITADO E ACIONADO O BOTÃO DE PESQUISA
    Wait Until Element Is Visible       ${ID_DIV_CEP_NAO_LOCALIZADO}                        60  
    Capture Element Screenshot	        ${ID_DIV_CEP_NAO_LOCALIZADO}
    Capture Page Screenshot

VALIDAR SE O FLUXO NÃO SEI O MEU CEP ESTÁ BUSCANDO DE FORMA CORRETA O ENDEREÇO DO CLIENTE ${CEP_VALIDO} ${NOME_DA_RUA} ${NOME_DA_CIDADE} ${NOME_DO_ESTADO}
    Wait Until Element Is Visible       ${ID_HOME_INFORME_SEU_CEP}                          60    
        Set Focus To Element            ${ID_HOME_INFORME_SEU_CEP}
    Capture Element Screenshot	        ${ID_HOME_INFORME_SEU_CEP}
    Capture Page Screenshot
    Click Element            	        ${ID_HOME_INFORME_SEU_CEP}
    Log to console                      INFORME O SEU CEP É EXIBIDO AO CLIENTE
    Wait Until Element Is Visible       ${ID_CEP_NAO_SEI_MEU_CEP}                           60     
    Capture Page Screenshot   
    Click Element            	        ${ID_CEP_NAO_SEI_MEU_CEP}
    Log to console                      CEP DIGITADO E ACIONADO O BOTÃO DE PESQUISA
    Wait Until Element Is Visible       ${ID_IMPUT_NOME_DA_RUA_NAO_SEI_CEP}                 60  
    Capture Page Screenshot
    Log to console                      MODAL NÃO SEI O MEU CEP É EXIBIDO AO CLIENTE PARA PREENCHER
    Input Text	                        ${ID_IMPUT_NOME_DA_RUA_NAO_SEI_CEP}                 ${NOME_DA_RUA}
    Log to console                      NOME DA RUA PREENCHIDO: ${NOME_DA_RUA}
    Input Text	                        ${ID_IMPUT_NOME_DA_CIDADE_NAO_SEI_CEP}              ${NOME_DA_CIDADE}
    Log to console                      NOME DA CIDADE PREENCHIDO: ${NOME_DA_CIDADE}
    Select From List By Value           ${ID_IMPUT_NOME_DO_ESTADO_NAO_SEI_CEP}              ${NOME_DO_ESTADO}
    Log to console                      NOME DO ESTADO PREENCHIDO: ${NOME_DO_ESTADO}
    Capture Page Screenshot
    Click Element                       ${ID_CEP_CONFIRMAR_BUSCA_NAO_SEI_MEU_CEP}
    Wait Until Element Is Visible       ${ID_CONFIRMA_ENDERECO_BUSCA_NAO_SEI_CEP}           60  
    Click Element                       ${ID_CONFIRMA_ENDERECO_BUSCA_NAO_SEI_CEP}
    Capture Element Screenshot	        ${ID_CONFIRMA_ENDERECO_BUSCA_NAO_SEI_CEP}
    Log to console                      ENDEREÇO DA BUSCA DO CLIENTE FOI SELECIONADO
    Click Element                       ${ID_NAO_SEI_CEP_CONFIRMAR_BUSCA} 
    Wait Until Element Is Visible       ${ID_CEP_EXIBIDO_AO_CLIENTE_CR}                     60
    Capture Element Screenshot          ${ID_CEP_EXIBIDO_AO_CLIENTE_CR}  

VALIDAR SE AO CLIENTE INFORMAR UM ENDEREÇO INCORRETO O SITE INFORMA CEP NÃO LOCALIZADO ${CEP_VALIDO} ${NOME_DA_RUA_INVALIDO} ${NOME_DA_CIDADE} ${NOME_DO_ESTADO} ${INFORMAR_CPF} ${INFORMAR_SENHA}
    Wait Until Element Is Visible       ${ID_HOME_INFORME_SEU_CEP}                          60    
        Set Focus To Element            ${ID_HOME_INFORME_SEU_CEP}
    Capture Element Screenshot	        ${ID_HOME_INFORME_SEU_CEP}
    Capture Page Screenshot
    Click Element            	        ${ID_HOME_INFORME_SEU_CEP}
    Log to console                      INFORME O SEU CEP É EXIBIDO AO CLIENTE
    Wait Until Element Is Visible       ${ID_CEP_NAO_SEI_MEU_CEP}                           60     
    Capture Page Screenshot   
    Click Element            	        ${ID_CEP_NAO_SEI_MEU_CEP}
    Log to console                      CEP DIGITADO E ACIONADO O BOTÃO DE PESQUISA
    Wait Until Element Is Visible       ${ID_IMPUT_NOME_DA_RUA_NAO_SEI_CEP}                 60  
    Capture Page Screenshot
    Log to console                      MODAL NÃO SEI O MEU CEP É EXIBIDO AO CLIENTE PARA PREENCHER
    Input Text	                        ${ID_IMPUT_NOME_DA_RUA_NAO_SEI_CEP}                 ${NOME_DA_RUA_INVALIDO}
    Log to console                      NOME DA RUA PREENCHIDO: ${NOME_DA_RUA_INVALIDO}
    Input Text	                        ${ID_IMPUT_NOME_DA_CIDADE_NAO_SEI_CEP}              ${NOME_DA_CIDADE}
    Log to console                      NOME DA CIDADE PREENCHIDO: ${NOME_DA_CIDADE}
    Select From List By Value           ${ID_IMPUT_NOME_DO_ESTADO_NAO_SEI_CEP}              ${NOME_DO_ESTADO}
    Log to console                      NOME DO ESTADO PREENCHIDO: ${NOME_DO_ESTADO}
    Capture Page Screenshot
    Click Element                       ${ID_CEP_CONFIRMAR_BUSCA_NAO_SEI_MEU_CEP}
    Wait Until Element Is Visible       ${P_AVISO_DE_CEP_NAO_ENCONTRADO}                     60  
    Capture Element Screenshot	        ${P_AVISO_DE_CEP_NAO_ENCONTRADO}
    Log to console                      ENDEREÇO DA BUSCA DO CLIENTE FOI SELECIONADO

VALIDAR SE AO REALIZAR A PARTIR DO BOTÃO FAÇA O LOGIN NO MODAL O CLIENTE VISUALIZA SEUS ENDEREÇOS SALVOS ${CEP_VALIDO} ${NOME_DA_RUA_INVALIDO} ${NOME_DA_CIDADE} ${NOME_DO_ESTADO} ${INFORMAR_CPF} ${INFORMAR_SENHA}
    Wait Until Element Is Visible       ${ID_HOME_INFORME_SEU_CEP}                          60    
        Set Focus To Element            ${ID_HOME_INFORME_SEU_CEP}
    Capture Element Screenshot	        ${ID_HOME_INFORME_SEU_CEP}
    Capture Page Screenshot
    Click Element            	        ${ID_HOME_INFORME_SEU_CEP}
    Log to console                      INFORME O SEU CEP É EXIBIDO AO CLIENTE
    Wait Until Element Is Visible       ${ID_CEP_FACA_SEU_LOGIN}                            60     
    Capture Page Screenshot   
    Click Element            	        ${ID_CEP_FACA_SEU_LOGIN}             

    Wait Until Element Is Visible       ${ID_LOGIN_CPF}                             30
    Input Text	                        ${ID_LOGIN_CPF}                             ${INFORMAR_CPF}
    Input Text	                        ${ID_LOGIN_SENHA}                           ${INFORMAR_SENHA}
    Click Element                       ${BUTTON_ENTRAR_VERDE} 
    Capture Page Screenshot
    Log to console                      CPF DO TESTE: ${INFORMAR_CPF} 
    Log to console                      SENHA: ${INFORMAR_SENHA}
    Log to console                      @USUARIO LOGADO, SEGUINDO PARA A HOME
    Wait Until Element Is Visible       ${ID_CEP_EXIBIDO_AO_CLIENTE_CR}                      30
    Capture Element Screenshot	        ${ID_CEP_EXIBIDO_AO_CLIENTE_CR}
    Capture Page Screenshot

QUANDO REALIZO UMA BUSCA ${busca_produto}
    Input Text	                        id:search                      ${busca_produto}
    Click Element	                    //button[@class='button-search']
    Capture Page Screenshot

VALIDAR AO AUMENTAR A QUANTIDADE DO PRODUTO O BOTÃO DE CLICK COLLECTION É OCULTADO
    Wait Until Element Is Visible       ${ID_BOTAO_CLICK_COLLECTION_PDP}                             60    
        Set Focus To Element            ${ID_BOTAO_CLICK_COLLECTION_PDP} 
                Sleep                   3
    Capture Element Screenshot	        ${ID_BOTAO_CLICK_COLLECTION_PDP}  
    Capture Page Screenshot
    Log to console                      ICONE CLICK COLECTION É EXIBIDO
    Click Element                       ${ID_AUMENTAR_A_QUANTIDADE_PDP} 
    Wait Until Element Is Not Visible   ${ID_BOTAO_CLICK_COLLECTION_PDP}                             3
    Log to console                      AO CLICAR EM AUMENTAR A QUANTIDADE O BOTÃO DE CLICK COLLECTION É OCULTADO
    Capture Page Screenshot

VALIDAR SE QUANDO O USUARIO CLICAR NO BOTÃO O MODAL VAI ABRIR
    Wait Until Element Is Visible       ${ID_SAIBA_MAIS_MINHA_ASSINATURA}                            60    
        Set Focus To Element            ${ID_SAIBA_MAIS_MINHA_ASSINATURA} 
                Sleep                   3
    Capture Element Screenshot	        ${ID_SAIBA_MAIS_MINHA_ASSINATURA}  
    Capture Page Screenshot
    Log to console                      ICONE SAIBA MAIS É EXIBIDO
    Click Element                       ${ID_SAIBA_MAIS_MINHA_ASSINATURA} 
    Wait Until Element Is Visible      ${DIV_CLASS_BENEFICIOS_PRIME}                                 60    
        Set Focus To Element            ${DIV_CLASS_BENEFICIOS_PRIME} 
                Sleep                   3
    Capture Element Screenshot	        ${DIV_CLASS_BENEFICIOS_PRIME} 
    Log to console                      TELA DE BENEFICIOS É EXIBIDA 
    Capture Page Screenshot

VALIDAR O COMPORTAMENTO PARA O CLIENTE PRIME
    Wait Until Element Is Visible       ${ID_SAIBA_MAIS_MINHA_ASSINATURA}                            60    
        Set Focus To Element            ${ID_SAIBA_MAIS_MINHA_ASSINATURA} 
                Sleep                   3
    Capture Element Screenshot	        ${ID_SAIBA_MAIS_MINHA_ASSINATURA}  
    Capture Page Screenshot
    Log to console                      ICONE SAIBA MAIS É EXIBIDO
    Click Element                       ${ID_SAIBA_MAIS_MINHA_ASSINATURA} 

     Wait Until Element Is Visible      ${DIV_CLASS_BENEFICIOS_PRIME}                                 60    
        Set Focus To Element            ${DIV_CLASS_BENEFICIOS_PRIME} 
                Sleep                   3
    Capture Element Screenshot	        ${DIV_CLASS_BENEFICIOS_PRIME} 
    Log to console                      TELA DE BENEFICIOS É EXIBIDA PARA O CLIENTE PRIME
    Capture Page Screenshot

VALIDAR O COMPORTAMENTO PARA O CLIENTE NÃO PRIME
    Wait Until Element Is Visible       ${DIV_CLASS_NOTIFICACAO_NAO_PRIME}                            60    
        Set Focus To Element            ${DIV_CLASS_NOTIFICACAO_NAO_PRIME} 
                Sleep                   3
    Capture Element Screenshot	        ${DIV_CLASS_NOTIFICACAO_NAO_PRIME}  
    Capture Page Screenshot             
    Log to console                      NOTIFICAÇÃO EXIBIDA PARA O CLIENTE NÃO PRIME

###RPA GOLDEN FRIDAY
1 - FOTO E HORÁRIO DO SKU: ${busca_produto} - CLIQUE AQUI PARA EXIBIR AS EVIDÊNCIAS
    Log                                 ************************************************************************************************************************************           
    Capture Page Screenshot             ${OUTPUTDIR}/Evidencias_Por_Nome_Do_Sku/${busca_produto}_busca.png                                
    Log                                 ************************************************************************************************************************************  
    Log to console                      EVIDÊNCIAS DO SKU: ${busca_produto} COLETADA COM SUCESSO.
    ${URL_ACESSADA}=                    Get Location
    Log                                 URL: ${URL_ACESSADA}
    Log to console                      URL: ${URL_ACESSADA}

2 - FOTO E HORÁRIO DA PÁGINA DO PRODUTO: ${busca_produto_pdp} - CLIQUE AQUI PARA EXIBIR AS EVIDÊNCIAS 
    Go To                               ${BASE_URL}web/p/d/${busca_produto_pdp}/verificadordeprecopdp
    ${URL_ACESSADA}=                    Get Location
    Log                                 URL: ${URL_ACESSADA}
    Log to console                      URL: ${URL_ACESSADA}
    #Wait Until Element Is Visible       ${ID_ACEITAR_COOKIES}                       60
    Wait Until Element Is Visible       ${ID_ICONE_CHATBLA_PDP}
        Sleep                           3    
    Execute javascript                  document.body.style.zoom="50%"   
    Log                                 ************************************************************************************************************************************               
    Capture Page Screenshot             ${OUTPUTDIR}/Evidencias_Por_Nome_Do_Sku/${busca_produto_pdp}_pdp.png   
    Log                                 ************************************************************************************************************************************           
    Log to console                      EVIDÊNCIAS DO SKU NA PDP: ${busca_produto_pdp} COLETADA COM SUCESSO.

1 - VALIDAR COMPONENTES DO HEADER DA PÁGINA DE PRODUÇÃO
    Log to console                      *** PROCURANDO ELEMENTO DE BUSCA NA HOME...
    Wait Until Element Is Visible       ${ID_INBOX_BUSCA_HOME}                                         ${TEMPO_PARA_ENCONTRAR_ELEMENTO}    
    Log to console                      *************** EVIDÊNCIAS ABAIXO CLICANDO EM CAPTURE ELEMENT SCREENSHOT ***************
    Capture Element Screenshot	        ${ID_INBOX_BUSCA_HOME}
    Capture Page Screenshot
    Log to console                      ELEMENTO DE BUSCA É EXIBIDO NA HOME: ${ID_INBOX_BUSCA_HOME}
    Log to console                      *** PROCURANDO ELEMENTO DE BUSCA DE CEP DO CATÁLOGO REGIONALIZADO NA HOME...
    Wait Until Element Is Visible       ${ID_HOME_INFORME_SEU_CEP}                                     ${TEMPO_PARA_ENCONTRAR_ELEMENTO}    
    Log to console                      *************** EVIDÊNCIAS ABAIXO CLICANDO EM CAPTURE ELEMENT SCREENSHOT ***************
    Capture Element Screenshot	        ${ID_HOME_INFORME_SEU_CEP}
    Log to console                      ELEMENTO DE BUSCA DE CEP DO CATÁLOGO REGIONALIZADO É EXIBIDO NA HOME: ${ID_HOME_INFORME_SEU_CEP}
    Log to console                      *** PROCURANDO ICONE DO CARRINHO NA HOME...
    Wait Until Element Is Visible       ${ID_BUTTON_ICONE_CARRINHO_HOME}                               ${TEMPO_PARA_ENCONTRAR_ELEMENTO}    
    Log to console                  *************** EVIDÊNCIAS ABAIXO CLICANDO EM CAPTURE ELEMENT SCREENSHOT ***************
    Capture Element Screenshot	        ${ID_BUTTON_ICONE_CARRINHO_HOME}
    Capture Page Screenshot
    Log to console                      ICONE DO CARRINHO É EXIBIDO NA HOME: ${ID_BUTTON_ICONE_CARRINHO_HOME}
    Log to console                      *** PROCURANDO ICONE DE FAVORITOS NA HOME...
    Wait Until Element Is Visible       ${ID_BUTTON_ICONE_FAVORITOS_HOME}                              ${TEMPO_PARA_ENCONTRAR_ELEMENTO}    
    Log to console                  *************** EVIDÊNCIAS ABAIXO CLICANDO EM CAPTURE ELEMENT SCREENSHOT ***************
    Capture Element Screenshot	        ${ID_BUTTON_ICONE_FAVORITOS_HOME}
    Capture Page Screenshot
    Log to console                      ICONE DE FAVORITOS É EXIBIDO NA HOME: ${ID_BUTTON_ICONE_FAVORITOS_HOME}    
                Sleep                   3

2 - VALIDAR COMPONENTES DA PARTE INFERIOR DA PÁGINA DE PRODUÇÃO 
    Press Keys                          NONE                                                            END
                Sleep                   3
    Press Keys                          NONE                                                            END
    Log to console                      *** PROCURANDO RODAPÉ DA HOME...
    Wait Until Element Is Visible       ${ID_DIV_INFERIOR_DA_HOME_RODAPE}                               ${TEMPO_PARA_ENCONTRAR_ELEMENTO}    
        Set Focus To Element            ${ID_DIV_INFERIOR_DA_HOME_RODAPE}
                Sleep                   3
        Log to console                  *************** EVIDÊNCIAS ABAIXO CLICANDO EM CAPTURE ELEMENT SCREENSHOT ***************
    Capture Element Screenshot	        ${ID_DIV_INFERIOR_DA_HOME_RODAPE}
    Capture Page Screenshot
    Log to console                      RODAPÉ É EXIBIDO NA HOME: ${ID_DIV_INFERIOR_DA_HOME_RODAPE}    
    Log to console                      *** PROCURANDO ICONE DA SECTIGO NA HOME...
    Wait Until Element Is Visible       ${ID_ICONE_SELO_SECTIGO}                                         ${TEMPO_PARA_ENCONTRAR_ELEMENTO}    
    Log to console                  *************** EVIDÊNCIAS ABAIXO CLICANDO EM CAPTURE ELEMENT SCREENSHOT ***************
    Capture Element Screenshot	        ${ID_ICONE_SELO_SECTIGO}
    Log to console                      ICONE DA SECTIGO É EXIBIDO NA HOME: ${ID_ICONE_SELO_SECTIGO}    
    Log to console                      *** PROCURANDO ICONE DE PAGAMENTO DO CARTÃO VISA NA HOME...
    Wait Until Element Is Visible       ${ID_ICONE_PAGAMENTO_VISA_HOME}                                  ${TEMPO_PARA_ENCONTRAR_ELEMENTO}    
    Log to console                  *************** EVIDÊNCIAS ABAIXO CLICANDO EM CAPTURE ELEMENT SCREENSHOT ***************
    Capture Element Screenshot	        ${ID_ICONE_PAGAMENTO_VISA_HOME}
    Log to console                      ICONE DE PAGAMENTO DO CARTÃO VISA É EXIBIDO NA HOME: ${ID_ICONE_PAGAMENTO_VISA_HOME}    
    Log to console                      *** PROCURANDO ICONE DE PAGAMENTO DO CARTÃO MASTER NA HOME...
    Wait Until Element Is Visible       ${ID_ICONE_PAGAMENTO_MASTER_HOME}                                ${TEMPO_PARA_ENCONTRAR_ELEMENTO}    
        Log to console                  *************** EVIDÊNCIAS ABAIXO CLICANDO EM CAPTURE ELEMENT SCREENSHOT ***************
    Capture Element Screenshot	        ${ID_ICONE_PAGAMENTO_MASTER_HOME}
    Log to console                      ICONE DE PAGAMENTO DO CARTÃO MASTER É EXIBIDO NA HOME: ${ID_ICONE_PAGAMENTO_MASTER_HOME}    
    Log to console                      *** PROCURANDO ICONE DE PAGAMENTO DO CARTÃO AMEX NA HOME...
    Wait Until Element Is Visible       ${ID_ICONE_PAGAMENTO_AMEX_HOME}                                  ${TEMPO_PARA_ENCONTRAR_ELEMENTO}    
        Log to console                  *************** EVIDÊNCIAS ABAIXO CLICANDO EM CAPTURE ELEMENT SCREENSHOT ***************
    Capture Element Screenshot	        ${ID_ICONE_PAGAMENTO_AMEX_HOME}
    Log to console                      ICONE DE PAGAMENTO DO CARTÃO AMEX É EXIBIDO NA HOME: ${ID_ICONE_PAGAMENTO_AMEX_HOME}    
    Log to console                      *** PROCURANDO ICONE DE PAGAMENTO DO CARTÃO DINNERS NA HOME...
    Wait Until Element Is Visible       ${ID_ICONE_PAGAMENTO_DINNERS_HOME}                               ${TEMPO_PARA_ENCONTRAR_ELEMENTO}    
        Log to console                  *************** EVIDÊNCIAS ABAIXO CLICANDO EM CAPTURE ELEMENT SCREENSHOT ***************
    Capture Element Screenshot	        ${ID_ICONE_PAGAMENTO_DINNERS_HOME}
    Log to console                      ICONE DE PAGAMENTO DO CARTÃO DINNERS É EXIBIDO NA HOME: ${ID_ICONE_PAGAMENTO_DINNERS_HOME}   
    Log to console                      *** PROCURANDO ICONE DE PAGAMENTO DO CARTÃO ELO NA HOME...
    Wait Until Element Is Visible       ${ID_ICONE_PAGAMENTO_ELO_HOME}                                   ${TEMPO_PARA_ENCONTRAR_ELEMENTO}    
        Log to console                  *************** EVIDÊNCIAS ABAIXO CLICANDO EM CAPTURE ELEMENT SCREENSHOT ***************
    Capture Element Screenshot	        ${ID_ICONE_PAGAMENTO_ELO_HOME}
    Log to console                      ICONE DE PAGAMENTO DO CARTÃO ELO É EXIBIDO NA HOME: ${ID_ICONE_PAGAMENTO_ELO_HOME}   
    Log to console                      *** PROCURANDO ICONE DE PAGAMENTO COM BOLETO NA HOME...
    Wait Until Element Is Visible       ${ID_ICONE_PAGAMENTO_BOLETO_HOME}                                ${TEMPO_PARA_ENCONTRAR_ELEMENTO}    
        Log to console                  *************** EVIDÊNCIAS ABAIXO CLICANDO EM CAPTURE ELEMENT SCREENSHOT ***************
    Capture Element Screenshot	        ${ID_ICONE_PAGAMENTO_BOLETO_HOME}
    Log to console                      ICONE DE PAGAMENTO COM BOLETO É EXIBIDO NA HOME: ${ID_ICONE_PAGAMENTO_BOLETO_HOME}   
    Log to console                      *** PROCURANDO ICONE DE PAGAMENTO DO CARTÃO PIX NA HOME...
    Wait Until Element Is Visible       ${ID_ICONE_PAGAMENTO_PIX_HOME}                                ${TEMPO_PARA_ENCONTRAR_ELEMENTO}    
        Log to console                  *************** EVIDÊNCIAS ABAIXO CLICANDO EM CAPTURE ELEMENT SCREENSHOT ***************
    Capture Element Screenshot	        ${ID_ICONE_PAGAMENTO_PIX_HOME}
    Log to console                      ICONE DE PAGAMENTO COM PIX NA HOME: ${ID_ICONE_PAGAMENTO_PIX_HOME}
    Log to console                      *** ICONE DO APLICATIVO GOOGLE PAY É EXIBIDO NA HOME
    Wait Until Element Is Visible       ${ID_ICONE_HOME_APP_GOOGLE_PLAY}                                ${TEMPO_PARA_ENCONTRAR_ELEMENTO}    
        Log to console                  *************** EVIDÊNCIAS ABAIXO CLICANDO EM CAPTURE ELEMENT SCREENSHOT ***************
    Capture Element Screenshot	        ${ID_ICONE_HOME_APP_GOOGLE_PLAY}
    Log to console                      ICONE DO APLICATIVO GOOGLE PAY É EXIBIDO NA HOME: ${ID_ICONE_HOME_APP_GOOGLE_PLAY}
    Log to console                      *** PROCURANDO ICONE DO APLICATIVO APP STORE É EXIBIDO NA HOME
    Wait Until Element Is Visible       ${ID_ICONE_HOME_APP_STORE}                                ${TEMPO_PARA_ENCONTRAR_ELEMENTO}    
        Log to console                  *************** EVIDÊNCIAS ABAIXO CLICANDO EM CAPTURE ELEMENT SCREENSHOT ***************
    Capture Element Screenshot	        ${ID_ICONE_HOME_APP_STORE}
    Log to console                      ICONE DO APLICATIVO APP STORE É EXIBIDO NA HOME: ${ID_ICONE_HOME_APP_STORE}
    #Log to console                      *** PROCURANDO SELO EBIT EXIBIDO NA HOME
    #Wait Until Element Is Visible       ${ID_ICONE_SELO_EBIT_HOME}                                ${TEMPO_PARA_ENCONTRAR_ELEMENTO}    
    #    Log to console                  *************** EVIDÊNCIAS ABAIXO CLICANDO EM CAPTURE ELEMENT SCREENSHOT ***************
    #Capture Element Screenshot	        ${ID_ICONE_SELO_EBIT_HOME}
   #Log to console                      SELO EBIT É EXIBIDO NA HOME: ${ID_ICONE_SELO_EBIT_HOME}
    Log to console                      *** PROCURANDO SELO RECLAME AQUI EXIBIDO NA HOME
    Wait Until Element Is Visible       ${ID_ICONE_SELO_RECLAME_AQUI_HOME}                                ${TEMPO_PARA_ENCONTRAR_ELEMENTO}    
        Log to console                  *************** EVIDÊNCIAS ABAIXO CLICANDO EM CAPTURE ELEMENT SCREENSHOT ***************
    Capture Element Screenshot	        ${ID_ICONE_SELO_RECLAME_AQUI_HOME}
    Log to console                      SELO RECLAME AQUI É EXIBIDO NA HOME: ${ID_ICONE_SELO_RECLAME_AQUI_HOME}
    Log to console                      *** PROCURANDO SELO COMPRE E CONFIE EXIBIDO NA HOME
    #Wait Until Element Is Visible       ${ID_ICONE_SELO_COMPRE_E_CONFIE_HOME}                             ${TEMPO_PARA_ENCONTRAR_ELEMENTO}    
    #    Log to console                  *************** EVIDÊNCIAS ABAIXO CLICANDO EM CAPTURE ELEMENT SCREENSHOT ***************
    #Capture Element Screenshot	        ${ID_ICONE_SELO_COMPRE_E_CONFIE_HOME}
    #Log to console                      SELO COMPRE E CONFIE É EXIBIDO NA HOME: ${ID_ICONE_SELO_COMPRE_E_CONFIE_HOME}

###CLICK&COLLECT
1 - VERIFICAR SE O BOTÃO RETIRAR EM LOJA É EXIBIDO NA TELA DE PRODUTOS E CLICKAR NO ELEMENTO 
    Wait Until Element Is Visible       ${ID_BOTAO_CLICK_COLLECTION_PDP}            30
            Set Focus To Element        ${ID_BOTAO_CLICK_COLLECTION_PDP}
            Sleep                       3
    Capture Element Screenshot          ${ID_BOTAO_CLICK_COLLECTION_PDP}  
    Log to console                      ELEMENTO ${ID_BOTAO_CLICK_COLLECTION_PDP} É EXIBIDO COM SUCESSO  
    Click Element                       ${ID_BOTAO_CLICK_COLLECTION_PDP}
    Wait Until Element Is Visible       ${DIV_MODAL_RETIRAR_EM_LOJA}                30
    Capture Element Screenshot          ${DIV_MODAL_RETIRAR_EM_LOJA}      
    Capture Page Screenshot

2 - VERIFICAR SE AO REALIZAR UMA BUSCA NO CAMPO CEP UM ICONE VERDE É EXIBIDO AO USUARIO
    Wait Until Element Is Visible       ${DIV_CEP_MODAL_RETIRAR_EM_LOJA}                30  
    Click Element                       ${DIV_CEP_MODAL_RETIRAR_EM_LOJA}  
    Wait Until Element Is Visible       ${LABEL_CEP_BUSCA_RETIRAR_EM_LOJA}              30
    Input Text	                        ${IMPUT_CEP_CLICK_COLLECTION}                   07077010
    Wait Until Element Is Visible       ${I_ICONE_VERIFICAÇÃO_VERDE_RETIRAR_EM_LOJA}    30
    Capture Element Screenshot          ${I_ICONE_VERIFICAÇÃO_VERDE_RETIRAR_EM_LOJA}  
    Capture Page Screenshot
    
2 - ESCOLHER A OPÇÃO BUSCA POR CIDADE E SELECIONAR UMA LOJA 
    Wait Until Element Is Visible       ${DIV_CIDADE_MODAL_RETIRAR_EM_LOJA}                     30  
    Click Element                       ${DIV_CIDADE_MODAL_RETIRAR_EM_LOJA}  
    Wait Until Element Is Visible       ${SELECT_BUSCA_ESTADO_CLICK_CLOCTION}                   30
    Select From List By Value           ${SELECT_BUSCA_ESTADO_CLICK_CLOCTION}           ${NOME_DO_ESTADO_BUSCA_CLICK}
    Log to console                      NOME DO ESTADO PREENCHIDO: ${NOME_DO_ESTADO_BUSCA_CLICK}
    Select From List By Value           ${SELECT_BUSCA_CIDADE_CLICK_CLOCTION}           ${NOME_DA_CIDADE_BUSCA_CLICK}
    Log to console                      NOME DO ESTADO PREENCHIDO: ${NOME_DA_CIDADE_BUSCA_CLICK} 
    Capture Page Screenshot
    Click Element                       ${BOTÃO_CONFIRMAR_BUSCA_ESTADO_CLICK_COLLECTION}
    Wait Until Element Is Visible       ${DIV_PRODUTO_DISPONIVEL_EM_LOJA_CICK_COLLECTON}         30
    Click Element                       ${DIV_PRODUTO_DISPONIVEL_EM_LOJA_CICK_COLLECTON}
    Capture Element Screenshot          ${DIV_PRODUTO_DISPONIVEL_EM_LOJA_CICK_COLLECTON}
    Log to console                      PRODUTO: ${SKU} DISPONIVEL EM LOJA 
    Capture Page Screenshot             
    Click Element                       ${BUTTON_APLICAR_CICK_COLLECTON}
    Capture Page Screenshot
    Click Element                       ${BUTTON_CONTINUAR_MODAL_INFO_CICK_COLLECTON}

3 - REALIZAR LOGIN E SEGUIR PARA O CARRINHO NO MODO CLICK COLLECTION
    Wait Until Element Is Visible       ${ID_LOGIN_CPF}                             30
    Input Text	                        ${ID_LOGIN_CPF}                             ${CPF_AMBIENTE}    
    Input Text	                        ${ID_LOGIN_SENHA}                           ${SENHA_AMBIENTE}
        Sleep                           1                      
    Click Element                       ${BUTTON_CONFIRMAR_VERDE_CLICK} 
    Log to console                      CPF DO TESTE: ${CPF_AMBIENTE} 
    Log to console                      SENHA: ${SENHA_AMBIENTE}
    Log to console                      @LOGIN EFETUADO COM SUCESSO

4 - PAGAMENTO COM O CARTÃO DE CRÉDITO NO CLICK_COLLECTION
    Wait Until Element Is Visible       ${LABEL_PAGAR_VIA_CARTAO_DE_CREDITO}                     200
    Log to console                      RADIO BUTTON DE SELEÇÃO DO CARTÃO DE CRÉDITO ESTÁ PRESENTE NA TELA DE PAGAMENTOS
    Click Element                       ${LABEL_PAGAR_VIA_CARTAO_DE_CREDITO}
    Log to console                      BOTÃO CONTINUAR ESTÁ HABILITADO
    Click Element                       ${BUTTON_CONTINUAR_VERDE_PAGAMENTO}     
    Log to console                      @SELECIONADO PAGAMENTO COM O CARTÃO DE CRÉDITO E AVANÇO DE TELA NO BOTÃO CONTINUAR
    Capture Page Screenshot
    Wait Until Element Is Visible       ${INPUT_NUMERO_CARTAO_DE_CREDITO}                        60
    Log to console                      @TELA DE PGAMENTO COM O CARTÃO DE CRÉDITO, DADOS DO CARTÃO PARA SEREM INSERIDOS.
    Capture Page Screenshot            
    Input Text                          ${INPUT_NUMERO_CARTAO_DE_CREDITO}                        ${${NUMERO_DO_CC_AMBIENTE}}       
    Log to console                      NÚMERO DO CARTÃO DE CRÉDITO É: ${${NUMERO_DO_CC_AMBIENTE}}          
    Input Text                          ${INPUT_NOME_TITULAR_DO_CARTAO}                          WILLIAM MOORE HZ                                                              
    Log to console                      NOME DO TITULAR DO CARTÃO DE CRÉDITO É: WILLIAM MOORE HZ    
    Input Text                          ${INPUT_VALIDADE_DO_CARTAO}                              ${DATA_VALIDADE_AMBIENTE}                             
    Log to console                      VALIDADE DO CARTÃO DE CRÉDITO É: ${DATA_VALIDADE_AMBIENTE} 
    Input Text                          ${INPUT_CPF_TITULAR_CARTAO}                              ${CPF_AMBIENTE}                          
    Log to console                      CPF DO TITULAR DO CARTÃO DE CRÉDITO É: ${CPF_AMBIENTE}       
    Log to console                      @SELECIONADO PAGAMENTO COM O CARTÃO DE CRÉDITOS, DADOS INSERIDOS COM SUCESSO
    Click Element                       ${BUTTON_CONTINUAR_VERDE_CONTINUAR_PARCELAMENTO}
    Wait Until Element Is Visible       ${DIV_PARCELAMENTO_CAARTAO_DE_CREDITO}                   60
    Click Element                       ${DIV_PARCELAMENTO_CAARTAO_DE_CREDITO}
    Wait Until Element Is Visible       ${DIV_PAGAMENTO_LISTA_A_VISTA}                           60                         
    Click Element                       ${DIV_PAGAMENTO_LISTA_A_VISTA} 
    Log to console                      @SELECIONADO O VALOR A VISTA            
    Wait Until Element Is Not Visible   ${DIV_LOADER_ESPERA}                                     60            
    Input Text                          ${INPUT_CVV}                                             ${CVV_DO_CC_AMBIENTE}  
    Capture Page Screenshot
    Click Element                       ${BUTTON_CONTINUAR_VERDE_FINALIZAR_COMPRA}   
    Wait Until Element Is Visible       ${DIV_NUMERO_DA_ORDEM_DE_COMPRA}                         200
            Capture Page Screenshot             
            Set Focus To Element        ${DIV_NUMERO_DA_ORDEM_DE_COMPRA}
            Sleep                       5 
    Capture Element Screenshot	        ${DIV_NUMERO_DA_ORDEM_DE_COMPRA}
    Capture Page Screenshot             
    ${NUMERO_DA_ORDEM}=                 Get Text                                                 ${DIV_NUMERO_DA_ORDEM_DE_COMPRA}
    Log to console                      ${NUMERO_DA_ORDEM}
    Log to console                      @PARCELA E CVV: ${CVV_DO_CC_AMBIENTE} INSERIDOS, BOTÃO DE FINALIZAR A COMPRA SELECIONADO PARA AVANÇAR DE TELA

ACESSAR TELA DE LOGIN COM CPF E SENHA - ${CPF_QRCODE} ${SENHA_QRCODE}
    Go To                               https://${AMBIENTE}.fastshop.com.br/web/checkout-v2/login
    Wait Until Element Is Visible       ${ID_LOGIN_CPF}             30
    Input Text                          ${ID_LOGIN_CPF}             ${CPF_QRCODE}
    Input Text                          ${ID_LOGIN_SENHA}           ${SENHA_QRCODE}
    Click Element                       ${BUTTON_ENTRAR_VERDE} 
    Capture Page Screenshot
    Sleep                               8
    Log to console                      CPF: ${CPF_QRCODE}
    Log to console                      LOGIN EFETUADO COM SUCESSO

ACESSAR TELA DE PEDIDOS E IR ATÉ O PEDIDO ${NUMERO_PEDIDO}
    Go To                                   https://${AMBIENTE}.fastshop.com.br/web/autoatendimento/acompanhar-pedidos
    Wait Until Element Is Visible           //h2[contains(text(), 'Acompanhar meus pedidos')][@class='sub-title d-none d-md-block']             40
    Capture Page Screenshot 
    Log to console                          TELA DE PEDIDOS ACESSADA COM SUCESSO
    #FOR        ${CONTADOR}     IN RANGE        1       7
        Run Keyword And Ignore Error        Scroll Element Into View    //button[contains(text(), 'Carregar mais pedidos')][@class='load-more']
        Set Focus To Element                //button[contains(text(), 'Carregar mais pedidos')][@class='load-more']
        Sleep                               3
        Click Element                       //button[contains(text(), 'Carregar mais pedidos')][@class='load-more']
        Sleep                               20
    #    Exit For Loop If                    '${CONTADOR}' == '6'
    #END
    Run Keyword And Ignore Error            Scroll Element Into View                //strong[contains(text(), '${NUMERO_PEDIDO}')]
    Set Focus To Element                    //strong[contains(text(), '${NUMERO_PEDIDO}')]
    Sleep                                   3
    Capture Page Screenshot
    Run Keyword And Ignore Error            Scroll Element Into View                    //div[contains(text(),'${DATA_HORA_DISPONIVEL_RETIRADA}')]
    Sleep                                   3
    Capture Page Screenshot
    Run Keyword And Ignore Error            Scroll Element Into View                    //button[contains(text(), 'Liberar QR CODE')]
    Sleep                                   3
    Capture Page Screenshot
    Log to console                          PEDIDO ENCONTRADO COM SUCESSO
    
CLICAR EM LIBERAR QR CODE
    Click Element                           //button[contains(text(), 'Liberar QR CODE')]
    Sleep                                   10
    Element Text Should Be                  //div[@class='box-title']       ${TITULO_GERAR_QRCODE}
    Element Text Should Be                  //*[@id="body"]/ngb-modal-window/div/div/div[2]/div/div/div[2]     ${DESCRICAO_GERAR_QRCODE}
    Element Text Should Be                  //*[@id="body"]/ngb-modal-window/div/div/div[2]/div/div/div[3]       ${PERGUNTA_GERAR_QRCODE}
    Element Text Should Be                  ${BUTTON_SIM_FIZ_CONFIRMACAO}      ${BOTAO_CONFIRMAR_GERAR_QRCODE}
    Element Text Should Be                  ${BUTTON_NAO_RECEBI}      ${BOTAO_NAO_RECEBI_GERAR_QRCODE}
    Capture Page Screenshot
    Log to console                          CLICAR EM LIBERAR QR CODE COM SUCESSO
    
CLICAR EM NÃO RECEBI    
    Click Element                           ${BUTTON_NAO_RECEBI}
    Sleep                                   5
    Element Text Should Be                  //*[@id="body"]/ngb-modal-window/div/div/div[2]/div/div     ${ENVIAR_NOVAMENTE} 
    Capture Page Screenshot
    Log to console                          CLICAR EM NAO RECEBI COM SUCESSO

CLICAR EM SIM, FIZ A CONFIRMAÇÃO SEM TER FEITO A CONFIRMAÇÃO
    Click Element                           ${BUTTON_SIM_FIZ_CONFIRMACAO}
    Sleep                                   5
    Element Text Should Be                  //*[@id="body"]/ngb-modal-window/div/div/div[2]/div/div[1]      ${TITULO_ERRO_QRCODE}
    Element Text Should Be                  //*[@id="body"]/ngb-modal-window/div/div/div[2]/div/div[2]      ${DESCRICAO_ERRO_QRCODE}
    Element Text Should Be                  ${BUTTON_REENVIAR_EMAIL}        ${BOTAO_REENVIAR}
    Capture Page Screenshot
    Log to console                          CLICAR NA CONFIRMACAO SEM TER FEITO A CONFIRMACAO
    Click Element                           ${BUTTON_REENVIAR_EMAIL}
    Sleep                                   10
    Element Text Should Be                  //div[@class='box-title']       ${TITULO_GERAR_QRCODE}
    Capture Page Screenshot 

CLICAR EM SIM, FIZ A CONFIRMAÇÃO
    #Confirmar e-mail manualmente
    Sleep                                   40
    Click Element                           ${BUTTON_SIM_FIZ_CONFIRMACAO}
    Sleep                                   5
    Log to console                          CONFIRMACAO REALIZADA COM SUCESSO
    Element Text Should Be                  //div[@class='box-title']       ${TITULO_GERAR_QRCODE}
    Element Text Should Be                  //*[@id="body"]/ngb-modal-window/div/div/div[2]/div/div[2]      ${DESCRICAO_QRCODE}
    Element Text Should Be                  //*[@id="body"]/ngb-modal-window/div/div/div[2]/div/div[3]/div/div      ${INFORMACOES_IMPORTANTE_QRCODE}
    Element Text Should Be                  ${BUTTON_FAZER_DOWNLOAD}        ${BOTAO_DOWNLOAD_QRCODE}
    Capture Page Screenshot
    Log to console                          QR CODE GERADO COM SUCESSO
    Click Element                           ${BUTTON_FAZER_DOWNLOAD}
    Sleep                                   3
    Log to console                          DOWNLOAD DO QR CODE COM SUCESSO

###ENTREGA NO MESMO DIA
Dado que estou na PDP do Produto 
    Delete All Cookies
    Go To                               http://${AMBIENTE}.fastshop.com.br/web/p/d/${SKU}/teste/
    Wait Until Element Is Visible       ${ID_ACEITAR_COOKIES}                       40
    Sleep                               1
    Click Element	                    ${ID_ACEITAR_COOKIES}
    Log to console                      TELA DE PRODUTO ACESSADA COM SUCESSO, COOKIES FORAM ACEITOS.
    Sleep                               2
    Capture Page Screenshot
    Set Focus To Element                //*[@id="body"]/app-root//app-product//app-sku-variations-grid
    Click Element                       //button[contains(text(),'110v')][@class='btn btn-option theme-first-border-color-selected']
    Sleep                               2
    Capture Page Screenshot
    Log to console                      VARIACAO ESCOLHIDA COM SUCESSO

Quando informo o CEP com entrega no mesmo dia   
    Set Focus To Element                //*[@id="body"]/app-root//app-product//app-freights
    Input Text                          //input[@class='ng-untouched ng-pristine ng-invalid']       ${CEP_MESMO_DIA}
    Set Focus To Element                //*[@id="body"]/app-root//app-product//app-freights
    Capture Page Screenshot
    Log to console                      CEP INFORMADO COM SUCESSO
    
E clico no botão Aplicar
    Set Focus To Element                //button[@class='btn btn-zipcode theme-first-btn']
    Sleep                               2
    Click Element                       //button[@class='btn btn-zipcode theme-first-btn']
    Sleep                               2
    Wait Until Element Is Visible       //*[@id="body"]/app-root//app-product       40            
    Set Focus To Element                //button[@class='btn-type-link theme-third-color pr-1']
    Sleep                               2
    Capture Page Screenshot
    Log to console                      CEP APLICADO COM SUCESSO

Então são exibidas as opções de entrega incluindo no mesmo dia, o horário e valor
    Element Text Should Be              //*[@id="body"]/app-root/div[2]/div[4]/app-product/div[2]/div[1]/div[1]/div/div[2]/div/div[5]/app-freights/div/div/div[2]/div/div[1]/app-pdp-freight-options/div/div[3]/div/div[1]/span[1]     ${DESCRICAO_MESMO_DIA}
    Element Text Should Be              //*[@id="body"]/app-root/div[2]/div[4]/app-product/div[2]/div[1]/div[1]/div/div[2]/div/div[5]/app-freights/div/div/div[2]/div/div[1]/app-pdp-freight-options/div/div[3]/div/div[1]/span[3]     ${PRAZO_MESMO_DIA}
    Capture Page Screenshot
    Log to console                      TIPOS DE ENTREGAS EXIBIDAS COM SUCESSO, INLCUINDO NO MESMO DIA

Quando informo o CEP com entrega no dia seguinte    
    Set Focus To Element                //*[@id="body"]/app-root//app-product//app-freights
    Input Text                          //input[@class='ng-untouched ng-pristine ng-invalid']       ${CEP_DIA_SEGUINTE}
    Capture Page Screenshot
    Log to console                      CEP INFORMADO COM SUCESSO

Então são exibidas as opções de entrega incluindo no dia seguinte, o horário e valor
    Element Text Should Be              //*[@id="body"]/app-root/div[2]/div[4]/app-product/div[2]/div[1]/div[1]/div/div[2]/div/div[5]/app-freights/div/div/div[2]/div/div[1]/app-pdp-freight-options/div/div[1]/div/div[1]/span[1]     ${DESCRICAO_DIA_SEGUINTE}
    Element Text Should Be              //*[@id="body"]/app-root/div[2]/div[4]/app-product/div[2]/div[1]/div[1]/div/div[2]/div/div[5]/app-freights/div/div/div[2]/div/div[1]/app-pdp-freight-options/div/div[1]/div/div[1]/span[3]     ${PRAZO_DIA_SEGUINTE}
    Capture Page Screenshot
    Log to console                      TIPOS DE ENTREGAS EXIBIDAS COM SUCESSO, INLCUINDO NO DIA SEGUINTE

Dado que realizo login com CPF no mesmo dia
    Delete All Cookies
    Go To                               http://${AMBIENTE}.fastshop.com.br/web/checkout-v2/login
    Wait Until Element Is Visible       ${ID_ACEITAR_COOKIES}                       40
    Sleep                               1
    Click Element	                    ${ID_ACEITAR_COOKIES}
    Log to console                      TELA DE LOGIN ACESSADA COM SUCESSO, COOKIES FORAM ACEITOS.
    Sleep                               2
    Capture Page Screenshot
    Input Text	                        ${ID_LOGIN_CPF}                             ${CPF_MESMO_DIA}
    Input Text	                        ${ID_LOGIN_SENHA}                           ${SENHA}
    Click Element                       ${BUTTON_ENTRAR_VERDE} 
    Sleep                               10
    Capture Page Screenshot
    Log to console                      LOGIN REALIZADO COM SUCESSO

E verifico se o carrinho está vazio
    Go To                               https://${AMBIENTE}.fastshop.com.br/web/checkout-v2/carrinho
    Wait Until Element Is Visible       //span[@class='text theme-first-color']     40
    Run Keyword If                      '${CARRINHO_ENTREGA}'=='Entrega 1 de 1'     Click Element     ${DIV_EXCLUIR_PRODUTO}
    Sleep                               3
    Capture Page Screenshot

E adiciono o produto ao carrinho
    Go To                               http://${AMBIENTE}.fastshop.com.br/web/p/d/${SKU}/teste/
    Wait Until Element Is Visible       //*[@id="body"]/app-root//app-product       40
    Capture Page Screenshot
    Set Focus To Element                //*[@id="body"]/app-root//app-product//app-sku-variations-grid
    Click Element                       //button[contains(text(),'110v')][@class='btn btn-option theme-first-border-color-selected']
    Sleep                               5
    Capture Page Screenshot
    Log to console                      VARIACAO ESCOLHIDA COM SUCESSO
    Set Focus To Element                ${BUTTON_ADICIONAR_AO_CARRINHO}
    Sleep                               3
    Click Element                       ${BUTTON_ADICIONAR_AO_CARRINHO}
    Sleep                               10
    Capture Page Screenshot
    Log to console                      PRODUTO ADICIONADO AO CARRINHO COM SUCESSO

E acesso o carrinho
    Go To                               https://${AMBIENTE}.fastshop.com.br/web/checkout-v2/carrinho
    Wait Until Element Is Visible       //span[@class='text theme-first-color']     40
    Capture Page Screenshot
    Log to console                      CARRINHO ACESSADO COM SUCESSO
    
Quando eu altero o tipo de entrega para no mesmo dia
    Click Element                       ${BUTTON_ALTERAR_ENTREGA}
    Wait Until Element Is Visible       //div[@class='title'][contains(text(),'Escolha uma forma de entrega')]        40
    Element Text Should Be              //*[@id="body"]/ngb-modal-window/div/div/div[2]/div/div/div[2]/owl-carousel-o/div/div[1]/owl-stage/div/div/div[3]/div/label/span     ${DESCRICAO_MESMO_DIA}
    Element Text Should Be              //div[@class='box-description'][contains(text(),'Até 22h')]     ${PRAZO_MESMO_DIA}\nR$ 20,00
    Click Element                       //*[@id="body"]/ngb-modal-window/div/div/div[2]/div/div/div[2]/owl-carousel-o/div/div[1]/owl-stage/div/div/div[3]/div/label/span
    Capture Page Screenshot
    Click Element                       ${BUTTON_APLICAR_ENTREGA}
    Log to console                      TIPO DE ENTREGA ALTERADA PARA NO MESMO DIA COM SUCESSO

Então o alerta de atenção no mesmo dia é exibido
    Wait Until Element Is Visible       //div[@class='attention-modal']     40
    Element Text Should Be              //div[@class='attention-modal']     ${ALERTA_ATENCAO_MESMO_DIA}
    Capture Page Screenshot
    Log to console                      ALERTA DE ATENCAO EXIBIDO COM SUCESSO
    Click Element                       ${BUTTON_OK_ALERTA_ATENCAO}
    Capture Page Screenshot

E a opção de entrega é atualizada para no mesmo dia
    Element Text Should Be              //div[@class='title']        Entrega ${DESCRICAO_MESMO_DIA}
    Element Text Should Be              //div[@class='delivery-time']       ${PRAZO_MESMO_DIA}
    Capture Page Screenshot

Dado que realizo login com CPF no dia seguinte
    Delete All Cookies
    Go To                               http://${AMBIENTE}.fastshop.com.br/web/checkout-v2/login
    Wait Until Element Is Visible       ${ID_ACEITAR_COOKIES}                       40
    Sleep                               1
    Click Element	                    ${ID_ACEITAR_COOKIES}
    Log to console                      TELA DE LOGIN ACESSADA COM SUCESSO, COOKIES FORAM ACEITOS.
    Sleep                               2
    Capture Page Screenshot
    Input Text	                        ${ID_LOGIN_CPF}                             ${CPF_DIA_SEGUINTE}
    Input Text	                        ${ID_LOGIN_SENHA}                           ${SENHA}
    Click Element                       ${BUTTON_ENTRAR_VERDE} 
    Sleep                               10
    Capture Page Screenshot
    Log to console                      LOGIN REALIZADO COM SUCESSO

Quando eu altero o tipo de entrega para no dia seguinte
    Click Element                       ${BUTTON_ALTERAR_ENTREGA}
    Wait Until Element Is Visible       //div[@class='title'][contains(text(),'Escolha uma forma de entrega')]        40
    Element Text Should Be              //*[@id="body"]/ngb-modal-window/div/div/div[2]/div/div/div[2]/owl-carousel-o/div/div[1]/owl-stage/div/div/div[3]/div/label/span     ${DESCRICAO_DIA_SEGUINTE}
    Element Text Should Be              //div[@class='box-description'][contains(text(),'Até 22h')]     ${PRAZO_DIA_SEGUINTE}\nR$ 20,00
    Click Element                       //*[@id="body"]/ngb-modal-window//owl-carousel-o//owl-stage//div[3]
    Capture Page Screenshot
    Click Element                       ${BUTTON_APLICAR_ENTREGA}
    Log to console                      TIPO DE ENTREGA ALTERADA PARA NO MESMO DIA COM SUCESSO

Então o alerta de atenção no dia seguinte é exibido
    Element Text Should Be              //div[@class='attention-modal']     ${ALERTA_ATENCAO_DIA_SEGUINTE}
    Capture Page Screenshot
    Log to console                      ALERTA DE ATENCAO EXIBIDO COM SUCESSO
    Click Element                       ${BUTTON_OK_ALERTA_ATENCAO}
    Capture Page Screenshot
    
E a opção de entrega é atualizada para no dia seguinte
    Element Text Should Be              //div[@class='title']        Entrega ${DESCRICAO_DIA_SEGUINTE}
    Element Text Should Be              //div[@class='delivery-time']       ${PRAZO_DIA_SEGUINTE}
    Capture Page Screenshot

E continuo com a compra até a tela de frete
    Click Element                       //button[contains(text(), 'Continuar compra')]
    Wait Until Element Is Visible       //div[@class='steps'][contains(text(), 'Endereço de entrega')]      40
    Capture Page Screenshot
    Log to console                      TELA DE ENTREGA ACESSADA COM SUCESSO
    Click Element                       ${BUTTON_CONTINUAR_VERDE_ENDERECO}
    Wait Until Element Is Visible      //div[@class='steps'][contains(text(), 'Frete')]      40
    Capture Page Screenshot
    Log to console                      TELA DE FRETE ACESSADA COM SUCESSO
    Element Text Should Be              //*[@id="body"]/app-root/div[2]/div[4]/app-select-delivery/div/div/div/div[1]/div/div/div[5]/div[2]/div/app-item-delivery/div/div/owl-carousel-o/div/div[1]/owl-stage/div/div/div[3]/div/label/span     ${DESCRICAO_MESMO_DIA}
    Element Text Should Be              //div[@class='box-description'][contains(text(),'Até 22h')]     ${PRAZO_MESMO_DIA}\nR$ 20,00

Quando eu seleciono o tipo de entrega para no mesmo dia
    Click Element                       //*[@id="body"]/app-root/div[2]/div[4]/app-select-delivery/div/div/div/div[1]/div/div/div[5]/div[2]/div/app-item-delivery/div/div/owl-carousel-o/div/div[1]/owl-stage/div/div/div[3]/div/label/span
    Wait Until Element Is Visible       //div[@class='attention-modal']     40

Quando eu seleciono o tipo de entrega para no dia seguinte
    Click Element                       //*[@id="body"]/app-root/div[2]/div[4]/app-select-delivery/div/div/div/div[1]/div/div/div[5]/div[2]/div/app-item-delivery/div/div/owl-carousel-o/div/div[1]/owl-stage/div/div/div[3]/div/label/span
    Wait Until Element Is Visible       //div[@class='attention-modal']     40

E seleciono o tipo de entrega para no mesmo dia
    Click Element                       //*[@id="body"]/app-root/div[2]/div[4]/app-select-delivery/div/div/div/div[1]/div/div/div[5]/div[2]/div/app-item-delivery/div/div/owl-carousel-o/div/div[1]/owl-stage/div/div/div[3]/div/label/span
    Wait Until Element Is Visible       //div[@class='attention-modal']     40

E confirmo o alerta de atenção no mesmo dia
    Element Text Should Be              //div[@class='attention-modal']     ${ALERTA_ATENCAO_MESMO_DIA}
    Capture Page Screenshot
    Log to console                      ALERTA DE ATENCAO EXIBIDO COM SUCESSO
    Click Element                       ${BUTTON_OK_ALERTA_ATENCAO}
    Capture Page Screenshot

E continuo com a compra até a forma de pagamento
    Click Element                       ${BUTTON_CONTINUAR_VERDE_FRETE}
    #Wait Until Element Is Visible       //div[@class='steps'][contains(text(), 'Serviços')]     40
    #Capture Page Screenshot
    #Click Element                       ${BUTTON_CONTINUAR_VERDE_SERVICOS}
    Wait Until Element Is Visible       //div[@class='steps'][contains(text(), 'Pagamento')]     40
    Capture Page Screenshot

Quando eu finalizo a compra com cartão de crédito
    Click Element                       ${LABEL_PAGAR_VIA_CARTAO_DE_CREDITO}
    Capture Page Screenshot
    Click Element                       ${BUTTON_CONTINUAR_VERDE_PAGAMENTO}
    Wait Until Element Is Visible       //div[@class='steps'][contains(text(), 'Pagamento')]     40
    Capture Page Screenshot
    Input Text                          ${INPUT_NUMERO_CARTAO_DE_CREDITO}       ${NUMERO_CARTAO}
    Input Text                          ${INPUT_NOME_TITULAR_DO_CARTAO}     ${NOME_TITULAR}
    Input Text                          ${INPUT_VALIDADE_DO_CARTAO}     ${DATA_VALIDADE}
    Input Text                          ${INPUT_CPF_TITULAR_CARTAO}     ${CPF_MESMO_DIA}
    Capture Page Screenshot
    Click Element                       ${BUTTON_CONTINUAR_VERDE_CONTINUAR_PARCELAMENTO}
    Wait Until Element Is Visible       //div[@class='steps'][contains(text(), 'Pagamento')]     40
    Capture Page Screenshot
    Click Element                       ${DIV_PAGAMENTO_LISTA_A_VISTA}
    Sleep                               8
    Input Text                          ${INPUT_CVV}        ${CVV}
    Capture Page Screenshot
    Click Element                       ${BUTTON_CONTINUAR_VERDE_FINALIZAR_COMPRA}

Então é exibida a finalização do pedido com a mensagem de entrega no mesmo dia
    Wait Until Element Is Visible       //h1[@class='title'][contains(text(), 'Pedido realizado com sucesso!')]     40
    Capture Page Screenshot
    Click Element                       ${SPAN_FECHAR_EBIT}
    Capture Page Screenshot
    Element Text Should Be              //div[@class='delivery ng-star-inserted']       Entrega ${DESCRICAO_MESMO_DIA}\n${PRAZO_MESMO_DIA}
    Element Text Should Be              //div[@class='attention ng-star-inserted']      ${IMPORTANTE_MESMO_DIA}

INICIO DO TESTES:
    Sleep     1
    Log to console                      AS VALIDACOES INICIANDO AGORA:

