*** Settings ***
Library     SeleniumLibrary
Library     RequestsLibrary
Library     Collections
#Library     ScreenCapLibrary
Library     FakerLibrary    locale=pt_BR
Resource    elements.robot
Resource    services.robot
Resource    setup_hz.robot
Resource    sprints_hz.robot
Resource    services_sprints.robot


*** Variables ***
${BASE_AMBIENTE}      dev
${BLACKOUT_URL}       https://www.fastshop.com.br/golden-72a076d8c52749d13753b6bd8764570410cd37194653d8bf52a0bf0938991fbb 
${BASE_URL}           https://www.fastshop.com.br/      #http://webapp2-${BASE_AMBIENTE}.fastshop.com.br/     #https://www.fastshop.com.br/      #https://www.fastshop.com.br/   #http://webapp2-${BASE_AMBIENTE}.fastshop.com.br/             #
${BASE_FEATURE_URL}   https://webapp2-feature.fastshop.com.br/web/
${BASE_PRODUÇÃO_URL}  https://webapp2-develop.fastshop.com.br/web/    #https://www.fastshop.com.br/web/
${OPTIONS}            add_argument("--disable-dev-shm-usage"); add_argument("--headless"); add_argument("--no-sandbox"); add_experimental_option('excludeSwitches', ['enable-logging'])

*** Keywords ***
Start Session
    #Open Browser                        about:blank     ${BROWSER}   remote_url=http://192.168.15.47:4444/wd/hub
    #Create Session	                     FastShop        https://apiqa.fastshop.com.br
    Open Browser                         about:blank     ${BROWSER}   options=${OPTIONS}  #add_experimental_option('excludeSwitches', ['enable-logging'])
    Set Selenium Implicit Wait           20
    #Delete All Cookies
    Set Window Size                      1920           1080
    Maximize Browser Window	 
    Go To                               https://www.fastshop.com.br/
    Log                                 ***********************************************************************************************************************************  
    Capture Page Screenshot	            INICIO_TELADEBLACKOUT.png
    Log                                 ***********************************************************************************************************************************  
    Add Cookie                          name=fastshopGoldenUser    value=true    path=/    domain=.fastshop.com.br    secure=False    expiry=${null} 
    ${cookie}=	                        Get Cookie	fastshopGoldenUser   
    Log to console                      ${cookie}
    #Go To                              https://www.fastshop.com.br/golden-72a076d8c52749d13753b6bd8764570410cd37194653d8bf52a0bf0938991fbb
    #Start Video Recording
    
Start Gif Recording

End Session
    Close All Browsers
    #FIM DO TESTE    cpf    senha    busca_produto
    #Stop Video Recording
    ##Stop Gif Recording

End Test
    Capture Page Screenshot	
    #${cookie}=	                        Get Cookie	fastshopGoldenUser   
    #Log to console                      ${cookie}
    
    
End Test product
    Capture Page Screenshot	
    ${OPTIONS}=                         Get Text                                    add_argument("--disable-dev-shm-usage"); add_argument("--headless"); add_argument("--no-sandbox")
    Set Global Variable                 ${OPTIONS}